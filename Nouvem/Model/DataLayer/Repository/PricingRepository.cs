﻿// -----------------------------------------------------------------------
// <copyright file="PricingRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Windows;
using System.Windows.Threading;
using Nouvem.AccountsIntegration.BusinessObject;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Shared;

    public class PricingRepository : NouvemRepositoryBase, IPricingRepository
    {
        /// <summary>
        /// Retrieve all the rounding options.
        /// </summary>
        /// <returns>A collection of rounding options.</returns>
        public IList<NouRoundingOption> GetRoundingOptions()
        {
            this.Log.LogDebug(this.GetType(), "GetRoundingOptions(): Attempting to retrieve all the rounding options");
            var roundingOptions = new List<NouRoundingOption>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    roundingOptions = entities.NouRoundingOptions.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} rounding options retrieved", roundingOptions.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return roundingOptions;
        }

        /// <summary>
        /// Retrieve all the rounding rules.
        /// </summary>
        /// <returns>A collection of rounding rules.</returns>
        public IList<NouRoundingRule> GetRoundingRules()
        {
            this.Log.LogDebug(this.GetType(), "GetRoundingRules(): Attempting to retrieve all the rounding rules");
            var roundingRules = new List<NouRoundingRule>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    roundingRules = entities.NouRoundingRules.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} rounding rules retrieved", roundingRules.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return roundingRules;
        }

        /// <summary>
        /// Adds a new collection of partners to the db.
        /// </summary>
        /// <param name="partners">The partners to add.</param>
        public void AddOrUpdatePrices(IList<Prices> prices)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var price in prices)
                    {
                        ProgressBar.Run();
                        try
                        {
                            entities.App_UpdatePrice(price.Price, price.Code, price.PriceBand, price.PartnerCode);
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}, Partner:{2}", ex.Message, ex.InnerException, price.Code));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieve all the special prices.
        /// </summary>
        /// <returns>A collection of special prices.</returns>
        public IList<SpecialPrice> GetSpecialPrices()
        {
            this.Log.LogDebug(this.GetType(), "GetRoundingOptions(): Attempting to retrieve all the special prices");
            var specialPrices = new List<SpecialPrice>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var specialPriceList = entities.SpecialPriceLists.Where(x => x.Deleted == null).ToList();
                    specialPrices = (from price in specialPriceList
                        select new SpecialPrice
                        {
                            SpecialPriceID = price.SpecialPriceListID,
                            BPGroupID = price.BPGroupID,
                            BPMasterID = price.BPMasterID,
                            INMasterID = price.INMasterID,
                            Price = price.Price,
                            StartDate = price.StartDate,
                            EndDate = price.EndDate
                        }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} special prices retrieved", specialPrices.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return specialPrices;
        }

        /// <summary>
        /// Method that makes a repository add or update the input special prices.
        /// </summary>
        /// <param name="specialPrices">The label associations to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateSpecialPrices(IList<SpecialPrice> specialPrices)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateSpecialPrices(): Attempting to update the special prices");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    specialPrices.ToList().ForEach(x =>
                    {
                        if (x.SpecialPriceID == 0)
                        {
                            var price = new SpecialPriceList
                            {
                                BPGroupID = x.BPGroupID,
                                BPMasterID = x.BPMasterID,
                                INMasterID = x.INMasterID,
                                Price = x.Price,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate
                            };

                            entities.SpecialPriceLists.Add(price);
                        }
                        else
                        {
                            // update
                            var dbPrice =
                                entities.SpecialPriceLists.FirstOrDefault(
                                    sp => sp.SpecialPriceListID == x.SpecialPriceID);

                            if (dbPrice != null)
                            {
                                dbPrice.Price = x.Price;
                                dbPrice.INMasterID = x.INMasterID;
                                dbPrice.BPGroupID = x.BPGroupID;
                                dbPrice.BPMasterID = x.BPMasterID;
                                dbPrice.StartDate = x.StartDate;
                                dbPrice.EndDate = x.EndDate;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Special price lists successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceMaster> GetPriceLists()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            var priceLists = new List<PriceMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceLists = (from priceList in entities.PriceLists
                        where !priceList.Deleted
                        select new PriceMaster
                        {
                            PriceListID = priceList.PriceListID,
                            BasePriceListID = priceList.BasePriceListID,
                            BasePriceListName = priceList.PriceListBase != null ? priceList.PriceListBase.CurrentPriceListName : string.Empty,
                            BPCurrencyID = priceList.BPCurrencyID,
                            CurrentPriceListName = priceList.CurrentPriceListName,
                            Factor = priceList.Factor,
                            NouRoundingOptionsID = priceList.NouRoundingOptionsID,
                            NouRoundingRulesID = priceList.NouRoundingRulesID,
                            StartDate = priceList.StartDate,
                            EndDate = priceList.EndDate,
                            CreationDate = priceList.CreationDate,
                            BPMasterID = priceList.BPMasterID,
                            //PriceListDetails = (from detail in priceList.PriceListDetails
                            //                    where !detail.Deleted
                            //                    select new PriceDetail
                            //                    {
                            //                        PriceListID = detail.PriceListID,
                            //                        PriceListDetailID = detail.PriceListDetailID,
                            //                        ItemCode = detail.INMaster.Code,
                            //                        ItemDescription = detail.INMaster.Name,
                            //                        INMasterID = detail.INMasterID,
                            //                        ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                            //                        BasePrice = entities.PriceListDetails.Where(x => x.PriceListID == detail.PriceList.BasePriceListID && x.INMasterID == detail.INMasterID && !x.Deleted).Select(basePrice => basePrice.Price).FirstOrDefault(),
                            //                        //BasePriceListName = entities.PriceLists.Where(x => x.PriceListID == detail.PriceList.BasePriceListID).Select(basePrice => basePrice.CurrentPriceListName).FirstOrDefault(),
                            //                        Price = detail.Price,
                            //                        NouOrderMethodID = detail.NouOrderMethodID,
                            //                        NouOrderMethodName = detail.NouOrderMethod.Name,
                            //                        NouPriceMethodName = detail.NouPriceMethod.Name,
                            //                        NouPriceMethodID = detail.NouPriceMethodID,
                            //                        NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                            //                        LabelPrice = detail.LabelPrice,
                            //                        ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                            //                        Remove = false,
                            //                        PriceListName = detail.PriceList.CurrentPriceListName,
                            //                        PriceListFactor = detail.PriceList.Factor
                            //                    }).ToList(),
                            //ActiveProducts = priceList.PriceListDetails.Count
                        }).ToList();

                    //foreach (var detail in priceLists.SelectMany(x => x.PriceListDetails))
                    //{
                    //    var factor = detail.PriceListFactor;
                    //    if (!detail.ManualPriceAndOrMethod && factor != 0)
                    //    {
                    //        var convertedDiscount = 100M + factor.ToDecimal();
                    //        detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                    //    }
                    //}

                    this.Log.LogDebug(this.GetType(), string.Format("{0} price lists retrieved", priceLists.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceLists;
        }

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceMaster> GetPriceListsAndDetails()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            var priceLists = new List<PriceMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceLists = (from priceList in entities.PriceLists
                                  where !priceList.Deleted
                                  select new PriceMaster
                                  {
                                      PriceListID = priceList.PriceListID,
                                      BasePriceListID = priceList.BasePriceListID,
                                      BasePriceListName = priceList.PriceListBase != null ? priceList.PriceListBase.CurrentPriceListName : string.Empty,
                                      BPCurrencyID = priceList.BPCurrencyID,
                                      CurrentPriceListName = priceList.CurrentPriceListName,
                                      Factor = priceList.Factor,
                                      NouRoundingOptionsID = priceList.NouRoundingOptionsID,
                                      NouRoundingRulesID = priceList.NouRoundingRulesID,
                                      StartDate = priceList.StartDate,
                                      EndDate = priceList.EndDate,
                                      CreationDate = priceList.CreationDate,
                                      BPMasterID = priceList.BPMasterID,
                                      PriceListDetails = (from detail in priceList.PriceListDetails
                                                          where !detail.Deleted
                                                          select new PriceDetail
                                                          {
                                                              PriceListID = detail.PriceListID,
                                                              PriceListDetailID = detail.PriceListDetailID,
                                                              ItemCode = detail.INMaster.Code,
                                                              ItemDescription = detail.INMaster.Name,
                                                              INMasterID = detail.INMasterID,
                                                              ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                                                              BasePrice = entities.PriceListDetails.Where(x => x.PriceListID == detail.PriceList.BasePriceListID && x.INMasterID == detail.INMasterID && !x.Deleted).Select(basePrice => basePrice.Price).FirstOrDefault(),
                                                              //BasePriceListName = entities.PriceLists.Where(x => x.PriceListID == detail.PriceList.BasePriceListID).Select(basePrice => basePrice.CurrentPriceListName).FirstOrDefault(),
                                                              Price = detail.Price,
                                                              NouOrderMethodID = detail.NouOrderMethodID,
                                                              NouOrderMethodName = detail.NouOrderMethod.Name,
                                                              NouPriceMethodName = detail.NouPriceMethod.Name,
                                                              NouPriceMethodID = detail.NouPriceMethodID,
                                                              NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                                              LabelPrice = detail.LabelPrice,
                                                              ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                                                              Remove = false,
                                                              PriceListName = detail.PriceList.CurrentPriceListName,
                                                              PriceListFactor = detail.PriceList.Factor
                                                          }).ToList()
                                  }).ToList();

                    foreach (var detail in priceLists.SelectMany(x => x.PriceListDetails))
                    {
                        var factor = detail.PriceListFactor;
                        if (!detail.ManualPriceAndOrMethod && factor != 0)
                        {
                            var convertedDiscount = 100M + factor.ToDecimal();
                            detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                        }
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} price lists retrieved", priceLists.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceLists;
        }

        /// <summary>
        /// Retrieves the price detail for the input data.
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price detail.</returns>
        public PriceDetail GetPriceListDetail(int priceListId, int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var detail =
                        entities.PriceListDetails.FirstOrDefault(
                            x => x.PriceListID == priceListId && x.INMasterID == inMasterId && !x.Deleted);

                    if (detail == null)
                    {
                        return null;
                    }

                    var priceDetail = new PriceDetail
                    {
                        PriceListID = detail.PriceListID,
                        PriceListDetailID = detail.PriceListDetailID,
                        INMasterID = detail.INMasterID,
                        Price = detail.Price,
                        NouOrderMethodID = detail.NouOrderMethodID,
                        NouOrderMethodName = detail.NouOrderMethod.Name,
                        NouPriceMethodName = detail.NouPriceMethod.Name,
                        NouPriceMethodID = detail.NouPriceMethodID,
                        NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                        NouRecipePriceMethodID = detail.NouPriceMethodID_Recipe,
                        ItemGroup = detail.INMaster.INGroup.Name,
                        ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false
                    };

                    return priceDetail;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the price detail for the input data.
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price detail.</returns>
        public PriceDetail GetPriceListDetailByPartner(int partnerId, int inMasterId, int basePriceListId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var partner = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == partnerId);
                    if (partner == null)
                    {
                        return null;
                    }

                    var priceListId = partner.PriceListID;
                    if (priceListId == null)
                    {
                        priceListId = basePriceListId;
                    }

                    var detail =
                        entities.PriceListDetails.FirstOrDefault(
                            x => x.PriceListID == priceListId && x.INMasterID == inMasterId && !x.Deleted);

                    if (detail == null)
                    {
                         detail =
                            entities.PriceListDetails.FirstOrDefault(
                                x => x.PriceListID == basePriceListId && x.INMasterID == inMasterId && !x.Deleted);
                    }

                    if (detail == null)
                    {
                        return null;
                    }

                    var priceDetail = new PriceDetail
                    {
                        PriceListID = detail.PriceListID,
                        PriceListDetailID = detail.PriceListDetailID,
                        INMasterID = detail.INMasterID,
                        Price = detail.Price,
                        NouOrderMethodID = detail.NouOrderMethodID,
                        NouOrderMethodName = detail.NouOrderMethod.Name,
                        NouPriceMethodName = detail.NouPriceMethod.Name,
                        NouPriceMethodID = detail.NouPriceMethodID,
                        NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                        ItemGroup = detail.INMaster.INGroup.Name,
                        ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false
                    };

                    return priceDetail;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the price method for the input data.
        /// </summary>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price method.</returns>
        public NouPriceMethod GetProductPriceMethod(int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var detail =
                        entities.INMasters.FirstOrDefault(
                            x => x.INMasterID == inMasterId);

                    if (detail == null)
                    {
                        return null;
                    }

                    return entities.NouPriceMethods.FirstOrDefault(x => x.NouPriceMethodID == detail.NouPriceMethodID);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public PriceMaster GetPriceList(int priceListId)
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            PriceMaster priceListToReturn = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var priceList = entities.PriceLists.FirstOrDefault(x => x.PriceListID == priceListId);
                    if (priceList != null)
                    {
                        priceListToReturn =
                            new PriceMaster
                                  {
                                      PriceListID = priceList.PriceListID,
                                      BasePriceListID = priceList.BasePriceListID,
                                      BasePriceListName = priceList.PriceListBase != null ? priceList.PriceListBase.CurrentPriceListName : string.Empty,
                                      BPCurrencyID = priceList.BPCurrencyID,
                                      CurrentPriceListName = priceList.CurrentPriceListName,
                                      Factor = priceList.Factor,
                                      NouRoundingOptionsID = priceList.NouRoundingOptionsID,
                                      NouRoundingRulesID = priceList.NouRoundingRulesID,
                                      StartDate = priceList.StartDate,
                                      EndDate = priceList.EndDate,
                                      CreationDate = priceList.CreationDate,
                                      BPMasterID = priceList.BPMasterID,
                                      PriceListDetails = (from detail in priceList.PriceListDetails
                                                          where !detail.Deleted
                                                          select new PriceDetail
                                                          {
                                                              PriceListID = detail.PriceListID,
                                                              PriceListDetailID = detail.PriceListDetailID,
                                                              ItemCode = detail.INMaster.Code,
                                                              ItemDescription = detail.INMaster.Name,
                                                              INMasterID = detail.INMasterID,
                                                              ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                                                              BasePrice = entities.PriceListDetails.Where(x => x.PriceListID == detail.PriceList.BasePriceListID && x.INMasterID == detail.INMasterID && !x.Deleted).Select(basePrice => basePrice.Price).FirstOrDefault(),
                                                              //BasePriceListName = entities.PriceLists.Where(x => x.PriceListID == detail.PriceList.BasePriceListID).Select(basePrice => basePrice.CurrentPriceListName).FirstOrDefault(),
                                                              Price = detail.Price,
                                                              NouOrderMethodID = detail.NouOrderMethodID,
                                                              NouOrderMethodName = detail.NouOrderMethod.Name,
                                                              NouPriceMethodName = detail.NouPriceMethod.Name,
                                                              NouPriceMethodID = detail.NouPriceMethodID,
                                                              NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                                              LabelPrice = detail.LabelPrice,
                                                              ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                                                              Remove = false,
                                                              PriceListName = detail.PriceList.CurrentPriceListName,
                                                              PriceListFactor = detail.PriceList.Factor
                                                          }).ToList(),
                                      ActiveProducts = priceList.PriceListDetails.Count
                                  };

                    foreach (var detail in priceListToReturn.PriceListDetails)
                    {
                        var factor = detail.PriceListFactor;
                        if (!detail.ManualPriceAndOrMethod && factor != 0)
                        {
                            var convertedDiscount = 100M + factor.ToDecimal();
                            detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                        }
                    }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceListToReturn;
        }

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceMaster> GetUpdatedPriceLists()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            var priceLists = new List<PriceMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceLists = (from priceList in entities.PriceLists
                                  where !priceList.Deleted && priceList.EditDate.HasValue && priceList.EditDate >= DateTime.Today
                                  select new PriceMaster
                                  {
                                      PriceListID = priceList.PriceListID,
                                      BasePriceListID = priceList.BasePriceListID,
                                      BasePriceListName = priceList.PriceListBase != null ? priceList.PriceListBase.CurrentPriceListName : string.Empty,
                                      BPCurrencyID = priceList.BPCurrencyID,
                                      CurrentPriceListName = priceList.CurrentPriceListName,
                                      Factor = priceList.Factor,
                                      NouRoundingOptionsID = priceList.NouRoundingOptionsID,
                                      NouRoundingRulesID = priceList.NouRoundingRulesID,
                                      StartDate = priceList.StartDate,
                                      EndDate = priceList.EndDate,
                                      CreationDate = priceList.CreationDate,
                                      BPMasterID = priceList.BPMasterID,
                                      PriceListDetails = (from detail in priceList.PriceListDetails
                                                          where !detail.Deleted
                                                          select new PriceDetail
                                                          {
                                                              PriceListID = detail.PriceListID,
                                                              PriceListDetailID = detail.PriceListDetailID,
                                                              ItemCode = detail.INMaster.Code,
                                                              ItemDescription = detail.INMaster.Name,
                                                              INMasterID = detail.INMasterID,
                                                              ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                                                              BasePrice = entities.PriceListDetails.Where(x => x.PriceListID == detail.PriceList.BasePriceListID && x.INMasterID == detail.INMasterID && !x.Deleted).Select(basePrice => basePrice.Price).FirstOrDefault(),
                                                              //BasePriceListName = entities.PriceLists.Where(x => x.PriceListID == detail.PriceList.BasePriceListID).Select(basePrice => basePrice.CurrentPriceListName).FirstOrDefault(),
                                                              Price = detail.Price,
                                                              NouOrderMethodID = detail.NouOrderMethodID,
                                                              NouOrderMethodName = detail.NouOrderMethod.Name,
                                                              NouPriceMethodName = detail.NouPriceMethod.Name,
                                                              NouPriceMethodID = detail.NouPriceMethodID,
                                                              NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                                              LabelPrice = detail.LabelPrice,
                                                              ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                                                              Remove = false,
                                                              PriceListFactor = detail.PriceList.Factor
                                                          }).ToList(),
                                      ActiveProducts = priceList.PriceListDetails.Count
                                  }).ToList();

                    foreach (var detail in priceLists.SelectMany(x => x.PriceListDetails))
                    {
                        var factor = detail.PriceListFactor;
                        if (!detail.ManualPriceAndOrMethod && factor != 0)
                        {
                            var convertedDiscount = 100M + factor.ToDecimal();
                            detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                        }
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} price lists retrieved", priceLists.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceLists;
        }

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceMaster> GetNewPriceLists()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            var priceLists = new List<PriceMaster>();
            //var ids = NouvemGlobal.PriceListMasters.Select(x => x.PriceListID).ToList();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceLists = (from priceList in entities.PriceLists
                                  where !priceList.Deleted && priceList.CreationDate >= DateTime.Today
                                  select new PriceMaster
                                  {
                                      PriceListID = priceList.PriceListID,
                                      BasePriceListID = priceList.BasePriceListID,
                                      BasePriceListName = priceList.PriceListBase != null ? priceList.PriceListBase.CurrentPriceListName : string.Empty,
                                      BPCurrencyID = priceList.BPCurrencyID,
                                      CurrentPriceListName = priceList.CurrentPriceListName,
                                      Factor = priceList.Factor,
                                      NouRoundingOptionsID = priceList.NouRoundingOptionsID,
                                      NouRoundingRulesID = priceList.NouRoundingRulesID,
                                      StartDate = priceList.StartDate,
                                      EndDate = priceList.EndDate,
                                      CreationDate = priceList.CreationDate,
                                      BPMasterID = priceList.BPMasterID,
                                      //PriceListDetails = (from detail in priceList.PriceListDetails
                                      //                    where !detail.Deleted
                                      //                    select new PriceDetail
                                      //                    {
                                      //                        PriceListID = detail.PriceListID,
                                      //                        PriceListDetailID = detail.PriceListDetailID,
                                      //                        ItemCode = detail.INMaster.Code,
                                      //                        ItemDescription = detail.INMaster.Name,
                                      //                        INMasterID = detail.INMasterID,
                                      //                        ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                                      //                        BasePrice = entities.PriceListDetails.Where(x => x.PriceListID == detail.PriceList.BasePriceListID && x.INMasterID == detail.INMasterID && !x.Deleted).Select(basePrice => basePrice.Price).FirstOrDefault(),
                                      //                        //BasePriceListName = entities.PriceLists.Where(x => x.PriceListID == detail.PriceList.BasePriceListID).Select(basePrice => basePrice.CurrentPriceListName).FirstOrDefault(),
                                      //                        Price = detail.Price,
                                      //                        NouOrderMethodID = detail.NouOrderMethodID,
                                      //                        NouOrderMethodName = detail.NouOrderMethod.Name,
                                      //                        NouPriceMethodName = detail.NouPriceMethod.Name,
                                      //                        NouPriceMethodID = detail.NouPriceMethodID,
                                      //                        NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                      //                        LabelPrice = detail.LabelPrice,
                                      //                        ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                                      //                        Remove = false,
                                      //                        PriceListFactor = detail.PriceList.Factor
                                      //                    }).ToList(),
                                      ActiveProducts = priceList.PriceListDetails.Count(x => !x.Deleted)
                                  }).ToList();
                    
                    this.Log.LogDebug(this.GetType(), string.Format("{0} price lists retrieved", priceLists.Count()));
                }

                foreach (var detail in priceLists.SelectMany(x => x.PriceListDetails))
                {
                    var factor = detail.PriceListFactor;
                    if (!detail.ManualPriceAndOrMethod && factor != 0)
                    {
                        var convertedDiscount = 100M + factor.ToDecimal();
                        detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceLists;
        }

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceDetail> GetUpdatedPriceListDetails()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            var priceListDetails = new List<PriceDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (
                        !entities.PriceListDetails.Any(
                            detail =>
                                detail.Deleted && detail.EditDate > NouvemGlobal.LastPriceListDetailsUpdate &&
                                detail.DeviceMasterID != NouvemGlobal.DeviceId))
                    {
                        NouvemGlobal.LastPriceListDetailsUpdate = DateTime.Now;
                        return priceListDetails;
                    }

                    priceListDetails = (from detail
                        in entities.PriceListDetails
                        where !detail.Deleted
                        select new PriceDetail
                        {
                            PriceListID = detail.PriceListID,
                            PriceListDetailID = detail.PriceListDetailID,
                            ItemCode = detail.INMaster.Code,
                            ItemDescription = detail.INMaster.Name,
                            INMasterID = detail.INMasterID,
                            ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                            BasePrice =
                                entities.PriceListDetails.Where(
                                    x =>
                                        x.PriceListID == detail.PriceList.BasePriceListID &&
                                        x.INMasterID == detail.INMasterID && !x.Deleted)
                                    .Select(basePrice => basePrice.Price)
                                    .FirstOrDefault(),
                            //BasePriceListName = entities.PriceLists.Where(x => x.PriceListID == detail.PriceList.BasePriceListID).Select(basePrice => basePrice.CurrentPriceListName).FirstOrDefault(),
                            Price = detail.Price,
                            NouOrderMethodID = detail.NouOrderMethodID,
                            NouOrderMethodName = detail.NouOrderMethod.Name,
                            NouPriceMethodName = detail.NouPriceMethod.Name,
                            NouPriceMethodID = detail.NouPriceMethodID,
                            NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                            LabelPrice = detail.LabelPrice,
                            ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                            Remove = false,
                            PriceListFactor = detail.PriceList.Factor
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            NouvemGlobal.LastPriceListDetailsUpdate = DateTime.Now;
            return priceListDetails;
        }

        /// <summary>
        /// Have the prices been edited flag.
        /// </summary>
        /// <returns>A flag as to whether a price has been edited.</returns>
        public bool AreTherePriceEdits()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            var edit = false;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (entities.PriceListDetails.Any(
                        detail =>
                            !detail.Deleted && detail.EditDate > NouvemGlobal.LastPriceListDetailsUpdate))
                    {
                        edit = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            NouvemGlobal.LastPriceListDetailsUpdate = DateTime.Now;
            return edit;
        }
      
        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceList> GetAllPriceLists()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceLists(): Attempting to retrieve all the price lists");
            var priceLists = new List<PriceList>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceLists = entities.PriceLists.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} price lists retrieved", priceLists.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceLists;
        }

        /// <summary>
        /// Adds or updates the price lists.
        /// </summary>
        /// <param name="priceList">The priceList to add.</param>
        /// <returns>A flag, indicating a successful add.</returns>
        public PriceList AddPriceList(PriceList priceList)
        {
            this.Log.LogDebug(this.GetType(), "AddPriceList(): Attempting to add a new price list");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceList.UserMasterID = NouvemGlobal.UserId;
                    priceList.DeviceMasterID = NouvemGlobal.DeviceId;
                    priceList.EditDate = DateTime.Now;
                    entities.PriceLists.Add(priceList);
                    entities.SaveChanges();
                    this.Log.LogError(this.GetType(), string.Format("New price list created. ID:{0}", priceList.PriceListID));
                    return priceList;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }
        }

        /// <summary>
        /// Adds or updates the price lists.
        /// </summary>
        /// <param name="priceLists">The priceLists to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePriceMasters(IList<PriceMaster> priceLists)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdatePriceMasters(): Attempting to update the price lists");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceLists.ToList().ForEach(x =>
                    {
                        if (x.PriceListID == 0)
                        {
                            // new
                            var newPriceList = new PriceList
                            {
                                CurrentPriceListName = x.CurrentPriceListName,
                                BasePriceListID = x.BasePriceListID,
                                BPCurrencyID = x.BPCurrencyID,
                                Factor = x.Factor,
                                UserMasterID = NouvemGlobal.UserId,
                                CreationDate = DateTime.Now,
                                NouRoundingOptionsID = x.NouRoundingOptionsID,
                                NouRoundingRulesID = x.NouRoundingRulesID,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                DeviceMasterID = NouvemGlobal.DeviceId,
                                BPMasterID = x.BPMasterID,
                                EditDate = DateTime.Now,
                                Deleted = false
                            };

                            entities.PriceLists.Add(newPriceList);

                            if (!newPriceList.BasePriceListID.IsNullOrZero())
                            {
                                // add the base price list details.
                                entities.SaveChanges();
                                var basePriceList =
                                    entities.PriceLists.FirstOrDefault(
                                        baseList => baseList.PriceListID == newPriceList.BasePriceListID);

                                if (basePriceList != null)
                                {
                                    var details = basePriceList.PriceListDetails.Where(detail => !detail.Deleted);

                                    foreach (var detail in details)
                                    {
                                        var newPriceListDetail = new PriceListDetail
                                        {
                                            PriceListID = newPriceList.PriceListID,
                                            INMasterID = detail.INMasterID,
                                            Price = detail.Price,
                                            NouOrderMethodID = detail.NouOrderMethodID,
                                            NouPriceMethodID = detail.NouPriceMethodID,
                                            NouPriceMethodID_Label = detail.NouPriceMethodID_Label,
                                            LabelPrice = detail.LabelPrice,
                                            UserMasterID = NouvemGlobal.UserId,
                                            DeviceMasterID = NouvemGlobal.DeviceId,
                                            EditDate = DateTime.Now,
                                            //ManualPriceAndOrMethods = detail.ManualPriceAndOrMethods,
                                            Deleted = false
                                        };

                                        entities.PriceListDetails.Add(newPriceListDetail);
                                        entities.SaveChanges();

                                        //NouvemGlobal.PriceListDetails.Add(new PriceDetail
                                        //{
                                        //    PriceListID = newPriceListDetail.PriceListID,
                                        //    PriceListDetailID = newPriceListDetail.PriceListDetailID,
                                        //    PriceListFactor = detail.PriceList.Factor,
                                        //    ItemCode = detail.INMaster.Code,
                                        //    ItemDescription = detail.INMaster.Name,
                                        //    INMasterID = detail.INMasterID,
                                        //    BasePriceListName = basePriceList.CurrentPriceListName,
                                        //    Price = detail.Price,
                                        //    NouOrderMethodID = detail.NouOrderMethodID,
                                        //    NouOrderMethodName = detail.NouOrderMethod.Name,
                                        //    NouPriceMethodName = detail.NouPriceMethod.Name,
                                        //    NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                        //    LabelPrice = detail.LabelPrice,
                                        //    NouPriceMethodID = detail.NouPriceMethodID,
                                        //    PriceListName = detail.PriceList.CurrentPriceListName,
                                        //    ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                                        //    Remove = false
                                        //});
                                    }
                                }
                            }
                        }
                        else
                        {
                            // update
                            var dbPriceList =
                                entities.PriceLists.FirstOrDefault(
                                    priceList => priceList.PriceListID == x.PriceListID && !priceList.Deleted);

                            if (dbPriceList != null)
                            {
                                dbPriceList.CurrentPriceListName = x.CurrentPriceListName;
                                dbPriceList.BPCurrencyID = x.BPCurrencyID;
                                dbPriceList.NouRoundingOptionsID = x.NouRoundingOptionsID;
                                dbPriceList.NouRoundingRulesID = x.NouRoundingRulesID;
                                dbPriceList.Factor = x.Factor;
                                dbPriceList.BPCurrencyID = x.BPCurrencyID;
                                dbPriceList.StartDate = x.StartDate;
                                dbPriceList.EndDate = x.EndDate;
                                dbPriceList.UserMasterID = NouvemGlobal.UserId;
                                dbPriceList.DeviceMasterID = NouvemGlobal.DeviceId;
                                dbPriceList.EditDate = DateTime.Now;
                                dbPriceList.BPMasterID = x.BPMasterID;
                                dbPriceList.Deleted = x.Deleted;

                                if (dbPriceList.BasePriceListID != x.BasePriceListID)
                                {
                                    dbPriceList.BasePriceListID = x.BasePriceListID;

                                    var detailsToRemove =
                                        entities.PriceListDetails.Where(
                                            detail => detail.PriceListID == dbPriceList.PriceListID).ToList();

                                    detailsToRemove.ForEach(d => d.Deleted = true);

                                    var basePriceList =
                                    entities.PriceLists.FirstOrDefault(
                                        baseList => baseList.PriceListID == x.BasePriceListID);

                                    if (basePriceList != null)
                                    {
                                        var details = basePriceList.PriceListDetails;

                                        foreach (var detail in details)
                                        {
                                            var newPriceListDetail = new PriceListDetail
                                            {
                                                PriceListID = dbPriceList.PriceListID,
                                                INMasterID = detail.INMasterID,
                                                Price = detail.Price,
                                                NouOrderMethodID = detail.NouOrderMethodID,
                                                NouPriceMethodID = detail.NouPriceMethodID,
                                                ManualPriceAndOrMethods = detail.ManualPriceAndOrMethods,
                                                UserMasterID = NouvemGlobal.UserId,
                                                DeviceMasterID = NouvemGlobal.DeviceId,
                                                NouPriceMethodID_Label = detail.NouPriceMethodID_Label,
                                                LabelPrice = detail.LabelPrice,
                                                EditDate = DateTime.Now,
                                                Deleted = false
                                            };

                                            entities.PriceListDetails.Add(newPriceListDetail);
                                        }
                                    }
                                }
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Price lists successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public IList<PriceDetail> GetPriceListDetails()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceListDetails(): Attempting to retrieve all the Price list details");
            var priceListDetails = new List<PriceDetail>();

            try
            {
                IList<App_GetPriceDetails_Result1> details = null;
                using (var entities = new NouvemEntities())
                {
                    details = entities.App_GetPriceDetails(0).ToList();
                }

                priceListDetails = (from detail in details
                    select new PriceDetail
                    {
                        PriceListID = detail.PriceListID,
                        PriceListDetailID = detail.PriceListDetailID,
                        PriceListFactor = detail.Factor,
                        ItemCode = detail.Code,
                        ItemDescription = detail.Name,
                        INMasterID = detail.INMasterID,
                        BasePrice = detail.BasePrice.ToDecimal(),
                        BasePriceListName = detail.BasePriceName,
                        Price = detail.Price,
                        NouOrderMethodID = detail.NouOrderMethodID,
                        NouOrderMethodName = detail.OrderMethodName,
                        NouPriceMethodName = detail.PriceMethodName,
                        NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                        NouRecipePriceMethodID = detail.NouPriceMethodID_Recipe,
                        LabelPrice = detail.LabelPrice,
                        NouPriceMethodID = detail.NouPriceMethodID,
                        PriceListName = detail.CurrentPriceListName,
                        ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                        Remove = false
                    }).ToList();

                    foreach (var detail in priceListDetails)
                    {
                        var factor = detail.PriceListFactor;
                        if (!detail.ManualPriceAndOrMethod && factor != 0)
                        {
                            var convertedDiscount = 100M + factor.ToDecimal();
                            detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                        }
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} price list details", priceListDetails.Count()));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceListDetails;
        }

        /// <summary>
        /// Retrieve all the price list customer groups.
        /// </summary>
        /// <returns>A collection of price list customer groups.</returns>
        public IList<App_GetPriceListCustomerGroups_Result> GetPriceListCustomerGroups()
        {
            var data = new List<App_GetPriceListCustomerGroups_Result>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    data = entities.App_GetPriceListCustomerGroups().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }

        /// <summary>
        /// Copies a price lists details to another price list.
        /// </summary>
        /// <param name="copyFrom">The price list id we are copying from.</param>
        /// <param name="copyTo">The price list id we are copying to.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        public bool CopyPriceListDetails(int copyFrom, int copyTo, int deviceId, int userId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_CopyPriceListDetails(copyFrom, copyTo,deviceId,userId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }


        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public IList<PriceDetail> GetPriceListDetails(int priceListId)
        {
            this.Log.LogDebug(this.GetType(), "GetPriceListDetails(): Attempting to retrieve all the Price list details");
            var priceListDetails = new List<PriceDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    IList<PriceListDetail> localPriceDetails;
                    if (priceListId == 0)
                    {
                        // all price list details
                        localPriceDetails = entities.PriceListDetails.Where(x => !x.Deleted).ToList();
                    }
                    else
                    {
                        localPriceDetails = entities.PriceListDetails.Where(x => x.PriceListID == priceListId && !x.Deleted).ToList();
                    }
                 
                    priceListDetails = (from detail in localPriceDetails
                                        select new PriceDetail
                                        {
                                            PriceListID = detail.PriceListID,
                                            PriceListDetailID = detail.PriceListDetailID,
                                            PriceListFactor = detail.PriceList.Factor,
                                            ItemCode = detail.INMaster.Code,
                                            ItemDescription = detail.INMaster.Name,
                                            INMasterID = detail.INMasterID,
                                            ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                                            BasePrice = detail.PriceList.PriceListBase != null ? detail.PriceList.PriceListBase
                                            .PriceListDetails.Where(x => x.INMasterID == detail.INMasterID && !x.Deleted)
                                            .Select(p => p.Price).FirstOrDefault() : 0,
                                            //BasePrice = localPriceDetails
                                            //.Where(x => x.PriceListID == detail.PriceList.BasePriceListID && x.INMasterID == detail.INMasterID
                                            //    && !x.Deleted).Select(basePrice => basePrice.Price).FirstOrDefault(),
                                            Price = detail.Price,
                                            NouOrderMethodID = detail.NouOrderMethodID,
                                            NouOrderMethodName = detail.NouOrderMethod.Name,
                                            NouPriceMethodName = detail.NouPriceMethod.Name,
                                            NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                            LabelPrice = detail.LabelPrice,
                                            NouPriceMethodID = detail.NouPriceMethodID,
                                            PriceListName = detail.PriceList.CurrentPriceListName,
                                            ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,Remove = false
                                        }).ToList();

                    foreach (var detail in priceListDetails)
                    {
                        var factor = detail.PriceListFactor;
                        if (!detail.ManualPriceAndOrMethod && factor != 0)
                        {
                            var convertedDiscount = 100M + factor.ToDecimal();
                            detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                        }
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} price list details", priceListDetails.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceListDetails;
        }

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public PriceDetail GetPriceListDetail(int priceListDetailId)
        {
            this.Log.LogDebug(this.GetType(), "GetPriceListDetails(): Attempting to retrieve all the Price list details");
            var priceListDetail = new PriceDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var detail = entities.PriceListDetails.FirstOrDefault(x => x.PriceListDetailID == priceListDetailId && !x.Deleted);
                    if (detail == null)
                    {
                        return new PriceDetail();
                    }

                    priceListDetail =  new PriceDetail
                                        {
                                            PriceListID = detail.PriceListID,
                                            PriceListDetailID = detail.PriceListDetailID,
                                            PriceListFactor = detail.PriceList.Factor,
                                            ItemCode = detail.INMaster.Code,
                                            ItemDescription = detail.INMaster.Name,
                                            INMasterID = detail.INMasterID,
                                            ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                                            BasePrice = detail.PriceList.PriceListBase != null ? detail.PriceList.PriceListBase
                                            .PriceListDetails.Where(x => x.INMasterID == detail.INMasterID && !x.Deleted)
                                            .Select(p => p.Price).FirstOrDefault() : 0,
                                            //BasePrice = localPriceDetails
                                            //.Where(x => x.PriceListID == detail.PriceList.BasePriceListID && x.INMasterID == detail.INMasterID
                                            //    && !x.Deleted).Select(basePrice => basePrice.Price).FirstOrDefault(),
                                            Price = detail.Price,
                                            NouOrderMethodID = detail.NouOrderMethodID,
                                            NouOrderMethodName = detail.NouOrderMethod.Name,
                                            NouPriceMethodName = detail.NouPriceMethod.Name,
                                            NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                            LabelPrice = detail.LabelPrice,
                                            NouPriceMethodID = detail.NouPriceMethodID,
                                            PriceListName = detail.PriceList.CurrentPriceListName,
                                            ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                                            Remove = false
                                        };
                   
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceListDetail;
        }

        /// <summary>
        /// Retrieve the first price detail for the input product.
        /// </summary>
        /// <returns>A corresponding price detail.</returns>
        public PriceDetail GetFirstProductPriceListDetail(int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var detail = entities.PriceListDetails.FirstOrDefault(x => x.INMasterID == inMasterId && !x.Deleted);
                    if (detail == null)
                    {
                        return null;
                    }

                    var priceListDetail = new PriceDetail
                    {
                        PriceListID = detail.PriceListID,
                        PriceListDetailID = detail.PriceListDetailID,
                        INMasterID = detail.INMasterID,
                        ItemGroup = detail.INMaster.INGroup != null ? detail.INMaster.INGroup.Name : string.Empty,
                        Price = detail.Price,
                        NouOrderMethodID = detail.NouOrderMethodID,
                        NouOrderMethodName = detail.NouOrderMethod.Name,
                        NouPriceMethodName = detail.NouPriceMethod.Name,
                        NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                        LabelPrice = detail.LabelPrice,
                        NouPriceMethodID = detail.NouPriceMethodID,
                        ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false
                    };

                    return priceListDetail;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public IList<PriceDetail> GetUpdatedPriceListDetails(int priceListId)
        {
            this.Log.LogDebug(this.GetType(), "GetPriceListDetails(): Attempting to retrieve all the Price list details");
            var priceListDetails = new List<PriceDetail>();
            try
            {
                IList<App_GetPriceDetails_Result1> localPriceDetails = null;
                using (var entities = new NouvemEntities())
                {
                    if (priceListId == 0)
                    {
                        // all price list details
                        if (
                            entities.PriceListDetails.Any(
                                x =>
                                    !x.Deleted && x.EditDate >= NouvemGlobal.LastPriceListDetailsUpdate &&
                                    x.DeviceMasterID != NouvemGlobal.DeviceId))
                        {
                            NouvemGlobal.LastPriceListDetailsUpdate = DateTime.Now;
                            return this.GetPriceListDetails();
                        }
                    }
                    else
                    {
                        localPriceDetails = entities.App_GetPriceDetails(priceListId).ToList();
                    }
                }

                priceListDetails = (from detail in localPriceDetails
                                    select new PriceDetail
                                    {
                                        PriceListID = detail.PriceListID,
                                        PriceListDetailID = detail.PriceListDetailID,
                                        PriceListFactor = detail.Factor,
                                        ItemCode = detail.Code,
                                        ItemDescription = detail.Name,
                                        INMasterID = detail.INMasterID,
                                        ItemGroup = detail.GroupName ?? string.Empty,
                                        BasePrice = detail.BasePrice.ToDecimal(),
                                        Price = detail.Price,
                                        NouOrderMethodID = detail.NouOrderMethodID,
                                        NouOrderMethodName = detail.OrderMethodName,
                                        NouPriceMethodName = detail.PriceMethodName,
                                        NouPriceMethodIDLabel = detail.NouPriceMethodID_Label,
                                        NouRecipePriceMethodID = detail.NouPriceMethodID_Recipe,
                                        LabelPrice = detail.LabelPrice,
                                        NouPriceMethodID = detail.NouPriceMethodID,
                                        PriceListName = detail.CurrentPriceListName,
                                        ManualPriceAndOrMethod = detail.ManualPriceAndOrMethods ?? false,
                                        Remove = false
                                    }).ToList();

                this.Log.LogDebug(this.GetType(), string.Format("{0} price list details", priceListDetails.Count()));

                foreach (var detail in priceListDetails)
                {
                    var factor = detail.PriceListFactor;
                    if (!detail.ManualPriceAndOrMethod && factor != 0)
                    {
                        var convertedDiscount = 100M + factor.ToDecimal();
                        detail.Price = Math.Round(detail.BasePrice / 100 * convertedDiscount, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceListDetails;
        }
        
        /// <summary>
        /// Retrieve all the inventory master products.
        /// </summary>
        /// <returns>A collection of inventory master products.</returns>
        public IList<PriceDetail> GetProducts()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceListDetails(): Attempting to retrieve all the Price list inventory master products");
            var priceListProducts = new List<PriceDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceListProducts = (from product in entities.INMasters
                                        select new PriceDetail
                                        {
                                            ItemCode = product.Code,
                                            ItemDescription = product.Name,
                                            INMasterID = product.INMasterID,
                                            ItemGroup = product.INGroup != null ? product.INGroup.Name : string.Empty
                                        }).ToList();


                    this.Log.LogDebug(this.GetType(), string.Format("{0} inventory master products retrieved", priceListProducts.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceListProducts;
        }

        /// <summary>
        /// Updates a price.
        /// </summary>
        /// <param name="price">The price to update to.</param>
        /// <param name="priceListId">The price book id.</param>
        /// <param name="inmasterId">The product id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePrice(decimal price, int priceListId, int inmasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var priceListDetail = entities.PriceListDetails.FirstOrDefault(x =>
                        x.PriceListID == priceListId && x.INMasterID == inmasterId && !x.Deleted);
                    if (priceListDetail != null)
                    {
                        priceListDetail.Price = price;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

       /// <summary>
       /// Determines if a product is on a price book.
       /// </summary>
       /// <param name="priceListId">The price book to check.</param>
       /// <param name="inmasterId">The product to check.</param>
       /// <returns>Flag, as to whether a product is on a price book.</returns>
        public bool IsProductOnPriceBook(int priceListId, int inmasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.PriceListDetails.Any(x =>
                        x.PriceListID == priceListId && x.INMasterID == inmasterId && !x.Deleted);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieve all the order methods.
        /// </summary>
        /// <returns>A collection of order methods.</returns>
        public IList<NouOrderMethod> GetOrderMethods()
        {
            this.Log.LogDebug(this.GetType(), "GetOrderMethods(): Attempting to retrieve all the order methods");
            var orderMethods = new List<NouOrderMethod>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orderMethods = entities.NouOrderMethods.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} order methods retrieved", orderMethods.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orderMethods;
        }

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        public IList<NouPriceMethod> GetPriceMethods()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceMethods(): Attempting to retrieve all the Price methods");
            var priceMethods = new List<NouPriceMethod>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceMethods = entities.NouPriceMethods.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} price methods retrieved", priceMethods.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceMethods;
        }

        /// <summary>
        /// Gets the price from a price list detail record.
        /// </summary>
        /// <param name="priceListDetailId">The price list detail.</param>
        /// <returns>A corresponding price.</returns>
        public decimal? GetPrice(int priceListDetailId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var detail =
                        entities.PriceListDetails.FirstOrDefault(
                            x => x.PriceListDetailID == priceListDetailId && !x.Deleted);
                    if (detail != null)
                    {
                        return detail.Price;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the price from a price list detail record.
        /// </summary>
        /// <param name="priceListId">The price list detail.</param>
        /// <returns>A corresponding price.</returns>
        public decimal? GetPartnerPrice(int priceListId, int inmasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var priceList = entities.PriceLists.FirstOrDefault(x => x.PriceListID == priceListId);
                    if (priceList != null)
                    {
                        var detail =
                        entities.PriceListDetails.FirstOrDefault(
                            x => x.PriceListID == priceListId && x.INMasterID == inmasterId && !x.Deleted);
                        if (detail != null)
                        {
                            return detail.Price;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Updates a price book with the input price..
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="partnerPriceListId">The partner price list id.</param>
        /// <param name="price">The new price.</param>
        /// <param name="productId">The product id.</param>
        /// <returns>A flag, indicating a successful change or not.</returns>
        public Tuple<bool, PriceDetail> UpdatePrice(int priceListId, int partnerPriceListId, decimal price, int productId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("UpdatePrice(): Attempting to update price. PriceListId:{0}, Price:{1}, ProductId:{2}, PartnerListId:{3}",
                                                            priceListId, price, productId, partnerPriceListId));

            var newAddedDetailId = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var priceListDetail =
                        entities.PriceListDetails.FirstOrDefault(
                            x => x.PriceListID == partnerPriceListId && x.INMasterID == productId && !x.Deleted);

                    if (priceListDetail == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Adding price change to customer price book");
                        //var basePriceDetail = NouvemGlobal.BasePriceList.PriceListDetails
                        //    .FirstOrDefault(x => x.INMasterID == productId);

                        var basePriceDetail =
                            entities.PriceListDetails.FirstOrDefault(
                                x => x.PriceListID == priceListId && x.INMasterID == productId && !x.Deleted);

                        if (basePriceDetail != null)
                        {
                            priceListDetail = new PriceListDetail
                            {
                                Price = price,
                                INMasterID = basePriceDetail.INMasterID,
                                NouOrderMethodID = basePriceDetail.NouOrderMethodID,
                                NouPriceMethodID = basePriceDetail.NouPriceMethodID,
                                NouPriceMethodID_Label = basePriceDetail.NouPriceMethodID_Label,
                                LabelPrice = basePriceDetail.LabelPrice,
                                PriceListID = partnerPriceListId,
                                ManualPriceAndOrMethods = true,
                                UserMasterID = NouvemGlobal.UserId,
                                DeviceMasterID = NouvemGlobal.DeviceId,
                                EditDate = DateTime.Now,
                                Deleted = false
                            };

                            entities.PriceListDetails.Add(priceListDetail);
                            entities.SaveChanges();
                            newAddedDetailId = priceListDetail.PriceListDetailID;
                            this.Log.LogDebug(this.GetType(), "Price list detail added to customer price list.");
                        }
                    }
                    else
                    {
                        var edit = new PriceEdit
                        {
                            PriceListDetailID = priceListDetail.PriceListDetailID,
                            OldPrice = priceListDetail.Price,
                            NewPrice = price,
                            OldPriceMethod = priceListDetail.NouPriceMethodID,
                            NewPriceMethod = priceListDetail.NouPriceMethodID,
                            EditDate = DateTime.Now,
                            DeviceID = NouvemGlobal.DeviceId,
                            UserID = NouvemGlobal.UserId
                        };

                        entities.PriceEdits.Add(edit);

                        priceListDetail.UserMasterID = NouvemGlobal.UserId;
                        priceListDetail.DeviceMasterID = NouvemGlobal.DeviceId;
                        priceListDetail.EditDate = DateTime.Now;
                        priceListDetail.Price = price;
                        priceListDetail.ManualPriceAndOrMethods = true;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("Price list detail id:{0} successfully updated with price:{1}", priceListDetail.PriceListDetailID, price));
                    }

                    var localPriceDetail = new PriceDetail();
                    if (newAddedDetailId > 0)
                    {
                        localPriceDetail =
                              this.GetPriceListDetail(newAddedDetailId);
                    }

                    return Tuple.Create(true, localPriceDetail);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(false, new PriceDetail()); ;
        }

        /// <summary>
        /// Adds or updates the price list details.
        /// </summary>
        /// <param name="priceListDetails">The priceList details to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePriceListDetails(IList<PriceDetail> priceListDetails, int progressAmt)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdatePriceListDetails(): Attempting to update the price list details");
            if (!priceListDetails.Any())
            {
                return true;
            }

            var edits = new List<PriceEdit>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var priceMethods = entities.NouPriceMethods.ToList();
                    var orderMethods = entities.NouOrderMethods.ToList();
                    var count = priceListDetails.Count;
                    ProgressBar.SetUp(0, count);
        
                    priceListDetails.ToList().ForEach(x =>
                    {
                        ProgressBar.Run();
                        if (x.PriceListDetailID == 0)
                        {
                            // new
                            var newPriceListDetail = new PriceListDetail
                            {
                                PriceListID = x.PriceListID,
                                INMasterID = x.INMasterID,
                                Price = x.Price,
                                NouOrderMethodID = x.NouOrderMethodID,
                                NouPriceMethodID = x.NouPriceMethodID,
                                LabelPrice = x.LabelPrice,
                                NouPriceMethodID_Label = x.NouPriceMethodIDLabel,
                                NouPriceMethodID_Recipe = x.NouRecipePriceMethodID,
                                ManualPriceAndOrMethods = x.ManualPriceAndOrMethod,
                                UserMasterID = NouvemGlobal.UserId,
                                DeviceMasterID = NouvemGlobal.DeviceId,
                                EditDate = DateTime.Now,
                                Deleted = false
                            };

                            entities.PriceListDetails.Add(newPriceListDetail);
                            entities.SaveChanges();

                            x.PriceListDetailID = newPriceListDetail.PriceListDetailID;
                            x.NouOrderMethodName = orderMethods.First(order => order.NouOrderMethodID == newPriceListDetail.NouOrderMethodID).Name;
                            x.NouPriceMethodName = priceMethods.First(price => price.NouPriceMethodID == newPriceListDetail.NouPriceMethodID).Name;
                            //NouvemGlobal.PriceListDetails.Add(x);
                            //var localPriceList =
                            //    NouvemGlobal.PriceListMasters.FirstOrDefault(
                            //        priceList => priceList.PriceListID == x.PriceListID);
                            //if (localPriceList != null)
                            //{
                            //    localPriceList.PriceListDetails.Add(x);
                            //    if (localPriceList.PriceListID == NouvemGlobal.BasePriceList.PriceListID)
                            //    {
                            //        NouvemGlobal.BasePriceList.PriceListDetails.Add(x);
                            //    }
                            //}
                        }
                        else
                        {
                            // update
                            var dbPriceListDetail =
                                entities.PriceListDetails.FirstOrDefault(
                                    priceListDetail =>
                                        priceListDetail.PriceListDetailID == x.PriceListDetailID &&
                                        !priceListDetail.Deleted);

                            if (dbPriceListDetail != null)
                            {
                                var newPrice = x.Price;
                                var newMethod = x.NouPriceMethodID;
                                var edit = new PriceEdit
                                {
                                    PriceListDetailID = dbPriceListDetail.PriceListDetailID,
                                    OldPrice = dbPriceListDetail.Price,
                                    NewPrice = newPrice,
                                    OldPriceMethod = dbPriceListDetail.NouPriceMethodID,
                                    NewPriceMethod = newMethod,
                                    EditDate = DateTime.Now,
                                    DeviceID = NouvemGlobal.DeviceId,
                                    UserID = NouvemGlobal.UserId
                                };

                                edits.Add(edit);

                                dbPriceListDetail.PriceListID = x.PriceListID;
                                dbPriceListDetail.INMasterID = x.INMasterID;
                                dbPriceListDetail.Price = x.Price;
                                dbPriceListDetail.NouOrderMethodID = x.NouOrderMethodID;
                                dbPriceListDetail.NouPriceMethodID = x.NouPriceMethodID;
                                dbPriceListDetail.NouPriceMethodID_Label = x.NouPriceMethodIDLabel;
                                dbPriceListDetail.NouPriceMethodID_Recipe = x.NouRecipePriceMethodID;
                                dbPriceListDetail.LabelPrice = x.LabelPrice;
                                dbPriceListDetail.ManualPriceAndOrMethods = x.ManualPriceAndOrMethod;
                                dbPriceListDetail.Deleted = x.Remove;
                                dbPriceListDetail.UserMasterID = NouvemGlobal.UserId;
                                dbPriceListDetail.DeviceMasterID = NouvemGlobal.DeviceId;
                                dbPriceListDetail.EditDate = DateTime.Now;
                            }
                        }
                    });

                    entities.SaveChanges();
                    var updates = entities.ApplicationUpdates.Where(x => x.DeviceMasterID != NouvemGlobal.DeviceId);
                    foreach (var applicationUpdate in updates)
                    {
                        applicationUpdate.Pricing = true;
                    }

                    entities.SaveChanges();

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        if (edits.Any())
                        {
                            this.AddEdits(edits);
                        }
                    }));

                    this.Log.LogDebug(this.GetType(), "Price list details successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Gets the grouped products.
        /// </summary>
        /// <returns>A collection of grouped products.</returns>
        public IList<ProductSelection> GetGroupedProducts()
        {
            this.Log.LogDebug(this.GetType(), "GetGroupedProducts(): Attempting to retrieve all the grouped products");
            var groupedProducts = new List<ProductSelection>();
       
            try
            {
                using (var entities = new NouvemEntities())
                {
                    // get the groups first
                    var groups = (from ingroup in entities.INGroups
                                  where !ingroup.Deleted
                        select new ProductSelection
                        {
                            NodeID = ingroup.INGroupID,
                            ParentID = ingroup.ParentInGroupID,
                            ProductName = ingroup.Name
                        }).ToList();

                    /* add the product nodes (Note - all nodes must be unique. Therefore, in order to ensure that there is no 
                     * clash with the group primary key nodes, we invert the immaster.IMMasterID. This will have no affect
                     * on the visual ui tree, as the immaster records will never have any sub nodes - they are leaf nodes)*/
                    var products = (from inmaster in entities.INMasters
                                    where inmaster.Deleted == null
                                  select new ProductSelection
                                  {
                                      NodeID = -inmaster.INMasterID,
                                      ParentID = inmaster.INGroupID,
                                      ProductName = inmaster.Name,
                                      GroupName = inmaster.INGroup != null ? inmaster.INGroup.Name : string.Empty,
                                      IsLeafNode = true
                                  }).ToList();

                    groupedProducts = groups.Union(products).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} product selections retrieved", groupedProducts.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return groupedProducts;
        }

        public IEnumerable<IList<INMaster>> GetSubCategoriesFor(int? catId)
        {
            using (var entities = new NouvemEntities())
            {
                var subs = entities.INGroups.Where(c => c.ParentInGroupID == catId);

                foreach (var sub in subs)
                {
                    yield return entities.INMasters.Where(x => x.INGroupID == sub.INGroupID).ToList();

                    // Recursive call
                    foreach (var subsub in this.GetSubCategoriesFor(sub.INGroupID))
                    {
                        yield return subsub;
                    }
                }
            }
        }

        /// <summary>
        /// Adds the pricing audit trail.
        /// </summary>
        /// <param name="edits">The edited data.</param>
        private void AddEdits(IList<PriceEdit> edits)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.PriceEdits.AddRange(edits);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

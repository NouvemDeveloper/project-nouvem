﻿// -----------------------------------------------------------------------
// <copyright file="BusinessPartnerRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;

    /// <summary>
    /// Repository class that handles the business partner crud operations.
    /// </summary>
    public class BusinessPartnerRepository : NouvemRepositoryBase, IBusinessPartnerRepository
    {
        /// <summary>
        /// Method that adds a new business partner.
        /// </summary>
        /// <param name="partner">The partner to add.</param>
        /// <returns>The newly added partner id.</returns>
        public int AddBusinessPartner(BusinessPartner partner)
        {
            if (partner.Details.BPGroupID.IsNullOrZero())
            {
                throw new Exception("Invalid BPGroupID");
            }

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var newPartner = new BPMaster();
                    newPartner.Code = partner.Details.Code;
                    newPartner.Name = partner.Details.Name;
                    newPartner.Remarks = partner.Details.Remarks;
                    newPartner.Notes = partner.Details.Notes;
                    newPartner.PopUpNotes = partner.Details.PopUpNotes;
                    newPartner.InvoiceNote = partner.Details.InvoiceNote;
                    newPartner.CompanyNo = partner.Details.CompanyNo;
                    newPartner.OrderLimit = partner.Details.OrderLimit;
                    newPartner.VATNo = partner.Details.VATNo;
                    newPartner.Web = partner.Details.Web;
                    newPartner.FAX = partner.Details.FAX;
                    newPartner.Tel = partner.Details.Tel;
                    newPartner.DeliveryChargeThreshold = partner.Details.DeliveryChargeThreshold;
                    newPartner.Balance = partner.Details.Balance;
                    newPartner.Insurance = partner.Details.Insurance;
                    newPartner.IFALevy = partner.Details.IFALevy;
                    newPartner.Scrapie = partner.Details.Scrapie;
                    newPartner.CreditLimit = partner.Details.CreditLimit;
                    newPartner.OnHold = partner.Details.OnHold;
                    newPartner.Uplift = partner.Details.Uplift;
                    newPartner.PORequired = partner.Details.PORequired;
                    newPartner.NouBPTypeID = partner.Details.NouBPTypeID;
                    newPartner.ActiveFrom = partner.Details.ActiveFrom;
                    newPartner.ActiveTo = partner.Details.ActiveTo;
                    newPartner.InActiveFrom = partner.Details.InActiveFrom;
                    newPartner.InActiveTo = partner.Details.InActiveTo;
                    newPartner.PriceListID = partner.Details.PriceListID;
                    newPartner.BPMasterID_Agent = partner.Details.BPMasterID_Agent;
                    newPartner.UserMasterID = NouvemGlobal.UserId;
                    newPartner.CreationDate = DateTime.Now;
                    newPartner.DeviceID = NouvemGlobal.DeviceId;
                    newPartner.EditDate = DateTime.Now;
                    newPartner.ProcessID = partner.Details.ProcessID;
                    newPartner.RouteMasterID = partner.Route != null ? partner.Route.RouteID : (int?) null;

                    if (partner.Details.BPCurrencyID != 0)
                    {
                        newPartner.BPCurrencyID = partner.Details.BPCurrencyID;
                    }

                    if (partner.Details.BPGroupID != 0)
                    {
                        newPartner.BPGroupID = partner.Details.BPGroupID;
                    }

                    if (partner.Details.NouBPTypeID != 0)
                    {
                        newPartner.NouBPTypeID = partner.Details.NouBPTypeID;
                    }

                    entities.BPMasters.Add(newPartner);
                    entities.SaveChanges();

                    if (partner.Contacts != null && partner.Contacts.Any())
                    {
                        partner.Contacts.ToList().ForEach(
                            contact =>
                                {
                                    if (contact.Details.PrimaryContact.ToBool())
                                    {
                                        // The primary contact has changed, so ensure the previous primary contact is revoked.
                                        entities.BPContacts.ToList().ForEach(x => x.PrimaryContact = false);
                                    }

                                    contact.Details.BPMasterID = newPartner.BPMasterID;
                                    entities.BPContacts.Add(contact.Details);
                                });
                    }

                    if (partner.Herds != null && partner.Herds.Any())
                    {
                        partner.Herds.ToList().ForEach(
                            herd =>
                            {
                                herd.BPMasterID = newPartner.BPMasterID;
                                entities.SupplierHerds.Add(herd);
                            });
                    }

                    if (partner.Addresses != null && partner.Addresses.Any())
                    {
                        partner.Addresses.ToList().ForEach(
                            address =>
                            {
                                address.Details.BPMasterID = newPartner.BPMasterID;
                                entities.BPAddresses.Add(address.Details);
                            });
                    }

                    if (partner.Attachments != null && partner.Attachments.Any())
                    {
                        partner.Attachments.ToList().ForEach(
                            attachment =>
                            {
                                attachment.BPMasterID = newPartner.BPMasterID;
                                entities.BPAttachments.Add(attachment);
                            });
                    }

                    if (partner.PartnerProperties != null)
                    {
                        partner.PartnerProperties.ToList().ForEach(x =>
                        {
                            var localPropertySelection = new BPPropertySelection
                            {
                                BPMasterID = newPartner.BPMasterID,
                                BPPropertyID = x.Details.BPPropertyID,
                                DisplayMessage = x.DisplayMessage,
                                Deleted = false
                            };

                            entities.BPPropertySelections.Add(localPropertySelection);
                        });
                    }

                    if (partner.LabelField != null)
                    {
                        entities.BPLabelFields.Add(partner.LabelField);
                        newPartner.BPLabelFieldID = partner.LabelField.BPLabelFieldID;
                    }

                    //partner.Audit.TableID = newPartner.BPMasterID;
                    //entities.Audits.Add(partner.Audit);
                    entities.SaveChanges();
                    entities.App_AddEntityUpdate(newPartner.BPMasterID, EntityType.BusinessPartner);
                    return newPartner.BPMasterID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }
        }

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnerToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateBusinessPartner(BusinessPartner partnerToUpdate)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to update business partner {0}", partnerToUpdate.Details.BPMasterID));
            if (partnerToUpdate.Details.BPGroupID.IsNullOrZero())
            {
                throw new Exception("Invalid BPGroupID");
            }

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbPartner =
                        entities.BPMasters.FirstOrDefault(
                            partner => partner.BPMasterID == partnerToUpdate.Details.BPMasterID);

                    if (dbPartner == null)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Business partner {0} not found", partnerToUpdate.Details.BPMasterID));
                        return false;
                    }
                  
                    dbPartner.Code = partnerToUpdate.Details.Code;
                    dbPartner.Name = partnerToUpdate.Details.Name;
                    dbPartner.Remarks = partnerToUpdate.Details.Remarks;
                    dbPartner.Notes = partnerToUpdate.Details.Notes;
                    dbPartner.PopUpNotes = partnerToUpdate.Details.PopUpNotes;
                    dbPartner.InvoiceNote = partnerToUpdate.Details.InvoiceNote;
                    dbPartner.CompanyNo = partnerToUpdate.Details.CompanyNo;
                    dbPartner.VATNo = partnerToUpdate.Details.VATNo;
                    dbPartner.OrderLimit = partnerToUpdate.Details.OrderLimit;
                    dbPartner.Insurance = partnerToUpdate.Details.Insurance;
                    dbPartner.IFALevy = partnerToUpdate.Details.IFALevy;
                    dbPartner.Scrapie = partnerToUpdate.Details.Scrapie;
                    dbPartner.Web = partnerToUpdate.Details.Web;
                    dbPartner.FAX = partnerToUpdate.Details.FAX;
                    dbPartner.Tel = partnerToUpdate.Details.Tel;
                    dbPartner.ProcessID = partnerToUpdate.Details.ProcessID;
                    dbPartner.BPMasterID_Agent = partnerToUpdate.Details.BPMasterID_Agent;
                    dbPartner.DeliveryChargeThreshold = partnerToUpdate.Details.DeliveryChargeThreshold;
                    dbPartner.NouBPTypeID = partnerToUpdate.Details.NouBPTypeID;
                    dbPartner.Balance = partnerToUpdate.Details.Balance;
                    dbPartner.CreditLimit = partnerToUpdate.Details.CreditLimit;
                    dbPartner.OnHold = partnerToUpdate.Details.OnHold;
                    dbPartner.Uplift = partnerToUpdate.Details.Uplift;
                    dbPartner.PORequired = partnerToUpdate.Details.PORequired;
                    dbPartner.ActiveFrom = partnerToUpdate.Details.ActiveFrom;
                    dbPartner.ActiveTo = partnerToUpdate.Details.ActiveTo;
                    dbPartner.InActiveFrom = partnerToUpdate.Details.InActiveFrom;
                    dbPartner.InActiveTo = partnerToUpdate.Details.InActiveTo;
                    dbPartner.DeviceID = NouvemGlobal.DeviceId;
                    dbPartner.EditDate = DateTime.Now;
                    dbPartner.PriceListID = partnerToUpdate.Details.PriceListID;
                    dbPartner.RouteMasterID = partnerToUpdate.Route != null
                        ? partnerToUpdate.Route.RouteID
                        : (int?) null;

                    if (partnerToUpdate.Details.BPCurrencyID != 0)
                    {
                        dbPartner.BPCurrencyID = partnerToUpdate.Details.BPCurrencyID;
                    }

                    if (partnerToUpdate.Details.BPGroupID != 0)
                    {
                        dbPartner.BPGroupID = partnerToUpdate.Details.BPGroupID;
                    }

                    if (partnerToUpdate.Details.NouBPTypeID != 0)
                    {
                        dbPartner.NouBPTypeID = partnerToUpdate.Details.NouBPTypeID;
                    }

                    // update the partner contacts.
                    partnerToUpdate.Contacts.ToList().ForEach(
                        contact =>
                            {
                                var dbContact =
                                    entities.BPContacts.FirstOrDefault(
                                        x => x.BPContactID == contact.Details.BPContactID && !contact.Details.Deleted);

                                if (contact.Details.PrimaryContact.ToBool())
                                {
                                    // The primary contact has changed, so ensure the previous primary contact is revoked.
                                    entities.BPContacts.Where(localContact => localContact.BPMasterID == partnerToUpdate.Details.BPMasterID && !localContact.Deleted)
                                        .ToList()
                                        .ForEach(
                                        x =>
                                            {
                                                x.PrimaryContact = false;
                                            });
                                }

                                if (dbContact != null)
                                {
                                    dbContact.Title = contact.Details.Title;
                                    dbContact.FirstName = contact.Details.FirstName;
                                    dbContact.LastName = contact.Details.LastName;
                                    dbContact.Email = contact.Details.Email;
                                    dbContact.Phone = contact.Details.Phone;
                                    dbContact.Mobile = contact.Details.Mobile;
                                    dbContact.JobTitle = contact.Details.JobTitle;
                                    dbContact.PrimaryContact = contact.Details.PrimaryContact;
                                    dbContact.Deleted = contact.Details.Deleted;
                                }
                                else
                                {
                                    // adding a new contact
                                    contact.Details.BPMasterID = partnerToUpdate.Details.BPMasterID;
                                    entities.BPContacts.Add(contact.Details);
                                }
                            });

                    if (partnerToUpdate.Herds != null)
                    {
                        // update the partner contacts.
                        partnerToUpdate.Herds.ToList().ForEach(
                            herd =>
                            {
                                var dbHerd =
                                    entities.SupplierHerds.FirstOrDefault(
                                        x => x.SupplierHerdID == herd.SupplierHerdID && x.Deleted == null);

                                if (dbHerd != null)
                                {
                                    dbHerd.HerdNumber = herd.HerdNumber;
                                    dbHerd.Beef = herd.Beef;
                                    dbHerd.Sheep = herd.Sheep;
                                    dbHerd.Deleted = herd.Deleted;
                                }
                                else
                                {
                                    // adding a new contact
                                    herd.BPMasterID = partnerToUpdate.Details.BPMasterID;
                                    entities.SupplierHerds.Add(herd);
                                }
                            });
                    }
                  
                    // update the partner addresses.
                    partnerToUpdate.Addresses.ToList().ForEach(
                        address =>
                        {
                            var dbAddress =
                                     entities.BPAddresses.FirstOrDefault(
                                         x => x.BPAddressID == address.Details.BPAddressID && !address.Details.Deleted);

                            if (address.Details.Shipping.ToBool())
                            {
                                // The shipping address has changed, so ensure the previous shipping address (if any) is revoked.
                                entities.BPAddresses.Where(localAddress => 
                                    localAddress.BPMasterID == partnerToUpdate.Details.BPMasterID
                                    && localAddress.Shipping == true
                                    && localAddress.BPAddressID != address.Details.BPAddressID
                                    && !localAddress.Deleted)
                                    .ToList()
                                    .ForEach(
                                    x =>
                                    {
                                        x.Shipping = false;
                                    });
                            }

                            if (address.Details.Billing.ToBool())
                            {
                                // The billing address has changed, so ensure the previous shipping address (if any) is revoked.
                                entities.BPAddresses.Where(localAddress => 
                                    localAddress.BPMasterID == partnerToUpdate.Details.BPMasterID 
                                    && localAddress.Billing == true
                                    && localAddress.BPAddressID != address.Details.BPAddressID
                                    && !localAddress.Deleted)
                                    .ToList()
                                    .ForEach(
                                    x =>
                                    {
                                        x.Billing = false;
                                    });
                            }

                            if (dbAddress != null)
                            {
                                // update
                                if (address.Details != null)
                                {
                                    dbAddress.AddressLine1 = address.Details.AddressLine1;
                                    dbAddress.AddressLine2 = address.Details.AddressLine2;
                                    dbAddress.AddressLine3 = address.Details.AddressLine3;
                                    dbAddress.AddressLine4 = address.Details.AddressLine4;
                                    dbAddress.AddressLine5 = address.Details.AddressLine5;
                                    dbAddress.PostCode = address.Details.PostCode;
                                    dbAddress.GLN = address.Details.GLN;
                                    dbAddress.Shipping = address.Details.Shipping;
                                    dbAddress.Billing = address.Details.Billing;
                                }

                                if (address.PlantDetails != null)
                                {
                                    dbAddress.PlantID = address.PlantDetails.PlantID == 0 ? null : (int?)address.PlantDetails.PlantID;
                                }
                            }
                            else
                            {
                                // adding a new address
                                address.Details.BPMasterID = partnerToUpdate.Details.BPMasterID;
                                address.Details.PlantID = address.PlantDetails.PlantID == 0 ? null : (int?)address.PlantDetails.PlantID;
                                entities.BPAddresses.Add(address.Details);
                            }
                        });

                    if (partnerToUpdate.PartnerProperties != null)
                    {
                        var propertiesToDelete =
                            entities.BPPropertySelections.Where(
                                x => !x.Deleted && x.BPMasterID == partnerToUpdate.Details.BPMasterID);

                        // Remove the current partner properties.
                        entities.BPPropertySelections.RemoveRange(propertiesToDelete);
                        partnerToUpdate.PartnerProperties.ToList().ForEach(x =>
                        {
                            var localPropertySelection = new BPPropertySelection
                            {
                                BPMasterID = partnerToUpdate.Details.BPMasterID,
                                BPPropertyID = x.Details.BPPropertyID,
                                DisplayMessage = x.DisplayMessage,
                                Deleted = false
                            };

                            entities.BPPropertySelections.Add(localPropertySelection);
                        });
                    }

                    partnerToUpdate.Attachments.ToList().ForEach(
                        attachment =>
                            {
                                var dbAttachment =
                                    entities.BPAttachments.FirstOrDefault(
                                        x => x.BPAttachmentID == attachment.BPAttachmentID);

                                if (dbAttachment != null)
                                {
                                    // updating an existing attachment
                                    if (attachment.Deleted)
                                    {
                                        entities.BPAttachments.Remove(dbAttachment);
                                    }
                                    else
                                    {
                                        dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                        dbAttachment.FileName = attachment.FileName;
                                        dbAttachment.File = attachment.File;
                                    }
                                }
                                else
                                {
                                    // adding a new attachment
                                    var localAttachment = new BPAttachment
                                                              {
                                                                  AttachmentDate = attachment.AttachmentDate,
                                                                  BPMasterID = partnerToUpdate.Details.BPMasterID,
                                                                  FileName = attachment.FileName,
                                                                  File = attachment.File,
                                                                  Deleted = false
                                                              };

                                    entities.BPAttachments.Add(localAttachment);
                                }
                            }
                        );

                    // entities.Audits.Add(partnerToUpdate.Audit);

                    if (partnerToUpdate.LabelField != null)
                    {
                        var localField =
                            entities.BPLabelFields.FirstOrDefault(
                                x => x.BPLabelFieldID == partnerToUpdate.LabelField.BPLabelFieldID);

                        if (localField != null)
                        {
                            #region update label partnerToUpdates

                            localField.Field1 = partnerToUpdate.LabelField.Field1;
                            localField.Field2 = partnerToUpdate.LabelField.Field2;
                            localField.Field3 = partnerToUpdate.LabelField.Field3;
                            localField.Field4 = partnerToUpdate.LabelField.Field4;
                            localField.Field5 = partnerToUpdate.LabelField.Field5;
                            localField.Field6 = partnerToUpdate.LabelField.Field6;
                            localField.Field7 = partnerToUpdate.LabelField.Field7;
                            localField.Field8 = partnerToUpdate.LabelField.Field8;
                            localField.Field9 = partnerToUpdate.LabelField.Field9;
                            localField.Field10 = partnerToUpdate.LabelField.Field10;
                            localField.Field11 = partnerToUpdate.LabelField.Field11;
                            localField.Field12 = partnerToUpdate.LabelField.Field12;
                            localField.Field13 = partnerToUpdate.LabelField.Field13;
                            localField.Field14 = partnerToUpdate.LabelField.Field14;
                            localField.Field15 = partnerToUpdate.LabelField.Field15;

                            #endregion
                        }
                        else
                        {
                            entities.BPLabelFields.Add(partnerToUpdate.LabelField);
                            dbPartner.BPLabelFieldID = partnerToUpdate.LabelField.BPLabelFieldID;
                        }
                    }
                    
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("Business partner {0} successfully updated.", partnerToUpdate.Details.BPMasterID));
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }
        }

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnersToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateBusinessPartnerPricing(IList<BusinessPartner> partnersToUpdate)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to update {0} business partners", partnersToUpdate.Count));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var partnerToUpdate in partnersToUpdate)
                    {
                        var dbPartner =
                           entities.BPMasters.FirstOrDefault(
                             partner => partner.BPMasterID == partnerToUpdate.Details.BPMasterID);

                        if (dbPartner != null)
                        {
                            dbPartner.PriceListID = partnerToUpdate.Details.PriceListID;
                            this.Log.LogDebug(this.GetType(), string.Format("Business partner {0} updating.", partnerToUpdate.Details.BPMasterID));
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Business partners successfully updated.");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets a bp partner.
        /// </summary>
        /// <param name="partnerId">The partner id to search for.</param>
        /// <returns>A bp partner.</returns>
        public BPMaster GetPartner(int partnerId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BPMasters.FirstOrDefault(x => x.BPMasterID == partnerId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the inactive partners.
        /// </summary>
        /// <returns>The inactive partners.</returns>
        public IList<App_GetInactivePartners_Result> GetInactivePartners()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetInactivePartners().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="attachment">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool DeleteAttachment(BPAttachment attachment)
        {
            this.Log.LogDebug(this.GetType(), string.Format("attempting to remove attachment with id {0}", attachment.BPAttachmentID));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttachment =
                        entities.BPAttachments.FirstOrDefault(x => x.BPAttachmentID.Equals(attachment.BPAttachmentID));

                    if (dbAttachment != null)
                    {
                        entities.BPAttachments.Remove(dbAttachment);
                        entities.SaveChanges();

                        this.Log.LogDebug(this.GetType(), string.Format("Attachment with id {0} successfully removed.", attachment.BPAttachmentID));
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return false;
        }

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="partnerId">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public int GetPartnerPriceListId(int partnerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("attempting to retrieve the price list id for partner: {0}", partnerId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var partner = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == partnerId);
                    if (partner != null)
                    {
                        var id = partner.PriceListID.ToInt();
                        this.Log.LogDebug(this.GetType(), string.Format("{0} found", id));
                        return id;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return 0;
        }

        /// <summary>
        /// Method which returns the non deleted business partner groups.
        /// </summary>
        /// <returns>A collection of non deleted business partner groups.</returns>
        public IList<BPGroup> GetGroups()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the business partner groups");
            List<BPGroup> groups;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    groups = entities.BPGroups
                        .Where(partnerGroup => !partnerGroup.Deleted)
                        .OrderBy(x => x.BPGroupName)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetGroups(): {0} business partner groups successfully retrieved.", groups.Count));
            return groups;
        }

        /// <summary>
        /// Method which returns the non deleted business partner types.
        /// </summary>
        /// <returns>A collection of non deleted business partner types.</returns>
        public IList<NouBPType> GetTypes()
        {
            this.Log.LogDebug(this.GetType(), "GetTypes(): Attempting to retrieve all the business partner types");
            List<NouBPType> types;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    types = entities.NouBPTypes
                        .Where(type => !type.Deleted)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetTypes(): {0} business partner types successfully retrieved.", types.Count));
            return types;
        }

        /// <summary>
        /// Method which returns the non deleted business partner properties.
        /// </summary>
        /// <returns>A collection of non deleted business partner properties.</returns>
        public IList<BPPropertySelection> GetProperties()
        {
            this.Log.LogDebug(this.GetType(), "GetProperties(): Attempting to retrieve all the business partner properties");
            var properties = new List<BPPropertySelection>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    properties = entities.BPPropertySelections.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetTypes(): {0} business partner properties successfully retrieved.", properties.Count));
            return properties;
        }

        /// <summary>
        /// Method which returns the non deleted business partner label fields.
        /// </summary>
        /// <returns>A collection of non deleted business partner label fields.</returns>
        public IList<BPLabelField> GetLabelFields()
        {
            var labels = new List<BPLabelField>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    labels = entities.BPLabelFields.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetTypes(): {0} business partner properties successfully retrieved.", labels.Count));
            return labels;
        }
       
        /// <summary>
        /// Method which returns the non deleted business partner properties.
        /// </summary>
        /// <returns>A collection of non deleted business partner properties.</returns>
        public IList<BPProperty> GetBPProperties()
        {
            this.Log.LogDebug(this.GetType(), "GetBPProperties(): Attempting to retrieve all the business partner properties");
            List<BPProperty> properties;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    properties = entities.BPProperties.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetBPProperties(): {0} business partner properties successfully retrieved.", properties.Count));
            return properties;
        }

        /// <summary>
        /// Returns a partners on hold status.
        /// </summary>
        /// <param name="partnerID">The partner id.</param>
        /// <returns>The partners on hold status.</returns>
        public int IsPartnerLiveOrderOnSystem(int partnerID)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.BPMasterID_Customer == partnerID && (x.NouDocStatusID == 1 || x.NouDocStatusID == 4) && x.Deleted == null);
                    if (order != null)
                    {
                        return order.Number;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Returns a partners on hold status.
        /// </summary>
        /// <param name="partnerID">The partner id.</param>
        /// <returns>The partners on hold status.</returns>
        public bool GetPartnerOnHoldStatus(int partnerID)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var partner = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == partnerID && x.Deleted == null);
                    if (partner != null)
                    {
                        return partner.OnHold.ToBool();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            
            return false;
        }

        /// <summary>
        /// Method that retrieves the business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public IList<ViewBusinessPartner> GetBusinessPartners()
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartners(): Attempting to retrieve the business partners");

            IList<ViewBusinessPartner> partners = new List<ViewBusinessPartner>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localPartners = entities.ViewBusinessPartners;
                    this.Log.LogDebug(this.GetType(), string.Format("GetBusinessPartners(): {0} business partners have been successfully retrieved", 1));

                    var numberOfPartners = localPartners.Count();
                    if (numberOfPartners == 0)
                    {
                        SystemMessage.Write(MessageType.Priority, Message.ApplicationLoaded);
                    }
                    else
                    {
                        ProgressBar.SetUp(0, numberOfPartners, Message.ApplicationLoaded);
                        foreach (var partner in localPartners)
                        {
                            partners.Add(partner);
                            // ProgressBar.Run();
                        }
                        
                        ProgressBar.Reset();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return partners;
        }

        /// <summary>
        /// Gets the partner matching in input id..
        /// </summary>
        /// <returns>A matching partner.</returns>
        public BusinessPartner GetBusinessPartner(int partnerId)
        {
            var partner = new BusinessPartner();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    partner.Details = entities.ViewBusinessPartners.First(x => x.BPMasterID == partnerId);
                    partner.PartnerType =
                        entities.NouBPTypes.FirstOrDefault(x => x.NouBPTypeID == partner.Details.NouBPTypeID);
                    partner.Addresses = (from dbAddress in entities.BPAddresses
                        where !dbAddress.Deleted && dbAddress.BPMasterID == partnerId
                        select new BusinessPartnerAddress { Details = dbAddress,
                            PlantDetails = dbAddress.Plant, PlantCountry = dbAddress.Plant.Country }).ToList();
                    partner.Contacts = (from dbContact in entities.BPContacts
                        where !dbContact.Deleted && dbContact.BPMasterID == partnerId
                        orderby dbContact.LastName
                        select new BusinessPartnerContact { Details = dbContact }).ToList();
                    partner.PartnerGroup =
                        entities.BPGroups.FirstOrDefault(x => x.BPGroupID == partner.Details.BPGroupID);
                    partner.PartnerCurrency = entities.BPCurrencies.FirstOrDefault(x => x.BPCurrencyID == partner.Details.BPCurrencyID);
                    partner.LabelField = entities.BPLabelFields.FirstOrDefault(x => x.BPLabelFieldID == partner.Details.BPLabelFieldID);
                    partner.PartnerProperties = (from prop in entities.BPPropertySelections.Where(x => x.BPMasterID == partnerId)
                        select new BusinessPartnerProperty
                        {
                            BPPropertyID = prop.BPPropertyID,
                            //Details = new BPProperty { BPPropertyID = prop.BPPropertyID },
                            DisplayMessage = prop.DisplayMessage
                        }).ToList();
                    partner.Attachments = entities.BPAttachments.Where(x => x.BPMasterID == partnerId && !x.Deleted)
                        .ToList();
                }

                foreach (var businessPartnerProperty in partner.PartnerProperties)
                {
                    businessPartnerProperty.CreateProperty();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return partner;
        }

        /// <summary>
        /// Gets the partner matching in input id..
        /// </summary>
        /// <returns>A matching partner.</returns>
        public ViewBusinessPartner GetBusinessPartnerShort(int partnerId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.ViewBusinessPartners.First(x => x.BPMasterID == partnerId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Method that retrieves the last 100 business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public IList<ViewBusinessPartner> GetRecentBusinessPartners()
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartners(): Attempting to retrieve the last 40 business partners");

            IList<ViewBusinessPartner> partners = new List<ViewBusinessPartner>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localPartners = entities.ViewBusinessPartners.Where(x => x.EditDate > DateTime.Today);
                    foreach (var partner in localPartners)
                    {
                        partners.Add(partner);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return partners;
        }

        /// <summary>
        /// Method which returns the non deleted business partners contacts.
        /// </summary>
        /// <returns>A collection of non deleted business partner contacts.</returns>
        public IList<BusinessPartnerContact> GetBusinessPartnerContacts()
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartnerContacts(): Attempting to retrieve all the business partner contacts");
            List<BusinessPartnerContact> contacts;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    contacts = (from dbContact in entities.BPContacts
                               where !dbContact.Deleted
                               orderby dbContact.LastName
                               select new BusinessPartnerContact { Details = dbContact}).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetBusinessPartnerContacts(): {0} business partners successfully retrieved.", contacts.Count));
            return contacts;
        }

        /// <summary>
        /// Method which returns the non deleted business partners contacts.
        /// </summary>
        /// <returns>A collection of non deleted business partner contacts.</returns>
        public IList<BusinessPartnerContact> GetBusinessPartnerContactsById(int partnerId)
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartnerContacts(): Attempting to retrieve all the business partner contacts");
            var contacts = new List<BusinessPartnerContact>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    contacts = (from dbContact in entities.BPContacts
                        where dbContact.BPMasterID == partnerId && !dbContact.Deleted
                        orderby dbContact.LastName
                        select new BusinessPartnerContact { Details = dbContact }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetBusinessPartnerContacts(): {0} business partners successfully retrieved.", contacts.Count));
            return contacts;
        }

        /// <summary>
        /// Method that retrieves the business partners addresses.
        /// </summary>
        /// <returns>A collection of business partner addresses.</returns>
        public IList<BusinessPartnerAddress> GetBusinessPartnerAddresses()
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartnersAddresses(): Attempting to retrieve the business partner addresses");

            IList<BusinessPartnerAddress> partnerAddresses;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    partnerAddresses = (from dbAddress in entities.BPAddresses
                                       where !dbAddress.Deleted
                                       select new BusinessPartnerAddress{ Details = dbAddress, PlantDetails = dbAddress.Plant, PlantCountry = dbAddress.Plant.Country}).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("GetBusinessPartnerAddresses(): {0} business partner addresses have been successfully retrieved", partnerAddresses.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return partnerAddresses;
        }

        /// <summary>
        /// Method that retrieves the business partners attachments.
        /// </summary>
        /// <returns>A collection of business partner attachments.</returns>
        public IList<BPAttachment> GetBusinessPartnerAttachments()
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartnersAttachments(): Attempting to retrieve the business partner addresses");

            IList<BPAttachment> partnerAttachments;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    partnerAttachments = entities.BPAttachments.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("GetBusinessPartnerAttachments(): {0} business partner addresses have been successfully retrieved", partnerAttachments.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return partnerAttachments;
        }

        /// <summary>
        /// Method that retrieves the partner groups.
        /// </summary>
        /// <returns>A collection of partner groups.</returns>
        public IList<PartnerGroup> GetPartnerGroups()
        {
            this.Log.LogDebug(this.GetType(), "GetPartnerGroups(): Attempting to retrieve the business partner groups");
            var partnerGroups = new List<PartnerGroup>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    partnerGroups = (from bpGroup in entities.BPGroups
                        where !bpGroup.Deleted
                        select new PartnerGroup { PartnerId = bpGroup.BPGroupID, Name = bpGroup.BPGroupName }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("GetPartnerGroups(): {0} business partner groups have been successfully retrieved", partnerGroups.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return partnerGroups;
        }

        /// <summary>
        /// Method that retrieves the partner types.
        /// </summary>
        /// <returns>A collection of partner types.</returns>
        public IList<PartnerType> GetPartnerTypes()
        {
            this.Log.LogDebug(this.GetType(), "GetPartnerTypes(): Attempting to retrieve the business partner Types");
            var partnerTypes = new List<PartnerType>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    partnerTypes = (from NouBPType in entities.NouBPTypes
                                     where !NouBPType.Deleted
                                     select new PartnerType { PartnerTypeId = NouBPType.NouBPTypeID, Type = NouBPType.Type }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("GetPartnerTypes(): {0} business partner Types have been successfully retrieved", partnerTypes.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return partnerTypes;
        }

        /// <summary>
        /// Method that retrieves the business partners currencies.
        /// </summary>
        /// <returns>A collection of business partner currencies.</returns>
        public IList<BPCurrency> GetBusinessPartnerCurrencies()
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartnersCurrencies(): Attempting to retrieve the business partner currencies");
            var partnerCurrencies = new List<BPCurrency>();
            try
            {
                //using (var entities = new NouvemEntities())
               // {
                var entities = new NouvemEntities();
                    partnerCurrencies = entities.BPCurrencies.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("GetBusinessPartnerCurrencies): {0} business partner currencies have been successfully retrieved", partnerCurrencies.Count));
               // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return partnerCurrencies;
        }

        /// <summary>
        /// Add or updates the properties list.
        /// </summary>
        /// <param name="properties">The properties to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateProperties(IList<BPProperty> properties)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateProperties(): Attempting to update the properties");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    properties.ToList().ForEach(x =>
                    {
                        if (x.BPPropertyID == 0)
                        {
                            // new
                            var newProperty = new BPProperty
                            {
                                Name = x.Name,
                                Deleted = false
                            };

                            entities.BPProperties.Add(newProperty);
                        }
                        else
                        {
                            // update
                            var dbProperty =
                                entities.BPProperties.FirstOrDefault(
                                    property => property.BPPropertyID == x.BPPropertyID && !property.Deleted);

                            if (dbProperty != null)
                            {
                                dbProperty.Name = x.Name;
                                dbProperty.Deleted = x.Deleted;
                            }
                        }
                    });
                  
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Properties successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Add or updates the partner types list.
        /// </summary>
        /// <param name="types">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePartnerTypes(ref List<PartnerType> types)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdatePartnerTypes(): Attempting to update the partner types");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    types.ToList().ForEach(x =>
                    {
                        if (x.PartnerTypeId == 0)
                        {
                            // new
                            var newType = new NouBPType
                            {
                                Type = x.Type,
                                Deleted = false
                            };

                            entities.NouBPTypes.Add(newType);
                            entities.SaveChanges();
                            x.PartnerTypeId = newType.NouBPTypeID;
                        }
                        else
                        {
                            // update
                            var dbType =
                                entities.NouBPTypes.FirstOrDefault(
                                    type => type.NouBPTypeID == x.PartnerTypeId && !type.Deleted);

                            if (dbType != null)
                            {
                                dbType.Type = x.Type;
                                dbType.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Partner Types successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Add or updates the partner groups list.
        /// </summary>
        /// <param name="groups">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePartnerGroups(ref List<PartnerGroup> groups)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdatePartnerGroups(): Attempting to update the partner groups");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    groups.ToList().ForEach(x =>
                    {
                        if (x.PartnerId == 0)
                        {
                            // new
                            var newGroup = new BPGroup
                            {
                                BPGroupName = x.Name,
                                Deleted = false
                            };

                            entities.BPGroups.Add(newGroup);
                            entities.SaveChanges();
                            x.PartnerId = newGroup.BPGroupID;
                        }
                        else
                        {
                            // update
                            var dbGroup =
                                entities.BPGroups.FirstOrDefault(
                                    group => group.BPGroupID == x.PartnerId && !group.Deleted);

                            if (dbGroup != null)
                            {
                                dbGroup.BPGroupName = x.Name;
                                dbGroup.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Partner Groups successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Add or updates the currencies list.
        /// </summary>
        /// <param name="currencies">The currencies to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateCurrencies(IList<BPCurrency> currencies)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateCurrencies(): Attempting to update the partner currencies");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    currencies.ToList().ForEach(x =>
                    {
                        if (x.BPCurrencyID == 0)
                        {
                            // new
                            entities.BPCurrencies.Add(x);
                        }
                        else
                        {
                            // update
                            var dbCurrency =
                                entities.BPCurrencies.FirstOrDefault(
                                    currency => currency.BPCurrencyID == x.BPCurrencyID && !currency.Deleted);

                            if (dbCurrency != null)
                            {
                                dbCurrency.Name = x.Name;
                                dbCurrency.RateToBase = x.RateToBase;
                                dbCurrency.Symbol = x.Symbol;
                                dbCurrency.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Currencies successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Determines is a partner code already exists in the db.
        /// </summary>
        /// <param name="code">The code to check.</param>
        /// <returns>Flag, as to whether the code exists.</returns>
        public bool DoesPartnerCodeExist(string code)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to determine if partner code: {0} already exists", code));
            var localCode = code.ToLower().Trim();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var result = entities.BPMasters.Any(x => x.Code.ToLower().Equals(localCode));
                    this.Log.LogDebug(this.GetType(), string.Format("Code Exists:{0}", result));
                    return result;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a partner agent.
        /// </summary>
        /// <param name="partnerId">The partner to update.</param>
        /// <param name="agentId">The agent id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePartnerAgent(int partnerId, int? agentId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var partner = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == partnerId);
                    if (partner != null)
                    {
                        partner.BPMasterID_Agent = agentId;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Editors.Helpers;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer.Interface;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using User = Microsoft.VisualBasic.ApplicationServices.User;

namespace Nouvem.Model.DataLayer.Repository
{
    public class LairageRepository : NouvemRepositoryBase, ILairageRepository
    {
        #region lairage

        #region order

        /// <summary> Adds a new lairage order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag indicating a successful order add, or not.</returns>
        public bool AddLairageOrder(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = new LairageOrder
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPMasterID = sale.Customer.BPMasterID,
                        NouKillTypeID = sale.NouKillTypeID,
                        DocumentDate = sale.DocumentDate,
                        DeliveryDate = sale.DeliveryDate,
                        DeliveryTime = sale.DeliveryTime,
                        ProposedKillDate = sale.ProposedKillDate,
                        NouDocStatusID = sale.NouDocStatusID,
                        UserMasterID = sale.SalesEmployeeID,
                        Remarks = sale.Remarks,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now
                    };

                    entities.LairageOrders.Add(order);
                    entities.SaveChanges();

                    // save the details
                    var saleID = order.LairageOrderID;
                    sale.SaleID = saleID;
                    foreach (var detail in sale.SaleDetails)
                    {
                        var orderDetail = new LairageOrderDetail
                        {
                            LairageOrderID = saleID,
                            QuantityReceivedOTM = detail.QuantityReceivedOTM,
                            QuantityReceivedUTM = detail.QuantityReceivedUTM,
                            INMasterID = detail.INMasterID
                        };

                        entities.LairageOrderDetails.Add(orderDetail);
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "New lairage order successfully created");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary> Updates a lairage order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag indicating a successful order update, or not.</returns>
        public bool UpdateLairageOrder(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.LairageOrders.FirstOrDefault(x => x.LairageOrderID == sale.SaleID);
                    if (dbOrder == null)
                    {
                        return false;
                    }

                    dbOrder.BPMasterID = sale.Customer.BPMasterID;
                    dbOrder.DocumentDate = sale.DocumentDate;
                    dbOrder.DeliveryDate = sale.DeliveryDate;
                    dbOrder.DeliveryTime = sale.DeliveryTime;
                    dbOrder.NouKillTypeID = sale.NouKillTypeID;
                    dbOrder.NouDocStatusID = sale.NouDocStatusID;
                    dbOrder.ProposedKillDate = sale.ProposedKillDate;
                    dbOrder.UserMasterID = sale.SalesEmployeeID;
                    dbOrder.Remarks = sale.Remarks;
                    dbOrder.DeviceID = NouvemGlobal.DeviceId;
                    dbOrder.EditDate = DateTime.Now;

                    foreach (var detail in sale.SaleDetails)
                    {
                        var dbOrderDetail =
                            entities.LairageOrderDetails.FirstOrDefault(x => x.LairageOrderDetailID == detail.SaleDetailID);
                        if (dbOrderDetail == null)
                        {
                            // add
                            var orderDetail = new LairageOrderDetail
                            {
                                LairageOrderID = sale.SaleID,
                                QuantityReceivedOTM = detail.QuantityReceivedOTM,
                                QuantityReceivedUTM = detail.QuantityReceivedUTM,
                                INMasterID = detail.INMasterID
                            };

                            entities.LairageOrderDetails.Add(orderDetail);
                        }
                        else
                        {
                            // update
                            dbOrderDetail.QuantityReceivedOTM = detail.QuantityReceivedOTM;
                            dbOrderDetail.QuantityReceivedUTM = detail.QuantityReceivedUTM;
                            dbOrderDetail.INMasterID = detail.INMasterID;
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "lairage order successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetLairageOrderByLastEdit()
        {
            Sale apOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.LairageOrders
                 .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                 .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }

                    apOrder = new Sale
                    {
                        SaleID = order.LairageOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        DocumentDate = order.DocumentDate,
                        DeliveryTime = order.DeliveryTime,
                        DeliveryDate = order.DeliveryDate,
                        ProposedKillDate = order.ProposedKillDate,
                        NouKillTypeID = order.NouKillTypeID,
                        NouDocStatusID = order.NouDocStatusID,
                        Remarks = order.Remarks,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        BPCustomer = order.BPMaster,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.LairageOrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.LairageOrderDetailID,
                                           SaleID = detail.LairageOrderID,
                                           QuantityReceivedOTM = detail.QuantityReceivedOTM,
                                           QuantityReceivedUTM = detail.QuantityReceivedUTM,
                                           INMasterID = detail.INMasterID,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apOrder;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetLairageOrderByID(int id)
        {
            Sale apOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.LairageOrders.FirstOrDefault(x => x.LairageOrderID == id);

                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }

                    apOrder = new Sale
                    {
                        SaleID = order.LairageOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        DocumentDate = order.DocumentDate,
                        DeliveryTime = order.DeliveryTime,
                        DeliveryDate = order.DeliveryDate,
                        NouKillTypeID = order.NouKillTypeID,
                        ProposedKillDate = order.ProposedKillDate,
                        NouDocStatusID = order.NouDocStatusID,
                        Remarks = order.Remarks,
                        SaleType = ViewType.LairageOrder,
                        BPCustomer = order.BPMaster,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.LairageOrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.LairageOrderDetailID,
                                           SaleID = detail.LairageOrderID,
                                           QuantityReceivedOTM = detail.QuantityReceivedOTM,
                                           QuantityReceivedUTM = detail.QuantityReceivedUTM,
                                           INMasterID = detail.INMasterID,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apOrder;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetLairageOrderByFirstLast(bool first)
        {
            Sale apOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    LairageOrder order = null;
                    if (first)
                    {
                        order = entities.LairageOrders.FirstOrDefault();
                    }
                    else
                    {
                        order = entities.LairageOrders.OrderByDescending(x => x.LairageOrderID).FirstOrDefault();
                    }

                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }

                    apOrder = new Sale
                    {
                        SaleID = order.LairageOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        DocumentDate = order.DocumentDate,
                        DeliveryTime = order.DeliveryTime,
                        DeliveryDate = order.DeliveryDate,
                        ProposedKillDate = order.ProposedKillDate,
                        NouDocStatusID = order.NouDocStatusID,
                        Remarks = order.Remarks,
                        NouKillTypeID = order.NouKillTypeID,
                        BPCustomer = order.BPMaster,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.LairageOrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.LairageOrderDetailID,
                                           SaleID = detail.LairageOrderID,
                                           QuantityReceivedOTM = detail.QuantityReceivedOTM,
                                           QuantityReceivedUTM = detail.QuantityReceivedUTM,
                                           INMasterID = detail.INMasterID,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apOrder;
        }


        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="partnerId">The partner id to search orders for.</param>
        /// <returns>A collection of all orders for the input partner.</returns>
        public IList<Sale> GetLairageOrdersBySupplier(int partnerId)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the orders");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.LairageOrders
                              where order.BPMasterID == partnerId
                              && order.Deleted == null
                              orderby order.LairageOrderID
                              select new Sale
                              {
                                  SaleID = order.LairageOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  DocumentDate = order.DocumentDate,
                                  DeliveryTime = order.DeliveryTime,
                                  DeliveryDate = order.DeliveryDate,
                                  ProposedKillDate = order.ProposedKillDate,
                                  NouKillTypeID = order.NouKillTypeID,
                                  NouDocStatusID = order.NouDocStatusID,
                                  BPCustomer = order.BPMaster,
                                  Remarks = order.Remarks,
                                  SaleType = ViewType.LairageOrder,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Deleted = order.Deleted
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllLairageOrdersByDate(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the orders");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.LairageOrders
                              where orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              && (order.DocumentDate >= start && order.DocumentDate <= end)
                              orderby order.LairageOrderID
                              select new Sale
                              {
                                  SaleID = order.LairageOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  DocumentDate = order.DocumentDate,
                                  DeliveryTime = order.DeliveryTime,
                                  DeliveryDate = order.DeliveryDate,
                                  ProposedKillDate = order.ProposedKillDate,
                                  NouKillTypeID = order.NouKillTypeID,
                                  NouDocStatusID = order.NouDocStatusID,
                                  BPCustomer = order.BPMaster,
                                  Remarks = order.Remarks,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Deleted = order.Deleted
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the kill line kill types.
        /// </summary>
        /// <returns>The kill line kill types.</returns>
        public IList<NouKillType> GetKillTypes()
        {
            var killTypes = new List<NouKillType>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.NouKillTypes.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return killTypes;
        }

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        public IList<SupplierHerd> GetSupplierHerds(int supplierId)
        {
            var herds = new List<SupplierHerd>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.SupplierHerds.Where(x => x.BPMasterID == supplierId && x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return herds;
        }

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        public IList<SupplierHerd> GetSupplierHerds()
        {
            var herds = new List<SupplierHerd>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.SupplierHerds.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return herds;
        }

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        public IList<SupplierHerd> GetSupplierPartnerHerds(int partnerId)
        {
            var herds = new List<SupplierHerd>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.SupplierHerds.Where(x => x.BPMasterID == partnerId && x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return herds;
        }

        #endregion

        #region intake

        /// <summary> Adds a new lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddLairageIntake(Sale sale)
        {
            var localStockTransactionId = 0;
            var batchNoId = 0;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = new APGoodsReceipt
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        ProposedKillDate = sale.ProposedKillDate,
                        GoodsReceiptDate = sale.LotAuthorisedDate,
                        FileTransferDate = sale.FileTransferedDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        Remarks = sale.Remarks,
                        DeliveryDate = sale.DeliveryDate,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        HerdQAS = sale.HerdQAS,
                        Reference = sale.Reference,
                        KillType = sale.KillType,
                        Processed = false,
                        BPMasterID_Customer = sale.CustomerID,
                        DeliveryTime = sale.DeliveryTime,
                        BPMasterID_Haulier = sale.Haulier != null ? sale.Haulier.BPMasterID : (int?)null,
                        BPMasterID_Agent = sale.Agent != null ? sale.Agent.BPMasterID : (int?)null,
                        HaulageCharge = sale.HaulageCharge,
                        Notes = sale.TimeInTransit,
                        PermitNumber = sale.PermitNo
                    };

                    if (sale.Customer != null)
                    {
                        // add the customer snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Customer.BPMasterID,
                            BPMaster_Name = sale.Customer.Name,
                            BPMaster_Code = sale.Customer.Code,
                            BPMaster_UpLift = sale.Customer.Uplift,
                            BPMaster_CreditLimit = sale.Customer.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Customer.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        order.BPMasterSnapshotID_Supplier = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Supplier = snapshot.BPMasterID;
                    }

                    if (sale.DeliveryAddress != null)
                    {
                        // add the address snapshot
                        var deliverySnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress.Details.AddressLine1,
                            AddressLine2 = sale.DeliveryAddress.Details.AddressLine2,
                            AddressLine3 = sale.DeliveryAddress.Details.AddressLine3,
                            AddressLine4 = sale.DeliveryAddress.Details.AddressLine4,
                            PostCode = sale.DeliveryAddress.Details.PostCode,
                            Shipping = sale.DeliveryAddress.Details.Shipping,
                            Billing = sale.DeliveryAddress.Details.Billing,
                            PlantID = sale.DeliveryAddress.Details.PlantID
                        };

                        entities.BPAddressSnapshots.Add(deliverySnapShot);
                        entities.SaveChanges();
                        order.BPAddressSnapshotID_Delivery = deliverySnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    entities.APGoodsReceipts.Add(order);

                    // pass the newly created apgoodsreceipt id back out.
                    sale.SaleID = order.APGoodsReceiptID;

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        var baseDoc = entities.LairageOrders.FirstOrDefault(x => x.LairageOrderID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                        }
                    }

                    entities.SaveChanges();

                    // save the details
                    var saleID = order.APGoodsReceiptID;
                    var detail = sale.SaleDetails.FirstOrDefault();

                    if (detail == null)
                    {
                        return Tuple.Create(order.APGoodsReceiptID, localStockTransactionId);
                    }

                    order.NouDocStatusID = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
                    var ssnapshot = new INMasterSnapshot
                    {
                        INMasterID = detail.InventoryItem.Master.INMasterID,
                        Code = detail.InventoryItem.Master.Code,
                        Name = detail.InventoryItem.Master.Name,
                        MaxWeight = detail.InventoryItem.Master.MaxWeight,
                        MinWeight = detail.InventoryItem.Master.MinWeight,
                        NominalWeight = detail.InventoryItem.Master.NominalWeight,
                        TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                    };

                    entities.INMasterSnapshots.Add(ssnapshot);

                    var orderDetail = new APGoodsReceiptDetail
                    {
                        APGoodsReceiptID = saleID,
                        QuantityOrdered = detail.QuantityOrdered,
                        QuantityReceived = detail.KillQuantityReceived,
                        INMasterID = detail.INMasterID,
                        INMasterSnapshotID = ssnapshot.INMasterSnapshotID
                    };

                    entities.APGoodsReceiptDetails.Add(orderDetail);
                    entities.SaveChanges();

                    // pass the apgoodsreceiptdetail id back out
                    detail.SaleDetailID = orderDetail.APGoodsReceiptDetailID;
                    var transData = detail.StockTransactionDetails != null ? detail.StockTransactionDetails.LastOrDefault() : null;
                    if (transData != null)
                    {
                        if (transData.AimsData != null)
                        {
                            transData.AimsData.APGoodsReceiptID = saleID;
                            entities.AimsDatas.Add(transData.AimsData);
                        }

                        transData.Attribute.StockTransactions = null;
                        transData.Attribute.FarmMovements = null;
                        entities.Attributes.Add(transData.Attribute);
                        entities.SaveChanges();

                        var attributeId = transData.Attribute.AttributeID;
                        transData.StockTransaction.AttributeID = attributeId;
                        transData.StockTransaction.InLocation = true;
                        transData.StockTransaction.MasterTableID = detail.SaleDetailID;
                        var localTrans = this.CreateTransaction(transData.StockTransaction);
                        localTrans.Attribute = null;
                        entities.StockTransactions.Add(localTrans);
                        entities.SaveChanges();
                        localTrans.Serial = localTrans.StockTransactionID;
                        entities.SaveChanges();

                        sale.ReturnedStockTransactionId = localTrans.StockTransactionID;
                        localStockTransactionId = attributeId;

                        if (transData.Movements != null)
                        {
                            try
                            {
                                foreach (var farmMovement in transData.Movements)
                                {
                                    farmMovement.Attribute = null;
                                    farmMovement.AttributeID = attributeId;
                                    entities.FarmMovements.Add(farmMovement);
                                    entities.SaveChanges();
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }

                    return Tuple.Create(order.APGoodsReceiptID, localStockTransactionId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, 0);
        }

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        public Tuple<bool, int> UpdateLairageIntake(Sale sale)
        {
            var transId = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == sale.SaleID);
                    if (dbOrder == null)
                    {
                        return Tuple.Create(false, 0);
                    }

                    dbOrder.DocumentDate = sale.DocumentDate;
                    dbOrder.DeliveryDate = sale.DeliveryDate;
                    dbOrder.ProposedKillDate = sale.ProposedKillDate;
                    dbOrder.Remarks = sale.Remarks;
                    dbOrder.NouDocStatusID = sale.NouDocStatusID;
                    dbOrder.GoodsReceiptDate = sale.LotAuthorisedDate;
                    dbOrder.FileTransferDate = sale.FileTransferedDate;
                    dbOrder.BaseDocumentReferenceID = sale.BaseDocumentReferenceID;
                    dbOrder.DeviceID = NouvemGlobal.DeviceId;
                    dbOrder.EditDate = DateTime.Now;
                    dbOrder.BPMasterID_Supplier = sale.Customer.BPMasterID;
                    dbOrder.HerdQAS = sale.HerdQAS;
                    dbOrder.KillType = sale.KillType;
                    dbOrder.Processed = false;
                    dbOrder.BPMasterID_Customer = sale.CustomerID;
                    dbOrder.Notes = sale.TimeInTransit;
                    dbOrder.DeliveryTime = sale.DeliveryTime;
                    dbOrder.BPMasterID_Haulier = sale.Haulier != null ? sale.Haulier.BPMasterID : (int?)null;
                    dbOrder.BPMasterID_Agent = sale.Agent != null ? sale.Agent.BPMasterID : (int?)null;
                    dbOrder.HaulageCharge = sale.HaulageCharge;
                    entities.SaveChanges();

                    // save the details
                    var saleID = dbOrder.APGoodsReceiptID;
                    var detail = sale.SaleDetails.FirstOrDefault();

                    if (detail == null)
                    {
                        return Tuple.Create(true, 0);
                    }

                    if (detail.SaleDetailID == 0)
                    {
                        var ssnapshot = new INMasterSnapshot
                        {
                            INMasterID = detail.InventoryItem.Master.INMasterID,
                            Code = detail.InventoryItem.Master.Code,
                            Name = detail.InventoryItem.Master.Name,
                            MaxWeight = detail.InventoryItem.Master.MaxWeight,
                            MinWeight = detail.InventoryItem.Master.MinWeight,
                            NominalWeight = detail.InventoryItem.Master.NominalWeight,
                            TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(ssnapshot);

                        var orderDetail = new APGoodsReceiptDetail
                        {
                            APGoodsReceiptID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            QuantityReceived = detail.KillQuantityReceived,
                            INMasterID = detail.INMasterID,
                            INMasterSnapshotID = ssnapshot.INMasterSnapshotID
                        };

                        entities.APGoodsReceiptDetails.Add(orderDetail);
                        entities.SaveChanges();

                        // pass the apgoodsreceiptdetail id back out
                        detail.SaleDetailID = orderDetail.APGoodsReceiptDetailID;
                        var transData = detail.StockTransactionDetails.LastOrDefault();
                        if (transData != null)
                        {
                            if (transData.AimsData != null)
                            {
                                transData.AimsData.APGoodsReceiptID = saleID;
                                entities.AimsDatas.Add(transData.AimsData);
                            }

                            if (transData.Attribute.AttributeID > 0)
                            {
                                // we are editing the attributes
                                var dbAttribute =
                                    entities.Attributes.FirstOrDefault(
                                        x => x.AttributeID == transData.Attribute.AttributeID);
                                if (dbAttribute != null)
                                {
                                    if (!transData.Attribute.SequencedDate.HasValue)
                                    {
                                        dbAttribute.NouCategory = transData.Attribute.NouCategory;
                                        dbAttribute.Category = transData.Attribute.Category;
                                        dbAttribute.Sex = transData.Attribute.Sex;
                                    }
                       
                                    dbAttribute.Herd = transData.Attribute.Herd;
                                    dbAttribute.AgeInMonths = transData.Attribute.AgeInMonths;
                                    dbAttribute.AgeInDays = transData.Attribute.AgeInDays;
                                    dbAttribute.Breed = transData.Attribute.Breed;
                                    dbAttribute.Casualty = transData.Attribute.Casualty;
                                    dbAttribute.Clipped = transData.Attribute.Clipped;
                                    dbAttribute.SupplierID = transData.Attribute.SupplierID;
                                    dbAttribute.Lame = transData.Attribute.Lame;
                                    dbAttribute.RearedIn = transData.Attribute.RearedIn;
                                    dbAttribute.FarmAssured = transData.Attribute.FarmAssured;
                                    dbAttribute.FreezerType = transData.Attribute.FreezerType;
                                    dbAttribute.Cleanliness = transData.Attribute.Cleanliness;
                                    dbAttribute.DateOfLastMove = transData.Attribute.DateOfLastMove;
                                    dbAttribute.DOB = transData.Attribute.DOB;
                                    dbAttribute.CurrentResidency = transData.Attribute.CurrentResidency;
                                    dbAttribute.TotalResidency = transData.Attribute.TotalResidency;
                                    dbAttribute.NoOfMoves = transData.Attribute.NoOfMoves;
                                    dbAttribute.Customer = transData.Attribute.Customer;
                                    dbAttribute.TotalResidency = transData.Attribute.TotalResidency;
                                    dbAttribute.CurrentResidency = transData.Attribute.CurrentResidency;
                                    dbAttribute.PreviousResidency = transData.Attribute.PreviousResidency;
                                    dbAttribute.Eartag = transData.Attribute.Eartag;
                                    dbAttribute.Imported = transData.Attribute.Imported;
                                    dbAttribute.Generic1 = transData.Attribute.Generic1;
                                    dbAttribute.CountryOfOrigin = transData.Attribute.CountryOfOrigin;
                                    dbAttribute.HoldingNumber = transData.Attribute.HoldingNumber;
                                    dbAttribute.Deleted = transData.Attribute.Deleted;
                                    dbAttribute.Total = transData.Attribute.Total;
                                    dbAttribute.RPA = transData.Attribute.RPA;
                                    dbAttribute.Attribute210 = transData.Attribute.Attribute210;
                                    dbAttribute.Attribute120 = transData.Attribute.Attribute120;

                                    if (!transData.Attribute.SequencedDate.HasValue)
                                    {
                                        var localTrans = entities.StockTransactions.FirstOrDefault(x =>
                                            x.AttributeID == transData.Attribute.AttributeID);
                                        if (localTrans != null)
                                        {
                                            localTrans.INMasterID = transData.StockTransaction.INMasterID;
                                        }
                                    }
                                }

                                entities.SaveChanges();
                            }
                            else
                            {
                                entities.Attributes.Add(transData.Attribute);

                                transData.StockTransaction.AttributeID = transData.Attribute.AttributeID;
                                transData.StockTransaction.InLocation = true;
                                transData.StockTransaction.MasterTableID = detail.SaleDetailID;
                                entities.StockTransactions.Add(transData.StockTransaction);
                                entities.SaveChanges();
                                transData.StockTransaction.Serial = transData.StockTransaction.StockTransactionID;
                                entities.SaveChanges();
                            }

                            sale.ReturnedStockTransactionId = transData.StockTransaction.StockTransactionID;
                            transId = transData.Attribute.AttributeID;
                        }
                    }
                    else
                    {
                        var lairageDetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == detail.SaleDetailID);
                        if (lairageDetail != null)
                        {
                            lairageDetail.QuantityReceived = detail.KillQuantityReceived;

                            // pass the apgoodsreceiptdetail id back out
                            detail.SaleDetailID = lairageDetail.APGoodsReceiptDetailID;
                            var transData = detail.StockTransactionDetails.LastOrDefault();
                            if (transData != null)
                            {
                                if (transData.Attribute.AttributeID > 0)
                                {
                                    // we are editing the attributes
                                    var dbAttribute =
                                        entities.Attributes.FirstOrDefault(
                                            x => x.AttributeID == transData.Attribute.AttributeID);
                                    if (dbAttribute != null)
                                    {
                                        if (!transData.Attribute.SequencedDate.HasValue)
                                        {
                                            dbAttribute.NouCategory = transData.Attribute.NouCategory;
                                            dbAttribute.Category = transData.Attribute.Category;
                                            dbAttribute.Sex = transData.Attribute.Sex;
                                        }
                                       
                                        dbAttribute.Herd = transData.Attribute.Herd;
                                        dbAttribute.AgeInMonths = transData.Attribute.AgeInMonths;
                                        dbAttribute.AgeInDays = transData.Attribute.AgeInDays;
                                        dbAttribute.Breed = transData.Attribute.Breed;
                                        dbAttribute.SupplierID = transData.Attribute.SupplierID;
                                       
                                        dbAttribute.Casualty = transData.Attribute.Casualty;
                                        dbAttribute.Clipped = transData.Attribute.Clipped;
                                        dbAttribute.Lame = transData.Attribute.Lame;
                                        dbAttribute.RearedIn = transData.Attribute.RearedIn;
                                        dbAttribute.DateOfLastMove = transData.Attribute.DateOfLastMove;
                                        dbAttribute.FarmAssured = transData.Attribute.FarmAssured;
                                        dbAttribute.FreezerType = transData.Attribute.FreezerType;
                                        dbAttribute.Cleanliness = transData.Attribute.Cleanliness;
                                        dbAttribute.DOB = transData.Attribute.DOB;
                                        dbAttribute.CurrentResidency = transData.Attribute.CurrentResidency;
                                        dbAttribute.TotalResidency = transData.Attribute.TotalResidency;
                                        dbAttribute.NoOfMoves = transData.Attribute.NoOfMoves;
                                        dbAttribute.Customer = transData.Attribute.Customer;
                                        dbAttribute.TotalResidency = transData.Attribute.TotalResidency;
                                        dbAttribute.CurrentResidency = transData.Attribute.CurrentResidency;
                                        dbAttribute.PreviousResidency = transData.Attribute.PreviousResidency;
                                  
                                        dbAttribute.Eartag = transData.Attribute.Eartag;
                                        dbAttribute.Imported = transData.Attribute.Imported;
                                        dbAttribute.Generic1 = transData.Attribute.Generic1;
                                        dbAttribute.Customer = transData.Attribute.Customer;
                                        dbAttribute.CountryOfOrigin = transData.Attribute.CountryOfOrigin;
                                        dbAttribute.HoldingNumber = transData.Attribute.HoldingNumber;
                                        dbAttribute.Total = transData.Attribute.Total;
                                        dbAttribute.Deleted = transData.Attribute.Deleted;
                                        dbAttribute.RPA = transData.Attribute.RPA;
                                        dbAttribute.Attribute210 = transData.Attribute.Attribute210;
                                        dbAttribute.Attribute120 = transData.Attribute.Attribute120;
                                        dbAttribute.Attribute220 = transData.Attribute.Attribute220;

                                        if (!transData.Attribute.SequencedDate.HasValue)
                                        {
                                            var localTrans = entities.StockTransactions.FirstOrDefault(x =>
                                                x.AttributeID == transData.Attribute.AttributeID);
                                            if (localTrans != null)
                                            {
                                                localTrans.INMasterID = transData.StockTransaction.INMasterID;
                                            }
                                        }
                                    }

                                    entities.SaveChanges();
                                }
                                else
                                {
                                    transData.Attribute.StockTransactions = null;
                                    transData.Attribute.FarmMovements = null;
                                    entities.Attributes.Add(transData.Attribute);
                                    entities.SaveChanges();

                                    var attributeId = transData.Attribute.AttributeID;
                                    transData.StockTransaction.AttributeID = attributeId;

                                    transData.StockTransaction.MasterTableID = detail.SaleDetailID;
                                    transData.StockTransaction.InLocation = true;
                                    var localTrans = this.CreateTransaction(transData.StockTransaction);
                                    localTrans.Attribute = null;
                                    entities.StockTransactions.Add(localTrans);
                                    entities.SaveChanges();
                                    localTrans.Serial = localTrans.StockTransactionID;
                                    entities.SaveChanges();

                                    transData.StockTransaction.StockTransactionID = localTrans.StockTransactionID;
                                    sale.ReturnedStockTransactionId = localTrans.StockTransactionID;
                                    transId = attributeId;

                                    if (transData.Movements != null)
                                    {
                                        try
                                        {
                                            foreach (var farmMovement in transData.Movements)
                                            {
                                                farmMovement.AttributeID = attributeId;
                                                farmMovement.Attribute = null;
                                                entities.FarmMovements.Add(farmMovement);
                                                entities.SaveChanges();
                                            }
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return Tuple.Create(true, transId); 
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(false, transId);
        }

        /// <summary>
        /// Removes a lairage intake item.
        /// </summary>
        /// <param name="attributeId">The attribute id</param>
        /// <returns>Flag, indicating a successful removal or not.</returns>
        public DateTime? GetAnimalSequenceDate(int attributeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == attributeId);
                    if (attribute != null)
                    {
                        return attribute.SequencedDate;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return null;
        }

        /// <summary>
        /// Adds aims data.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public bool AddAIMsData(AimsData data)
        {
            try
            {
                if (data.RequestType != null && data.RequestType == "AD" && data.Sex == null)
                {
                    return true;
                }

                using (var entities = new NouvemEntities())
                {
                    entities.AimsDatas.Add(data);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        public bool UpdateLairageIntakeSupplier(Sale sale, int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == sale.SaleID);
                    if (dbOrder == null)
                    {
                        return false;
                    }

                    dbOrder.DeliveryDate = sale.DeliveryDate;
                    dbOrder.DocumentDate = sale.DocumentDate;
                    dbOrder.Notes = sale.TimeInTransit;
                    dbOrder.ProposedKillDate = sale.ProposedKillDate;
                    dbOrder.GoodsReceiptDate = sale.LotAuthorisedDate;
                    dbOrder.FileTransferDate = sale.FileTransferedDate;
                    dbOrder.DeviceID = NouvemGlobal.DeviceId;
                    dbOrder.EditDate = DateTime.Now;
                    dbOrder.BPMasterID_Supplier = id;
                    dbOrder.BPMasterID_Haulier = sale.Haulier != null ? sale.Haulier.BPMasterID : (int?)null;
                    dbOrder.BPMasterID_Agent = sale.Agent != null ? sale.Agent.BPMasterID : (int?)null;
                    dbOrder.HaulageCharge = sale.HaulageCharge;
                    dbOrder.DeliveryTime = sale.DeliveryTime;
                    dbOrder.PermitNumber = sale.PermitNo;
                    dbOrder.BPMasterID_Customer = sale.CustomerID;

                    var stock = dbOrder.APGoodsReceiptDetails.FirstOrDefault();
                    if (stock != null)
                    {
                        var trans = entities.StockTransactions.Where(x =>
                            x.MasterTableID == stock.APGoodsReceiptDetailID && x.NouTransactionTypeID ==
                            NouvemGlobal.TransactionTypeGoodsReceiptId && x.Deleted == null);

                        foreach (var tran in trans)
                        {
                            if (tran.AttributeID != null)
                            {
                                var attribute =
                                    entities.Attributes.FirstOrDefault(x => x.AttributeID == tran.AttributeID);
                                if (attribute != null)
                                {
                                    attribute.Attribute200 = sale.Attribute;
                                    attribute.Generic1 = sale.Attribute2;
                                    //if (ApplicationSettings.ApplySelectedCustomerToAllLairageAnimals && sale.CustomerID > 0)
                                    //{
                                    //    attribute.Customer = sale.CustomerID;
                                    //}
                                }
                            }
                        }
                    }

                    entities.App_UpdateIntakeSupplierId(id);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes a lairage intake item.
        /// </summary>
        /// <param name="attributeId">The attribute id</param>
        /// <returns>Flag, indicating a successful removal or not.</returns>
        public string RemoveLairageIntakeItem(int attributeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == attributeId);
                    if (attribute != null)
                    {
                        if (attribute.SequencedDate.HasValue)
                        {
                            return string.Format(Message.AnimalAlreadySequenced, attribute.SequencedDate);
                        }

                        attribute.Deleted = DateTime.Now;
                        var stock = entities.StockTransactions.FirstOrDefault(x => x.AttributeID == attributeId);
                        if (stock != null)
                        {
                            stock.Deleted = DateTime.Now;
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return ex.Message;
            }

            return string.Empty;
        }

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        public bool UpdateLairageIntakeStatus(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == sale.SaleID);
                    if (dbOrder == null)
                    {
                        return false;
                    }
         
                    dbOrder.NouDocStatusID = sale.NouDocStatusID;
                    dbOrder.EditDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(sale.OtherReference))
                    {
                        dbOrder.MoveResponse = sale.OtherReference;
                    }

                    if (!string.IsNullOrEmpty(sale.MoveToLairageReference))
                    {
                        dbOrder.MoveToLairageReference = sale.MoveToLairageReference;
                    }

                    dbOrder.PermitNumber = sale.PermitNo;
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the lairage animal pricing/rpa.
        /// </summary>
        /// <param name="details">The details to update.</param>
        /// <returns>A flag, as to whether the updates were succcessful.</returns>
        public bool UpdateLairagePrices(IList<StockDetail> details)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        var dbAttribute =
                            entities.Attributes.FirstOrDefault(x => x.AttributeID == stockDetail.AttributeID);
                        if (dbAttribute != null)
                        {
                            dbAttribute.Price = stockDetail.UnitPrice;
                            dbAttribute.Total = stockDetail.TotalExclVat;
                            dbAttribute.RPA = stockDetail.RPA;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Logs the aims data.
        /// </summary>
        /// <param name="data">The data to log.</param>
        /// <returns>Flag, as to successful logging or not.</returns>
        public bool LogAimsData(AimsData data)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.AimsDatas.Add(data);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the lairage animal pricing/rpa.
        /// </summary>
        /// <param name="details">The details to update.</param>
        /// <returns>A flag, as to whether the updates were succcessful.</returns>
        public bool UpdateKillAttributes(IList<StockDetail> details)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        var dbAttribute =
                            entities.Attributes.FirstOrDefault(x => x.AttributeID == stockDetail.AttributeID);
                        if (dbAttribute != null)
                        {
                            this.Log.LogError(this.GetType(), string.Format("UpdateKillAttributes(): Eartag:{0}, Rpa:{1},Destination:{2}", stockDetail.Eartag, stockDetail.RPA, stockDetail.Generic2));
                            dbAttribute.RPA = stockDetail.RPA;
                            dbAttribute.Generic2 = stockDetail.Generic2;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the qa status of the input batch.
        /// </summary>
        /// <param name="details">The input batch of animals.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateFarmAssured(IList<StockDetail> details)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        var dbAttribute =
                            entities.Attributes.FirstOrDefault(x => x.AttributeID == stockDetail.AttributeID);
                        if (dbAttribute != null)
                        {
                            dbAttribute.FarmAssured = stockDetail.FarmAssured;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a lairage lot queue no.
        /// </summary>
        /// <param name="lots">The lots to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateLotQueue(IList<Sale> lots)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var lot in lots)
                    {
                        var intake = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == lot.SaleID);
                        if (intake != null)
                        {
                            intake.ReferenceID = lot.ReferenceID;
                            entities.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllLairageIntakes(IList<int?> orderStatuses, DateTime start, DateTime end, string searchMode)
        {
            this.Log.LogDebug(this.GetType(), "GetAllLairageIntakes(): Attempting to get all the goods received");
            var orders = new List<Sale>();
            var localOrders = new List<Sale>();
            var usingProposedKillDate = searchMode == Strings.ProposedKilldate;

            var localStatuses = orderStatuses;
            var isFilled = false;
            if (orderStatuses.Count == 1 && orderStatuses.First() == NouvemGlobal.NouDocStatusFilled.NouDocStatusID)
            {
                isFilled = true;
                localStatuses = new List<int?> { NouvemGlobal.NouDocStatusActive.NouDocStatusID, NouvemGlobal.NouDocStatusInProgress.NouDocStatusID, NouvemGlobal.NouDocStatusMoved.NouDocStatusID };
            }

            try
            {
                IList<App_GetAllLairageIntakeData_Result> dborders;
                using (var entities = new NouvemEntities())
                {
                    dborders = entities.App_GetAllLairageIntakeData(start, end, usingProposedKillDate).ToList();
                }

                orders = (from order in dborders
                          where order.Deleted == null
                          && localStatuses.Contains(order.NouDocStatusID)

                          orderby order.CreationDate
                          select new Sale
                          {
                              SaleID = order.APGoodsReceiptID,
                              DocumentNumberingID = order.DocumentNumberingID,
                              BPMasterID = order.BPMasterID_Supplier,
                              Number = order.Number,
                              DocumentDate = order.DocumentDate,
                              CreationDate = order.CreationDate,
                              ProposedKillDate = order.ProposedKillDate,
                              KillType = order.KillType,
                              SaleType = ViewType.LairageIntake,
                              DeviceId = order.DeviceID,
                              OtherReference = order.MoveResponse,
                              EditDate = order.EditDate,
                              HerdQAS = order.HerdQAS,
                              NouDocStatusID = order.NouDocStatusID,
                              Remarks = order.Remarks,
                              DeliveryDate = order.DeliveryDate,
                              SupplierId = order.BPMasterID_Supplier,
                              BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                              SalesEmployeeID = order.UserMasterID,
                              GradingDate = order.GradingDate,
                              CarcassesKilled = order.CarcassesKilled ?? 0,
                              CarcassesNotKilled = order.CarcassesNotKilled ?? 0,
                              CarcassesOrdered = order.CarcassesOrdered ?? 0,
                              CarcassesSequenced = order.CarcassesSequenced ?? 0,
                              UTM = order.UTM ?? 0,
                              QASAnimals = order.FarmAssured ?? 0,
                              ReferenceID = order.ReferenceID,
                              PermitNo = order.PermitNumber,
                              CustomerID = order.BPMasterID_Customer,
                              Deleted = order.Deleted,
                          }).ToList();

                foreach (var order in orders)
                {
                    if (order.CarcassesNotKilled == 0 && order.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                    {
                        order.NouDocStatusID = NouvemGlobal.NouDocStatusFilled.NouDocStatusID;
                    }

                    var supplier =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == order.SupplierId);
                    if (supplier != null)
                    {
                        order.Customer = supplier.Details;
                    }
                    else
                    {
                        var localPartner = BusinessLogic.DataManager.Instance.GetBusinessPartner(order.BPMasterID.ToInt());
                        if (localPartner != null)
                        {
                            order.Customer = localPartner.Details;
                            NouvemGlobal.AddNewPartner(localPartner);
                        }
                    }
                }

                if (isFilled)
                {
                    localOrders =
                        orders.Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusFilled.NouDocStatusID)
                            .ToList();
                }
                else
                {
                    localOrders = orders;
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localOrders;
        }

        /// <summary> Adds a new lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public int MoveLairageIntake(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == sale.SaleID);
                    if (dbOrder == null)
                    {
                        return 0;
                    }

                    var order = new APGoodsReceipt
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        ProposedKillDate = sale.ProposedKillDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        Remarks = sale.Remarks,
                        DeliveryDate = sale.DeliveryDate,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        HerdQAS = sale.HerdQAS,
                        Reference = sale.Reference,
                        KillType = dbOrder.KillType,
                        Processed = false,
                        BPMasterID_Haulier = dbOrder.BPMasterID_Haulier,
                        BPMasterID_Agent = dbOrder.BPMasterID_Agent,
                        HaulageCharge = dbOrder.HaulageCharge,
                        Notes = sale.TimeInTransit,
                        BPMasterID_Supplier = dbOrder.BPMasterID_Supplier,
                        BPMasterSnapshotID_Supplier = dbOrder.BPMasterSnapshotID_Supplier,
                        BPAddressSnapshotID_Delivery = dbOrder.BPAddressSnapshotID_Delivery,
                        BPAddressID_Delivery = dbOrder.BPAddressID_Delivery,
                        BPAddressID_Invoice = dbOrder.BPAddressID_Invoice
                    };

                    entities.APGoodsReceipts.Add(order);

                    // pass the newly created apgoodsreceipt id back out.
                    sale.SaleID = order.APGoodsReceiptID;
                    entities.SaveChanges();

                    // save the details
                    var saleID = order.APGoodsReceiptID;
                    var detail = sale.SaleDetails.FirstOrDefault();

                    if (detail == null)
                    {
                        return order.APGoodsReceiptID;
                    }

                    order.NouDocStatusID = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
                    var ssnapshot = new INMasterSnapshot
                    {
                        INMasterID = detail.InventoryItem.Master.INMasterID,
                        Code = detail.InventoryItem.Master.Code,
                        Name = detail.InventoryItem.Master.Name,
                        MaxWeight = detail.InventoryItem.Master.MaxWeight,
                        MinWeight = detail.InventoryItem.Master.MinWeight,
                        NominalWeight = detail.InventoryItem.Master.NominalWeight,
                        TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                    };

                    entities.INMasterSnapshots.Add(ssnapshot);

                    var orderDetail = new APGoodsReceiptDetail
                    {
                        APGoodsReceiptID = saleID,
                        QuantityOrdered = detail.QuantityOrdered,
                        QuantityReceived = detail.KillQuantityReceived,
                        INMasterID = detail.INMasterID,
                        INMasterSnapshotID = ssnapshot.INMasterSnapshotID
                    };

                    entities.APGoodsReceiptDetails.Add(orderDetail);
                    entities.SaveChanges();

                    // pass the apgoodsreceiptdetail id back out
                    detail.SaleDetailID = orderDetail.APGoodsReceiptDetailID;

                    foreach (var saleStockDetail in sale.StockDetails)
                    {
                        ProgressBar.Run();
                        var stock = entities.StockTransactions.FirstOrDefault(x =>
                            x.StockTransactionID == saleStockDetail.StockTransactionID);
                        if (stock != null)
                        {
                            var dbAttribute =
                                entities.Attributes.FirstOrDefault(x => x.AttributeID == stock.AttributeID);

                            if (dbAttribute != null && !dbAttribute.SequencedDate.HasValue)
                            {
                                stock.Deleted = DateTime.Now;
                                var newStock = this.CreateTransaction(stock);
                                newStock.MasterTableID = detail.SaleDetailID;
                                newStock.TransactionDate = DateTime.Now;
                                newStock.Deleted = null;
                                entities.StockTransactions.Add(newStock);
                                entities.SaveChanges();
                                newStock.Serial = newStock.StockTransactionID;
                                entities.SaveChanges();
                            }
                        }
                    }

                    return order.APGoodsReceiptID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input id search term.
        /// </summary>
        /// <param name="apReceiptId">The order serch id.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetLairageIntakeById(int apReceiptId, bool includeMovements = true)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetLairageIntakeById(): Attempting to get the order receipts for search term:{0}", apReceiptId));
            var apReceipt = new Sale();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == apReceiptId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receipt Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPHaulier = order.BPMasterHaulier,
                        HaulageCharge = order.HaulageCharge,
                        BPAgent = order.BPMaster2,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        LotAuthorisedDate = order.GoodsReceiptDate,
                        FileTransferedDate = order.FileTransferDate,
                        SaleType = ViewType.LairageIntake,
                        HerdQAS = order.HerdQAS,
                        DeliveryTime = order.DeliveryTime,
                        OtherReference = order.MoveResponse,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        NouDocStatusID = order.NouDocStatusID,
                        TimeInTransit = order.Notes,
                        Remarks = order.Remarks,
                        CustomerID = order.BPMasterID_Customer,
                        DeliveryDate = order.DeliveryDate,
                        ProposedKillDate = order.ProposedKillDate,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SalesEmployeeID = order.UserMasterID,
                        BPCustomer = order.BPMasterSupplier,
                        PermitNo = order.PermitNumber,
                        MoveToLairageReference = order.MoveToLairageReference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.APGoodsReceiptDetails
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           KillQuantityReceived = detail.QuantityReceived.HasValue ? (int)detail.QuantityReceived : 0,
                                           SaleDetailType = ViewType.LairageIntake,
                                           INMasterID = detail.INMasterID,
                                           StockDetails = (from stock in entities.StockTransactions.Where(x => x.MasterTableID == detail.APGoodsReceiptDetailID
                                               && x.Deleted == null && x.IsBox == true)
                                                           join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                                           join localKillPaymentItem in entities.KillPaymentItems.Where(x => x.Deleted == null)
                                                           on stock.StockTransactionID equals localKillPaymentItem.StockTransactionID into killPaymentItems
                                                           from killPaymentItem in killPaymentItems.DefaultIfEmpty()
                                                           select new StockDetail
                                                           {
                                                               LoadingStockDetail = true,
                                                               StockTransactionID = stock.StockTransactionID,
                                                               AttributeID = attribute.AttributeID,
                                                               KillType = attribute.KillType,
                                                               Eartag = attribute.Eartag,
                                                               Breed = attribute.NouBreed,
                                                               HerdNo = attribute.Herd,
                                                               DOB = attribute.DOB,
                                                               Attribute238 = attribute.Attribute238,
                                                               SequencedDate = attribute.SequencedDate,
                                                               CarcassNumber = attribute.CarcassNumber,
                                                               GradingDate = attribute.GradingDate,
                                                               Generic1 = attribute.Generic1,
                                                               Identigen = attribute.Identigen,
                                                               HoldingNumber = attribute.HoldingNumber,
                                                               AgeInMonths = attribute.AgeInMonths,
                                                               AgeInDays = attribute.AgeInDays,
                                                               Cleanliness = attribute.NouCleanliness,
                                                               Category = attribute.NouCategory,
                                                               ThirdParty = attribute.Attribute199,
                                                               Sex = attribute.Sex,
                                                               RearedIn = attribute.RearedIn,
                                                               CustomerID = attribute.Customer,
                                                               FarmAssured = attribute.FarmAssured,
                                                               FreezerType = attribute.FreezerType,
                                                               Clipped = attribute.Clipped,
                                                               Casualty = attribute.Casualty,
                                                               Lame = attribute.Lame,
                                                               Attribute180 = attribute.Attribute180,
                                                               Attribute220 = attribute.Attribute220,
                                                               NumberOfMoves = attribute.NoOfMoves,
                                                               PreviousResidency = attribute.PreviousResidency,
                                                               TotalResidency = attribute.TotalResidency,
                                                               CurrentResidency = attribute.CurrentResidency,
                                                               DateOfLastMove = attribute.DateOfLastMove,
                                                               CountryOfOrigin = attribute.CountryOfOrigin,
                                                               Imported = attribute.Imported,
                                                               Posted = killPaymentItem != null ? killPaymentItem.Posted : null,
                                                               Exported = killPaymentItem != null ? killPaymentItem.Export : null,
                                                               UnitPrice = killPaymentItem != null ? killPaymentItem.UnitPrice : attribute.Price,
                                                               TotalExclVat = killPaymentItem != null ? killPaymentItem.TotalExclVAT : attribute.Total,
                                                               TotalIncVat = killPaymentItem != null ? killPaymentItem.TotalIncVAT : null,
                                                               TransactionWeight = stock.TransactionWeight,
                                                               WeightSide1 = attribute.Side1Weight ?? 0,
                                                               WeightSide2 = attribute.Side2Weight ?? 0,
                                                               Grade = attribute.Grade,
                                                               Attribute200 = attribute.Attribute200,
                                                               Attribute210 = attribute.Attribute210,
                                                               Attribute120 = attribute.Attribute120,
                                                               RPA = attribute.RPA ?? false,
                                                               PaidWeight = killPaymentItem != null ? killPaymentItem.PaidWeight : null,
                                                               UsingStockPrice = killPaymentItem.TotalExclVAT == null && attribute.Total != null,
                                                               //Movements = includeMovements && attribute.FarmMovements != null ? attribute.FarmMovements.ToList() : null
                                                           })
                                                           .ToList()
                                       }).ToList(),
                    };

                    if (includeMovements)
                    {
                        var localDetail = apReceipt.SaleDetails.FirstOrDefault();
                        if (localDetail != null)
                        {
                            var details = localDetail.StockDetails;
                            if (details != null)
                            {
                                foreach (var stockDetail in details)
                                {
                                    var moves =
                                        entities.FarmMovements.Where(
                                            x => x.AttributeID == stockDetail.AttributeID && x.Deleted == null);
                                    if (moves.Any())
                                    {
                                        stockDetail.Movements = moves.ToList();
                                    }

                                    stockDetail.AimsData =
                                        entities.AimsDatas.FirstOrDefault(x => x.Eartag == stockDetail.Eartag);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apReceipt;
        }

        /// <summary>
        /// Gets the lairage animal details.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetLairageAnimalDetails(DateTime start, DateTime end)
        {
            var carcasses = new List<StockDetail>();
            var details = new List<StockDetail>();
            //!x.KillPaymentItems.All(pay => pay.Deleted == null)
            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                            entities.StockTransactions.Where(x => !x.KillPaymentItems.Any(pay => pay.Deleted == null)
                                                                  && x.Deleted == null && x.IsBox == true 
                                                                  && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                  && DbFunctions.TruncateTime(x.TransactionDate) >= start &&
                                                                  DbFunctions.TruncateTime(x.TransactionDate) <= end)
                            join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                            select new StockDetail
                            {
                                 LoadingStockDetail = true,
                                 Paid = false,
                                 StockTransactionID = stock.StockTransactionID,
                                 APGoodsReceiptDetailID = stock.MasterTableID,
                                 AttributeID = attribute.AttributeID,
                                 Label1 = attribute.Label1,
                                 Label2 = attribute.Label2,
                                 TransactionWeight = stock.TransactionWeight,
                                 KillType = attribute.KillType,
                                 TransactionDate = stock.TransactionDate,
                                 GradingDate = attribute.GradingDate,
                                 Detained = attribute.Detained,
                                 Condemned = attribute.Condemned,
                                 Eartag = attribute.Eartag,
                                 Breed = attribute.NouBreed,
                                 Grade = attribute.Grade,
                                 DOB = attribute.DOB,
                                ThirdParty = attribute.Attribute199,
                                HoldingNumber = attribute.HoldingNumber,
                                 Generic1 = attribute.Generic1,
                                 Generic2 = attribute.Generic2,
                                 Identigen = attribute.Identigen,
                                 KillNumber = attribute.KillNumber,
                                 CarcassNumber = attribute.CarcassNumber,
                                 CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                                 AgeInMonths = attribute.AgeInMonths,
                                 AgeInDays = attribute.AgeInDays,
                                 Cleanliness = attribute.NouCleanliness,
                                 SequencedDate = attribute.SequencedDate,
                                 Category = attribute.NouCategory,
                                 Sex = attribute.Sex,
                                 CustomerID = attribute.Customer,
                                 FarmAssured = attribute.FarmAssured,
                                 FreezerType = attribute.FreezerType,
                                 Clipped = attribute.Clipped,
                                 Casualty = attribute.Casualty,
                                 Lame = attribute.Lame,
                                 IsFullCarcass = stock.IsBox,
                                 NumberOfMoves = attribute.NoOfMoves,
                                 PreviousResidency = attribute.PreviousResidency,
                                 TotalResidency = attribute.TotalResidency,
                                 CurrentResidency = attribute.CurrentResidency,
                                 DateOfLastMove = attribute.DateOfLastMove,
                                 Imported = attribute.Imported,
                                 PaidWeight = attribute.ColdWeight,
                                 CountryOfOrigin = attribute.CountryOfOrigin,
                                 Killed = attribute.GradingDate != null ? 1 : 0,
                                 UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                                 OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                                 SupplierID = intake.BPMasterID_Supplier
                            })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllLairageAnimalDetails(DateTime start, DateTime end)
        {
            var carcasses = new List<StockDetail>();
            var details = new List<StockDetail>();
     
            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null && x.IsBox == true 
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                   && DbFunctions.TruncateTime(x.TransactionDate) >= start &&
                                                                   DbFunctions.TruncateTime(x.TransactionDate) <= end)
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             TransactionDate = stock.TransactionDate,
                             Label1 = attribute.Label1,
                             Label2 = attribute.Label2,
                             GradingDate = attribute.GradingDate,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             KillType = attribute.KillType,
                             Grade = attribute.Grade,
                             ThirdParty = attribute.Attribute199,
                             Identigen = attribute.Identigen,
                             DOB = attribute.DOB,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             FreezerType = attribute.FreezerType,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             Killed = attribute.GradingDate != null ? 1 : 0,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the RPA carcasses to be sent.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetRPAAnimalDetails(DateTime start, DateTime end)
        {
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null && x.IsBox == true
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId)
                         join attribute in entities.Attributes.Where(x => x.Deleted == null && DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                   DbFunctions.TruncateTime(x.GradingDate) <= end && (x.RPA == null || x.RPA == false)) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         join localKillPaymentItem in entities.KillPaymentItems.Where(x => x.Deleted == null)
                                                          on stock.StockTransactionID equals localKillPaymentItem.StockTransactionID into killPaymentItems
                         from killPaymentItem in killPaymentItems.DefaultIfEmpty()
                         join localKillPayment in entities.KillPayments
                                                          on killPaymentItem.KillPaymentID equals localKillPayment.KillPaymentID into killPayments
                         from killPayment in killPayments.DefaultIfEmpty()
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Number = intake.Number,
                             PaymentNumber = killPayment.Number,
                             NouDocStatusID = killPayment.NouDocStatusID,
                             Paid = killPaymentItem.UnitPrice.HasValue &&
                             (killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                             || killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID),

                             PaidCount = killPaymentItem.UnitPrice.HasValue &&
                             (killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                             || killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID) ? 1 : 0,

                             UnpaidCount = !killPaymentItem.UnitPrice.HasValue ||
                             (killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                             || killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                             || killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID) ? 1 : 0,
                             RPA = attribute.RPA ?? false,
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             KillType = attribute.KillType,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             TransactionDate = stock.TransactionDate,
                             GradingDate = attribute.GradingDate,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             WeightSide1 = attribute.Side1Weight ?? 0,
                             WeightSide2 = attribute.Side2Weight ?? 0,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             Clipped = attribute.Clipped,
                             FreezerType = attribute.FreezerType,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the RPA carcasses to be sent.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllRPAAnimalDetails(DateTime start, DateTime end)
        {
            var details = new List<StockDetail>();
           
            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null && x.IsBox == true
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId)
                         join attribute in entities.Attributes.Where(x => x.Deleted == null && DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                   DbFunctions.TruncateTime(x.GradingDate) <= end) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         join localKillPaymentItem in entities.KillPaymentItems.Where(x => x.Deleted == null)
                                                          on stock.StockTransactionID equals localKillPaymentItem.StockTransactionID into killPaymentItems
                         from killPaymentItem in killPaymentItems.DefaultIfEmpty()
                         join localKillPayment in entities.KillPayments
                                                          on killPaymentItem.KillPaymentID equals localKillPayment.KillPaymentID into killPayments
                         from killPayment in killPayments.DefaultIfEmpty()
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Number = intake.Number,
                             PaymentNumber = killPayment.Number,
                             NouDocStatusID = killPayment.NouDocStatusID,
                             Paid = killPaymentItem.UnitPrice.HasValue && 
                             (killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID 
                             ||killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID) ,

                             PaidCount = killPaymentItem.UnitPrice.HasValue &&
                             (killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                             || killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID) ? 1 : 0,

                             UnpaidCount = !killPaymentItem.UnitPrice.HasValue ||
                             (killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                             || killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                             || killPayment.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID) ? 1 : 0,

                             RPA = attribute.RPA ?? false,
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             KillType = attribute.KillType,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             TransactionDate = stock.TransactionDate,
                             GradingDate = attribute.GradingDate,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             WeightSide1 = attribute.Side1Weight ?? 0,
                             WeightSide2 = attribute.Side2Weight ?? 0,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             FreezerType = attribute.FreezerType,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledLairageAnimalDetails(DateTime start, DateTime end, string searchType)
        {
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null && x.IsBox == true
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId)
                         join attribute in entities.Attributes.Where(x => searchType == Strings.KillDate ? DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                   DbFunctions.TruncateTime(x.GradingDate) <= end : true) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts.Where(x => searchType == Strings.ProposedKilldate ? DbFunctions.TruncateTime(x.ProposedKillDate) >= start &&
                                                                   DbFunctions.TruncateTime(x.ProposedKillDate) <= end : true) on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID

                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             Detained = attribute.Detained,
                             Label1 = attribute.Label1,
                             Label2 = attribute.Label2,
                             Condemned = attribute.Condemned,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             TransactionDate = stock.TransactionDate,
                             GradingDate = attribute.GradingDate,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             KillType = attribute.KillType,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             Attribute220 = attribute.Attribute220,
                             CarcassNumber = attribute.CarcassNumber,
                             WeightSide1 = attribute.Side1Weight ?? 0,
                             WeightSide2 = attribute.Side2Weight ?? 0,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             FreezerType = attribute.FreezerType,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             Killed = attribute.GradingDate != null ? 1 : 0,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the killed carcass side details.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllCarcassSides(DateTime start, DateTime end, string searchType)
        {
            //return this.GetAllTestCarcassSides();

            var details = new List<StockDetail>();
            end = end.AddDays(1);

            try
            {
                IList<CarcassSideData> data;
                using (var entities = new NouvemEntities())
                {
                    data =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null && x.IsBox == null
                                                                   &&
                                                                   x.NouTransactionTypeID ==
                                                                   NouvemGlobal.TransactionTypeGoodsReceiptId)
                         join attribute in
                             entities.Attributes.Where(
                                 x =>
                                     searchType == Strings.KillDate
                                         ? x.GradingDate >= start &&
                                           x.GradingDate < end
                                         : true) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals
                             intakeDetail.APGoodsReceiptDetailID
                         join intake in
                             entities.APGoodsReceipts.Where(
                                 x =>
                                     searchType == Strings.ProposedKilldate
                                         ? x.ProposedKillDate >= start &&
                                           x.ProposedKillDate < end
                                         : true) on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         select new CarcassSideData
                         {
                             StocktransactionId = stock.StockTransactionID,
                             MasterTableId = stock.MasterTableID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             TransactionDate = stock.TransactionDate,
                             IsBox = stock.IsBox,
                             Attribute = attribute,
                             SupplierId = intake.BPMasterID_Supplier,
                             Category = attribute.NouCategory,
                             Breed = attribute.NouBreed
                         }).ToList();
                    //select new {StockTransactionID = stock.StockTransactionID, 
                    //    MasterTableID = stock.MasterTableID,
                    //    TransactionWeight = stock.TransactionWeight,
                    //    Serial = stock.Serial,
                    //    TransactionDate = stock.TransactionDate,
                    //    IsBox = stock.IsBox,
                    //    Attribute = attribute,
                    //    BPMasterID_Supplier = intake.BPMasterID_Supplier
                    //}).ToList();
                }

                foreach (var item in data)
                {
                    details.Add(new StockDetail
                    {
                        LoadingStockDetail = true,
                        StockTransactionID = item.StocktransactionId,
                        APGoodsReceiptDetailID = item.MasterTableId,
                        AttributeID = item.Attribute.AttributeID,
                        TransactionWeight = item.TransactionWeight,
                        Serial = item.Serial,
                        Detained = item.Attribute.Detained,
                        Condemned = item.Attribute.Condemned,
                        Eartag = item.Attribute.Eartag,
                        KillType = item.Attribute.KillType,
                        Breed = item.Breed,
                        Grade = item.Attribute.Grade,
                        DOB = item.Attribute.DOB,
                        TransactionDate = item.TransactionDate,
                        GradingDate = item.Attribute.GradingDate,
                        Identigen = item.Attribute.Identigen,
                        HoldingNumber = item.Attribute.HoldingNumber,
                        CountryOfOrigin = item.Attribute.CountryOfOrigin,
                        KillNumber = item.Attribute.KillNumber,
                        Generic1 = item.Attribute.Generic1,
                        Generic2 = item.Attribute.Generic2,
                        CarcassNumber = item.Attribute.CarcassNumber,
                        WeightSide1 = item.Attribute.Side1Weight ?? 0,
                        WeightSide2 = item.Attribute.Side2Weight ?? 0,
                        CarcassSide = item.Attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                        AgeInMonths = item.Attribute.AgeInMonths,
                        AgeInDays = item.Attribute.AgeInDays,
                        Cleanliness = item.Attribute.NouCleanliness,
                        SequencedDate = item.Attribute.SequencedDate,
                        Category = item.Category,
                        Sex = item.Attribute.Sex,
                        CustomerID = item.Attribute.Customer,
                        FarmAssured = item.Attribute.FarmAssured,
                        FreezerType = item.Attribute.FreezerType,
                        Clipped = item.Attribute.Clipped,
                        Casualty = item.Attribute.Casualty,
                        Lame = item.Attribute.Lame,
                        IsFullCarcass = item.IsBox,
                        NumberOfMoves = item.Attribute.NoOfMoves,
                        PreviousResidency = item.Attribute.PreviousResidency,
                        TotalResidency = item.Attribute.TotalResidency,
                        CurrentResidency = item.Attribute.CurrentResidency,
                        DateOfLastMove = item.Attribute.DateOfLastMove,
                        Imported = item.Attribute.Imported,
                        PaidWeight = item.Attribute.ColdWeight,
                        Attribute220 = item.Attribute.Attribute220,
                        Killed = item.Attribute.GradingDate != null ? 1 : 0,
                        UTM = item.Attribute.AgeInMonths != null && item.Attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                        OTM = item.Attribute.AgeInMonths != null && item.Attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                        SupplierID = item.SupplierId
                    });
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the killed carcass side details.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllTestCarcassSides()
        {
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var data = entities.TestSpeed().ToList();

                    foreach (var testSpeedResult in data)
                    {
                        details.Add(new StockDetail{ GradingDate = DateTime.Now});
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            var carcasses = new List<StockDetail>();
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null 
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                   && x.IsBox == true)
                         join attribute in entities.Attributes.Where(x => DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                             DbFunctions.TruncateTime(x.GradingDate) <= end && x.NouDocStatusID != status) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             TransactionDate = stock.TransactionDate,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             Eartag = attribute.Eartag,
                             KillType = attribute.KillType,
                             NouDocStatusID = attribute.NouDocStatusID,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             GradingDate = attribute.GradingDate,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            var carcasses = new List<StockDetail>();
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null 
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                   && x.IsBox == true)
                         join attribute in entities.Attributes.Where(x => DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                 DbFunctions.TruncateTime(x.GradingDate) <= end && x.Deleted == null) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             TransactionDate = stock.TransactionDate,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             Eartag = attribute.Eartag,
                             KillType = attribute.KillType,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             GradingDate = attribute.GradingDate,
                             DOB = attribute.DOB,
                             NouDocStatusID = attribute.NouDocStatusID,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             Killed = attribute.GradingDate != null ? 1 : 0,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllIdentigenKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            var carcasses = new List<StockDetail>();
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null 
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                   && x.IsBox == true)
                         join attribute in entities.Attributes.Where(x => DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                  DbFunctions.TruncateTime(x.GradingDate) <= end && !string.IsNullOrEmpty(x.Identigen)) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             TransactionDate = stock.TransactionDate,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             Eartag = attribute.Eartag,
                             GradingDate = attribute.GradingDate,
                             ExportedDate = attribute.Exported,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             KillType = attribute.KillType,
                             DOB = attribute.DOB,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             Killed = attribute.GradingDate != null ? 1 : 0,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetIdentigenKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            var carcasses = new List<StockDetail>();
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null 
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                   && x.IsBox == true)
                         join attribute in entities.Attributes.Where(x => DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                 DbFunctions.TruncateTime(x.GradingDate) <= end && !string.IsNullOrEmpty(x.Identigen) && x.Exported == null) 
                         on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         join intake in entities.APGoodsReceipts on intakeDetail.APGoodsReceiptID equals intake.APGoodsReceiptID
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             GradingDate = attribute.GradingDate,
                             ExportedDate = attribute.Exported,
                             TransactionDate = stock.TransactionDate,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             Generic2 = attribute.Generic2,
                             KillType = attribute.KillType,
                             DOB = attribute.DOB,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             Generic1 = attribute.Generic1,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             Killed = attribute.GradingDate != null ? 1 : 0,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             SupplierID = intake.BPMasterID_Supplier
                         })
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }


        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledAnimalDetailsByPrice(DateTime start, DateTime end)
        {
            var details = new List<StockDetail>();
            var thirdParty = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details = (from payment in entities.KillPayments
                               join detail in entities.KillPaymentItems on payment.KillPaymentID equals detail.KillPaymentID
                               join stock in entities.StockTransactions on detail.StockTransactionID equals stock
                                   .StockTransactionID
                               join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                               where payment.DocumentDate >= start && payment.DocumentDate <= end && detail.Deleted == null &&
                                     payment.Deleted == null
                               select new StockDetail
                               {
                                   LoadingStockDetail = true,
                                   UnitPrice = detail.UnitPrice,
                                   CarcassNumber = attribute.CarcassNumber,
                                   Eartag = attribute.Eartag,
                                   WeightSide1 = attribute.Side1Weight ?? 0,
                                   WeightSide2 = attribute.Side2Weight ?? 0,
                                   Category = attribute.NouCategory,
                                   Grade = attribute.Grade,
                                   PaidWeight = detail.PaidWeight,
                                   Classifier = stock.UserMaster != null ? stock.UserMaster.Reference : string.Empty,
                                   StockTransactionID = stock.StockTransactionID,
                                   APGoodsReceiptDetailID = stock.MasterTableID,
                                   AttributeID = attribute.AttributeID,
                                   TransactionWeight = stock.TransactionWeight,
                                   Serial = stock.Serial,
                                   TransactionDate = stock.TransactionDate,
                                   Detained = attribute.Detained,
                                   Condemned = attribute.Condemned,
                                   TBYes = attribute.TBYes,
                                   KillType = attribute.KillType,
                                   Breed = attribute.NouBreed,
                                   GradingDate = attribute.GradingDate,
                                   DOB = attribute.DOB,
                                   NouDocStatusID = attribute.NouDocStatusID,
                                   Identigen = attribute.Identigen,
                                   HoldingNumber = attribute.HoldingNumber,
                                   CountryOfOrigin = attribute.CountryOfOrigin,
                                   KillNumber = attribute.KillNumber,
                                   Generic1 = attribute.Generic1,
                                   AgeInMonths = attribute.AgeInMonths,
                                   AgeInDays = attribute.AgeInDays,
                                   Cleanliness = attribute.NouCleanliness,
                                   SequencedDate = attribute.SequencedDate,
                                   Sex = attribute.Sex,
                                   CustomerID = attribute.Customer,
                                   FarmAssured = attribute.FarmAssured,
                                   FreezerType = attribute.FreezerType,
                                   Clipped = attribute.Clipped,
                                   Casualty = attribute.Casualty,
                                   Lame = attribute.Lame,
                                   IsFullCarcass = stock.IsBox,
                                   NumberOfMoves = attribute.NoOfMoves,
                                   PreviousResidency = attribute.PreviousResidency,
                                   TotalResidency = attribute.TotalResidency,
                                   CurrentResidency = attribute.CurrentResidency,
                                   DateOfLastMove = attribute.DateOfLastMove,
                                   Imported = attribute.Imported,
                                   SupplierID = attribute.SupplierID,
                                   HerdNo = attribute.Herd,
                                   ThirdParty = attribute.Attribute199
                               }).ToList();

                    thirdParty = (from stock in entities.StockTransactions
                                  join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                  where attribute.GradingDate >= start && attribute.GradingDate <= end && stock.Deleted == null && attribute.Attribute199 == "Yes"
                                  select new StockDetail
                                  {
                                      LoadingStockDetail = true,
                                      UnitPrice = 0,
                                      CarcassNumber = attribute.CarcassNumber,
                                      Eartag = attribute.Eartag,
                                      WeightSide1 = attribute.Side1Weight ?? 0,
                                      WeightSide2 = attribute.Side2Weight ?? 0,
                                      Category = attribute.NouCategory,
                                      Grade = attribute.Grade,
                                      PaidWeight = attribute.ColdWeight,
                                      Classifier = stock.UserMaster != null ? stock.UserMaster.Reference : string.Empty,
                                      StockTransactionID = stock.StockTransactionID,
                                      APGoodsReceiptDetailID = stock.MasterTableID,
                                      AttributeID = attribute.AttributeID,
                                      TransactionWeight = stock.TransactionWeight,
                                      Serial = stock.Serial,
                                      TransactionDate = stock.TransactionDate,
                                      Detained = attribute.Detained,
                                      Condemned = attribute.Condemned,
                                      TBYes = attribute.TBYes,
                                      KillType = attribute.KillType,
                                      Breed = attribute.NouBreed,
                                      GradingDate = attribute.GradingDate,
                                      DOB = attribute.DOB,
                                      NouDocStatusID = attribute.NouDocStatusID,
                                      Identigen = attribute.Identigen,
                                      HoldingNumber = attribute.HoldingNumber,
                                      CountryOfOrigin = attribute.CountryOfOrigin,
                                      KillNumber = attribute.KillNumber,
                                      Generic1 = attribute.Generic1,
                                      AgeInMonths = attribute.AgeInMonths,
                                      AgeInDays = attribute.AgeInDays,
                                      Cleanliness = attribute.NouCleanliness,
                                      SequencedDate = attribute.SequencedDate,
                                      Sex = attribute.Sex,
                                      CustomerID = attribute.Customer,
                                      FarmAssured = attribute.FarmAssured,
                                      FreezerType = attribute.FreezerType,
                                      Clipped = attribute.Clipped,
                                      Casualty = attribute.Casualty,
                                      Lame = attribute.Lame,
                                      IsFullCarcass = stock.IsBox,
                                      NumberOfMoves = attribute.NoOfMoves,
                                      PreviousResidency = attribute.PreviousResidency,
                                      TotalResidency = attribute.TotalResidency,
                                      CurrentResidency = attribute.CurrentResidency,
                                      DateOfLastMove = attribute.DateOfLastMove,
                                      Imported = attribute.Imported,
                                      SupplierID = attribute.SupplierID,
                                      HerdNo = attribute.Herd,
                                      ThirdParty = attribute.Attribute199
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details.Union(thirdParty).ToList();
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledAnimalDetailsByPriceUnsent(DateTime start, DateTime end)
        {
            var details = new List<StockDetail>();
            var thirdParty = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details = (from payment in entities.KillPayments
                               join detail in entities.KillPaymentItems on payment.KillPaymentID equals detail.KillPaymentID
                               join stock in entities.StockTransactions on detail.StockTransactionID equals stock
                                   .StockTransactionID
                               join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                               where payment.DocumentDate >= start && payment.DocumentDate <= end && detail.Deleted == null &&
                                     payment.Deleted == null && attribute.Exported2 == null
                               select new StockDetail
                               {
                                   LoadingStockDetail = true,
                                   UnitPrice = detail.UnitPrice,
                                   CarcassNumber = attribute.CarcassNumber,
                                   Eartag = attribute.Eartag,
                                   WeightSide1 = attribute.Side1Weight ?? 0,
                                   WeightSide2 = attribute.Side2Weight ?? 0,
                                   Category = attribute.NouCategory,
                                   Grade = attribute.Grade,
                                   PaidWeight = detail.PaidWeight,
                                   Classifier = stock.UserMaster != null ? stock.UserMaster.Reference : string.Empty,
                                   StockTransactionID = stock.StockTransactionID,
                                   APGoodsReceiptDetailID = stock.MasterTableID,
                                   AttributeID = attribute.AttributeID,
                                   TransactionWeight = stock.TransactionWeight,
                                   Serial = stock.Serial,
                                   TransactionDate = stock.TransactionDate,
                                   Detained = attribute.Detained,
                                   Condemned = attribute.Condemned,
                                   TBYes = attribute.TBYes,
                                   KillType = attribute.KillType,
                                   Breed = attribute.NouBreed,
                                   GradingDate = attribute.GradingDate,
                                   DOB = attribute.DOB,
                                   NouDocStatusID = attribute.NouDocStatusID,
                                   Identigen = attribute.Identigen,
                                   HoldingNumber = attribute.HoldingNumber,
                                   CountryOfOrigin = attribute.CountryOfOrigin,
                                   KillNumber = attribute.KillNumber,
                                   Generic1 = attribute.Generic1,
                                   AgeInMonths = attribute.AgeInMonths,
                                   AgeInDays = attribute.AgeInDays,
                                   Cleanliness = attribute.NouCleanliness,
                                   SequencedDate = attribute.SequencedDate,
                                   Sex = attribute.Sex,
                                   CustomerID = attribute.Customer,
                                   FarmAssured = attribute.FarmAssured,
                                   FreezerType = attribute.FreezerType,
                                   Clipped = attribute.Clipped,
                                   Casualty = attribute.Casualty,
                                   Lame = attribute.Lame,
                                   IsFullCarcass = stock.IsBox,
                                   NumberOfMoves = attribute.NoOfMoves,
                                   PreviousResidency = attribute.PreviousResidency,
                                   TotalResidency = attribute.TotalResidency,
                                   CurrentResidency = attribute.CurrentResidency,
                                   DateOfLastMove = attribute.DateOfLastMove,
                                   Imported = attribute.Imported,
                                   SupplierID = attribute.SupplierID,
                                   HerdNo = attribute.Herd,
                                   ThirdParty = attribute.Attribute199
                               }).ToList();

                    thirdParty = (from stock in entities.StockTransactions
                                  join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                  where attribute.GradingDate >= start && attribute.GradingDate <= end && stock.Deleted == null && attribute.Attribute199 == "Yes"
                                  select new StockDetail
                                  {
                                      LoadingStockDetail = true,
                                      UnitPrice = 0,
                                      CarcassNumber = attribute.CarcassNumber,
                                      Eartag = attribute.Eartag,
                                      WeightSide1 = attribute.Side1Weight ?? 0,
                                      WeightSide2 = attribute.Side2Weight ?? 0,
                                      Category = attribute.NouCategory,
                                      Grade = attribute.Grade,
                                      PaidWeight = attribute.ColdWeight,
                                      Classifier = stock.UserMaster != null ? stock.UserMaster.Reference : string.Empty,
                                      StockTransactionID = stock.StockTransactionID,
                                      APGoodsReceiptDetailID = stock.MasterTableID,
                                      AttributeID = attribute.AttributeID,
                                      TransactionWeight = stock.TransactionWeight,
                                      Serial = stock.Serial,
                                      TransactionDate = stock.TransactionDate,
                                      Detained = attribute.Detained,
                                      Condemned = attribute.Condemned,
                                      TBYes = attribute.TBYes,
                                      KillType = attribute.KillType,
                                      Breed = attribute.NouBreed,
                                      GradingDate = attribute.GradingDate,
                                      DOB = attribute.DOB,
                                      NouDocStatusID = attribute.NouDocStatusID,
                                      Identigen = attribute.Identigen,
                                      HoldingNumber = attribute.HoldingNumber,
                                      CountryOfOrigin = attribute.CountryOfOrigin,
                                      KillNumber = attribute.KillNumber,
                                      Generic1 = attribute.Generic1,
                                      AgeInMonths = attribute.AgeInMonths,
                                      AgeInDays = attribute.AgeInDays,
                                      Cleanliness = attribute.NouCleanliness,
                                      SequencedDate = attribute.SequencedDate,
                                      Sex = attribute.Sex,
                                      CustomerID = attribute.Customer,
                                      FarmAssured = attribute.FarmAssured,
                                      FreezerType = attribute.FreezerType,
                                      Clipped = attribute.Clipped,
                                      Casualty = attribute.Casualty,
                                      Lame = attribute.Lame,
                                      IsFullCarcass = stock.IsBox,
                                      NumberOfMoves = attribute.NoOfMoves,
                                      PreviousResidency = attribute.PreviousResidency,
                                      TotalResidency = attribute.TotalResidency,
                                      CurrentResidency = attribute.CurrentResidency,
                                      DateOfLastMove = attribute.DateOfLastMove,
                                      Imported = attribute.Imported,
                                      SupplierID = attribute.SupplierID,
                                      HerdNo = attribute.Herd,
                                      ThirdParty = attribute.Attribute199
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details.Union(thirdParty).ToList();
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetBeefKilledAnimalDetailsByStatus(DateTime start, DateTime end)
        {
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null 
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                   )
                         join attribute in entities.Attributes.Where(x => DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                 DbFunctions.TruncateTime(x.GradingDate) <= end && x.GradingDate != null && x.Exported2 == null) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails.Where(x => x.APGoodsReceipt.BPMasterID_Supplier == ApplicationSettings.ScotBeefSupplierID) on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             TransactionDate = stock.TransactionDate,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             ExportedDate = attribute.Exported2,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             KillType = attribute.KillType,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             GradingDate = attribute.GradingDate,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             Attribute220 = attribute.Attribute220,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             WeightSide1 = attribute.Side1Weight ?? 0,
                             WeightSide2 = attribute.Side2Weight ?? 0,
                             Killed = attribute.GradingDate != null ? 1 : 0,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             Number = stock.IsBox == true ? entities.APGoodsReceiptDetails.FirstOrDefault(x => x.APGoodsReceiptDetailID == stock.MasterTableID)
                             .APGoodsReceipt.Number : 0,
                             SupplierID = intakeDetail.APGoodsReceipt.BPMasterID_Supplier
                         })
                            .ToList();

                    foreach (var groupedCarcasses in details.GroupBy(x => x.CarcassNumber))
                    {
                        var intakeCarcass = groupedCarcasses.FirstOrDefault(x => x.IsFullCarcass == true);
                        var killCarcasses = groupedCarcasses.Where(x => x.IsFullCarcass == null).ToList();
                        var left = killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side1);
                        var right = killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side2);

                        if (intakeCarcass != null)
                        {
                            if (left != null)
                            {
                                intakeCarcass.LabelIDSide1 = left.StockTransactionID;
                            }

                            if (right != null)
                            {
                                intakeCarcass.LabelIDSide2 = right.StockTransactionID;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details.Where(x => x.IsFullCarcass == true).ToList();
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllBeefKilledAnimalDetailsByStatus(DateTime start, DateTime end)
        {
            var details = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details =
                        (from stock in
                             entities.StockTransactions.Where(x => x.Deleted == null
                                                                   && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                                                   )
                         join attribute in entities.Attributes.Where(x => DbFunctions.TruncateTime(x.GradingDate) >= start &&
                                                                 DbFunctions.TruncateTime(x.GradingDate) <= end && x.GradingDate != null) on stock.AttributeID equals attribute.AttributeID
                         join intakeDetail in entities.APGoodsReceiptDetails.Where(x => x.APGoodsReceipt.BPMasterID_Supplier == ApplicationSettings.ScotBeefSupplierID) on stock.MasterTableID equals intakeDetail.APGoodsReceiptDetailID
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             Paid = stock.KillPaymentItems.Any(pay => pay.Deleted == null),
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Serial = stock.Serial,
                             TransactionDate = stock.TransactionDate,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             ExportedDate = attribute.Exported2,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             KillType = attribute.KillType,
                             Identigen = attribute.Identigen,
                             HoldingNumber = attribute.HoldingNumber,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             KillNumber = attribute.KillNumber,
                             GradingDate = attribute.GradingDate,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             PaidWeight = attribute.ColdWeight,
                             Attribute220 = attribute.Attribute220,
                             WeightSide1 = attribute.Side1Weight ?? 0,
                             WeightSide2 = attribute.Side2Weight ?? 0,
                             Killed = attribute.GradingDate != null ? 1 : 0,
                             UTM = attribute.AgeInMonths != null && attribute.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             OTM = attribute.AgeInMonths != null && attribute.AgeInMonths > ApplicationSettings.GraderLabelUOMAge ? 1 : 0,
                             Number = stock.IsBox == true ? entities.APGoodsReceiptDetails.FirstOrDefault(x => x.APGoodsReceiptDetailID == stock.MasterTableID)
                             .APGoodsReceipt.Number : 0,
                             SupplierID = intakeDetail.APGoodsReceipt.BPMasterID_Supplier
                         })
                            .ToList();

                    foreach (var groupedCarcasses in details.GroupBy(x => x.CarcassNumber))
                    {
                        var intakeCarcass = groupedCarcasses.FirstOrDefault(x => x.IsFullCarcass == true);
                        var killCarcasses = groupedCarcasses.Where(x => x.IsFullCarcass == null).ToList();
                        var left = killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side1);
                        var right = killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side2);

                        if (intakeCarcass != null)
                        {
                            if (left != null)
                            {
                                intakeCarcass.LabelIDSide1 = left.StockTransactionID;
                            }

                            if (right != null)
                            {
                                intakeCarcass.LabelIDSide2 = right.StockTransactionID;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details.Where(x => x.IsFullCarcass == true).ToList();
        }

        /// <summary>
        /// Marks the exported identigen records as being exported.
        /// </summary>
        /// <param name="details">The records to export.</param>
        /// <returns>Flag, as to success export or not.</returns>
        public bool ExportCarcasses(IList<StockDetail> details, bool useExport1)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        var dbAttribute =
                            entities.Attributes.FirstOrDefault(x => x.AttributeID == stockDetail.AttributeID);
                        if (dbAttribute != null)
                        {
                            if (useExport1)
                            {
                                dbAttribute.Exported = DateTime.Now;
                            }
                            else
                            {
                                dbAttribute.Exported2 = DateTime.Now;
                            }
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Marks the exported identigen records as being exported.
        /// </summary>
        /// <param name="details">The records to export.</param>
        /// <returns>Flag, as to success export or not.</returns>
        public bool ChangeCarcassStatus(IList<StockDetail> details, int status)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        var dbAttribute =
                            entities.Attributes.FirstOrDefault(x => x.AttributeID == stockDetail.AttributeID);
                        if (dbAttribute != null)
                        {
                            dbAttribute.NouDocStatusID = status;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the device last edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetLairageIntakeByLastEdit()
        {
            var apReceipt = new Sale();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                                                        .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receipt Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPHaulier = order.BPMasterHaulier,
                        BPAgent = order.BPMaster2,
                        HaulageCharge = order.HaulageCharge,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeliveryTime = order.DeliveryTime,
                        ProposedKillDate = order.ProposedKillDate,
                        SaleType = ViewType.LairageIntake,
                        LotAuthorisedDate = order.GoodsReceiptDate,
                        FileTransferedDate = order.FileTransferDate,
                        HerdQAS = order.HerdQAS,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        OtherReference = order.MoveResponse,
                        NouDocStatusID = order.NouDocStatusID,
                        CustomerID = order.BPMasterID_Customer,
                        Remarks = order.Remarks,
                        TimeInTransit = order.Notes,
                        DeliveryDate = order.DeliveryDate,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SalesEmployeeID = order.UserMasterID,
                        BPCustomer = order.BPMasterSupplier,
                        PermitNo = order.PermitNumber,
                        MoveToLairageReference = order.MoveToLairageReference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.APGoodsReceiptDetails
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           KillQuantityReceived = detail.QuantityReceived.HasValue ? (int)detail.QuantityReceived : 0,
                                           SaleDetailType = ViewType.LairageIntake,
                                           INMasterID = detail.INMasterID,
                                           StockDetails = (from stock in entities.StockTransactions.Where(x => x.MasterTableID == detail.APGoodsReceiptDetailID
                                               && x.Deleted == null && x.IsBox == true)
                                                           join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                                           join localKillPaymentItem in entities.KillPaymentItems.Where(x => x.Deleted == null)
                                                           on stock.StockTransactionID equals localKillPaymentItem.StockTransactionID into killPaymentItems
                                                           from killPaymentItem in killPaymentItems.DefaultIfEmpty()
                                                           select new StockDetail
                                                           {
                                                               LoadingStockDetail = true,
                                                               StockTransactionID = stock.StockTransactionID,
                                                               AttributeID = attribute.AttributeID,
                                                               Eartag = attribute.Eartag,
                                                               Breed = attribute.NouBreed,
                                                               HerdNo = attribute.Herd,
                                                               DOB = attribute.DOB,
                                                               Generic1 = attribute.Generic1,
                                                               AgeInMonths = attribute.AgeInMonths,
                                                               AgeInDays = attribute.AgeInDays,
                                                               RearedIn = attribute.RearedIn,
                                                               Cleanliness = attribute.NouCleanliness,
                                                               Category = attribute.NouCategory,
                                                               Sex = attribute.Sex,
                                                               Attribute180 = attribute.Attribute180,
                                                               Attribute200 = attribute.Attribute200,
                                                               Attribute220 = attribute.Attribute220,
                                                               Attribute238 = attribute.Attribute238,
                                                               Attribute210 = attribute.Attribute210,
                                                               Attribute120 = attribute.Attribute120,
                                                               KillType = attribute.KillType,
                                                               HoldingNumber = attribute.HoldingNumber,
                                                               CustomerID = attribute.Customer,
                                                               SequencedDate = attribute.SequencedDate,
                                                               GradingDate = attribute.GradingDate,
                                                               ThirdParty = attribute.Attribute199,
                                                               FarmAssured = attribute.FarmAssured,
                                                               Clipped = attribute.Clipped,
                                                               Casualty = attribute.Casualty,
                                                               FreezerType = attribute.FreezerType,
                                                               Lame = attribute.Lame,
                                                               NumberOfMoves = attribute.NoOfMoves,
                                                               Identigen = attribute.Identigen,
                                                               PreviousResidency = attribute.PreviousResidency,
                                                               TotalResidency = attribute.TotalResidency,
                                                               CurrentResidency = attribute.CurrentResidency,
                                                               DateOfLastMove = attribute.DateOfLastMove,
                                                               CountryOfOrigin = attribute.CountryOfOrigin,
                                                               Imported = attribute.Imported,
                                                               Posted = killPaymentItem != null ? killPaymentItem.Posted : null,
                                                               Exported = killPaymentItem != null ? killPaymentItem.Export : null,
                                                               UnitPrice = killPaymentItem != null ? killPaymentItem.UnitPrice : attribute.Price,
                                                               TotalExclVat = killPaymentItem != null ? killPaymentItem.TotalExclVAT : attribute.Total,
                                                               TotalIncVat = killPaymentItem != null ? killPaymentItem.TotalIncVAT : null,
                                                               TransactionWeight = stock.TransactionWeight,
                                                               WeightSide1 = attribute.Side1Weight ?? 0,
                                                               WeightSide2 = attribute.Side2Weight ?? 0,
                                                               RPA = attribute.RPA ?? false,
                                                               Grade = attribute.Grade,
                                                               PaidWeight = killPaymentItem != null ? killPaymentItem.PaidWeight : null,
                                                               UsingStockPrice = killPaymentItem != null && killPaymentItem.TotalExclVAT == null && attribute.Total != null
                                                           })
                                                           .ToList()
                                       }).ToList(),
                    };

                    var localDetail = apReceipt.SaleDetails.FirstOrDefault();
                    if (localDetail != null)
                    {
                        var details = localDetail.StockDetails;
                        if (details != null)
                        {
                            foreach (var stockDetail in details)
                            {
                                var moves =
                                    entities.FarmMovements.Where(
                                        x => x.AttributeID == stockDetail.AttributeID && x.Deleted == null);
                                if (moves.Any())
                                {
                                    stockDetail.Movements = moves.ToList();
                                }

                                stockDetail.AimsData =
                                    entities.AimsDatas.FirstOrDefault(x => x.Eartag == stockDetail.Eartag);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apReceipt;
        }

        /// <summary>
        /// Gets the lairage associated payment status.
        /// </summary>
        /// <param name="intakeId">The intake id.</param>
        /// <returns>The lairage associated payment status.</returns>
        public int? GetLairagePaymentStatus(int intakeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var payment = entities.KillPayments.FirstOrDefault(x => x.BaseDocumentReferenceID == intakeId && x.Deleted == null);
                    if (payment != null)
                    {
                        if (payment.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                             || payment.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                            || payment.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID)
                        {
                            return payment.Number;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the device last edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetLairageIntakeByFirstLast(bool first)
        {
            var apReceipt = new Sale();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    APGoodsReceipt order = null;
                    if (first)
                    {
                        order = entities.APGoodsReceipts
                            .FirstOrDefault(x => x.DocumentNumbering.NouDocumentName.DocumentName.Equals(Constant.LairageIntake));
                    }
                    else
                    {
                        order = entities.APGoodsReceipts.OrderByDescending(x => x.APGoodsReceiptID)
                            .FirstOrDefault(x => x.DocumentNumbering.NouDocumentName.DocumentName.Equals(Constant.LairageIntake));
                    }

                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }
                   
                    apReceipt = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPHaulier = order.BPMasterHaulier,
                        HaulageCharge = order.HaulageCharge,
                        BPAgent = order.BPMaster2,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeliveryTime = order.DeliveryTime,
                        LotAuthorisedDate = order.GoodsReceiptDate,
                        FileTransferedDate = order.FileTransferDate,
                        ProposedKillDate = order.ProposedKillDate,
                        SaleType = ViewType.LairageIntake,
                        HerdQAS = order.HerdQAS,
                        OtherReference = order.MoveResponse,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        NouDocStatusID = order.NouDocStatusID,
                        Remarks = order.Remarks,
                        DeliveryDate = order.DeliveryDate,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SalesEmployeeID = order.UserMasterID,
                        BPCustomer = order.BPMasterSupplier,
                        PermitNo = order.PermitNumber,
                        CustomerID = order.BPMasterID_Customer,
                        MoveToLairageReference = order.MoveToLairageReference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.APGoodsReceiptDetails
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           KillQuantityReceived = detail.QuantityReceived.HasValue ? (int)detail.QuantityReceived : 0,
                                           SaleDetailType = ViewType.LairageIntake,
                                           INMasterID = detail.INMasterID,
                                           StockDetails = (from stock in entities.StockTransactions.Where(x => x.MasterTableID == detail.APGoodsReceiptDetailID
                                                && x.Deleted == null && x.IsBox == true)
                                                           join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                                           join localKillPaymentItem in entities.KillPaymentItems.Where(x => x.Deleted == null)
                                                           on stock.StockTransactionID equals localKillPaymentItem.StockTransactionID into killPaymentItems
                                                           from killPaymentItem in killPaymentItems.DefaultIfEmpty()
                                                           select new StockDetail
                                                           {
                                                               LoadingStockDetail = true,
                                                               StockTransactionID = stock.StockTransactionID,
                                                               AttributeID = attribute.AttributeID,
                                                               Attribute180 = attribute.Attribute180,
                                                               Attribute238 = attribute.Attribute238,
                                                               Attribute220 = attribute.Attribute220,
                                                               Attribute120 = attribute.Attribute120,
                                                               Eartag = attribute.Eartag,
                                                               Breed = attribute.NouBreed,
                                                               HerdNo = attribute.Herd,
                                                               DOB = attribute.DOB,
                                                               Attribute210 = attribute.Attribute210,
                                                               Generic1 = attribute.Generic1,
                                                               AgeInMonths = attribute.AgeInMonths,
                                                               AgeInDays = attribute.AgeInDays,
                                                               RearedIn = attribute.RearedIn,
                                                               SequencedDate = attribute.SequencedDate,
                                                               Cleanliness = attribute.NouCleanliness,
                                                               Category = attribute.NouCategory,
                                                               Sex = attribute.Sex,
                                                               KillType = attribute.KillType,
                                                               HoldingNumber = attribute.HoldingNumber,
                                                               CustomerID = attribute.Customer,
                                                               FarmAssured = attribute.FarmAssured,
                                                               Clipped = attribute.Clipped,
                                                               Casualty = attribute.Casualty,
                                                               Lame = attribute.Lame,
                                                               ThirdParty = attribute.Attribute199,
                                                               NumberOfMoves = attribute.NoOfMoves,
                                                               Identigen = attribute.Identigen,
                                                               FreezerType = attribute.FreezerType,
                                                               PreviousResidency = attribute.PreviousResidency,
                                                               TotalResidency = attribute.TotalResidency,
                                                               CurrentResidency = attribute.CurrentResidency,
                                                               DateOfLastMove = attribute.DateOfLastMove,
                                                               CountryOfOrigin = attribute.CountryOfOrigin,
                                                               Imported = attribute.Imported,
                                                               Posted = killPaymentItem != null ? killPaymentItem.Posted : null,
                                                               Exported = killPaymentItem != null ? killPaymentItem.Export : null,
                                                               UnitPrice = killPaymentItem != null ? killPaymentItem.UnitPrice : attribute.Price,
                                                               TotalExclVat = killPaymentItem != null ? killPaymentItem.TotalExclVAT : attribute.Total,
                                                               TotalIncVat = killPaymentItem != null ? killPaymentItem.TotalIncVAT : null,
                                                               TransactionWeight = stock.TransactionWeight,
                                                               WeightSide1 = attribute.Side1Weight ?? 0,
                                                               WeightSide2 = attribute.Side2Weight ?? 0,
                                                               RPA = attribute.RPA ?? false,
                                                               Grade = attribute.Grade,
                                                               PaidWeight = killPaymentItem != null ? killPaymentItem.PaidWeight : null,
                                                               UsingStockPrice = killPaymentItem != null && killPaymentItem.TotalExclVAT == null && attribute.Total != null
                                                           })
                                                            .ToList()
                                       }).ToList(),
                    };

                    var localDetail = apReceipt.SaleDetails.FirstOrDefault();
                    if (localDetail != null)
                    {
                        var details = localDetail.StockDetails;
                        if (details != null)
                        {
                            foreach (var stockDetail in details)
                            {
                                var moves =
                                    entities.FarmMovements.Where(
                                        x => x.AttributeID == stockDetail.AttributeID && x.Deleted == null);
                                if (moves.Any())
                                {
                                    stockDetail.Movements = moves.ToList();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apReceipt;
        }

        /// <summary>
        /// Determines if an eartag is in the system.
        /// </summary>
        /// <param name="eartag">The eartag to check.</param>
        /// <returns>Flag, as to whether an eartag is in the system.</returns>
        public int IsEartagInSystem(string eartag)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var tag = entities.Attributes.FirstOrDefault(x => x.Deleted == null && x.Eartag == eartag);
                    if (tag != null)
                    {
                        var trans = entities.StockTransactions.FirstOrDefault(x => x.AttributeID == tag.AttributeID);
                        if (trans != null)
                        {
                            var detail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == trans.MasterTableID);
                            if (detail != null)
                            {
                                return detail.APGoodsReceipt.Number;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Determines if an eartag is in the system.
        /// </summary>
        /// <param name="eartag">The eartag to check.</param>
        /// <returns>Flag, as to whether an eartag is in the system.</returns>
        public bool IsSheepEartagInSystem(string eartag, int lotNo)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var existing = (from stock in entities.StockTransactions
                        join attribute in entities.Attributes
                            on stock.AttributeID equals attribute.AttributeID
                        where stock.MasterTableID == lotNo && attribute.Eartag == eartag
                        select new {attribute.Eartag}).Any();

                    return existing;

                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion

        #endregion

        #region sequencer

        /// <summary>
        /// Re-order the carcasses at the sequencer.
        /// </summary>
        /// <param name="carcassToMoveUp">The carcass to move up.</param>
        /// <param name="carcassToMoveDown">The carcass to move down.</param>
        /// <param name="docResetNo">The carcass document reset number.</param>
        /// <returns>A flag to indicate that the re-ordering was successful.</returns>
        public bool ReOrderCarcasses(StockDetail carcassToMoveUp, StockDetail carcassToMoveDown, int docResetNo)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    // can't use ref fields in anonymous method bodies, so use local pointers.
                    var carcUp = carcassToMoveUp;
                    var carcDown = carcassToMoveDown;

                    var carcassUp =
                        entities.Attributes.FirstOrDefault(carcass => carcass.AttributeID == carcUp.AttributeID && carcass.Deleted == null);

                    var carcassDown =
                        entities.Attributes.FirstOrDefault(carcass => carcass.AttributeID == carcDown.AttributeID && carcass.Deleted == null);

                    if (carcassUp != null && carcassDown != null)
                    {
                        var localCarcassNumberUp = carcassUp.CarcassNumber == 1 ? docResetNo : carcassUp.CarcassNumber - 1;
                        var localCarcassNumberDown = carcassDown.CarcassNumber == docResetNo ? 1 : carcassDown.CarcassNumber + 1;
                        var localSeqDate = carcassUp.SequencedDate;

                        carcassUp.CarcassNumber = localCarcassNumberUp;
                        carcassUp.KillNumber = carcassUp.KillNumber - 1;
                        carcassUp.SequencedDate = carcassDown.SequencedDate;
                        carcassDown.CarcassNumber = localCarcassNumberDown;
                        carcassDown.KillNumber = carcassDown.KillNumber + 1;
                        carcassDown.SequencedDate = localSeqDate;

                        entities.SaveChanges();

                        // pass our changes back out
                        carcassToMoveUp.CarcassNumber = localCarcassNumberUp;
                        carcassToMoveUp.KillNumber = carcassToMoveUp.KillNumber - 1;
                        carcassToMoveUp.SequencedDate = carcassUp.SequencedDate;
                        carcassToMoveDown.CarcassNumber = localCarcassNumberDown;
                        carcassToMoveDown.KillNumber = carcassToMoveDown.KillNumber + 1;
                        carcassToMoveDown.SequencedDate = carcassDown.SequencedDate;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a stock attribute.
        /// </summary>
        /// <param name="stock">The attrribute to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddStockAttribute(StocktransactionDetail stock)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Attributes.Add(stock.Attribute);
                    entities.SaveChanges();
                    stock.StockTransaction.AttributeID = stock.Attribute.AttributeID;
                    entities.StockTransactions.Add(stock.StockTransaction);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the age bands.
        /// </summary>
        /// <returns>The age bands.</returns>
        public IList<AgeBand> GetAgeBands()
        {
            var bands = new List<AgeBand>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    bands = entities.AgeBands.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return bands;
        }

        /// <summary>
        /// Updates a stock attribute.
        /// </summary>
        /// <returns>A flag,indicating a successful update or not.</returns>
        public bool UpdateStockAttribute(StockDetail detail)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == detail.AttributeID);
                    if (dbAttribute != null)
                    {
                        var dbStock =
                            entities.StockTransactions.FirstOrDefault(x => x.AttributeID == dbAttribute.AttributeID && x.Deleted == null);
                        if (dbStock != null)
                        {
                            detail.StockTransactionID = dbStock.StockTransactionID;
                            dbStock.IsBox = true;
                            if (!detail.ProductID.IsNullOrZero())
                            {
                                if (detail.LogCategoryChange || dbStock.INMasterID == ApplicationSettings.BeefIntakeID)
                                {
                                    var localId = dbStock.INMasterID;
                                    dbStock.INMasterID = detail.ProductID.ToInt();
                                    this.Log.LogInfo(this.GetType(), $"Changing product id at sequencer:Eartag:{dbAttribute.Eartag}, CarcassNo:{dbAttribute.CarcassNumber}: From {localId} to {dbStock.INMasterID}");
                                }
                            }
                        }

                        if (detail.LogCategoryChange || dbStock.INMasterID == ApplicationSettings.BeefIntakeID)
                        {
                            var localId = dbStock.INMasterID;
                            dbStock.INMasterID = detail.ProductID.ToInt();
                            this.Log.LogInfo(this.GetType(), $"Changing product id at sequencer:Eartag:{dbAttribute.Eartag}, CarcassNo:{dbAttribute.CarcassNumber}: From {localId} to {dbStock.INMasterID}");
                        }

                        dbAttribute.Halal = detail.Halal;
                        dbAttribute.Eartag = detail.Eartag;
                        dbAttribute.KillNumber = detail.KillNumber;
                        dbAttribute.CarcassNumber = detail.CarcassNumber;
                        dbAttribute.SequencedDate = DateTime.Now;
                        dbAttribute.Category = detail.CategoryID;
                        dbAttribute.Breed = detail.BreedID;
                        dbAttribute.Sex = detail.Sex;
                        dbAttribute.DOB = detail.DOB;
                        dbAttribute.AgeInMonths = detail.AgeInMonths;
                        dbAttribute.CountryOfOrigin = detail.CountryOfOrigin;
                        dbAttribute.Attribute180 = detail.Attribute180;

                        if (ApplicationSettings.ShowFarmAssuredAtSequencer)
                        {
                            dbAttribute.FarmAssured = detail.FarmAssured;
                        }
                     
                        entities.SaveChanges();

                        if (detail.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                        {
                            var intakeDetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(x => x.APGoodsReceiptDetailID == dbStock.MasterTableID);
                            if (intakeDetail != null)
                            {
                                var intake =
                                    entities.APGoodsReceipts.FirstOrDefault(
                                        x => x.APGoodsReceiptID == intakeDetail.APGoodsReceiptID);
                                if (intake != null)
                                {
                                    intake.NouDocStatusID = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
                                    entities.SaveChanges();
                                }
                            }
                        }

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Set the intake processed falg.
        /// </summary>
        /// <param name="intakeId">The intake id.</param>
        /// <param name="isProcessed">The processed flag.</param>
        /// <returns>Flag, st to successful update or not.</returns>
        public bool SetIntakeProcessing(int intakeId, bool isProcessed)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var intake = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == intakeId);
                    if (intake != null)
                    {
                        intake.Processed = isProcessed;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a stock attribute.
        /// </summary>
        /// <returns>A flag,indicating a successful update or not.</returns>
        public bool UpdateRPAAttribute(IList<StockDetail> details)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        var dbDetail = entities.Attributes.FirstOrDefault(x => x.AttributeID == stockDetail.AttributeID);
                        if (dbDetail != null)
                        {
                            dbDetail.RPA = stockDetail.RPA;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Undos a carcass sequenced, reverting the numbers back 1.
        /// </summary>
        /// <returns>A string, indicating an error or not.</returns>
        public string UndoSequencedCarcass(StockDetail detail, int docNumberId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == detail.AttributeID);
                    if (dbAttribute != null)
                    {
                        if (dbAttribute.GradingDate.HasValue)
                        {
                            return string.Format(Message.CarcassAlreadyGraded, dbAttribute.GradingDate);
                        }

                        dbAttribute.KillNumber = null;
                        dbAttribute.CarcassNumber = null;
                        dbAttribute.SequencedDate = null;
                        dbAttribute.Category = null;

                        var killNo = entities.KillNumbers.OrderByDescending(x => x.KillNumberID).FirstOrDefault();
                        if (killNo != null)
                        {
                            killNo.KillNumber1 -= 1;
                        }

                        var dbNumber =
                            entities.DocumentNumberings.FirstOrDefault(
                               number => number.DocumentNumberingID == docNumberId);

                        if (dbNumber != null)
                        {
                            dbNumber.NextNumber -= 1;
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return ex.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        public int GetNextKillNumber(bool isSequencer = true)
        {
            var nextKillNo = 1;
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (isSequencer)
                    {
                        var kill = entities.KillNumbers.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                        if (kill != null)
                        {
                            nextKillNo = kill.KillNumber1 + 1;
                        }
                    }
                    else
                    {
                        var kill = entities.Attributes
                            .Where(x => x.KillType == Constant.Beef && DbFunctions.TruncateTime(x.SequencedDate) == today && x.GradingDate == null && x.KillNumber.HasValue);
                       
                        if (kill.Any())
                        {
                            // carcasses in the queue, so return the kill number of the first.
                            nextKillNo = kill.Min(x => x.KillNumber).ToInt();
                        }
                        else
                        {
                            var nextKill = entities.KillNumbers.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                            if (nextKill != null)
                            {
                                nextKillNo = nextKill.KillNumber1 + 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return nextKillNo;
        }

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        public int GetNextKillNumberSheep(bool isSequencer = true)
        {
            var nextKillNo = 1;
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (isSequencer)
                    {
                        var kill = entities.KillNumberSheep.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                        if (kill != null)
                        {
                            nextKillNo = kill.KillNumber + 1;
                        }
                    }
                    else
                    {
                        var kill = entities.Attributes
                            .Where(x => x.KillType == Constant.Sheep && DbFunctions.TruncateTime(x.SequencedDate) == today && x.GradingDate == null && x.KillNumber.HasValue);

                        if (kill.Any())
                        {
                            // carcasses in the queue, so return the kill number of the first.
                            nextKillNo = kill.Min(x => x.KillNumber).ToInt();
                        }
                        else
                        {
                            var nextKill = entities.KillNumberSheep.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                            if (nextKill != null)
                            {
                                nextKillNo = nextKill.KillNumber + 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return nextKillNo;
        }

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        public int GetNextKillNumberPig(bool isSequencer = true)
        {
            var nextKillNo = 1;
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (isSequencer)
                    {
                        var kill = entities.KillNumberPigs.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                        if (kill != null)
                        {
                            nextKillNo = kill.KillNumber + 1;
                        }
                    }
                    else
                    {
                        var kill = entities.Attributes
                            .Where(x => x.KillType == Constant.Pig && DbFunctions.TruncateTime(x.SequencedDate) == today && x.GradingDate == null && x.KillNumber.HasValue);

                        if (kill.Any())
                        {
                            // carcasses in the queue, so return the kill number of the first.
                            nextKillNo = kill.Min(x => x.KillNumber).ToInt();
                        }
                        else
                        {
                            var nextKill = entities.KillNumberPigs.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                            if (nextKill != null)
                            {
                                nextKillNo = nextKill.KillNumber + 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return nextKillNo;
        }

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        public int UpdateKillNumber()
        {
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var kill = entities.KillNumbers.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                    if (kill == null)
                    {
                        entities.KillNumbers.Add(new KillNumber {KillNumber1 = 1, KillDate = DateTime.Today});
                        entities.SaveChanges();
                        return 1;
                    }

                    var updatedNo = kill.KillNumber1 += 1;
                    entities.SaveChanges();
                    return updatedNo;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        public int UpdateKillNumberSheep()
        {
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var kill = entities.KillNumberSheep.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                    if (kill == null)
                    {
                        entities.KillNumberSheep.Add(new KillNumberSheep { KillNumber = 1, KillDate = DateTime.Today });
                        entities.SaveChanges();
                        return 1;
                    }

                    var updatedNo = kill.KillNumber += 1;
                    entities.SaveChanges();
                    return updatedNo;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        public int UpdateKillNumberPig()
        {
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var kill = entities.KillNumberPigs.FirstOrDefault(x => DbFunctions.TruncateTime(x.KillDate) == today);
                    if (kill == null)
                    {
                        entities.KillNumberPigs.Add(new KillNumberPig { KillNumber = 1, KillDate = DateTime.Today });
                        entities.SaveChanges();
                        return 1;
                    }

                    var updatedNo = kill.KillNumber += 1;
                    entities.SaveChanges();
                    return updatedNo;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }


        ///// <summary>
        ///// Retrieves the stock detail corresponsing to the entered eartag.
        ///// </summary>
        ///// <param name="eartag">The eartag to search for.</param>
        ///// <returns>A stock detail.</returns>
        //public StockDetail GetLairageIntakeStockByEartag(string eartag, string killNo = "")
        //{
        //    var stockDetail = new StockDetail();

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            IQueryable<Attribute> attributes;
        //            if (string.IsNullOrWhiteSpace(killNo))
        //            {
        //                attributes = entities.Attributes.Where(x => x.Eartag.EndsWith(eartag) && x.SequencedDate == null && x.Deleted == null);
        //            }
        //            else
        //            {
        //                attributes = entities.Attributes.Where(x => x.Eartag.EndsWith(eartag) && x.Attribute220 == killNo && x.SequencedDate == null && x.Deleted == null);
        //            }

        //            if (!attributes.Any())
        //            {
        //                this.Log.LogDebug(this.GetType(), string.Format("unsequenced stock attribute not found for eartag:{0}", eartag));
        //                var localCarcass = entities.Attributes.FirstOrDefault(x => x.Eartag.EndsWith(eartag) && x.SequencedDate != null && x.Deleted == null);
        //                if (localCarcass != null)
        //                {
        //                    stockDetail.Error = string.Format(Message.AnimalAlreadySequenced, localCarcass.SequencedDate);
        //                }
        //                else
        //                {
        //                    stockDetail.Error = Message.EartagNotFound;
        //                }

        //                return stockDetail;
        //            }

        //            if (attributes.Count() > 1)
        //            {
        //                // multiple eartags found
        //                stockDetail.Error = string.Format(Message.MultipleEartagsFound, attributes.Count());
        //                return stockDetail;
        //            }

        //            var attribute = attributes.First();
        //            stockDetail = new StockDetail
        //            {
        //                AttributeID = attribute.AttributeID,
        //                Eartag = attribute.Eartag,
        //                Breed = attribute.NouBreed,
        //                DOB = attribute.DOB,
        //                AgeInMonths = attribute.AgeInMonths,
        //                AgeInDays = attribute.AgeInDays,
        //                HoldingNumber = attribute.HoldingNumber,
        //                Cleanliness = attribute.NouCleanliness,
        //                Category = attribute.NouCategory,
        //                Sex = attribute.Sex,
        //                CountryOfOrigin = attribute.CountryOfOrigin,
        //                CustomerID = attribute.Customer,
        //                FarmAssured = attribute.FarmAssured,
        //                Clipped = attribute.Clipped,
        //                Casualty = attribute.Casualty,
        //                Lame = attribute.Lame,
        //                NumberOfMoves = attribute.NoOfMoves,
        //                PreviousResidency = attribute.PreviousResidency,
        //                TotalResidency = attribute.TotalResidency,
        //                FreezerType = attribute.FreezerType,
        //                CurrentResidency = attribute.CurrentResidency,
        //                DateOfLastMove = attribute.DateOfLastMove,
        //                KillNumber = attribute.KillNumber,
        //                CarcassNumber = attribute.CarcassNumber,
        //                SequencedDate = attribute.SequencedDate,
        //                ExternalKillNumber = attribute.Attribute220,
        //                GradingDate = attribute.GradingDate,
        //                Imported = attribute.Imported
        //            };

        //            var carcass = entities.StockTransactions.FirstOrDefault(x => x.AttributeID == attribute.AttributeID);
        //            if (carcass != null)
        //            {
        //                var receiptdetail =
        //                    entities.APGoodsReceiptDetails.FirstOrDefault(
        //                        x => x.APGoodsReceiptDetailID == carcass.MasterTableID);
        //                if (receiptdetail != null)
        //                {
        //                    if (receiptdetail.APGoodsReceipt != null &&
        //                        receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
        //                    {
        //                        stockDetail.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
        //                        stockDetail.NouDocStatusID = receiptdetail.APGoodsReceipt.NouDocStatusID;
        //                        stockDetail.LotAuthorisedDate = receiptdetail.APGoodsReceipt.GoodsReceiptDate;
        //                        stockDetail.Number = receiptdetail.APGoodsReceipt.Number;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return stockDetail;
        //}


        /// <summary>
        /// Retrieves the stock detail corresponsing to the entered eartag.
        /// </summary>
        /// <param name="eartag">The eartag to search for.</param>
        /// <returns>A stock detail.</returns>
        public StockDetail GetLairageIntakeStockByEartag(string eartag, string killNo = "")
        {
            var stockDetail = new StockDetail();

            try
            {
                App_GetLairageCarcass_Result attributes;
                using (var entities = new NouvemEntities())
                {
                    attributes = entities.App_GetLairageCarcass(eartag, killNo).FirstOrDefault();

                    if (attributes == null)
                    {
                        this.Log.LogError(this.GetType(), string.Format("unsequenced stock attribute not found for eartag:{0}", eartag));
                        var localCarcass = entities.Attributes.FirstOrDefault(x => x.Eartag.EndsWith(eartag) && x.SequencedDate != null && x.Deleted == null);
                        if (localCarcass != null)
                        {
                            stockDetail.Error = string.Format(Message.AnimalAlreadySequenced, localCarcass.SequencedDate);
                        }
                        else
                        {
                            stockDetail.Error = Message.EartagNotFound;
                        }

                        return stockDetail;
                    }
                }

                var attribute = attributes;
                stockDetail = new StockDetail
                {
                    AttributeID = attribute.AttributeID,
                    Eartag = attribute.Eartag,
                    Breed = new NouBreed { Name = attribute.BreedName, NouBreedID = attribute.NouBreedID.ToInt() },
                    DOB = attribute.DOB,
                    AgeInMonths = attribute.AgeInMonths,
                    AgeInDays = attribute.AgeInDays,
                    HoldingNumber = attribute.HoldingNumber,
                    Cleanliness = new NouCleanliness { NouCleanlinessID = attribute.Cleanliness.ToInt() },
                    Category = new NouCategory { CategoryID = attribute.CategoryID.ToInt(), CAT = attribute.CAT, Name = attribute.CategoryName },
                    Sex = attribute.Sex,
                    CountryOfOrigin = attribute.CountryOfOrigin,
                    CustomerID = attribute.Customer,
                    FarmAssured = attribute.FarmAssured,
                    Clipped = attribute.Clipped,
                    Casualty = attribute.Casualty,
                    Lame = attribute.Lame,
                    NumberOfMoves = attribute.NoOfMoves,
                    PreviousResidency = attribute.PreviousResidency,
                    TotalResidency = attribute.TotalResidency,
                    FreezerType = attribute.FreezerType,
                    CurrentResidency = attribute.CurrentResidency,
                    DateOfLastMove = attribute.DateOfLastMove,
                    KillNumber = attribute.KillNumber,
                    CarcassNumber = attribute.CarcassNumber,
                    SequencedDate = attribute.SequencedDate,
                    ExternalKillNumber = attribute.Attribute220,
                    GradingDate = attribute.GradingDate,
                    Imported = attribute.Imported,
                    SupplierID = attribute.BPMasterID_Supplier,
                    NouDocStatusID = attribute.NouDocStatusID,
                    LotAuthorisedDate = attribute.GoodsReceiptDate,
                    Number = attribute.IntakeNo
                };
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return stockDetail;
        }

        /// <summary>
        /// Retrieves the stock detail corresponsing to the entered eartag.
        /// </summary>
        /// <param name="eartag">The eartag to search for.</param>
        /// <returns>A stock detail.</returns>
        public StockDetail VerifyEartag(string eartag)
        {
            var stockDetail = new StockDetail();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var attribute = entities.Attributes.FirstOrDefault(x => x.Eartag.EndsWith(eartag) && x.Deleted == null);
                    if (attribute == null)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("stock attribute not found for eartag:{0}", eartag));
                        stockDetail.Error = Message.EartagNotFound;
                        return stockDetail;
                    }

                    stockDetail = new StockDetail
                    {
                        AttributeID = attribute.AttributeID,
                        Eartag = attribute.Eartag,
                        Breed = attribute.NouBreed,
                        DOB = attribute.DOB,
                        AgeInMonths = attribute.AgeInMonths,
                        AgeInDays = attribute.AgeInDays,
                        HoldingNumber = attribute.HoldingNumber,
                        Cleanliness = attribute.NouCleanliness,
                        Category = attribute.NouCategory,
                        Sex = attribute.Sex,
                        CountryOfOrigin = attribute.CountryOfOrigin,
                        CustomerID = attribute.Customer,
                        FarmAssured = attribute.FarmAssured,
                        Clipped = attribute.Clipped,
                        Casualty = attribute.Casualty,
                        Lame = attribute.Lame,
                        NumberOfMoves = attribute.NoOfMoves,
                        PreviousResidency = attribute.PreviousResidency,
                        TotalResidency = attribute.TotalResidency,
                        FreezerType = attribute.FreezerType,
                        CurrentResidency = attribute.CurrentResidency,
                        DateOfLastMove = attribute.DateOfLastMove,
                        KillNumber = attribute.KillNumber,
                        CarcassNumber = attribute.CarcassNumber,
                        SequencedDate = attribute.SequencedDate,
                        GradingDate = attribute.GradingDate,
                        Imported = attribute.Imported
                    };

                    var carcass = entities.StockTransactions.FirstOrDefault(x => x.AttributeID == attribute.AttributeID);
                    if (carcass != null)
                    {
                        var receiptdetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == carcass.MasterTableID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                stockDetail.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                stockDetail.NouDocStatusID = receiptdetail.APGoodsReceipt.NouDocStatusID;
                                stockDetail.LotAuthorisedDate = receiptdetail.APGoodsReceipt.GoodsReceiptDate;
                                stockDetail.Number = receiptdetail.APGoodsReceipt.Number;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                stockDetail.Error = ex.Message;
            }

            return stockDetail;
        }


        /// <summary>
        /// Retrieves the daily sequenced, ungraded carcasses.
        /// </summary>
        /// <returns>A collection of daily sequenced, ungraded carcasses..</returns>
        public IList<StockDetail> GetCurrentSequencedStock()
        {
            var stockDetails = new List<StockDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                        stockDetails = (from attribute in entities.Attributes
                        where attribute.SequencedDate > DateTime.Today
                              && attribute.GradingDate == null && attribute.Deleted == null
                        select new StockDetail
                        {
                            AttributeID = attribute.AttributeID,
                            Eartag = attribute.Eartag,
                            Breed = attribute.NouBreed,
                            DOB = attribute.DOB,
                            AgeInMonths = attribute.AgeInMonths,
                            AgeInDays = attribute.AgeInDays,
                            Cleanliness = attribute.NouCleanliness,
                            Category = attribute.NouCategory,
                            Sex = attribute.Sex,
                            KillType = attribute.KillType,
                            HoldingNumber = attribute.HoldingNumber,
                            CustomerID = attribute.Customer,
                            FarmAssured = attribute.FarmAssured,
                            FreezerType = attribute.FreezerType,
                            Clipped = attribute.Clipped,
                            Casualty = attribute.Casualty,
                            CountryOfOrigin = attribute.CountryOfOrigin,
                            Lame = attribute.Lame,
                            ExternalKillNumber = attribute.Attribute220,
                            NumberOfMoves = attribute.NoOfMoves,
                            PreviousResidency = attribute.PreviousResidency,
                            TotalResidency = attribute.TotalResidency,
                            CurrentResidency = attribute.CurrentResidency,
                            DateOfLastMove = attribute.DateOfLastMove,
                            KillNumber = attribute.KillNumber,
                            CarcassNumber = attribute.CarcassNumber,
                            SequencedDate = attribute.SequencedDate,
                            GradingDate = attribute.GradingDate,
                            Imported = attribute.Imported,
                            SupplierID = attribute.SupplierID,
                            Halal = attribute.Halal
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return stockDetails;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="transId">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcassByStockId(int transId)
        {
            StockDetail carcass = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    carcass =
                        (from stock in
                             entities.StockTransactions
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where stock.StockTransactionID == transId
                         select new StockDetail
                         {
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             DOB = attribute.DOB,
                             Identigen = attribute.Identigen,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             HoldingNumber = attribute.HoldingNumber,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             KillType = attribute.KillType,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             Grade = attribute.Grade,
                             FreezerType = attribute.FreezerType,
                             KillNumber = attribute.KillNumber,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             ProductID = stock.INMasterID
                         }).FirstOrDefault();

                    if (carcass != null)
                    {
                        var receiptdetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                carcass.KillDate = receiptdetail.APGoodsReceipt.KillDate;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="transId">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcassById(int transId)
        {
            StockDetail carcass = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    carcass =
                        (from stock in
                             entities.StockTransactions
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where attribute.AttributeID == transId 
                         select new StockDetail
                         {
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             DOB = attribute.DOB,
                             Identigen = attribute.Identigen,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             HoldingNumber = attribute.HoldingNumber,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             KillType = attribute.KillType,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             Grade = attribute.Grade,
                             FreezerType = attribute.FreezerType,
                             KillNumber = attribute.KillNumber,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported
                         }).FirstOrDefault();

                    if (carcass != null)
                    {
                        var receiptdetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                carcass.KillDate = receiptdetail.APGoodsReceipt.KillDate;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextCarcassByIntakeId(int id)
        {
            var carcass = new StockDetail();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var allCarcasses =
                        (from stock in
                             entities.StockTransactions
                         join goodsIn in entities.APGoodsReceiptDetails on stock.MasterTableID equals goodsIn.APGoodsReceiptDetailID
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where goodsIn.APGoodsReceiptDetailID == id && (attribute.Eartag == null || attribute.Eartag == string.Empty)
                               && stock.Deleted == null && stock.NouTransactionTypeID == 2
                         select new StockDetail
                         {
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             Eartag = attribute.Eartag,
                             DOB = attribute.DOB,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             HoldingNumber = attribute.HoldingNumber,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             KillType = attribute.KillType,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                         }).ToList();

                    var count = allCarcasses.Count;
                    if (count > 0)
                    {
                        carcass = allCarcasses.First();
                        carcass.Count = count;
                      
                        var receiptdetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                carcass.KillDate = receiptdetail.APGoodsReceipt.KillDate;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextCarcassWithEartagsByIntakeId(int id)
        {
            var carcass = new StockDetail {StockDetails = new List<StockDetail>()};

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var allCarcasses =
                        (from stock in
                             entities.StockTransactions
                         join goodsIn in entities.APGoodsReceiptDetails on stock.MasterTableID equals goodsIn.APGoodsReceiptDetailID
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where goodsIn.APGoodsReceiptDetailID == id 
                               && stock.Deleted == null && stock.NouTransactionTypeID == 2
                         select new StockDetail
                         {
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             Eartag = attribute.Eartag,
                             DOB = attribute.DOB,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             HoldingNumber = attribute.HoldingNumber,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             SequencedDate = attribute.SequencedDate,
                             KillType = attribute.KillType,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                         }).ToList();

                    var count = allCarcasses != null ? allCarcasses.Count(x => x.SequencedDate == null) : 0;
                    if (count > 0)
                    {
                        carcass = allCarcasses.First();
                        carcass.Count = count;
                        carcass.StockDetails = allCarcasses;

                        var receiptdetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                carcass.KillDate = receiptdetail.APGoodsReceipt.KillDate;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextPigCarcassByIntakeId(int id)
        {
            var carcass = new StockDetail { KillType = Constant.Pig };

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var allCarcasses =
                        (from stock in
                             entities.StockTransactions
                         join goodsIn in entities.APGoodsReceiptDetails on stock.MasterTableID equals goodsIn.APGoodsReceiptDetailID
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where goodsIn.APGoodsReceiptDetailID == id 
                         select new StockDetail
                         {
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             Eartag = attribute.Eartag,
                             DOB = attribute.DOB,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             HoldingNumber = attribute.HoldingNumber,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             GradingDate = attribute.GradingDate,
                             KillType = attribute.KillType,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CustomerID = attribute.Customer,
                             FreezerType = attribute.FreezerType,
                             FarmAssured = attribute.FarmAssured
                         }).ToList();

                    var unProcessedCarcasses = allCarcasses.Where(x => x.GradingDate == null).ToList();
                    var count = unProcessedCarcasses.Count;

                    if (count > 0)
                    {
                        carcass = unProcessedCarcasses.First();
                        carcass.Count = count;

                        var receiptdetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                carcass.KillDate = receiptdetail.APGoodsReceipt.KillDate;
                            }
                        }

                        if (!carcass.CustomerID.HasValue)
                        {
                            var lastCustomer = allCarcasses.FirstOrDefault(x => x.CustomerID != null);
                            if (lastCustomer != null)
                            {
                                carcass.CustomerID = lastCustomer.CustomerID;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextSheepCarcassByIntakeId(int id)
        {
            var carcass = new StockDetail { KillType = Constant.Sheep };

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var allCarcasses =
                        (from stock in
                             entities.StockTransactions
                         join goodsIn in entities.APGoodsReceiptDetails on stock.MasterTableID equals goodsIn.APGoodsReceiptDetailID
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where goodsIn.APGoodsReceiptDetailID == id
                         select new StockDetail
                         {
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             Eartag = attribute.Eartag,
                             DOB = attribute.DOB,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             HoldingNumber = attribute.HoldingNumber,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             GradingDate = attribute.GradingDate,
                             KillType = attribute.KillType,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             CustomerID = attribute.Customer,
                             FreezerType = attribute.FreezerType,
                             FarmAssured = attribute.FarmAssured
                         }).ToList();

                    var unProcessedCarcasses = allCarcasses.Where(x => x.GradingDate == null).ToList();
                    var count = unProcessedCarcasses.Count;

                    if (count > 0)
                    {
                        carcass = unProcessedCarcasses.First();
                        carcass.Count = count;

                        var receiptdetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                carcass.KillDate = receiptdetail.APGoodsReceipt.KillDate;
                            }
                        }

                        if (!carcass.CustomerID.HasValue)
                        {
                            var lastCustomer = allCarcasses.FirstOrDefault(x => x.CustomerID != null);
                            if (lastCustomer != null)
                            {
                                carcass.CustomerID = lastCustomer.CustomerID;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Adds a new grader pig carcass.
        /// </summary>
        /// <param name="transId">The previous pig carcass to copy.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddNewPigCarcass(int masterTableId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTrans = entities.StockTransactions.First(x => x.MasterTableID == masterTableId 
                        && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId);
                    var newTrans = this.CreateTransaction(dbTrans);
                    newTrans.TransactionWeight = null;
                    newTrans.TransactionQTY = null;
                    newTrans.GrossWeight = null;
                    newTrans.Tare = null;
                    newTrans.TransactionDate = DateTime.Now;
                    newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                    newTrans.UserMasterID = NouvemGlobal.UserId;

                    var dbAttribute = entities.Attributes.First(x => x.AttributeID == dbTrans.AttributeID);
                    var newAttribute = this.CreateAttribute(dbAttribute);
                    newAttribute.KillType = Constant.Pig;
                    newAttribute.Category = dbAttribute.Category;
                    newAttribute.AgeInMonths = dbAttribute.AgeInMonths;
                    newAttribute.Sex = dbAttribute.Sex;
                    newAttribute.AgeInDays = dbAttribute.AgeInDays;
                    newAttribute.TotalResidency = dbAttribute.TotalResidency;
                    newAttribute.CurrentResidency = dbAttribute.CurrentResidency;
                    entities.Attributes.Add(newAttribute);
                    entities.SaveChanges();

                    newTrans.AttributeID = newAttribute.AttributeID;
                    entities.StockTransactions.Add(newTrans);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a new grader sheep carcass.
        /// </summary>
        /// <param name="transId">The previous pig carcass to copy.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddNewSheepCarcass(int masterTableId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTrans = entities.StockTransactions.First(x => x.MasterTableID == masterTableId
                        && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId);
                    var newTrans = this.CreateTransaction(dbTrans);
                    newTrans.TransactionWeight = null;
                    newTrans.TransactionQTY = null;
                    newTrans.GrossWeight = null;
                    newTrans.Tare = null;
                    newTrans.TransactionDate = DateTime.Now;
                    newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                    newTrans.UserMasterID = NouvemGlobal.UserId;

                    var dbAttribute = entities.Attributes.First(x => x.AttributeID == dbTrans.AttributeID);
                    var newAttribute = this.CreateAttribute(dbAttribute);
                    newAttribute.KillType = Constant.Sheep;
                    newAttribute.Category = dbAttribute.Category;
                    newAttribute.AgeInMonths = dbAttribute.AgeInMonths;
                    newAttribute.Sex = dbAttribute.Sex;
                    newAttribute.AgeInDays = dbAttribute.AgeInDays;
                    newAttribute.TotalResidency = dbAttribute.TotalResidency;
                    newAttribute.CurrentResidency = dbAttribute.CurrentResidency;
                    entities.Attributes.Add(newAttribute);
                    entities.SaveChanges();

                    newTrans.AttributeID = newAttribute.AttributeID;
                    entities.StockTransactions.Add(newTrans);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion

        #region grader

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public IList<StockDetail> GetTodaysCarcasses(string killType)
        {
            var carcasses = new List<StockDetail>();
            var today = DateTime.Today;
          
            try
            {
                IList<StockDetail> dbCarcasses;
                using (var entities = new NouvemEntities())
                {
                     dbCarcasses = (from attribute in entities.Attributes
                                        .Where(x => x.SequencedDate >= today && x.KillType == killType)
                                        .OrderByDescending(x => x.SequencedDate).Take(ApplicationSettings.GraderAnimalsToRetrieve)
                                         join stock in entities.StockTransactions on attribute.AttributeID equals stock.AttributeID
                                       where stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId && stock.Deleted == null
                                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             StockTransactionID = stock.StockTransactionID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             KillType = attribute.KillType,
                             HoldingNumber = attribute.HoldingNumber,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             Identigen = attribute.Identigen,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             RearedIn = attribute.RearedIn,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             KillNumber = attribute.KillNumber,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             ExternalKillNumber = attribute.Attribute220,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             SupplierID = attribute.SupplierID,
                             Attribute245 = attribute.Attribute245,
                             Attribute246 = attribute.Attribute246,
                             Attribute247 = attribute.Attribute247
                         }).ToList();


                    //var dbCarcasses =
                    //    (from stock in
                    //         entities.StockTransactions.Where(x => x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId).OrderByDescending(x => x.StockTransactionID).Take(50)
                    //     join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                    //     where DbFunctions.TruncateTime(attribute.SequencedDate) == today && stock.Deleted == null
                    //     select new StockDetail
                    //     {
                    //         LoadingStockDetail = true,
                    //         StockTransactionID = stock.StockTransactionID,
                    //         APGoodsReceiptDetailID = stock.MasterTableID,
                    //         AttributeID = attribute.AttributeID,
                    //         TransactionWeight = stock.TransactionWeight,
                    //         Detained = attribute.Detained,
                    //         Condemned = attribute.Condemned,
                    //         KillType = attribute.KillType,
                    //         HoldingNumber = attribute.HoldingNumber,
                    //         Eartag = attribute.Eartag,
                    //         Breed = attribute.NouBreed,
                    //         Grade = attribute.Grade,
                    //         DOB = attribute.DOB,
                    //         Identigen = attribute.Identigen,
                    //         CountryOfOrigin = attribute.CountryOfOrigin,
                    //         Generic1 = attribute.Generic1,
                    //         Generic2 = attribute.Generic2,
                    //         KillNumber = attribute.KillNumber,
                    //         CarcassNumber = attribute.CarcassNumber,
                    //         CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                    //         AgeInMonths = attribute.AgeInMonths,
                    //         AgeInDays = attribute.AgeInDays,
                    //         Cleanliness = attribute.NouCleanliness,
                    //         SequencedDate = attribute.SequencedDate,
                    //         Category = attribute.NouCategory,
                    //         Sex = attribute.Sex,
                    //         CustomerID = attribute.Customer,
                    //         FarmAssured = attribute.FarmAssured,
                    //         FreezerType = attribute.FreezerType,
                    //         Clipped = attribute.Clipped,
                    //         Casualty = attribute.Casualty,
                    //         Lame = attribute.Lame,
                    //         IsFullCarcass = stock.IsBox,
                    //         NumberOfMoves = attribute.NoOfMoves,
                    //         PreviousResidency = attribute.PreviousResidency,
                    //         TotalResidency = attribute.TotalResidency,
                    //         CurrentResidency = attribute.CurrentResidency,
                    //         DateOfLastMove = attribute.DateOfLastMove,
                    //         Imported = attribute.Imported,
                    //         SupplierID = attribute.SupplierID
                    //     }).ToList();
                }

                foreach (var groupedCarcasses in dbCarcasses.GroupBy(x => x.CarcassNumber))
                {
                    StockDetail carcass;
                    if (groupedCarcasses.Count() == 1)
                    {
                        // beef sequenced and sitting in the kill queue, or a sheep carcass.
                        carcass = groupedCarcasses.ElementAt(0);

                        if (carcass.NouKillType == KillType.Sheep || carcass.NouKillType == KillType.Pig)
                        {
                            carcass.WeightSide1 = Math.Round(carcass.TransactionWeight.ToDecimal(), 2);
                        }

                        carcasses.Add(carcass);
                    }
                    else
                    {
                        // remove the parent transaction, and combine the split carcass sides.
                        var killCarcasses = groupedCarcasses.Where(x => x.IsFullCarcass == null).ToList();
                        carcass = killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side1);
                        if (carcass != null)
                        {
                            carcass.WeightSide1 = Math.Round(carcass.TransactionWeight.ToDecimal(), 2);
                            carcass.DetainedSide1 = carcass.Detained;
                            carcass.CondemnedSide1 = carcass.Condemned;

                            if (killCarcasses.Count > 1)
                            {
                                var side2Carcass =
                                    killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side2);
                                if (side2Carcass != null)
                                {
                                    carcass.WeightSide2 = Math.Round(side2Carcass.TransactionWeight.ToDecimal(), 2);
                                    carcass.DetainedSide2 = side2Carcass.Detained;
                                    carcass.CondemnedSide2 = side2Carcass.Condemned;
                                }
                            }
                        }

                        carcasses.Add(carcass);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcasses;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcass(int carcassNo, string mode)
        {
            StockDetail carcass = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    carcass =
                        (from stock in
                            entities.StockTransactions
                            join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                            where stock.Deleted == null && attribute.CarcassNumber == carcassNo && attribute.SequencedDate > DateTime.Today && attribute.GradingDate == null && attribute.KillType == mode
                               && stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                            select new StockDetail
                            {
                                StockTransactionID = stock.StockTransactionID,
                                INMasterID = stock.INMasterID,
                                APGoodsReceiptDetailID = stock.MasterTableID,
                                AttributeID = attribute.AttributeID,
                                Eartag = attribute.Eartag,
                                Breed = attribute.NouBreed,
                                KillType = attribute.KillType,
                                DOB = attribute.DOB,
                                Identigen = attribute.Identigen,
                                AgeInMonths = attribute.AgeInMonths,
                                AgeInDays = attribute.AgeInDays,
                                HoldingNumber = attribute.HoldingNumber,
                                HerdNo = attribute.Herd,
                                Cleanliness = attribute.NouCleanliness,
                                SequencedDate = attribute.SequencedDate,
                                Category = attribute.NouCategory,
                                Sex = attribute.Sex,
                                CountryOfOrigin = attribute.CountryOfOrigin,
                                Generic1 = attribute.Generic1,
                                Generic2 = attribute.Generic2,
                                CustomerID = attribute.Customer,
                                FarmAssured = attribute.FarmAssured,
                                FreezerType = attribute.FreezerType,
                                Clipped = attribute.Clipped,
                                Casualty = attribute.Casualty,
                                Lame = attribute.Lame,
                                Grade = attribute.Grade,
                                KillNumber = attribute.KillNumber,
                                NumberOfMoves = attribute.NoOfMoves,
                                PreviousResidency = attribute.PreviousResidency,
                                TotalResidency = attribute.TotalResidency,
                                CurrentResidency = attribute.CurrentResidency,
                                DateOfLastMove = attribute.DateOfLastMove,
                                Imported = attribute.Imported,
                                RearedIn = attribute.RearedIn,
                                Attribute200 = attribute.Attribute200,
                                Attribute220 = attribute.Attribute220,
                                Attribute238 = attribute.Attribute238,
                                Attribute245 = attribute.Attribute245,
                                Attribute246 = attribute.Attribute246,
                                Attribute247 = attribute.Attribute247,
                                Attribute249 = attribute.Attribute249
                            }).FirstOrDefault();

                    if (carcass != null)
                    {
                        var receiptdetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                        if (receiptdetail != null)
                        {
                            if (receiptdetail.APGoodsReceipt != null &&
                                receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                            {
                                carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                                carcass.KillDate = receiptdetail.APGoodsReceipt.KillDate;
                                if (carcass.CustomerID == null)
                                {
                                    carcass.CustomerID = receiptdetail.APGoodsReceipt.BPMasterID_Customer;
                                }
                            }
                        }

                        var today = DateTime.Today;
                        var matchingCarcasses = entities.Attributes.Where(x => x.CarcassNumber == carcassNo && x.KillType == mode && DbFunctions.TruncateTime(x.GradingDate) == today);
                        if (matchingCarcasses.Count() == 1)
                        {
                            // crash recovery - side 1 already graded, so switch to side 2.
                            carcass.CarcassSide = StockDetail.Side.Side2;
                            carcass.Error = matchingCarcasses.First().Grade;
                            var id = matchingCarcasses.First().AttributeID;
                            var stock = entities.StockTransactions.FirstOrDefault(x => x.AttributeID == id);
                            if (stock != null)
                            {
                                carcass.WeightSide1 = stock.TransactionWeight.ToDecimal();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Gets an intake supplier id.
        /// </summary>
        /// <param name="id">The transaction id.</param>
        /// <returns>An intake supplier id.</returns>
        public int? GetSupplierId(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var receiptdetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == id);
                    if (receiptdetail != null)
                    {
                        if (receiptdetail.APGoodsReceipt != null &&
                            receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                        {
                            return receiptdetail.APGoodsReceipt.BPMasterID_Supplier;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Updaes the whole carcass weights.
        /// </summary>
        /// <param name="carcassNo">The carcass number of the animal to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateCarcassWeight(int carcassNo, string killType)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_UpdateCarcassWeight(carcassNo, killType);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }


        /// <summary>
        /// Gets the lot kill data.
        /// </summary>
        /// <param name="lotNo">The header record id.</param>
        /// <returns>Kill lot data (killdate, or null if not yet killed).</returns>
        public IList<DateTime?> GetLotCount(int lotNo)
        {
            var data = new List<DateTime?>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    data = entities.App_GetLotCount(lotNo).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcassForEdit(int attributeId)
        {
            var carcass = new StockDetail();
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.Attributes.First(x => x.AttributeID == attributeId);
                    var carcassNo = dbAttribute.CarcassNumber;
                    var gradingDate = dbAttribute.GradingDate.ToDate().Date;

                    var dbCarcasses =
                        (from stock in
                             entities.StockTransactions
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where  stock.Deleted == null && attribute.CarcassNumber == carcassNo && attribute.KillType == Constant.Beef
                         && DbFunctions.TruncateTime(attribute.GradingDate) == gradingDate
                            && stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                         select new StockDetail
                         {
                             LoadingStockDetail = true,
                             StockTransactionID = stock.StockTransactionID,
                             INMasterID = stock.INMasterID,
                             APGoodsReceiptDetailID = stock.MasterTableID,
                             AttributeID = attribute.AttributeID,
                             TransactionWeight = stock.TransactionWeight,
                             Detained = attribute.Detained,
                             Condemned = attribute.Condemned,
                             KillType = attribute.KillType,
                             HoldingNumber = attribute.HoldingNumber,
                             Eartag = attribute.Eartag,
                             Breed = attribute.NouBreed,
                             Grade = attribute.Grade,
                             DOB = attribute.DOB,
                             Identigen = attribute.Identigen,
                             CountryOfOrigin = attribute.CountryOfOrigin,
                             Generic1 = attribute.Generic1,
                             Generic2 = attribute.Generic2,
                             KillNumber = attribute.KillNumber,
                             CarcassNumber = attribute.CarcassNumber,
                             CarcassSide = attribute.CarcassSide == 1 ? StockDetail.Side.Side1 : StockDetail.Side.Side2,
                             AgeInMonths = attribute.AgeInMonths,
                             AgeInDays = attribute.AgeInDays,
                             Cleanliness = attribute.NouCleanliness,
                             GradingDate = attribute.GradingDate,
                             SequencedDate = attribute.SequencedDate,
                             Category = attribute.NouCategory,
                             Sex = attribute.Sex,
                             CustomerID = attribute.Customer,
                             FarmAssured = attribute.FarmAssured,
                             FreezerType = attribute.FreezerType,
                             Clipped = attribute.Clipped,
                             Casualty = attribute.Casualty,
                             Lame = attribute.Lame,
                             IsFullCarcass = stock.IsBox,
                             NumberOfMoves = attribute.NoOfMoves,
                             PreviousResidency = attribute.PreviousResidency,
                             TotalResidency = attribute.TotalResidency,
                             CurrentResidency = attribute.CurrentResidency,
                             DateOfLastMove = attribute.DateOfLastMove,
                             Imported = attribute.Imported,
                             RearedIn = attribute.RearedIn,
                             Attribute200 = attribute.Attribute200,
                             Attribute220 = attribute.Attribute220,
                             Attribute238 = attribute.Attribute238,
                             Attribute245 = attribute.Attribute245,
                             Attribute246 = attribute.Attribute246,
                             Attribute247 = attribute.Attribute247
                         }).ToList();


                    foreach (var groupedCarcasses in dbCarcasses.GroupBy(x => x.CarcassNumber))
                    {
                        if (groupedCarcasses.Count() == 1)
                        {
                            // beef sequenced and sitting in the kill queue, or a sheep carcass.
                            carcass = groupedCarcasses.ElementAt(0);

                            if (carcass.NouKillType == KillType.Sheep)
                            {
                                carcass.WeightSide1 = Math.Round(carcass.TransactionWeight.ToDecimal(), 2);
                            }
                        }
                        else
                        {
                            // remove the parent transaction, and combine the split carcass sides.
                            var killCarcasses = groupedCarcasses.Where(x => x.IsFullCarcass == null).ToList();
                            carcass = killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side1);
                            if (carcass != null)
                            {
                                carcass.WeightSide1 = Math.Round(carcass.TransactionWeight.ToDecimal(), 2);
                                carcass.DetainedSide1 = carcass.Detained;
                                carcass.CondemnedSide1 = carcass.Condemned;

                                if (killCarcasses.Count > 1)
                                {
                                    var side2Carcass =
                                        killCarcasses.FirstOrDefault(x => x.CarcassSide == StockDetail.Side.Side2);
                                    if (side2Carcass != null)
                                    {
                                        carcass.WeightSide2 = Math.Round(side2Carcass.TransactionWeight.ToDecimal(), 2);
                                        carcass.DetainedSide2 = side2Carcass.Detained;
                                        carcass.CondemnedSide2 = side2Carcass.Condemned;
                                    }
                                }
                            }
                        }

                        if (carcass != null)
                        {
                            var receiptdetail =
                               entities.APGoodsReceiptDetails.FirstOrDefault(
                                   x => x.APGoodsReceiptDetailID == carcass.APGoodsReceiptDetailID);
                            if (receiptdetail != null)
                            {
                                if (receiptdetail.APGoodsReceipt != null &&
                                    receiptdetail.APGoodsReceipt.BPMasterSupplier != null)
                                {
                                    carcass.SupplierID = receiptdetail.APGoodsReceipt.BPMasterID_Supplier;

                                    if (carcass.CustomerID == null)
                                    {
                                        carcass.CustomerID = receiptdetail.APGoodsReceipt.BPMasterID_Customer;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcass;
        }

        /// <summary>
        /// Gets the stock serial id associated with the input barcode and side.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <param name="carcassSide">The carcass side to search for.</param>
        /// <returns>A stock serial id associated with the input barcode and side</returns>
        public int GetCarcassTransaction(int carcassNo, int carcassSide)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetCarcassTransaction:No:{0}, Side:{1}", carcassNo, carcassSide));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var carcass =
                        (from stock in
                             entities.StockTransactions
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where attribute.CarcassNumber == carcassNo && attribute.CarcassSide == carcassSide
                            && stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId && stock.Deleted == null && stock.IsBox == null
                         select new { stock.Serial} ).FirstOrDefault();

                    if (carcass != null)
                    {
                        return carcass.Serial.ToInt();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Gets the next carcass number at the grader.
        /// </summary>
        /// <param name="carcassResetNo">The reset value.</param>
        /// <returns>The next carcass number at the grader.</returns>
        public int GetLastcarcassNumber(int nextCarcassNo, string killType)
        {
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                      // get the last intake carcass that has been graded and consumed today.
                    var carcasses = entities.Attributes
                        .Where(x => DbFunctions.TruncateTime(x.SequencedDate) == today
                                    && x.KillType == killType && x.GradingDate == null 
                                    && x.CarcassNumber.HasValue);

                    if (carcasses.Any())
                    {
                        // There are sequenced carcasses in the line, so we use the first carcass number.
                        return carcasses.OrderBy(x => x.KillNumber).First().CarcassNumber.ToInt();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            // nothing in the kill queue, so return the next carcass number.
            return nextCarcassNo;
        }

        /// <summary>
        /// Adds a grader carcass side weighing.
        /// </summary>
        /// <param name="detail">The carcass to add.</param>
        /// <returns>Flag, as to a successful add or not.</returns>
        public bool AddGraderTransaction(StockDetail detail)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans =
                        entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == detail.StockTransactionID);

                    var attribute = new Attribute
                    {
                        AgeInMonths = detail.AgeInMonths,
                        AgeInDays = detail.AgeInDays,
                        Eartag = detail.Eartag,
                        Herd = detail.HerdNo,
                        Breed = detail.BreedID,
                        DOB = detail.DOB,
                        fatColour = detail.FatColourName,
                        Category = detail.CategoryID,
                        Sex = detail.Sex,
                        Grade = detail.Grade,
                        FarmAssured = detail.FarmAssured,
                        FreezerType = detail.FreezerType,
                        HoldingNumber = detail.HoldingNumber,
                        Clipped = detail.Clipped,
                        Casualty = detail.Casualty,
                        CountryOfOrigin = detail.CountryOfOrigin,
                        Lame = detail.Lame,
                        KillType = Constant.Beef,
                        Generic1 = detail.Generic1,
                        Cleanliness = detail.CleanlinessID,
                        NoOfMoves = detail.NumberOfMoves,
                        CurrentResidency = detail.CurrentResidency,
                        PreviousResidency = detail.PreviousResidency,
                        TotalResidency = detail.TotalResidency,
                        DateOfLastMove = detail.DateOfLastMove,
                        Imported = detail.Imported,
                        CarcassNumber = detail.CarcassNumber,
                        KillNumber = detail.KillNumber,
                        CarcassSide = detail.CarcassSideNo,
                        SequencedDate = detail.SequencedDate,
                        GradingDate = DateTime.Now,
                        Condemned = detail.Condemned,
                        Detained = detail.Detained,
                        NotInsured = detail.NotInsured,
                        TBYes = detail.TBYes,
                        BurstBelly = detail.BurstBelly,
                        ColdWeight = detail.ColdWeight,
                        Generic2 = detail.Generic2,
                        Customer = detail.CustomerID,
                        Identigen = detail.Identigen,
                        RearedIn = detail.RearedIn,
                        SupplierID = detail.SupplierID,
                        LegMissing = detail.LegMissing,
                        Abscess = detail.Abscess,
                        Attribute200 = detail.Attribute200,
                        Attribute220 = detail.Attribute220,
                        Attribute238 = detail.Attribute238,
                        Attribute244 = detail.Attribute244,
                        Attribute245 = detail.Attribute245,
                        Attribute246 = detail.Attribute246,
                        Attribute247 = detail.Attribute247,
                        Attribute249 = detail.Attribute249
                    };

                    if (trans != null && trans.Attribute != null)
                    {
                        attribute.FarmAssured = trans.Attribute.FarmAssured;
                    }

                    entities.Attributes.Add(attribute);
                    entities.SaveChanges();
                   
                    if (trans != null)
                    {
                        if (attribute.CarcassSide == 2 || detail.GradeWholeBeefAnimal)
                        {
                            trans.Consumed = DateTime.Now;
                            trans.TransactionWeight = detail.TotalCarcassWeight;
                            trans.UserMasterID = NouvemGlobal.UserId;
                            trans.DeviceMasterID = NouvemGlobal.DeviceId;
                            trans.Attribute.ColdWeight = detail.TotalCarcassColdWeight;
                            trans.Attribute.GradingDate = DateTime.Now;
                            trans.Attribute.Grade = detail.Grade;
                            trans.Attribute.Side2Weight = detail.TransactionWeight;
                            trans.Attribute.Generic2 = detail.Generic2;
                            trans.Attribute.fatColour = detail.FatColourName;
                            trans.Attribute.Identigen = detail.Identigen;
                            //trans.Attribute.Category = detail.CategoryID;
                            //trans.Attribute.Sex = detail.Sex;
                            trans.Attribute.FarmAssured = detail.FarmAssured;
                            trans.Attribute.RearedIn = detail.RearedIn;
                            //trans.INMasterID = detail.INMasterID;
                            trans.Attribute.Attribute244 = detail.Attribute244;
                            trans.Attribute.Attribute245 = detail.Attribute245;
                            trans.Attribute.Attribute246 = detail.Attribute246;
                            trans.Attribute.Attribute247 = detail.Attribute247;
                        }
                        else
                        {
                            trans.Attribute.Side1Weight = detail.TransactionWeight;
                            trans.Attribute.Grade = detail.Grade;
                            //trans.INMasterID = detail.INMasterID;
                        }

                        if (detail.TBYes.ToBool())
                        {
                            trans.Attribute.TBYes = true;
                        }

                        if (detail.Detained.ToBool())
                        {
                            trans.Attribute.Detained = true;
                        }

                        if (detail.Condemned.ToBool())
                        {
                            trans.Attribute.Condemned = true;
                        }

                        if (detail.BurstBelly.ToBool())
                        {
                            trans.Attribute.BurstBelly = true;
                        }

                        if (detail.NotInsured.ToBool())
                        {
                            trans.Attribute.NotInsured = true;
                        }

                        var newTrans = this.CreateDeepCopyTransaction(trans);
                        newTrans.AttributeID = attribute.AttributeID;
                        newTrans.TransactionWeight = detail.TransactionWeight;
                        newTrans.ManualWeight = detail.ManualWeight;
                        newTrans.Tare = detail.Tare;
                        newTrans.GrossWeight = detail.GrossWeight;
                        newTrans.TransactionDate = DateTime.Now;
                        newTrans.TransactionQTY = 1;
                        newTrans.Pieces = 1;

                        entities.StockTransactions.Add(newTrans);
                        entities.SaveChanges();
                        newTrans.Serial = newTrans.StockTransactionID;

                        if (attribute.CarcassSide == 1)
                        {
                            trans.Attribute.Label1 = newTrans.StockTransactionID;
                        }
                        else
                        {
                            trans.Attribute.Label2 = newTrans.StockTransactionID;
                        }

                        entities.SaveChanges();
                        detail.LabelID = newTrans.StockTransactionID;

                        if (!detail.KillDate.HasValue)
                        {
                            var intakeDetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == newTrans.MasterTableID);
                            if (intakeDetail != null)
                            {
                                var intake =
                                    entities.APGoodsReceipts.FirstOrDefault(
                                        x => x.APGoodsReceiptID == intakeDetail.APGoodsReceiptID);
                                if (intake != null)
                                {
                                    intake.KillDate = attribute.GradingDate;
                                    entities.SaveChanges();
                                    detail.KillDate = attribute.GradingDate;
                                }
                            }
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a grader carcass side weighing.
        /// </summary>
        /// <param name="detail">The carcass to add.</param>
        /// <returns>Flag, as to a successful add or not.</returns>
        public bool EditGraderTransaction(StockDetail detail)
        {
            var gradingDate = detail.GradingDate.ToDate().Date;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = (from stock in
                                         entities.StockTransactions
                                     join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                     where attribute.CarcassNumber == detail.CarcassNumber 
                                     && DbFunctions.TruncateTime(attribute.GradingDate) == gradingDate
                                     && attribute.CarcassSide == detail.CarcassSideNo
                                        && stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId && stock.Deleted == null && stock.IsBox == null
                                     select new { Attribute = attribute, Stock = stock }).FirstOrDefault();
                    if (dbAttribute == null)
                    {
                        return false;
                    }

                    var grade = dbAttribute.Attribute.Grade;
                    var wgt = dbAttribute.Stock.TransactionWeight;
                    var stockId = dbAttribute.Stock.StockTransactionID;
                    var carcassNo = dbAttribute.Attribute.CarcassNumber;

                    dbAttribute.Attribute.Breed = detail.BreedID;
                    dbAttribute.Attribute.fatColour = detail.FatColourName;
                    //dbAttribute.Attribute.Category = detail.CategoryID;
                    //dbAttribute.Attribute.Sex = detail.Sex;
                    dbAttribute.Attribute.Grade = detail.Grade;
                    dbAttribute.Attribute.FarmAssured = detail.FarmAssured;
                    dbAttribute.Attribute.HoldingNumber = detail.HoldingNumber;
                    dbAttribute.Attribute.Clipped = detail.Clipped;
                    dbAttribute.Attribute.Casualty = detail.Casualty;
                    dbAttribute.Attribute.CountryOfOrigin = detail.CountryOfOrigin;
                    dbAttribute.Attribute.Lame = detail.Lame;
                    dbAttribute.Attribute.Generic1 = detail.Generic1;
                    dbAttribute.Attribute.Cleanliness = detail.CleanlinessID;
                    dbAttribute.Attribute.Condemned = detail.Condemned;
                    dbAttribute.Attribute.Detained = detail.Detained;
                    dbAttribute.Attribute.NotInsured = detail.NotInsured;
                    dbAttribute.Attribute.TBYes = detail.TBYes;
                    dbAttribute.Attribute.BurstBelly = detail.BurstBelly;
                    dbAttribute.Attribute.ColdWeight = detail.ColdWeight;
                    dbAttribute.Attribute.Generic2 = detail.Generic2;
                    dbAttribute.Attribute.Customer = detail.CustomerID;
                    dbAttribute.Attribute.Identigen = detail.Identigen;
                    dbAttribute.Attribute.RearedIn = detail.RearedIn;
                    dbAttribute.Attribute.Attribute245 = detail.Attribute245;

                    dbAttribute.Stock.TransactionWeight = detail.TransactionWeight;
                    dbAttribute.Stock.GrossWeight = detail.GrossWeight;
                    dbAttribute.Stock.INMasterID = detail.INMasterID;

                    var trans =
                        (from stock in
                             entities.StockTransactions
                         join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                         where attribute.CarcassNumber == detail.CarcassNumber && attribute.Eartag == detail.Eartag 
                         && stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId && stock.Deleted == null && stock.IsBox == true
                         select new { Attribute = attribute, Stock = stock }).FirstOrDefault();

                    if (trans != null)
                    {
                        trans.Stock.Consumed = DateTime.Now;
                        trans.Stock.TransactionWeight = detail.TotalCarcassWeight;
                        trans.Stock.Attribute.ColdWeight = detail.TotalCarcassColdWeight;
                        trans.Stock.Attribute.Grade = detail.Grade;
                        trans.Stock.Attribute.Generic2 = detail.Generic2;
                        trans.Stock.Attribute.fatColour = detail.FatColourName;
                        trans.Stock.Attribute.Identigen = detail.Identigen;
                        //trans.Stock.Attribute.Category = detail.CategoryID;
                        //trans.Stock.Attribute.Sex = detail.Sex;
                        trans.Stock.Attribute.RearedIn = detail.RearedIn;
                        //trans.Stock.INMasterID = detail.INMasterID;

                        if (dbAttribute.Attribute.CarcassSide == 2)
                        {
                            trans.Stock.Attribute.Side2Weight = detail.TransactionWeight;
                        }
                        else
                        {
                            trans.Attribute.Side1Weight = detail.TransactionWeight;
                        }
                    }

                    var edit = new CarcassEdit
                    {
                        StockTransactionID = stockId,
                        CarcassNumber = carcassNo,
                        Grade = grade,
                        Weight = wgt,
                        EditDate = DateTime.Now,
                        DeviceID = NouvemGlobal.DeviceId,
                        UserMasterID = NouvemGlobal.UserId
                    };

                    entities.CarcassEdits.Add(edit);

                    entities.SaveChanges();
                    detail.LabelID = dbAttribute.Stock.StockTransactionID;
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a sheep record with the grading data.
        /// </summary>
        /// <param name="detail">The carcass to update.</param>
        /// <returns>Flag, as to a successful update or not.</returns>
        public bool UpdateGraderSheepTransaction(StockDetail detail)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == detail.AttributeID);
                    if (dbAttribute == null)
                    {
                        return false;
                    }

                    dbAttribute.SequencedDate = detail.SequencedDate;
                    dbAttribute.CarcassNumber = detail.CarcassNumber;
                    dbAttribute.KillNumber = detail.KillNumber;
                    dbAttribute.DOB = detail.DOB;
                    dbAttribute.SupplierID = detail.SupplierID;
                    dbAttribute.Breed = detail.BreedID;
                    dbAttribute.fatColour = detail.FatColourName;
                    dbAttribute.Category = detail.CategoryID;
                    dbAttribute.Sex = detail.Sex;
                    dbAttribute.Grade = detail.Grade;
                    dbAttribute.FarmAssured = detail.FarmAssured;
                    dbAttribute.HoldingNumber = detail.HoldingNumber;
                    dbAttribute.Clipped = detail.Clipped;
                    dbAttribute.Casualty = detail.Casualty;
                    dbAttribute.RearedIn = detail.RearedIn;
                    dbAttribute.CountryOfOrigin = detail.CountryOfOrigin;
                    dbAttribute.Lame = detail.Lame;
                    dbAttribute.Generic1 = detail.Generic1;
                    dbAttribute.Generic2 = detail.Generic2;
                    dbAttribute.Cleanliness = detail.CleanlinessID;
                    dbAttribute.GradingDate = DateTime.Now;
                    dbAttribute.Condemned = detail.Condemned;
                    dbAttribute.Detained = detail.Detained;
                    dbAttribute.NotInsured = detail.NotInsured;
                    dbAttribute.TBYes = detail.TBYes;
                    dbAttribute.BurstBelly = detail.BurstBelly;
                    dbAttribute.ColdWeight = detail.ColdWeight;
                    dbAttribute.Customer = detail.CustomerID;
                    dbAttribute.Identigen = detail.Identigen;
                    dbAttribute.Side1Weight = detail.TransactionWeight;
                    dbAttribute.Attribute245 = detail.Attribute245;
                    dbAttribute.Attribute246 = detail.Attribute246;
                    dbAttribute.Attribute247 = detail.Attribute247;

                    if (detail.Update)
                    {
                        dbAttribute.Eartag = detail.Eartag;
                    }

                    var dbTrans =
                        entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == detail.StockTransactionID);
                    if (dbTrans == null)
                    {
                        return false;
                    }

                    dbTrans.TransactionWeight = detail.TransactionWeight;
                    dbTrans.Tare = detail.Tare;
                    dbTrans.GrossWeight = detail.GrossWeight;
                    dbTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                    dbTrans.ManualWeight = detail.ManualWeight;

                    if (!detail.KillDate.HasValue)
                    {
                        var intakeDetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == detail.APGoodsReceiptDetailID);
                        if (intakeDetail != null)
                        {
                            var intake =
                                entities.APGoodsReceipts.FirstOrDefault(
                                    x => x.APGoodsReceiptID == intakeDetail.APGoodsReceiptID);
                            if (intake != null)
                            {
                                intake.KillDate = dbAttribute.GradingDate;
                                detail.KillDate = dbAttribute.GradingDate;
                            }
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a pig record with the grading data.
        /// </summary>
        /// <param name="detail">The carcass to update.</param>
        /// <returns>Flag, as to a successful update or not.</returns>
        public bool UpdateGraderPigTransaction(StockDetail detail)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttrribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == detail.AttributeID);
                    if (dbAttrribute == null)
                    {
                        return false;
                    }

                    dbAttrribute.CarcassNumber = detail.CarcassNumber;
                    dbAttrribute.KillNumber = detail.KillNumber;
                    dbAttrribute.SequencedDate = DateTime.Now;
                    dbAttrribute.Breed = detail.BreedID;
                    dbAttrribute.fatColour = detail.FatColourName;
                    dbAttrribute.Category = detail.CategoryID;
                    dbAttrribute.Sex = detail.Sex;
                    dbAttrribute.Grade = detail.Grade;
                    dbAttrribute.FarmAssured = detail.FarmAssured;
                    dbAttrribute.HoldingNumber = detail.HoldingNumber;
                    dbAttrribute.Clipped = detail.Clipped;
                    dbAttrribute.Casualty = detail.Casualty;
                    dbAttrribute.CountryOfOrigin = detail.CountryOfOrigin;
                    dbAttrribute.RearedIn = detail.RearedIn;
                    dbAttrribute.FarmAssured = detail.FarmAssured;
                    dbAttrribute.Lame = detail.Lame;
                    dbAttrribute.Generic1 = detail.Generic1;
                    dbAttrribute.Generic2 = detail.Generic2;
                    dbAttrribute.Cleanliness = detail.CleanlinessID;
                    dbAttrribute.GradingDate = DateTime.Now;
                    dbAttrribute.Condemned = detail.Condemned;
                    dbAttrribute.Detained = detail.Detained;
                    dbAttrribute.NotInsured = detail.NotInsured;
                    dbAttrribute.TBYes = detail.TBYes;
                    dbAttrribute.BurstBelly = detail.BurstBelly;
                    dbAttrribute.ColdWeight = detail.ColdWeight;
                    dbAttrribute.Customer = detail.CustomerID;
                    dbAttrribute.Identigen = detail.Identigen;
                    dbAttrribute.Side1Weight = detail.TransactionWeight;
                    dbAttrribute.SupplierID = detail.SupplierID;
                    dbAttrribute.Attribute245 = detail.Attribute245;
                    dbAttrribute.Attribute246 = detail.Attribute246;
                    dbAttrribute.Attribute247 = detail.Attribute247;

                    var dbTrans =
                        entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == detail.StockTransactionID);
                    if (dbTrans == null)
                    {
                        return false;
                    }

                    dbTrans.TransactionWeight = detail.TransactionWeight;
                    dbTrans.Tare = detail.Tare;
                    dbTrans.GrossWeight = detail.GrossWeight;
                    dbTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                    dbTrans.ManualWeight = detail.ManualWeight;

                    if (!detail.KillDate.HasValue)
                    {
                        var intakeDetail =
                            entities.APGoodsReceiptDetails.FirstOrDefault(
                                x => x.APGoodsReceiptDetailID == detail.APGoodsReceiptDetailID);
                        if (intakeDetail != null)
                        {
                            var intake =
                                entities.APGoodsReceipts.FirstOrDefault(
                                    x => x.APGoodsReceiptID == intakeDetail.APGoodsReceiptID);
                            if (intake != null)
                            {
                                intake.KillDate = dbAttrribute.GradingDate;
                                detail.KillDate = dbAttrribute.GradingDate;
                            }
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Changes the transaction weight.
        /// </summary>
        /// <param name="trans">The transaction to change.</param>
        /// <returns>Flag, as to successfull change or not.</returns>
        public Tuple<bool,KillType,string,string> ChangeTransactionWeight(StockTransaction trans)
        {
            var carcassNo = 0;
            DateTime gradingDate = DateTime.Now;
            var grade = string.Empty;
            decimal? wgt = 0;
            var killType = KillType.Beef;
            var reference = string.Empty;
            var reference2 = string.Empty;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTrans =
                        entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == trans.StockTransactionID);
                    if (dbTrans != null)
                    {
                        try
                        {
                            wgt = dbTrans.TransactionWeight;
                            dbTrans.TransactionWeight = trans.TransactionWeight;
                            dbTrans.GrossWeight = trans.GrossWeight;

                            if (dbTrans.AttributeID.HasValue)
                            {
                                var dbAttribute =
                                    entities.Attributes.FirstOrDefault(x => x.AttributeID == dbTrans.AttributeID);
                                if (dbAttribute != null)
                                {
                                    carcassNo = dbAttribute.CarcassNumber.ToInt();
                                    gradingDate = dbAttribute.GradingDate.ToDate();
                                    grade = dbAttribute.Grade;
                                    reference = dbAttribute.Attribute200;
                                    dbAttribute.ColdWeight = trans.TransactionWeight * .98m;
                                    trans.Reference = dbAttribute.AgeInMonths;
                                    reference2 = dbAttribute.Attribute238;
                                    if (dbAttribute.KillType == Constant.Sheep)
                                    {
                                        killType = KillType.Sheep;
                                    }
                                }
                            }

                            entities.SaveChanges();

                            var receiptDetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == trans.MasterTableID);
                            if (receiptDetail != null)
                            {
                                trans.BPMasterID = receiptDetail.APGoodsReceipt.BPMasterID_Supplier;
                            }

                            if (killType == KillType.Beef)
                            {
                                this.UpdateCarcassHeaderWeight(carcassNo, gradingDate.Date);
                            }

                            var edit = new CarcassEdit
                            {
                                StockTransactionID = trans.StockTransactionID,
                                CarcassNumber = carcassNo,
                                Grade = grade,
                                Weight = wgt,
                                EditDate = DateTime.Now,
                                DeviceID = NouvemGlobal.DeviceId,
                                UserMasterID = NouvemGlobal.UserId
                            };

                            entities.CarcassEdits.Add(edit);
                            entities.SaveChanges();
                            return Tuple.Create(true, killType, reference, reference2);
                        }
                        finally
                        {
                            if (killType == KillType.Beef)
                            {
                                this.UpdateCarcassWeight(carcassNo, killType.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(false, killType,reference,reference2);
        }

        /// <summary>
        /// Removes the identigen bottle number.
        /// </summary>
        /// <param name="identigen">The number to serach for.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool MinusScanIdentigen(string identigen)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttributes = entities.Attributes.Where(x => x.Identigen == identigen);
                    if (dbAttributes.Any())
                    {
                        dbAttributes.ToList().ForEach(x => x.Identigen = string.Empty);
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Determines if the identigen number has already been assigned..
        /// </summary>
        /// <param name="identigen">The number to serach for.</param>
        /// <returns>Carcass number if already assigned.</returns>
        public int HasIdentigenBeenAssigned(string identigen)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.Attributes.FirstOrDefault(x => x.Identigen == identigen);
                    if (dbAttribute != null)
                    {
                        return dbAttribute.CarcassNumber.ToInt();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Edits a carcss.
        /// </summary>
        /// <param name="stocktransactionId">The stock id.</param>
        /// <param name="carcassNo">The animal carcass no.</param>
        /// <param name="side1Wgt">It's side1 wgt.</param>
        /// <param name="side2Wgt">It's side2 wgt.</param>
        /// <param name="grade">The animal grade.</param>
        /// <param name="gradingDate">The kill date.</param>
        /// <param name="categoryId">The category.</param>
        /// <param name="farmAssured">The qa status.</param>
        /// <param name="rpa">The rpa status.</param>
        /// <param name="generic2">Generic 2 value.</param>
        /// <param name="killType">The kill type (beef,sheep,pig).</param>
        /// <param name="deviceId">The device id.</param>
        /// <param name="userId">The user editing.</param>
        /// <returns>Flag, as to successful edit or not.</returns>
        public bool EditCarcass(int deviceId, int userId, int? stocktransactionId = null, int? carcassNo = null, 
            decimal? side1Wgt = null, decimal? side2Wgt = null, string grade = "", DateTime? gradingDate = null, 
            int? categoryId = null, bool? farmAssured = null, bool? rpa = null, string generic2 = "", string killType = "")
        {
            try
            {
                using (var entities = new NouvemEntities())
                {

                    entities.App_EditCarcass(carcassNo, stocktransactionId, gradingDate, grade, categoryId,
                        rpa, generic2, side1Wgt, side2Wgt, farmAssured, killType, deviceId, userId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion

        #region private

        /// <summary>
        /// Creates a deep copy transaction.
        /// </summary>
        /// <param name="transaction">The transaction to deep copy</param>
        /// <returns>A deep copy transaction.</returns>
        private StockTransaction CreateDeepCopyTransaction(StockTransaction transaction)
        {
            return new StockTransaction
            {
                MasterTableID = transaction.MasterTableID,
                BatchNumberID = transaction.BatchNumberID,
                //Serial = transaction.Serial,
                TransactionDate = DateTime.Now,
                NouTransactionTypeID = transaction.NouTransactionTypeID,
                TransactionQTY = transaction.TransactionQTY,
                //TransactionWeight = transaction.TransactionWeight,
                //GrossWeight = transaction.GrossWeight,
                //Tare = transaction.Tare,
                //Pieces = transaction.Pieces,
                Alibi = transaction.Alibi,
                WarehouseID = transaction.WarehouseID,
                INMasterID = transaction.INMasterID,
                ContainerID = transaction.ContainerID,
                //Consumed = transaction.Consumed,
                ManualWeight = transaction.ManualWeight,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                //IsBox = null,
                InLocation = true,
                //StockTransactionID_Container = transaction.StockTransactionID_Container,
                //LabelID = transaction.LabelID,
                Reference = transaction.Reference,
                Deleted = transaction.Deleted,
                BPMasterID = transaction.BPMasterID,
                StoredLabelID = transaction.StoredLabelID
            };
        }

        private bool UpdateCarcassHeaderWeight(int carcassNo, DateTime gradingDate)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbCarcasses =
                        (from stock in
                            entities.StockTransactions
                            join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                            where attribute.CarcassNumber == carcassNo && DbFunctions.TruncateTime(attribute.GradingDate) == gradingDate && stock.Deleted == null
                                  && stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                            select new {Stock = stock, Attribute = attribute});

                    var hotWeight = dbCarcasses.Where(x => x.Stock.IsBox == null).Sum(x => x.Stock.TransactionWeight);
                    var coldWeight = hotWeight*0.98m;

                    var side1 = dbCarcasses.FirstOrDefault(x => x.Attribute.CarcassSide == 1);
                    var side2 = dbCarcasses.FirstOrDefault(x => x.Attribute.CarcassSide == 2);

                    var header = dbCarcasses.FirstOrDefault(x => x.Stock.IsBox == true);
                    if (header != null)
                    {
                        header.Stock.TransactionWeight = hotWeight;
                        header.Attribute.ColdWeight = coldWeight;
                        if (side1 != null)
                        {
                            header.Attribute.Side1Weight = side1.Stock.TransactionWeight;
                        }

                        if (side2 != null)
                        {
                            header.Attribute.Side2Weight = side2.Stock.TransactionWeight;
                        }

                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    
    public partial class App_GetTemplateRecordAttributes_Result
    {
        public Nullable<System.DateTime> Deleted { get; set; }
        public Nullable<int> AttributeTemplateRecordsID { get; set; }
        public Nullable<int> AttributeMasterID { get; set; }
        public Nullable<int> AttributeID_Parent { get; set; }
        public int AttributeWorkflowID { get; set; }
        public string Answer { get; set; }
        public Nullable<int> DeviceID { get; set; }
        public Nullable<int> UserMasterID { get; set; }
    }
}

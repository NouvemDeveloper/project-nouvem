//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class AttributeAllocation
    {
        public int AttributeAllocationID { get; set; }
        public int AttributeTemplateID { get; set; }
        public int AttributeMasterID { get; set; }
        public Nullable<int> AttributeTabNameID { get; set; }
        public Nullable<int> Sequence { get; set; }
        public string DefaultValueSQL { get; set; }
        public Nullable<bool> Batch { get; set; }
        public Nullable<bool> Transaction { get; set; }
        public bool RequiredBeforeContinue { get; set; }
        public Nullable<int> NouAttributeResetID { get; set; }
        public Nullable<bool> UseAsWorkflow { get; set; }
        public string LoadMacro { get; set; }
        public string PostSelectionMACRO { get; set; }
        public Nullable<int> AttributeAttachmentID { get; set; }
        public int UserMasterID { get; set; }
        public int DeviceID { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<System.DateTime> EditDate { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public bool RequiredBeforeCompletion { get; set; }
    
        public virtual AttributeTabName AttributeTabName { get; set; }
        public virtual AttributeTemplate AttributeTemplate { get; set; }
        public virtual NouAttributeReset NouAttributeReset { get; set; }
        public virtual AttributeMaster AttributeMaster { get; set; }
    }
}

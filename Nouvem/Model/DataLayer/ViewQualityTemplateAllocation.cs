//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class ViewQualityTemplateAllocation
    {
        public string Name { get; set; }
        public int QualityTemplateNameID { get; set; }
        public int QualityMasterID { get; set; }
        public string QualityCode { get; set; }
        public string QualityDescription { get; set; }
        public string QualityType { get; set; }
        public Nullable<bool> Batch { get; set; }
        public Nullable<bool> Transaction { get; set; }
        public Nullable<bool> Required { get; set; }
        public Nullable<bool> Reset { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class ARQuoteDetail
    {
        public int ARQuoteDetailID { get; set; }
        public int ARQuoteID { get; set; }
        public int INMasterID { get; set; }
        public int INMasterSnapshotID { get; set; }
        public Nullable<decimal> QuantityOrdered { get; set; }
        public Nullable<decimal> WeightOrdered { get; set; }
        public Nullable<decimal> QuantityDelivered { get; set; }
        public Nullable<decimal> WeightDelivered { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<decimal> DiscountPercentage { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<decimal> DiscountPrice { get; set; }
        public Nullable<decimal> LineDiscountPercentage { get; set; }
        public Nullable<decimal> LineDiscountAmount { get; set; }
        public Nullable<decimal> UnitPriceAfterDiscount { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> TotalIncVAT { get; set; }
        public Nullable<decimal> TotalExclVAT { get; set; }
        public Nullable<int> VATCodeID { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public Nullable<int> PriceListID { get; set; }
        public string Notes { get; set; }
    
        public virtual ARQuote ARQuote { get; set; }
        public virtual VATCode VATCode { get; set; }
        public virtual INMasterSnapshot INMasterSnapshot { get; set; }
        public virtual INMaster INMaster { get; set; }
        public virtual PriceList PriceList { get; set; }
    }
}

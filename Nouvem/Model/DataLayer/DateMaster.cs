//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class DateMaster
    {
        public DateMaster()
        {
            this.DateTemplateAllocations = new HashSet<DateTemplateAllocation>();
            this.BatchTraceabilities = new HashSet<BatchTraceability>();
            this.TransactionTraceabilities = new HashSet<TransactionTraceability>();
            this.APOrders = new HashSet<APOrder>();
            this.APQuotes = new HashSet<APQuote>();
            this.AROrders = new HashSet<AROrder>();
            this.ARQuotes = new HashSet<ARQuote>();
            this.DateMaster1 = new HashSet<DateMaster>();
            this.DateDays = new HashSet<DateDay>();
        }
    
        public int DateMasterID { get; set; }
        public int NouDateTypeID { get; set; }
        public string DateCode { get; set; }
        public string DateDescription { get; set; }
        public Nullable<int> GS1AIMasterID { get; set; }
        public bool Deleted { get; set; }
        public Nullable<int> DateMasterID_BasedOn { get; set; }
    
        public virtual NouDateType NouDateType { get; set; }
        public virtual ICollection<DateTemplateAllocation> DateTemplateAllocations { get; set; }
        public virtual ICollection<BatchTraceability> BatchTraceabilities { get; set; }
        public virtual ICollection<TransactionTraceability> TransactionTraceabilities { get; set; }
        public virtual ICollection<APOrder> APOrders { get; set; }
        public virtual ICollection<APQuote> APQuotes { get; set; }
        public virtual ICollection<AROrder> AROrders { get; set; }
        public virtual ICollection<ARQuote> ARQuotes { get; set; }
        public virtual ICollection<DateMaster> DateMaster1 { get; set; }
        public virtual DateMaster DateMaster2 { get; set; }
        public virtual ICollection<DateDay> DateDays { get; set; }
    }
}

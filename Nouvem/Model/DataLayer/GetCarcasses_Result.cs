//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    
    public partial class GetCarcasses_Result
    {
        public Nullable<System.DateTime> KillDate { get; set; }
        public Nullable<long> KillNumber { get; set; }
        public Nullable<decimal> EstimatedColdWeight { get; set; }
        public string Eartag { get; set; }
        public string Grade { get; set; }
        public string Breed { get; set; }
        public string Herd { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string GroupDescription { get; set; }
        public long id { get; set; }
    }
}

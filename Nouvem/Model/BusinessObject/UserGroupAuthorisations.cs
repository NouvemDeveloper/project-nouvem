﻿// -----------------------------------------------------------------------
// <copyright file="UserGroupAuthorisations.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.Collections.Generic;
    using Nouvem.Model.DataLayer;

    /// <summary>
    /// Class that models the user group rules.
    /// </summary>
    public class UserGroupAuthorisations
    {
        /// <summary>
        /// Gets or sets the user group.
        /// </summary>
        public UserGroup_ UserGroup { get; set; }

        /// <summary>
        /// Gets or sets the associated collection of rules.
        /// </summary>
        public IList<ViewGroupRule> Rules { get; set; }


    }
}

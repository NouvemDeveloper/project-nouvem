﻿// -----------------------------------------------------------------------
// <copyright file="TareCalculator.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.ComponentModel;

    public class TareCalculator : INotifyPropertyChanged
    {
        /// <summary>
        /// The total line tare.
        /// </summary>
        private double totalLineTare;

        /// <summary>
        /// The total line qty.
        /// </summary>
        private int quantity;

        /// <summary>
        /// Gets or sets the total tare.
        /// </summary>
        public static double TotalTare { get; set; }

        /// <summary>
        /// Gets or sets the container id.
        /// </summary>
        public int ContainerID { get; set; }

        /// <summary>
        /// Gets or sets the container product id.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the tare qty.
        /// </summary>
        public int Quantity
        {
            get
            {
                return this.quantity;
            }

            set
            {
                this.quantity = value;
                this.OnPropertyChanged("Quantity");
            }
        }

        /// <summary>
        /// Gets or sets the container name.
        /// </summary>
        public string ContainerName { get; set; }

        /// <summary>
        /// Gets or sets the container tare.
        /// </summary>
        public double Tare { get; set; }

        /// <summary>
        /// Gets or sets the total tare.
        /// </summary>
        public double TotalLineTare
        {
            get
            {
                return this.totalLineTare;
            }

            set
            {
                this.totalLineTare = value;
                this.OnPropertyChanged("TotalLineTare");
            }
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

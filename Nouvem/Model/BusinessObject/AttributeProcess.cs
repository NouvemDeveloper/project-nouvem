﻿// -----------------------------------------------------------------------
// <copyright file="AttributeProcess.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;

namespace Nouvem.Model.BusinessObject
{
    public class AttributeProcess
    {
        public enum ProcessType
        {
            /// <summary>
            /// Is it a visible type.
            /// </summary>
            Visible,

            /// <summary>
            /// Is it an editable type.
            /// </summary>
            Editable
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public ProcessType Type { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int AttributeProcessId { get; set; }

        /// <summary>
        /// Gets or sets the allocation id.
        /// </summary>
        public int AttributeAllocationId { get; set; }

        /// <summary>
        /// Gets or sets the process.
        /// </summary>
        public Process Process { get; set; }

        /// <summary>
        /// Gets or sets the process id.
        /// </summary>
        public int ProcessId
        {
            get
            {
                return this.Process != null ? this.Process.ProcessID : 0;
            }
        }

        /// <summary>
        /// Gets or sets the process name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.Process != null ? this.Process.Name : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the process is visible.
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the process is editable.
        /// </summary>
        public bool Editable { get; set; }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="CleanlinessRating.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    /// <summary>
    /// Class that models the cleanliness of the animal.
    /// </summary>
    public class CleanlinessRating
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CleanlinessRating"/> class.
        /// </summary>
        protected CleanlinessRating()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets the id of the cleanliness rating.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets the description of the cleanliness rating.
        /// </summary>
        public string Description { get; set; }

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="CleanlinessRating"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="CleanlinessRating"/> object.</returns>
        public static CleanlinessRating CreateNewCleanlinessRating()
        {
            return new CleanlinessRating();
        }

        /// <summary>
        /// Create a new <see cref="CleanlinessRating"/> with specific parameters.
        /// </summary>
        /// <param name="id">The id of the cleanliness rating.</param>
        /// <param name="description">The description of the cleanliness rating.</param>
        /// <returns>A new <see cref="CleanlinessRating"/> object populated with the values passed.</returns>
        public static CleanlinessRating CreateCleanlinessRating(int id, string description)
        {
            return new CleanlinessRating()
            {
                Id = id,
                Description = description
            };
        }

        #endregion

        #endregion
    }
}

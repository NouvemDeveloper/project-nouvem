﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class ProductionBatchData
    {
        public int? BatchId { get; set; }
        public int? CurrentBatchId { get; set; }
        public string BatchNo { get; set; }
        public string CurrentBatchNo { get; set; }
        public decimal Remaining { get; set; }
        public decimal CurrentRemaining { get; set; }
    }
}

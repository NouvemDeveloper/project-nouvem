﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Model.DataLayer;

namespace Nouvem.Model.BusinessObject
{
    public class AttributeSetUp
    {
        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public AttributeMaster AttributeMaster { get; set; }

        /// <summary>
        /// Gets or sets the attribute look up name.
        /// </summary>
        public string AttributeLookUpName { get; set; }

        /// <summary>
        /// Gets or sets the user alerts.
        /// </summary>
        public IList<UserAlert> UserAlerts { get; set; }

        /// <summary>
        /// Gets or sets the user group alerts.
        /// </summary>
        public IList<UserGroupAlert> UserGroupAlerts { get; set; }

        /// <summary>
        /// Gets or sets the user alerts.
        /// </summary>
        public IList<UserAttributePermission> UserPermissions { get; set; }

        /// <summary>
        /// Gets or sets the user group alerts.
        /// </summary>
        public IList<UserGroupAttributePermission> UserGroupPermissions { get; set; }

        /// <summary>
        /// Gets or sets the user groups for non standard responses handling.
        /// </summary>
        public IList<UserGroupNonStandard> UserGroupsNonStandard { get; set; }

        /// <summary>
        /// Gets or sets the user groups for non standard responses handling.
        /// </summary>
        public IList<AttributeAttachment> AttributeAttachments { get; set; }

        /// <summary>
        /// Gets or sets the user alerts.
        /// </summary>
        public IList<string> AttributeTemplates { get; set; }
    }
}

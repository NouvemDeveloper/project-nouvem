﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Model.DataLayer;

namespace Nouvem.Model.BusinessObject
{
    public class CarcassSideData
    {
        public int StocktransactionId { get; set; }
        public int? MasterTableId { get; set; }
        public decimal? TransactionWeight { get; set; }
        public int? Serial { get; set; }
        public DateTime TransactionDate { get; set; }
        public bool? IsBox { get; set; }
        public int? SupplierId { get; set; }
        public Model.DataLayer.Attribute Attribute { get; set; }
        public NouCategory Category { get; set; }
        public NouBreed Breed { get; set; }
    }
}

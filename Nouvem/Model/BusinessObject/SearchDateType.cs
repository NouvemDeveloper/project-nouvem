﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    public class SearchDateType : INotifyPropertyChanged
    {
        private bool isSelected;
        public int NouSearchDateTypeID { get; set; }
        public string DateType { get; set; }
        public DateTime? Deleted { get; set; }

        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
                Messenger.Default.Send(Tuple.Create(this.NouSearchDateTypeID, value), Token.SearchDateSelected);
            }
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SpecialPrice.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.ComponentModel;

    public class SpecialPrice : INotifyPropertyChanged
    {
        /// <summary>
        /// The group id.
        /// </summary>
        private int? bpGroupId;

        /// <summary>
        /// The master id.
        /// </summary>
        private int? bpMasterId;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialPrice"/> class.
        /// </summary>
        public SpecialPrice()
        {
            this.StartDate = DateTime.Today;
            this.EndDate = DateTime.Today;
        }

        /// <summary>
        /// Gets or sets the special price id.
        /// </summary>
        public int SpecialPriceID { get; set; }

        /// <summary>
        /// Gets or sets the special price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the in master id.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the bp master id.
        /// </summary>
        public int? BPMasterID
        {
            get
            {
                return this.bpMasterId;
            }

            set
            {
                this.bpMasterId = value;
                this.OnPropertyChanged("BPMasterID");
            }
        }

        /// <summary>
        /// Gets or sets the in master id.
        /// </summary>
        public int? BPGroupID
        {
            get
            {
                return this.bpGroupId;
            }

            set
            {
                this.bpGroupId = value;
                this.OnPropertyChanged("BPGroupID");
            }
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime Deleted { get; set; }

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}

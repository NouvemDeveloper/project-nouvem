﻿// -----------------------------------------------------------------------
// <copyright file="StockTake.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.BusinessObject
{
    using System;

    public class StockTake
    {
        /// <summary>
        /// Gets or sets the stock take id.
        /// </summary>
        public int StockTakeID { get; set; }

        /// <summary>
        /// Gets or sets the stock transaction id.
        /// </summary>
        public int? StockTransactionID { get; set; }

        /// <summary>
        /// Gets or sets the allowable locations.
        /// </summary>
        public string Locations { get; set; }

        /// <summary>
        /// Gets or sets the stock take creation date.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the stock take description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the stock take warehouse id.
        /// </summary>
        public int? WarehouseID { get; set; }

        /// <summary>
        /// Gets or sets the doc ststus id.
        /// </summary>
        public int? NouDocStatusID { get; set; }

        /// <summary>
        /// Gets or sets the stock take deletion date.
        /// </summary>
        public DateTime Deleted { get; set; }

        /// <summary>
        /// Gets or sets the stock take end time.
        /// </summary>
        public DateTime CutOffDate { get; set; }

        /// <summary>
        /// Gets the stock take status.
        /// </summary>
        public string Status
        {
            get
            {
                return this.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                    ? Constant.Active
                    : this.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                        ? Constant.InProgress
                        : Strings.Complete;
            }
        }

        /// <summary>
        /// Gets or sets the associated stock take detail records.
        /// </summary>
        public IList<StockTakeData> StockTakeDetails { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class CompleteOrderCheck
    {
        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public int ARDispatchDetailId { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public decimal? QuantityDelivered { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public decimal? WeightDelivered { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public int? NouPriceMethodId { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public decimal? NominalWeight { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public int? PriceListId { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public int INMasterId { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public decimal? TotalExVat { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public decimal? TotalIncVat { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public decimal? UnitPriceAfterDiscount { get; set; }
    }
}

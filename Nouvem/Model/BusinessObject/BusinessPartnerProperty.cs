﻿// -----------------------------------------------------------------------
// <copyright file="BusinessPartnerProperty.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;

    /// <summary>
    /// Models the business partner properties.
    /// </summary>
    public class BusinessPartnerProperty 
    {
        /// <summary>
        /// Gets or sets the property details
        /// </summary>
        public BPProperty Details { get; set; }

        /// <summary>
        /// Gets or set the id.
        /// </summary>
        public int BPPropertyID { get; set; }

        /// <summary>
        /// Gets or sets the property message.
        /// </summary>
        public string DisplayMessage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the property is selected.
        /// </summary>
        public bool IsSelected { get; set; }

        public void CreateProperty()
        {
            this.Details = new BPProperty {BPPropertyID = this.BPPropertyID};
        }
    }
}

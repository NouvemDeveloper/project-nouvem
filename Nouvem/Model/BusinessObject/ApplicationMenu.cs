﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationMenu.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Class which models the application menu.
    /// </summary>
    public class ApplicationMenu
    {
        private ApplicationMenu sample;

        private string identifier;

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public ApplicationMenu SelectedNode
        {
            get
            {
                return this.sample;
            }

            set
            {
                this.sample = value;
            }
        }


        /// <summary>
        /// Gets or sets the display name for the node.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the display name for the node.
        /// </summary>
        public string TreeNodeText { get; set; }

        /// <summary>
        /// The report name.
        /// </summary>
        public bool? IsHistoric { get; set; }

        /// <summary>
        /// Gets or sets the node identifier.
        /// </summary>
        public string Identifier
        {
            get
            {
                if (string.IsNullOrEmpty(this.identifier))
                {
                    return this.TreeNodeText;
                }

                return this.identifier;
            }

            set
            {
                this.identifier = value;
            }
        }

        /// <summary>
        /// Gets or sets the image to appear.
        /// </summary>
        public BitmapImage DisplayImage { get; set; }
     
        /// <summary>
        /// Gets or sets the node indent value.
        /// </summary>
        public Thickness Indent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current node is a report.
        /// </summary>
        public bool IsReport { get; set; }

        /// <summary>
        /// Gets or sets the path of the report.
        /// </summary>
        public string ReportPath { get; set; }

        /// <summary>
        /// Gets or sets the self referential application menu nodes.
        /// </summary>
        public ObservableCollection<ApplicationMenu> ApplicationMenuNodes { get; set; } = new ObservableCollection<ApplicationMenu>();

        /// <summary>
        /// Gets or sets the 'pressed node' command.
        /// </summary>
        public ICommand Command { get; set; }

        /// <summary>
        /// Gets or sets the 'pressed node' command parameter.
        /// </summary>
        public string CommandParameter { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class IntakeLabel
    {
        public string TransactionWeight { get; set; }
        public string TransactionQuantity { get; set; }
        public string Serial { get; set; }
    }
}

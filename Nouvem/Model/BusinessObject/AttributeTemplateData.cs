﻿// -----------------------------------------------------------------------
// <copyright file="AttributeTemplateData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    public class AttributeTemplateData
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int AttributeTemplateID { get; set; }

        /// <summary>
        /// Gets or sets the group id.
        /// </summary>
        public int? AttributeTemplateGroupID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public bool IsWorkflow { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public bool? Inactive { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string AttributeType { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public bool IsNew { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserMasterID { get; set; }

        /// <summary>
        /// Gets or sets the device id.
        /// </summary>
        public int? DeviceID { get; set; }
    }
}


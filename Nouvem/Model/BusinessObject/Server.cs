﻿// -----------------------------------------------------------------------
// <copyright file="Server.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    public class Server
    {
        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public int ConnectionNo { get; set; }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the server default database.
        /// </summary>
        public string DefaultDatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the server user id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the server password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the server password.
        /// </summary>
        public string EncryptedValue { get; set; } = "####";

        /// <summary>
        /// Gets or sets the server password.
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether windows authentication is being used.
        /// </summary>
        public bool IntegratedSecurity
        {
            get
            {
                return string.IsNullOrEmpty(UserId) && string.IsNullOrEmpty(Password);
            }
        }

        /// <summary>
        /// Gets the db credentials.
        /// </summary>
        public string Credentials
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(this.Password))
                {
                    // sql server authentication
                    return string.Join(
                        "|",
                        this.CompanyName,
                        this.ServerName,
                        this.DatabaseName,
                        this.UserId,
                        this.Password);
                }
                else
                {
                    // windows authentication
                    return string.Join("|", this.CompanyName, this.ServerName, this.DatabaseName);
                }
            }
        }
    }
}

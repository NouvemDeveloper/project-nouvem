﻿// -----------------------------------------------------------------------
// <copyright file="ProductStockData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;
    using Nouvem.Shared;

    public class ProductStockData
    {
        /// <summary>
        /// Gets or sets the product stock.
        /// </summary>
        public App_GetStockByProduct_Result ProductStock { get; set; }

        /// <summary>
        /// Gets or sets the committed stock qty asmounts.
        /// </summary>
        public double? CommittedQty { get; set; }

        /// <summary>
        /// Gets or sets the committed stock wgt amounts.
        /// </summary>
        public double? CommittedWgt { get; set; }

        /// <summary>
        /// Gets or sets the available stock qty amounts.
        /// </summary>
        public decimal? AvailableQty { get; set; }

        /// <summary>
        /// Gets or sets the available stock wgt amounts.
        /// </summary>
        public decimal? AvailableWgt { get; set; }

        public DateTime ReferenceDate { get; set; }
        public  decimal? AllocatedWgt { get; set; }
        public decimal? AllocatedQty { get; set; }
        public  decimal? DueInWgt { get; set; }
        public decimal? DueInQty{ get; set; }
        public int OrderNo { get; set; }
        public int OrderId { get; set; }

        public decimal? OpeningWgt { get; set; }
        public decimal? OpeningQty { get; set; }
        public decimal? ClosingWgt { get; set; }
        public decimal? ClosingQty { get; set; }
        public string Error { get; set; }
        public string OrderType { get; set; }
        public string Product { get; set; }
        public string Code{ get; set; }
        public InventoryItem ProductData { get; set; }

        public string MRPDate
        {
            get { return this.ReferenceDate == DateTime.Today ? Strings.Today : this.ReferenceDate.ToShortDateString(); }
        }

        /// <summary>
        /// Gets or sets the committed stock qty asmounts.
        /// </summary>
        public static decimal? CommittedStockQty { get; set; }

        /// <summary>
        /// Gets or sets the committed stock wgt amounts.
        /// </summary>
        public static decimal? CommittedStockWgt { get; set; }

        /// <summary>
        /// Gets or sets the stock qty asmounts.
        /// </summary>
        public static decimal StockQty { get; set; }

        /// <summary>
        /// Gets or sets the stock wgt amounts.
        /// </summary>
        public static decimal StockWgt { get; set; }

        /// <summary>
        /// Gets or sets the stock wgt amounts.
        /// </summary>
        public static string Batch { get; set; }
    }
}

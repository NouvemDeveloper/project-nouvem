﻿// -----------------------------------------------------------------------
// <copyright file="NouCountry.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    using System.ComponentModel;

    public class NouCountry : INotifyPropertyChanged
    {
        /// <summary>
        /// The group id.
        /// </summary>
        public int CountryID { get; set; }

        /// <summary>
        /// The group description.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The group deleted date.
        /// </summary>
        public bool BornIn { get; set; }

        /// <summary>
        /// The group deleted date.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }
      
        /// <summary>
        /// The selected falg.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The selected flag.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
                Messenger.Default.Send(Tuple.Create(this.CountryID, value, this.BornIn), Token.CountryItemSelected);
            }
        }

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event raise.
        /// </summary>
        /// <param name="propertyName">The property whose value will be broadcast.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}



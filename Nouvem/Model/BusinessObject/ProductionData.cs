﻿// -----------------------------------------------------------------------
// <copyright file="ProductionData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using System.Reflection;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.DataLayer;

    public class ProductionData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductionData"/> class.
        /// </summary>
        public ProductionData()
        {
            this.Order = new PROrder();
            this.OutputsSpec = new List<PRSpecOutput>();
            this.Outputs = new List<PROrderOutput>();
            this.CarcassTypesSpec = new List<PRInputCarcassType>();
            this.CountriesSpec = new List<PRInputCountry>();
            this.PlantsSpec = new List<PRInputPlant>();
            this.Countries = new List<PRInputCountryOrder>();
            this.Plants = new List<PRInputPlantOrder>();
            this.CarcassTypes = new List<PRInputCarcassTypeOrder>();
            this.ProductionDetails = new List<SaleDetail>();
        }

        /// <summary>
        /// Gets or sets the production order.
        /// </summary>
        public PROrder Order { get; set; }

        /// <summary>
        /// Gets or sets the associated goods in detail. (If in Recall Production mode)
        /// </summary>
        public IList<APGoodsReceiptDetail> GoodsInDetails { get; set; }

        /// <summary>
        /// Gets or sets the spec.
        /// </summary>
        public PRSpec Specification { get; set; }

        /// <summary>
        /// Gets or sets the production type.
        /// </summary>
        public string ProductionType { get; set; }

        /// <summary>
        /// Gets or sets the production type.
        /// </summary>
        public string OrderMethod { get; set; }

        /// <summary>
        /// Gets or sets the associated batch number.
        /// </summary>
        public BatchNumber BatchNumber { get; set; }

        /// <summary>
        /// Gets or sets the associated batch traceabilities.
        /// </summary>
        public IList<BatchTraceability> BatchTraceabilities { get; set; }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public BPContact BPContact { get; set; }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public bool AutoCompleteRecipeMix
        {
            get { return this.Order != null && this.Order.AutoCompleteMix == true; }
        }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public bool MultiReceipts
        {
            get { return this.Order != null && this.Order.MultipleReceipts == true; }
        }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public bool ReworkStockNonAdjust
        {
            get { return this.Order != null && this.Order.ReworkNonAdjust == true; }
        }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public string Data
        {
            get { return $"{this.BatchProduct}  {this.RemainingWgt}\\{this.RemainingQty}  Batch:{this.Order.Reference}"; }
        }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public int WarehouseId { get; set; }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public int WarehouseID
        {
            get { return this.Generic4.ToInt(); }
        }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        ///public StockDetail LastBatchAttribute { get; set; }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public bool ProductionAttributesSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a mix is ready to be completed.
        /// </summary>
        public bool MixReadyForCompletion { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are using this for stock movement.
        /// </summary>
        public bool IsStockBatch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are using this for stock movement.
        /// </summary>
        public bool IsIntakeBatch { get; set; }

        /// <summary>
        /// Gets or sets the associated customer.
        /// </summary>
        public BPMaster BPCustomer { get; set; }

        /// <summary>
        /// Gets or sets the associated doc status.
        /// </summary>
        public NouDocStatu DocStatus { get; set; }

        /// <summary>
        /// Gets or sets the associated doc status.
        /// </summary>
        public int? BatchProductId { get; set; }

        /// <summary>
        /// Gets or sets the associated doc status.
        /// </summary>
        public int MixCount { get; set; }

        /// <summary>
        /// Gets or sets the associated document type.
        /// </summary>
        public DocumentNumberingType DocumentNumberingType { get; set; }

        /// <summary>
        /// Gets or sets the associated carcass types
        /// </summary>
        public IList<PRInputCarcassTypeOrder> CarcassTypes { get; set; }

        /// <summary>
        /// Gets or sets the associated confirmation scores.
        /// </summary>
        public IList<PRInputConfirmationOrder> Confirmations { get; set; }

        /// <summary>
        /// Gets or sets the associated fat scores.
        /// </summary>
        public IList<PRInputFatScoreOrder> FatScores { get; set; }

        /// <summary>
        /// Gets or sets the associated outputs.
        /// </summary>
        public IList<PROrderOutput> Outputs { get; set; }

        /// <summary>
        /// Gets or sets the associated inputs.
        /// </summary>
        public IList<PROrderInput> Inputs { get; set; }

        /// <summary>
        /// Gets or sets the associated spec carcass types
        /// </summary>
        public IList<PRInputCountry> CountriesSpec { get; set; }

        /// <summary>
        /// Gets or sets the associated spec carcass types
        /// </summary>
        public IList<PRInputCountryOrder> Countries { get; set; }

        /// <summary>
        /// Gets or sets the associated spec carcass types
        /// </summary>
        public IList<PRInputPlant> PlantsSpec { get; set; }

        /// <summary>
        /// Gets or sets the associated spec carcass types
        /// </summary>
        public IList<PRInputPlantOrder> Plants { get; set; }

        /// <summary>
        /// Gets or sets the associated spec carcass types
        /// </summary>
        public IList<PRInputCarcassType> CarcassTypesSpec { get; set; }

        /// <summary>
        /// Gets or sets the associated spec confirmation scores.
        /// </summary>
        public IList<PRInputConfirmation> ConfirmationsSpec { get; set; }

        /// <summary>
        /// Gets or sets the associated spec fat scores.
        /// </summary>
        public IList<PRInputFatScore> FatScoresSpec { get; set; }

        /// <summary>
        /// Gets or sets the associated spec outputs.
        /// </summary>
        public IList<PRSpecOutput> OutputsSpec { get; set; }

        /// <summary>
        /// Gets or sets the associated transactions.
        /// </summary>
        public IList<SaleDetail> ProductionDetails { get; set; }

        /// <summary>
        /// Gets or sets the product batch wgt.
        /// </summary>
        public decimal? IntakeWeight { get; set; }

        /// <summary>
        /// Gets or sets the product batch qty.
        /// </summary>
        public decimal? IntakeQty { get; set; }

        /// <summary>
        /// Gets or sets the product batch wgt.
        /// </summary>
        public decimal? BatchWeight { get; set; }
        
        /// <summary>
        /// Gets or sets the product batch qty.
        /// </summary>
        public decimal? BatchQty { get; set; }

        public decimal RemainingWgt { get; set; }

        public decimal RemainingQty { get; set; }

        public decimal BatchStockRemaining
        {
            get
            {
                if (this.Generic7.CompareIgnoringCase("Batch Qty") || this.Generic7.CompareIgnoringCase("Product Qty"))
                {
                    return this.RemainingQty;
                }

                return this.RemainingWgt;
            }
        }

        public string WeightData
        {
            get
            {
                if (this.Generic7.CompareIgnoringCase("Batch Qty") || this.Generic7.CompareIgnoringCase("Product Qty"))
                {
                    return $"{this.RemainingQty}/{this.IntakeQty}";
                }

                return $"{this.RemainingWgt}/{this.IntakeWeight}";
            }
        }

        public DateTime OrderDate
        {
            get { return this.Generic1.ToDate(); }
        }

        public int Number
        {
            get { return this.Order != null ? this.Order.Number : 0; }
        }
       
        /// <summary>
        /// Gets or sets the product batch qty.
        /// </summary>
        public int? BatchQuantity { get; set; }

        /// <summary>
        /// Gets or sets the sub location.
        /// </summary>
        public int? SubLocationId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse.
        /// </summary>
        public string Warehouse { get; set; }

        /// <summary>
        /// Gets or sets the sub location.
        /// </summary>
        public string SubLocation { get; set; }

        /// <summary>
        /// Gets the full location.
        /// </summary>
        public string Location
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.SubLocation))
                {
                    return this.Warehouse;
                }

                return string.Format("{0} - {1}", this.Warehouse, this.SubLocation);
            }
        }

        /// <summary>
        /// Gets or sets the specification name.
        /// </summary>
        public string SpecificationName
        {
            get
            {
                return this.Specification != null ? this.Specification.Name : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public string Reference
        {
            get
            {
                return this.Order != null ? this.Order.Reference : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the batch no.
        /// </summary>
        public string BatchNo
        {
            get
            {
                return this.BatchNumber != null ? this.BatchNumber.Number : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the batch no.
        /// </summary>
        public int? BatchID
        {
            get
            {
                return this.BatchNumber != null ? this.BatchNumber.BatchNumberID : (int?)null;
            }
        }

        /// <summary>
        /// Gets or sets the specification name.
        /// </summary>
        public string Spec
        {
            get
            {
                return string.Format("{0} - {1}", this.SpecificationName, this.Reference);
            }
        }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public string OrderID
        {
            get
            {
                return this.Order != null ? this.Order.PROrderID.ToString() : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public decimal? BatchWeightDisplay
        {
            get { return this.Order?.BatchWeight; }
        }

        /// <summary>
        /// Gets or sets the specification name.
        /// </summary>
        public int DocumentNo
        {
            get
            {
                return this.Order.Number;
            }
        }

        /// <summary>
        /// Gets or sets the release date
        /// </summary>
        public DateTime? ReleaseDate
        {
            get
            {
                return this.Order.ReleaseDateTime;
            }
        }

        /// <summary>
        /// Gets or sets the release date
        /// </summary>
        public DateTime? ScheduledDate
        {
            get
            {
                return this.Order.ScheduledDate;
            }
        }

        /// <summary>
        /// Gets or sets the release date
        /// </summary>
        public string ProductionDate
        {
            get
            {
                return this.Order.ScheduledDate.HasValue ? this.Order.ScheduledDate.ToDate().ToShortDateString() : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        public DateTime CreationDate
        {
            get
            {
                return this.Order.CreationDate;
            }
        }

        public int? DepartmentID
        {
            get
            {
                if (this.Order != null && this.Order.INMasterID.HasValue)
                {
                    var product =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.Order.INMasterID);
                    if (product != null)
                    {
                        return product.Master.DepartmentID;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the kill date.
        /// </summary>
        public DateTime? KillDate { get; set; }

        /// <summary>
        /// Gets or sets the born in country.
        /// </summary>
        public string BornIn { get; set; }

        /// <summary>
        /// Gets or sets the reared in country.
        /// </summary>
        public string RearedIn { get; set; }

        /// <summary>
        /// Gets or sets the slaughtered in in plant.
        /// </summary>
        public string SlaughteredIn { get; set; }

        /// <summary>
        /// Gets or sets the cut in plant.
        /// </summary>
        public string CutIn { get; set; }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public string Customer
        {
            get
            {
                return this.BPCustomer != null ? this.BPCustomer.Name : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the contact.
        /// </summary>
        public string Contact
        {
            get
            {
                return this.BPContact != null ? string.Format("{0} {1}", this.BPContact.FirstName, this.BPContact.LastName) : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        public int? INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the intake product.
        /// </summary>
        public string Product
        {
            get
            {
                if (this.INMasterID.HasValue)
                {
                    var localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.INMasterID);
                    if (localProduct != null)
                    {
                        return localProduct.Name;
                    }
                }

                if (this.Order.INMasterID.HasValue)
                {
                    var localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.Order.INMasterID);
                    if (localProduct != null)
                    {
                        return localProduct.Name;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string PartnerName { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string BatchProduct { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic1 { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic2 { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic3 { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic4 { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic5 { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic6 { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic7 { get; set; }

        /// <summary>
        /// Gets or sets the batch product.
        /// </summary>
        public string Generic8 { get; set; }

        public int? RecipeBatchId
        {
            get { return this.Generic3.Equals("Intake") ? this.Order.PROrderID_Base : this.Order.PROrderID; }
        }

        /// <summary>
        /// Gets the intake card data to display.
        /// </summary>
        public string DisplayData
        {
            get
            {
                try
                {
                    string[] properties;
                    if (this.IsStockBatch)
                    {
                        properties = ApplicationSettings.DisplayStockProductionData.Split(',');
                    }
                    else if (this.IsIntakeBatch)
                    {
                        properties = ApplicationSettings.DisplayProductionBatchData.Split(',');
                    }
                    else
                    {
                        properties = ApplicationSettings.DisplayProductionData.Split(',');
                    }
                      
                    var data = this.SetDisplayCardData(properties);
                    data = data.Remove(data.LastIndexOf(Environment.NewLine), 1);
                    return data;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Set the card display data.
        /// </summary>
        /// <param name="data">The stored property fields to display values for.</param>
        /// <returns>The card display data.</returns>
        public string SetDisplayCardData(string[] data)
        {
            var displayData = string.Empty;
            var typeSource = this.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var type in data)
            {
                foreach (var property in propertyInfo)
                {
                    var fieldName = property.Name;
                    var match = type.Equals(fieldName);

                    if (match)
                    {
                        var value = property.GetValue(this, null);
                        if (value == null)
                        {
                            continue;
                        }

                        displayData += string.Format("{0}{1}", value, Environment.NewLine);
                    }
                }
            }

            return displayData;
        }
    }
}

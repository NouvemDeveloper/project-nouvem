﻿// -----------------------------------------------------------------------
// <copyright file="GoodsReceiptData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer;

    public class GoodsReceiptData
    {
        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber { get; set; }

        /// <summary>
        /// Gets or sets the batch traceabilities.
        /// </summary>
        public List<BatchTraceability> BatchTraceabilities { get; set; }

        /// <summary>
        /// Gets or sets the transaction data (StockTransaction, and traceabilities).
        /// </summary>
        public List<StockTransactionData> TransactionData { get; set; }

        /// <summary>
        /// Gets or sets any error message.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets any reference.
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Gets or sets the associated table id.
        /// </summary>
        public int? PROrderId { get; set; }

        /// <summary>
        /// Gets or sets the associated table id.
        /// </summary>
        public int DetailId { get; set; }

        /// <summary>
        /// Gets or sets the associated table id.
        /// </summary>
        public int PalletId { get; set; }

        /// <summary>
        /// Gets or sets the product group.
        /// </summary>
        public INGroup ProductGroup { get; set; }

        /// <summary>
        /// Gets or sets the product.
        /// </summary>
        public INMaster Product { get; set; }

        /// <summary>
        /// Gets or sets the product tare.
        /// </summary>
        public double ProductTare { get; set; }

        /// <summary>
        /// Gets or sets the production batch kill date (usually oldest).
        /// </summary>
        public DateTime? ProductionBatchKillDate { get; set; }

        /// <summary>
        /// Gets or sets the production batch kill date (usually oldest).
        /// </summary>
        public string ProductionBatchBornIn { get; set; }

        /// <summary>
        /// Gets or sets the production batch kill date (usually oldest).
        /// </summary>
        public string ProductionBatchRearedIn { get; set; }

        /// <summary>
        /// Gets or sets the production batch kill date (usually oldest).
        /// </summary>
        public string ProductionBatchSlaughteredIn { get; set; }

        /// <summary>
        /// Gets or sets the production batch kill date (usually oldest).
        /// </summary>
        public string ProductionBatchCutIn { get; set; }
    }
}

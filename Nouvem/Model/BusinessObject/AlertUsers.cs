﻿// -----------------------------------------------------------------------
// <copyright file="AlertUsers.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.Collections.Generic;
    using Nouvem.Model.DataLayer;

    public class AlertUsers
    {
        /// <summary>
        /// Gets or sets the users to alert.
        /// </summary>
        public IList<UserAlert> UserAlerts { get; set; }

        /// <summary>
        /// Gets or sets the user groups to alert.
        /// </summary>
        public IList<UserGroupAlert> UserGroupAlerts { get; set; }
    }
}

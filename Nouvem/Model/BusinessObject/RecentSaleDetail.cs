﻿// -----------------------------------------------------------------------
// <copyright file="SaleDetail.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------


using System.Linq;
using Nouvem.Global;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Sales;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Generic class which models the sale orders (quote, sale etc)
    /// </summary>
    public class RecentSaleDetail 
    {

        /// <summary>
        /// Gets or sets the sale detail id.
        /// </summary>
        public int SaleDetailID { get; set; }

        /// <summary>
        /// Gets or sets the sale id.
        /// </summary>
        public int SaleID { get; set; }

        /// <summary>
        /// Gets or sets the associated price list id.
        /// </summary>
        public int PriceListID { get; set; }
      
        /// <summary>
        /// Gets or sets the associated inventory item.
        /// </summary>
        public InventoryItem InventoryItem { get; set; }
        
        /// <summary>
        /// Gets or sets the inventory item id.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the quantity ordered.
        /// </summary>
        public double? QuantityOrdered { get; set; }

        /// <summary>
        /// Gets or sets the weight ordered.
        /// </summary>
        public double? WeightOrdered { get; set; }
        
        /// <summary>
        /// Gets or sets the quantity delivered.
        /// </summary>
        public double? QuantityDelivered { get; set; }

        /// <summary>
        /// Gets or sets the weight delivered.
        /// </summary>
        public double? WeightDelivered { get; set; }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal? UnitPrice { get; set; }
        
        /// <summary>
        /// Gets or sets the unit price after discount.
        /// </summary>
        public decimal? UnitPriceAfterDiscount { get; set; }

        /// <summary>
        /// Gets the current product.
        /// </summary>
        public void SetInventoryItem()
        {
            this.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.INMasterID);
        }
    }
}


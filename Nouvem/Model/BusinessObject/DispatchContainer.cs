﻿// -----------------------------------------------------------------------
// <copyright file="DispatchContainer.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Nouvem.Global;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.BusinessObject
{
    using System;

    public class DispatchContainer
    {
        public DispatchContainer()
        {
            this.Sales = new List<Sale>();
        }

        /// <summary>
        /// Gets or sets the container id.
        /// </summary>
        public int DispatchContainerID { get; set; }

        /// <summary>
        /// Gets or sets the container ref.
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Gets or sets the container creation date.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the container shipping date.
        /// </summary>
        public DateTime ShippingDate
        {
            get
            {
                if (this.Sales != null && this.Sales.Any())
                {
                    return this.Sales.First().ShippingDate.ToDate();
                }

                return this.CreationDate;
            }
        }

        /// <summary>
        /// Gets or sets the container creation date.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the container id.
        /// </summary>
        public int? DeviceMasterID { get; set; }

        /// <summary>
        /// Gets or sets the container id.
        /// </summary>
        public int? UserMasterID { get; set; }

        /// <summary>
        /// Gets or sets the container id.
        /// </summary>
        public int NouDocStatusID { get; set; }

        /// <summary>
        /// Gets a value indicating whether the container is printed.
        /// </summary>
        public bool IsComplete
        {
            get
            {
                return this.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the container is printed.
        /// </summary>
        public string IsCompleteToYesNo
        {
            get
            {
                return this.IsComplete ? Strings.Yes : Strings.No;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the container is printed.
        /// </summary>
        public string AllPrintedToYesNo
        {
            get
            {
                if (this.Sales.All(x => x.Invoice != null && x.Invoice.Printed == true))
                {
                    return Strings.Yes;
                }

                return Strings.No;
            }
        }

        /// <summary>
        /// Gets or sets the container id.
        /// </summary>
        public IList<Sale> Sales { get; set; }

        /// <summary>
        /// The ui display text.
        /// </summary>
        public string DisplayData
        {
            get
            {
                var localDate = this.ShippingDate;
                if (localDate.Date < DateTime.Today.AddDays(-100))
                {
                    localDate = this.CreationDate;
                }

                return string.Format("{0}{1}{2}", this.Reference, Environment.NewLine, localDate.Date.ToShortDateString());
            }
        }
    }
}

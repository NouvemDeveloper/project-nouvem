﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class SystemAuthorisation
    {
        public int NouAuthorisationID { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
        public int NouAuthorisationGroupNameID { get; set; }
        public string CodeSetting { get; set; }
        public bool Deleted { get; set; }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="PaymentDeductionItem.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel;

namespace Nouvem.Model.BusinessObject
{
    using System;

    public class PaymentDeductionItem : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or set the deduction id.
        /// </summary>
        private int killPaymentDeductionID;

        /// <summary>
        /// Gets the price edit flag.
        /// </summary>
        private bool allowPriceEdit;

        /// <summary>
        /// Gets the zero price flag.
        /// </summary>
        private bool allowZeroPrice;

        /// <summary>
        /// Gets or set the deduction total.
        /// </summary>
        private decimal totalIncVAT { get; set; }

        /// <summary>
        /// Gets or set the deduction sub total.
        /// </summary>
        private decimal subTotalExclVAT { get; set; }

        /// <summary>
        /// Gets or set the deduction vat.
        /// </summary>
        private decimal vat { get; set; }

        /// <summary>
        /// Gets or set the deduction price.
        /// </summary>
        private decimal price { get; set; }

        /// <summary>
        /// Gets or set the deduction item id.
        /// </summary>
        public bool LoadingDeductionItem { get; set; }

        /// <summary>
        /// Gets or set the deduction item id.
        /// </summary>
        public bool AllAnimalDeductionItem { get; set; }

        /// <summary>
        /// Gets or set the deduction item id.
        /// </summary>
        public int KillPaymentDeductionItemID { get; set; }

        /// <summary>
        /// Gets a value indicating whether the item deduction is linked to a deduction with a category.
        /// </summary>
        public bool HasCategory
        {
            get
            {
                return this.Deduction != null && this.Deduction.CategoryID.HasValue;
            }
        }

        /// <summary>
        /// Gets or set the deduction id.
        /// </summary>
        public int KillPaymentDeductionID
        {
            get
            {
                return this.killPaymentDeductionID;
            }

            set
            {
                this.killPaymentDeductionID = value;
                this.OnPropertyChanged("KillPaymentDeductionID");

                if (!this.LoadingDeductionItem)
                {
                    if (this.Deduction != null)
                    {
                        this.Price = this.Deduction.Price.ToDecimal();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or set the kill payment id.
        /// </summary>
        public int KillPaymentID { get; set; }

        private PaymentDeduction deduction;
        /// <summary>
        /// Gets or set the associated kill payment deduction.
        /// </summary>
        public PaymentDeduction Deduction
        {
            get
            {
                return this.deduction;
            }

            set
            {
                this.deduction = value;
                if (value != null)
                {
                    this.AllowPriceEdit = value.AllowPriceEdit.YesNoToNonNullableBool();
                }
                else
                {
                    this.AllowPriceEdit = true;
                }
            }
        }

        /// <summary>
        /// Gets or set the deduction total.
        /// </summary>
        public decimal TotalIncVAT
        {
            get
            {
                return this.totalIncVAT;
            }

            set
            {
                this.totalIncVAT = value;
                this.OnPropertyChanged("TotalIncVAT");
            }
        }

        /// <summary>
        /// Gets or set the deduction sub total.
        /// </summary>
        public decimal SubTotalExclVAT
        {
            get
            {
                return this.subTotalExclVAT;
            }

            set
            {
                this.subTotalExclVAT = value;
                this.OnPropertyChanged("SubTotalExclVAT");
            }
        }

        /// <summary>
        /// Gets or set the deduction vat.
        /// </summary>
        public decimal Vat
        {
            get
            {
                return this.vat;
            }

            set
            {
                this.vat = value;
                this.OnPropertyChanged("Vat");
            }
        }

        /// <summary>
        /// Gets or set the deduction price.
        /// </summary>
        public decimal Price
        {
            get
            {
                return this.price;
            }

            set
            {
                this.price = value;
                this.OnPropertyChanged("Price");
                if (!this.LoadingDeductionItem)
                {
                    this.CalculateTotals();
                }
            }
        }

        /// <summary>
        /// Gets tor sets the apply method.
        /// </summary>
        public string NouApplyMethodName
        {
            get
            {
                if (this.Deduction != null)
                {
                    return this.Deduction.NouApplyMethodName;
                }

                return ApplyMethod.OncePerPayment;
            }
        }

        /// <summary>
        /// Gets tor sets the deleted flag.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets tor sets the price edit flag.
        /// </summary>
        public bool AllowPriceEdit
        {
            get
            {
                return this.allowPriceEdit;
            }

            set
            {
                this.allowPriceEdit = value;
                this.OnPropertyChanged("AllowPriceEdit");
            }
        }

        /// <summary>
        /// Gets tor sets the vat arte.
        /// </summary>
        public decimal VatRate
        {
            get
            {
                return this.Deduction != null ? this.Deduction.VatRate : 0;
            }
        }

        /// <summary>
        /// Gets tor sets the zero flag flag.
        /// </summary>
        public bool AllowZeroPrice
        {
            get
            {
                return this.Deduction != null && this.Deduction.AllowZeroPrice.YesNoToNonNullableBool();
            }
        }

        /// <summary>
        /// Calculation of the deduction line totals.
        /// </summary>
        public void CalculateTotals()
        {
            #region validation

            if (this.Deduction == null)
            {
                return;
            }

            #endregion
         
            this.SubTotalExclVAT = this.Price;
            this.TotalIncVAT = this.Price;
            this.Vat = 0;
          
            if (this.VatRate > 0 && this.Price > 0)
            {
                this.TotalIncVAT = Math.Round(((100 + this.VatRate)/100)*this.Price, 2);
                this.Vat = this.TotalIncVAT - this.SubTotalExclVAT;
            }

            Messenger.Default.Send(true, Token.CalculatePayment);
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}


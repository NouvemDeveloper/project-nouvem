﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class AttributeLookUpData
    {
        public int AttributeLookupID { get; set; }
        public string Attribute_Name { get; set; }
        public int AttributeMasterID { get; set; }
        public DateTime Deleted { get; set; }
        public string AttributeCode { get; set; }
        public string AttributeDescription { get; set; }
    }
}

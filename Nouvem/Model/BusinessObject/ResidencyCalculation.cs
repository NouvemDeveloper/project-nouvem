﻿// -----------------------------------------------------------------------
// <copyright file="ResidencyCalculation.cs" company="DEM Machines Limited">
// Copyright (c) DEM Machines Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Nouvem.Model.BusinessObject
{
    using System;
    using Nouvem.Model.Enum;

    /// <summary>
    /// Class which models the calculation of days residency for an individual movement.
    /// </summary>
    public class ResidencyCalculation
    {
        #region Constructor

        /// <summary>
        /// Prevents a default instance of the <see cref="ResidencyCalculation"/> class from being created.
        /// </summary>
        private ResidencyCalculation()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the nature of the calculation.
        /// </summary>
        public ResidencyCalculationType CalculationType { get; set; }

        /// <summary>
        /// Gets or sets the lot number for the calculation.
        /// </summary>
        public string LotNo { get; set; }

        /// <summary>
        /// Gets or sets the eartag for the calculation.
        /// </summary>
        public string Eartag { get; set; }

        /// <summary>
        /// Gets or sets the location (farm, mart) from which the animal moved.
        /// </summary>
        public string FromId { get; set; }

        /// <summary>
        /// Gets or sets the location (farm, mart) to which the animal moved.
        /// </summary>
        public string ToId { get; set; }

        /// <summary>
        /// Gets or sets the date on which the animal moved to the farm.
        /// </summary>
        public DateTime MoveToDate { get; set; }

        /// <summary>
        /// Gets or sets the on which the animal left the farm. This will be the current date if the animal is still resident.
        /// </summary>
        public DateTime MoveFromDate { get; set; }

        /// <summary>
        /// Gets or sets the quality assurance certificate start date.
        /// </summary>
        public DateTime? CertStartDate { get; set; }

        /// <summary>
        /// Gets or sets the quality assurance certificate expiry date.
        /// </summary>
        public DateTime? CertExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the quality assurance certificate date is used for calculating residency instead of the move date.
        /// </summary>
        public bool IsCertDateUsed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the historical data for the certificate is used for calculating residency instead of the move date.
        /// </summary>
        public bool IsHistoricalDataUsed { get; set; }

        /// <summary>
        /// Gets a value indicating whether the calculation is quality assurance applicable.
        /// </summary>
        /// <remarks>Including this field as it is in the existing report.</remarks>
        public bool BordQualityAssuranceApplicable
        {
            get
            {
                return this.DaysAllocated > 0;
            }
        }

        /// <summary>
        /// Gets or sets the number of days residency allocated.
        /// </summary>
        public int DaysAllocated { get; set; }

        /// <summary>
        /// Gets or sets the number of farms allowed when the calculation relates to the total number of days residency.
        /// </summary>
        public int FarmsAllowed { get; set; }

        /// <summary>
        /// Gets or sets an information message relating to the residency calculation.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the calculation is to be used for total residency calculation purposes.
        /// </summary>
        public bool UseForCalculations { get; set; }

        #endregion

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="ResidencyCalculation"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="ResidencyCalculation"/> object.</returns>
        public static ResidencyCalculation CreateNewResidencyCalculation()
        {
            return new ResidencyCalculation();
        }

        /// <summary>
        /// Create a new empty <see cref="ResidencyCalculation"/> populated with the values passed.
        /// </summary>
        /// <param name="calculationType">The nature of the calculation.</param>
        /// <param name="lotNo">The lot number for the calculation.</param>
        /// <param name="eartag">The eartag for the calculation.</param>
        /// <param name="fromId">The location (farm, mart) from which the animal moved</param>
        /// <param name="toId">The location (farm, mart) to which the animal moved.</param>
        /// <param name="moveToDate">The date on which the animal moved to the far.</param>
        /// <param name="moveFromDate">The on which the animal left the farm.</param>
        /// <param name="certStartDate">The quality assurance certificate start date.</param>
        /// <param name="certExpiryDate">The quality assurance certificate expiry date.</param>
        /// <param name="isCertDateUsed">A value indicating whether the quality assurance certificate date is used for calculating residency instead of the move date.</param>
        /// <param name="isHistoricalDataUsed">A value indicating whether the historical data was used for calculating residency instead of the move date.</param>
        /// <param name="daysAllocated">The number of days residency allocated.</param>
        /// <param name="farmsAllowed">The number of farms allowed when the calculation relates to the total number of days residency.</param>
        /// <param name="message">An information message relating to the residency calculation.</param>
        /// <param name="useForCalculations">flag that determines if we are using for residency calculations.</param>
        /// <returns>A new instance of the <see cref="ResidencyCalculation"/> populated with the values passed.</returns>
        public static ResidencyCalculation CreateResidencyCalculation(ResidencyCalculationType calculationType, string lotNo, string eartag, string fromId, string toId, DateTime moveToDate, DateTime moveFromDate, DateTime? certStartDate, DateTime? certExpiryDate, bool isCertDateUsed, bool isHistoricalDataUsed, int daysAllocated, int farmsAllowed, string message, bool useForCalculations = true)
        {
            return new ResidencyCalculation
            {
                CalculationType = calculationType,
                LotNo = lotNo,
                Eartag = eartag,
                FromId = fromId,
                ToId = toId,
                MoveFromDate = moveFromDate,
                MoveToDate = moveToDate,
                CertStartDate = certStartDate,
                CertExpiryDate = certExpiryDate,
                IsCertDateUsed = isCertDateUsed,
                IsHistoricalDataUsed = isHistoricalDataUsed,
                DaysAllocated = daysAllocated,
                FarmsAllowed = farmsAllowed,
                Message = message,
                UseForCalculations = useForCalculations
            };
        }

        #endregion
    }
}


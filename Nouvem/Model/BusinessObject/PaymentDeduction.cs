﻿// -----------------------------------------------------------------------
// <copyright file="PaymentDeduction.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Linq;
using Nouvem.Global;
using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.Model.BusinessObject
{
    public class PaymentDeduction
    {
        /// <summary>
        /// Gets or set the deduction id.
        /// </summary>
        public int KillPaymentDeductionID { get; set; }

        /// <summary>
        /// Gets or set the deduction name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or set the deduction category name.
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Gets or set the deduction apply method.
        /// </summary>
        public int? NouApplyMethodID { get; set; }

        /// <summary>
        /// Gets or set the deduction vat code.
        /// </summary>
        public int? VatCodeID { get; set; }

        /// <summary>
        /// Gets or set the deduction vat code.
        /// </summary>
        public VATCode Vat { get; set; }

        /// <summary>
        /// Gets or set the deduction apply method.
        /// </summary>
        public NouApplyMethod NouApplyMethod { get; set; }

        /// <summary>
        /// Gets or set the category id.
        /// </summary>
        public int? CategoryID { get; set; }

        /// <summary>
        /// Gets or set the kill type.
        /// </summary>
        public int? NouKillTypeID { get; set; }

        /// <summary>
        /// Gets the kill type.
        /// </summary>
        public string KillType
        {
            get
            {
                if (this.NouKillTypeID != null)
                {
                    var killCat = NouvemGlobal.NouKillTypes.FirstOrDefault(x => x.NouKillTypeID == this.NouKillTypeID);
                    if (killCat != null)
                    {
                        return killCat.Name;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the apply method name.
        /// </summary>
        public string NouApplyMethodName
        {
            get
            {
                if (this.NouApplyMethod != null)
                {
                    return this.NouApplyMethod.Name;
                }

                return ApplyMethod.OncePerAnimal;
            }
        }

        /// <summary>
        /// Gets the vat rate.
        /// </summary>
        public decimal VatRate
        {
            get
            {
                if (this.Vat != null)
                {
                    return this.Vat.Percentage.ToDecimal();
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or set the deduction price.
        /// </summary>
        public decimal? Price { get; set; }
       
        /// <summary>
        /// Gets the allow price edit yes no value.
        /// </summary>
        public string AllowPriceEdit { get; set; }

        /// <summary>
        /// Gets the allow price edit yes no value.
        /// </summary>
        public string AllowZeroPrice { get; set; }

        /// <summary>
        /// Gets tor sets the deleted flaf.
        /// </summary>
        public DateTime? Deleted { get; set; }
    }
}

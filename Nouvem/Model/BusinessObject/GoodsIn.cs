﻿// -----------------------------------------------------------------------
// <copyright file="GoodsIn.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared.Localisation;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using DevExpress.Utils;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Shared;

    public class GoodsIn : INotifyPropertyChanged
    {
        #region field

        /// <summary>
        /// The batch number associated with the goods in detail.
        /// </summary>
        private BatchNumber batchNumber;

        /// <summary>
        /// The serial number associated with the goods in detail.
        /// </summary>
        private int? serialNumber;

        /// <summary>
        /// The item gross weight.
        /// </summary>
        private double grossWeight;

        /// <summary>
        /// The item tare weight.
        /// </summary>
        private double tareWeight;

        /// <summary>
        /// The item nett weight.
        /// </summary>
        private double nettWeight;

        /// <summary>
        /// The item nett weight.
        /// </summary>
        private decimal? price;

        /// <summary>
        /// Flag, as to whether the item line is to be printed or reprinted.
        /// </summary>
        private string printMode = Strings.PrintUpper;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GoodsIn"/> class.
        /// </summary>
        public GoodsIn()
        {
            this.PieceLabels = new List<GoodsIn>();
        }

        #endregion

        #region property

        public IList<Tuple<string, string, string>> RowData { get; set; } = new List<Tuple<string, string, string>>();

        public string Product { get; set; }
        public int Qty { get; set; }
        public DateTime CreationDate { get; set; }
        public string User { get; set; }
        public string Device{ get; set; }

        public bool Delete { get; set; }
        public string Item1 { get; set; }
        public string Item2 { get; set; }
        public string Item3 { get; set; }
        public string Item4 { get; set; }
        public string Item5 { get; set; }
        public string Item6 { get; set; }
        public string Item7 { get; set; }
        public string Item8 { get; set; }
        public string Item9 { get; set; }
        public string Item10 { get; set; }
        public string Item11 { get; set; }
        public string Item12 { get; set; }
        public string Item13 { get; set; }
        public string Item14 { get; set; }
        public string Item15 { get; set; }
        public string Item16 { get; set; }
        public string Item17 { get; set; }
        public string Item18 { get; set; }
        public string Item19 { get; set; }
        public string Item20 { get; set; }
        public string Item21 { get; set; }
        public string Item22 { get; set; }
        public string Item23 { get; set; }
        public string Item24 { get; set; }
        public string Item25 { get; set; }
        public string Item26 { get; set; }
        public string Item27 { get; set; }
        public string Item28 { get; set; }
        public string Item29 { get; set; }
        public string Item30 { get; set; }

        public string UseByDate { get; set; }
        public string KillDate { get; set; }
        public string PackedOnDate { get; set; }
        public string RearedIn { get; set; }
        public string SlaughteredIn { get; set; }
        public string CutIn{ get; set; }
        public string BestBeforeDate { get; set; }
        public string BornIn { get; set; }

        /// <summary>
        /// Gets or sets the stock transaction id.
        /// </summary>
        public int StockTransactionID { get; set; }

        /// <summary>
        /// Gets or sets the gross wgt.
        /// </summary>
        public double GrossWeight
        {
            get
            {
                return this.grossWeight;
            }

            set
            {
                this.grossWeight = value;
                this.OnPropertyChanged("GrossWeight");
                //this.NettWeight = value - this.tareWeight;
            }
        }

        /// <summary>
        /// Gets or sets the gross wgt.
        /// </summary>
        public decimal? Price
        {
            get
            {
                return this.price;
            }

            set
            {
                this.price = value;
                this.OnPropertyChanged("Price");
                //this.NettWeight = value - this.tareWeight;
            }
        }

        /// <summary>
        /// Gets or sets the tare wgt.
        /// </summary>
        public double TareWeight
        {
            get
            {
                return this.tareWeight;
            }

            set
            {
                this.tareWeight = value;
                this.NettWeight = this.grossWeight - value;
            }
        }

        /// <summary>
        /// Gets or sets the nett weight.
        /// </summary>
        public double NettWeight
        {
            get
            {
                return this.nettWeight;
            }

            set
            {
                this.nettWeight = value;
                this.GrossWeight = value + this.tareWeight;
            }
        }

        public int BatchNumberID { get; set; }
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber
        {
            get
            {
                return this.batchNumber;
            }

            set
            {
                this.batchNumber = value;
                this.OnPropertyChanged("BatchNumber");
            }
        }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public int? SerialNumber
        {
            get
            {
                return this.serialNumber;
            }

            set
            {
                this.serialNumber = value;
                this.OnPropertyChanged("SerialNumber");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether an item line can be printed.
        /// </summary>
        public string PrintMode
        {
            get
            {
                return this.printMode;
            }

            set
            {
                this.printMode = value;
                this.OnPropertyChanged("PrintMode");
            }
        }

        /// <summary>
        /// Gets or set the box piece labels.
        /// </summary>
        public IList<GoodsIn> PieceLabels { get; set; }

        #endregion

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

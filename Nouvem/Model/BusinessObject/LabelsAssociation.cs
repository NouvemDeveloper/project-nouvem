﻿// -----------------------------------------------------------------------
// <copyright file="LabelAssociation.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.Model.DataLayer;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.ComponentModel;

    public class LabelsAssociation : INotifyPropertyChanged
    {
        /// <summary>
        /// Is the customer group enabled flag.
        /// </summary>
        private bool isCustomerGroupEnabled;

        /// <summary>
        /// Is the customer enabled flag.
        /// </summary>
        private bool isCustomerEnabled;

        /// <summary>
        /// Is the product group enabled flag.
        /// </summary>
        private bool isProductGroupEnabled;

        /// <summary>
        /// Is the product enabled flag.
        /// </summary>
        private bool isProductEnabled;

        /// <summary>
        /// The bp group id.
        /// </summary>
        private int? bpGroupID;

        /// <summary>
        /// The bp masterid.
        /// </summary>
        private int? bpMasterID;

        /// <summary>
        /// The in group id.
        /// </summary>
        private int? inGroupID;

        /// <summary>
        /// The in master id.
        /// </summary>
        private int? inMasterID;

        /// <summary>
        /// The printer id.
        /// </summary>
        private int? wareHouseId;

        /// <summary>
        /// The printer id.
        /// </summary>
        private int? printerIDItem;

        /// <summary>
        /// The printer id.
        /// </summary>
        private int? printerIDBox;

        /// <summary>
        /// The printer id.
        /// </summary>
        private int? printerIDPallet;

        /// <summary>
        /// The printer id.
        /// </summary>
        private int? printerIDShipping;

        /// <summary>
        /// The printer id.
        /// </summary>
        private int? printerIDPiece;

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelsAssociation"/> class.
        /// </summary>
        public LabelsAssociation()
        {
            this.IsCustomerEnabled = true;
            this.IsCustomerGroupEnabled = true;
            this.IsProductEnabled = true;
            this.IsProductGroupEnabled = true;
            this.PieceLabels = new List<Label>();
            this.ItemLabels = new List<Label>();
            this.BoxLabels = new List<Label>();
            this.ShippingLabels = new List<Label>();
            this.LabelProcesses = new List<LabelProcess>();
            this.PalletLabels = new List<Label>();
            this.PieceLabelsForCombo = new List<object>();
            this.ItemLabelsForCombo = new List<object>();
            this.BoxLabelsForCombo = new List<object>();
            this.PalletLabelsForCombo = new List<object>();
            this.ShippingLabelsForCombo = new List<object>();
            this.LabelProcessesForCombo = new List<object>();
            this.ItemLabelQty = 1;
            this.BoxLabelQty = 1;
            this.PalletLabelQty = 1;
            this.PieceLabelQty = 1;
            this.ShippingLabelQty = 1;
        }

        /// <summary>
        /// Gets or sets the label association id.
        /// </summary>
        public int LabelAssociationID { get; set; }

        /// <summary>
        /// Gets or sets the box label qty.
        /// </summary>
        public int? PieceLabelQty { get; set; }

        /// <summary>
        /// Gets or sets the box label qty.
        /// </summary>
        public int? BoxLabelQty { get; set; }

        /// <summary>
        /// Gets or sets the item label qty.
        /// </summary>
        public int? ItemLabelQty { get; set; }

        /// <summary>
        /// Gets or sets the pallet label qty.
        /// </summary>
        public int? PalletLabelQty { get; set; }

        /// <summary>
        /// Gets or sets the shipping label qty.
        /// </summary>
        public int? ShippingLabelQty { get; set; }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int? BPMasterID
        {
            get
            {
                return this.bpMasterID;
            }

            set
            {
                this.bpMasterID = value;
                this.OnPropertyChanged("BPMasterID");
            }
        }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int? WarehouseID
        {
            get
            {
                return this.wareHouseId;
            }

            set
            {
                this.wareHouseId = value;
                this.OnPropertyChanged("WarehouseID");
            }
        }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int? PrinterIDItem
        {
            get
            {
                return this.printerIDItem;
            }

            set
            {
                this.printerIDItem = value;
                this.OnPropertyChanged("PrinterIDItem");
            }
        }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int? PrinterIDBox
        {
            get
            {
                return this.printerIDBox;
            }

            set
            {
                this.printerIDBox = value;
                this.OnPropertyChanged("PrinterIDBox");
            }
        }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int? PrinterIDPallet
        {
            get
            {
                return this.printerIDPallet;
            }

            set
            {
                this.printerIDPallet = value;
                this.OnPropertyChanged("PrinterIDPallet");
            }
        }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int? PrinterIDShipping
        {
            get
            {
                return this.printerIDShipping;
            }

            set
            {
                this.printerIDShipping = value;
                this.OnPropertyChanged("PrinterIDShipping");
            }
        }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public int? PrinterIDPiece
        {
            get
            {
                return this.printerIDPiece;
            }

            set
            {
                this.printerIDPiece = value;
                this.OnPropertyChanged("PrinterIDPiece");
            }
        }

        /// <summary>
        /// Gets or sets the customer group id.
        /// </summary>
        public int? BPGroupID
        {
            get
            {
                return this.bpGroupID;
            }

            set
            {
                this.bpGroupID = value;
                this.OnPropertyChanged("BPGroupID");
            }
        }

        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        public int? INMasterID
        {
            get
            {
                return this.inMasterID;
            }

            set
            {
                this.inMasterID = value;
                this.OnPropertyChanged("INMasterID");
            }
        }

        /// <summary>
        /// Gets or sets the product group id.
        /// </summary>
        public int? INGroupID
        {
            get
            {
                return this.inGroupID;
            }

            set
            {
                this.inGroupID = value;
                this.OnPropertyChanged("INGroupID");
            }
        }

        /// <summary>
        /// Gets or sets the associated piece labels for the grid combo.
        /// </summary>
        public IList<object> LabelProcessesForCombo { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public IList<LabelProcess> LabelProcesses { get; set; }

        /// <summary>
        /// Gets or sets the associated piece labels.
        /// </summary>
        public IList<Label> PieceLabels { get; set; }

        /// <summary>
        /// Gets or sets the associated pallet labels.
        /// </summary>
        public IList<Label> PalletLabels { get; set; }

        /// <summary>
        /// Gets or sets the associated box labels.
        /// </summary>
        public IList<Label> BoxLabels { get; set; }

        /// <summary>
        /// Gets or sets the associated shipping labels.
        /// </summary>
        public IList<Label> ShippingLabels { get; set; }

        /// <summary>
        /// Gets or sets the associated item labels.
        /// </summary>
        public IList<Label> ItemLabels { get; set; }

        /// <summary>
        /// Gets or sets the associated piece labels for the grid combo.
        /// </summary>
        public IList<object> PieceLabelsForCombo { get; set; }

        /// <summary>
        /// Gets or sets the associated item labels for the grid combo.
        /// </summary>
        public IList<object> ItemLabelsForCombo { get; set; }

        /// <summary>
        /// Gets or sets the associated box labels for the grid combo.
        /// </summary>
        public IList<object> BoxLabelsForCombo { get; set; }

        /// <summary>
        /// Gets or sets the associated pallet labels for the grid combo.
        /// </summary>
        public IList<object> PalletLabelsForCombo { get; set; }

        /// <summary>
        /// Gets or sets the associated shipping labels for the grid combo.
        /// </summary>
        public IList<object> ShippingLabelsForCombo { get; set; }

        /// <summary>
        /// Gets or sets the associated printer.
        /// </summary>
        public Printer PrinterItem { get; set; }

        /// <summary>
        /// Gets or sets the associated printer.
        /// </summary>
        public Printer PrinterPallet { get; set; }

        /// <summary>
        /// Gets or sets the associated printer.
        /// </summary>
        public Printer PrinterBox { get; set; }

        /// <summary>
        /// Gets or sets the associated printer.
        /// </summary>
        public Printer PrinterShipping { get; set; }

        /// <summary>
        /// Gets or sets the associated printer.
        /// </summary>
        public Printer PrinterPiece { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether the association is using a customer group.
        /// </summary>
        public bool? UsingCustomerGroup { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether the association is using a product group.
        /// </summary>
        public bool? UsingProductGroup { get; set; }

        /// <summary>
        /// Gets or sets the associated partner.
        /// </summary>
        public BPMaster BPMaster { get; set; }

        /// <summary>
        /// Gets or sets the associated product.
        /// </summary>
        public INMaster INMaster { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer group is enabled.
        /// </summary>
        public bool IsCustomerGroupEnabled
        {
            get
            {
                return this.isCustomerGroupEnabled;
            }

            set
            {
                this.isCustomerGroupEnabled = value;
                this.OnPropertyChanged("IsCustomerGroupEnabled");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the customer is enabled.
        /// </summary>
        public bool IsCustomerEnabled
        {
            get
            {
                return this.isCustomerEnabled;
            }

            set
            {
                this.isCustomerEnabled = value;
                this.OnPropertyChanged("IsCustomerEnabled");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Product group is enabled.
        /// </summary>
        public bool IsProductGroupEnabled
        {
            get
            {
                return this.isProductGroupEnabled;
            }

            set
            {
                this.isProductGroupEnabled = value;
                this.OnPropertyChanged("IsProductGroupEnabled");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Product is enabled.
        /// </summary>
        public bool IsProductEnabled
        {
            get
            {
                return this.isProductEnabled;
            }

            set
            {
                this.isProductEnabled = value;
                this.OnPropertyChanged("IsProductEnabled");
            }
        }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}




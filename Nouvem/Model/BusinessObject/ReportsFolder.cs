﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class ReportsFolder
    {
        /// <summary>
        /// Gets or sets the folder id.
        /// </summary>
        public int ReportFolderId { get; set; }

        /// <summary>
        /// Gets or sets the folder parent id.
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Gets or sets the folder name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the folder deletion date.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the folder reports.
        /// </summary>
        public IList<ReportData> Reports { get; set; }
    }
}

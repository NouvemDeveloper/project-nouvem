﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class BatchData : INotifyPropertyChanged
    {
        private int batchNumberID;
        public int PROrderID { get; set; }
        public string Reference { get; set; }
        public Nullable<decimal> Price { get; set; }
        public int StockTransactionID { get; set; }
        public int WarehouseID { get; set; }
        public int? INGroupID { get; set; }
        public int? BPGroupID { get; set; }
        public int? BPMasterID { get; set; }
        public int? ProcessID { get; set; }
        public Nullable<int> Serial { get; set; }
        public string CreationDate { get; set; }
        public string ScheduledDate { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public Nullable<decimal> Wgt { get; set; }
        public Nullable<decimal> Tare { get; set; }
        public string UserName { get; set; }
        public string Device { get; set; }
        public string Product { get; set; }
        public string ProductCode { get; set; }
        public int BatchNumberID
        {
            get { return this.batchNumberID; }
            set
            {
                this.batchNumberID = value;
                
            }
        }
        public string BatchNumber { get; set; }
        public int? AttributeID { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public string Attribute4 { get; set; }
        public string Attribute5 { get; set; }
        public string Attribute6 { get; set; }
        public string Attribute7 { get; set; }
        public string Attribute8 { get; set; }
        public string Attribute9 { get; set; }
        public string Attribute10 { get; set; }
        public string Attribute11 { get; set; }
        public string Attribute12 { get; set; }
        public string Attribute13 { get; set; }
        public string Attribute14 { get; set; }
        public string Attribute15 { get; set; }
        public string Attribute16 { get; set; }
        public string Attribute17 { get; set; }
        public string Attribute18 { get; set; }
        public string Attribute19 { get; set; }
        public string Attribute20 { get; set; }
        public string Attribute21 { get; set; }
        public string Attribute22 { get; set; }
        public string Attribute23 { get; set; }
        public string Attribute24 { get; set; }
        public string Attribute25 { get; set; }
        public string Attribute26 { get; set; }
        public string Attribute27 { get; set; }
        public string Attribute28 { get; set; }
        public string Attribute29 { get; set; }
        public string Attribute30 { get; set; }
        public string Generic1 { get; set; }
        public string Generic2 { get; set; }
        public string Generic3 { get; set; }
        public string Generic4 { get; set; }
        public int INMasterID { get; set; }
        public int TransID { get; set; }
        public bool LinesOnly { get; set; }
        public bool AttributesOnly { get; set; }

        public IList<BatchData> Batches { get; set; }

        public bool Delete { get; set; }

        public int ID
        {
            get { return this.LinesOnly ? this.INMasterID : this.StockTransactionID; }
        }

        public string DisplayData
        {
            get { return $"{this.Product} - {this.Reference}"; }
        }


        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

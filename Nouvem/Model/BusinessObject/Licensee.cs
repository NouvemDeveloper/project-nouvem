﻿// -----------------------------------------------------------------------
// <copyright file="Licensee.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Shared;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;

    /// <summary>
    /// Class which models a licensee (user/device)
    /// </summary>
    public class Licensee
    {
        /// <summary>
        /// Gets or sets the device/user id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the device/user assocaited licence details id
        /// </summary>
        public int? LicenceDetailsId { get; set; }
      
        /// <summary>
        /// Gets or sets the device/user name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the device/user identifier (mac or user name)
        /// </summary>
        public string Identifier { get; set; }
      
        /// <summary>
        /// Gets or sets the device ip.
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// Gets or sets the licensee licenceKey.
        /// </summary>
        public string LicenceKey { get; set; }

        /// <summary>
        /// Gets or sets the license start date.
        /// </summary>
        public DateTime? ValidFrom { get; set; }

        /// <summary>
        /// Gets or sets the license end date.
        /// </summary>
        public DateTime? ValidTo { get; set; }

        /// <summary>
        /// Gets or sets the license import date.
        /// </summary>
        public DateTime? ImportDate { get; set; }

        /// <summary>
        /// Gets or sets the license issue date.
        /// </summary>
        public DateTime? IssueDate { get; set; }

        /// <summary>
        /// Gets or sets the days the license is valid for.
        /// </summary>
        public int? NoOfDays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the licensee is a device or not.
        /// </summary>
        public bool IsDevice { get; set; }

        /// <summary>
        /// Gets or sets the device/user license type.
        /// </summary>
        public NouLicenceType LicenseType { get; set; }

        /// <summary>
        /// Gets or sets the user group the licencee belongs to.
        /// </summary>
        public UserGroup_ LicenceeGroup { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the licensee licence is to be deallocated.
        /// </summary>
        public bool DeallocateLicence { get; set; }

        /// <summary>
        /// Gets or sets the authorisations.
        /// </summary>
        public IDictionary<string, string> Authorisations { get; set; }

        /// <summary>
        /// Gets a value indicating a licence status.
        /// </summary>
        public LicenceStatus LicenceStatus
        {
            get
            {
                return LicenseType == null || LicenseType.NouLicenceTypeID == 0
                    ? LicenceStatus.NoLicence
                    : !ValidFrom.IsValidDate(ValidTo) ? LicenceStatus.Expired 
                    : LicenceStatus.Valid;
            }
        }
    }
}

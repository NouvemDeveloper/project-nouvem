﻿// -----------------------------------------------------------------------
// <copyright file="DocNumber.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using Nouvem.Model.DataLayer;

    public class DocNumber
    {
        /// <summary>
        /// Gets or sets the document number id.
        /// </summary>
        public int DocumentNumberingID { get; set; }

        /// <summary>
        /// Gets or sets the document name id.
        /// </summary>
        public int NouDocumentNameID { get; set; }
        
        /// <summary>
        /// Gets or sets the document number type id.
        /// </summary>
        public int DocumentNumberingTypeID { get; set; }
        
        /// <summary>
        /// Gets or sets the first document number.
        /// </summary>
        public int FirstNumber { get; set; }

        /// <summary>
        /// Gets or sets the next document number.
        /// </summary>
        public int NextNumber { get; set; }

        /// <summary>
        /// Gets or sets the current document number.
        /// </summary>
        public int CurrentNumber { get; set; }

        /// <summary>
        /// Gets or sets the last document number.
        /// </summary>
        public int LastNumber { get; set; }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the associated document name.
        /// </summary>
        public NouDocumentName DocName { get; set; }

        /// <summary>
        /// Gets or sets the associated document type.
        /// </summary>
        public DocumentNumberingType DocType { get; set; }
    }
}

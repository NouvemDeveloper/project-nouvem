﻿// -----------------------------------------------------------------------
// <copyright file="LicenseDetails.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.ComponentModel;
    using Nouvem.Model.DataLayer;

    public class LicenseDetails : INotifyPropertyChanged
    {
        private bool isSelected;

        /// <summary>
        /// Gets the grouped license details (all license types totals)
        /// </summary>
        public ViewGroupedLicenseDetail Details { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the license type is selected.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

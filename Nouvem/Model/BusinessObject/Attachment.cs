﻿// -----------------------------------------------------------------------
// <copyright file="Attachment.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;

    public class Attachment
    {
        /// <summary>
        /// Gets or sets the attachment id.
        /// </summary>
        public int AttachmentID { get; set; }

        /// <summary>
        /// Gets or sets the reference table id.
        /// </summary>
        public int SaleAttachmentID { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file attachment date.
        /// </summary>
        public DateTime AttachmentDate { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        public byte[] File { get; set; }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime? Deleted { get; set; }
    }
}

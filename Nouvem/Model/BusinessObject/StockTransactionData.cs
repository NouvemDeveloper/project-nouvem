﻿// -----------------------------------------------------------------------
// <copyright file="StockTransactionData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Reflection;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Nouvem.Model.DataLayer;
    using Nouvem.Shared;

    public class StockTransactionData : INotifyPropertyChanged
    {
        #region field

        /// <summary>
        /// The transaction selected flag.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The is in stock location flag.
        /// </summary>
        private bool isStockLocation = true;

        #endregion

        public StockTransactionData()
        {
            this.LabelStatus = this.Deleted ? "Deleted" : this.IsBox ? "Box" : "Item";
        }

        #region public interface

        /// <summary>
        /// Gets or sets the stock transaction.
        /// </summary>
        public static StockTransactionData TransactionData { get; set; }

        /// <summary>
        /// Gets or sets the stock transaction.
        /// </summary>
        public StockTransaction Transaction { get; set; }

        /// <summary>
        /// Gets or sets the stock attributes.
        /// </summary>
        public StockAttribute StockAttribute { get; set; }

        /// <summary>
        /// Gets or sets the stock transaction traceabilities.
        /// </summary>
        public List<TransactionTraceability> TransactionTraceabilities { get; set; }

        /// <summary>
        /// Gets or sets a value indicating if this is the transaction to process.
        /// </summary>
        public bool Process { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the transaction stock is in a stock location.
        /// </summary>
        public bool IsStockLocation
        {
            get
            {
                return this.isStockLocation;
            }

            set
            {
                this.isStockLocation = value;
            }
        }

        /// <summary>
        /// Gets a value that indicates whther this is a box transaction.
        /// </summary>
        public bool Deleted
        {
            get
            {
                return this.Transaction != null && this.Transaction.Deleted.HasValue;
            }
        }

        /// <summary>
        /// Gets a value that indicates whther this is a box transaction.
        /// </summary>
        public bool IsBox
        {
            get
            {
                return this.Transaction != null && this.Transaction.IsBox == true;
            }
        }

        /// <summary>
        /// Gets the device name.
        /// </summary>
        public string DeviceName
        {
            get
            {
                return this.Transaction != null && this.Transaction.DeviceMaster != null ? this.Transaction.DeviceMaster.DeviceName : string.Empty;
            }
        }

        /// <summary>
        /// Gets the user name.
        /// </summary>
        public string UserName
        {
            get
            {
                return this.Transaction != null && this.Transaction.UserMaster != null ? this.Transaction.UserMaster.UserName : string.Empty;
            }
        }

        /// <summary>
        /// Gets the transaction serial number.
        /// </summary>
        public int Serial
        {
            get
            {
                return this.Transaction != null ? this.Transaction.Serial.ToInt() : 0;
            }
        }

        /// <summary>
        /// Gets the transaction serial number.
        /// </summary>
        public string Comments
        {
            get
            {
                return this.Transaction != null ? this.Transaction.Comments ?? string.Empty : string.Empty;
            }
        }

        /// <summary>
        /// Gets the transaction serial number.
        /// </summary>
        public int StocktransactionID
        {
            get
            {
                return this.Transaction != null ? this.Transaction.StockTransactionID : 0;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the label has been retrieved by scanning.
        /// </summary>
        public bool RetrievedByScanning { get; set; }

        /// <summary>
        /// Gets or sets the product.
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        public int? INMasterId { get; set; }

        /// <summary>
        /// Gets or sets the product group id.
        /// </summary>
        public int? INGroupId { get; set; }

        /// <summary>
        /// Gets or sets the partner id.
        /// </summary>
        public int? BPMasterId { get; set; }

        /// <summary>
        /// Gets or sets the partner group id.
        /// </summary>
        public int? BPGroupId { get; set; }

        /// <summary>
        /// Gets or sets the batch reference.
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Gets or sets the batch reference.
        /// </summary>
        public string Reference2 { get; set; }

        /// <summary>
        /// Gets or sets the batch reference.
        /// </summary>
        public int? ReferenceID { get; set; }

        /// <summary>
        /// Gets the transaction date.
        /// </summary>
        public DateTime Date
        {
            get
            {
                return this.Transaction != null ? this.Transaction.TransactionDate : DateTime.Now;
            }
        }

        /// <summary>
        /// Gets the transaction date.
        /// </summary>
        public string DateOnly
        {
            get
            {
                return this.Date.ToShortDateString();
            }
        }

        /// <summary>
        /// Gets the transaction date.
        /// </summary>
        public string Time
        {
            get
            {
                return this.Date.ToString("HH:mm:ss");
            }
        }

        /// <summary>
        /// Gets the transaction batch number.
        /// </summary>
        public string Batch
        {
            get
            {
                return this.Transaction != null && this.Transaction.BatchNumber != null ? this.Transaction.BatchNumber.Number : string.Empty;
            }
        }

        /// <summary>
        /// Gets the transaction wgt.
        /// </summary>
        public decimal Weight
        {
            get
            {
                return this.Transaction != null ? Math.Round(this.Transaction.TransactionWeight.ToDecimal(),3) : 0;
            }
        }

        /// <summary>
        /// Gets the transaction qty.
        /// </summary>
        public decimal Quantity
        {
            get
            {
                return this.Transaction != null ? Math.Round(this.Transaction.TransactionQTY.ToDecimal(),0) : 0;
            }
        }

        private decimal labelWeight;
        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public decimal LabelWeight
        {
            get
            {
                return this.labelWeight;
            }

            set
            {
                this.labelWeight = value;
                this.OnPropertyChanged("LabelWeight");
                this.LabelWeightKg = value.ToString();
            }
        }

        private string labelWeightKg;
        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public string LabelWeightKg
        {
            get
            {
                return this.labelWeightKg;
            }

            set
            {
                value = string.Format("{0} KG", value);
                this.labelWeightKg = value;
                this.OnPropertyChanged("LabelWeightKg");
            }
        }

        private decimal labelQuantity;
        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public decimal LabelQuantity
        {
            get
            {
                return this.labelQuantity;
            }

            set
            {
                this.labelQuantity = value;
                this.OnPropertyChanged("LabelQuantity");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the transaction is selected on the ui.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        private string labelStatus;
        /// <summary>
        /// Gets or sets a value indicating whether the transaction is selected on the ui.
        /// </summary>
        public string LabelStatus
        {
            get
            {
                return this.labelStatus;
            }

            set
            {
                this.labelStatus = value;
                this.OnPropertyChanged("LabelStatus");
            }
        }

        /// <summary>
        /// Gets the intake card data to display.
        /// </summary>
        public string DisplayData
        {
            get
            {
                try
                {
                    var data = string.Empty;
                    data = this.SetDisplayCardData(ApplicationSettings.DisplayLabelData.Split(','));

                    data = data.Remove(data.LastIndexOf(Environment.NewLine), 1);
                    return data;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets the box items count.
        /// </summary>
        public int BoxItemsCount { get; set; }

        /// <summary>
        /// Set the card display data.
        /// </summary>
        /// <param name="data">The stored property fields to display values for.</param>
        /// <returns>The card display data.</returns>
        public string SetDisplayCardData(string[] data)
        {
            var displayData = string.Empty;
            var typeSource = this.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var type in data)
            {
                foreach (var property in propertyInfo)
                {
                    var fieldName = property.Name;
                    var match = type.Equals(fieldName);

                    if (match)
                    {
                        var value = property.GetValue(this, null);
                        if (value == null)
                        {
                            continue;
                        }

                        displayData += string.Format("{0}{1}", value, Environment.NewLine);
                    }
                }
            }

            return displayData;
        }

        #endregion

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

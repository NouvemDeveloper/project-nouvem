﻿// -----------------------------------------------------------------------
// <copyright file="BusinessPartnerContact.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// ----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;

    public class BusinessPartnerContact
    {
        /// <summary>
        /// Gets or sets the contact details.
        /// </summary>
        public BPContact Details { get; set; }

        /// <summary>
        /// Gets the partner full name.
        /// </summary>
        public string FullName
        {
            get
            {
                var firstName = Details.FirstName ?? string.Empty;
                var lastName = Details.LastName ?? string.Empty;
                return string.Format("{0} {1}", firstName, lastName);
            }
        }
    }
}

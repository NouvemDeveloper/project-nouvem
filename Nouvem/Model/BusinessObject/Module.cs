﻿// -----------------------------------------------------------------------
// <copyright file=Module.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    using System.Collections.Generic;

    public class Module : INotifyPropertyChanged
    {
        /// <summary>
        /// The selected falg.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The show email flag.
        /// </summary>
        private bool showEmail;

        /// <summary>
        /// The show email flag.
        /// </summary>
        private bool useAtModule;

        /// <summary>
        /// The selected flag.
        /// </summary>
        public bool UseAtModule
        {
            get
            {
                return this.useAtModule;
            }

            set
            {
                this.useAtModule = value;
                this.OnPropertyChanged("UseAtModule");
                Messenger.Default.Send(Token.Message, Token.UpdateModules);
            }
        }

        /// <summary>
        /// The selected flag.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
                Messenger.Default.Send(Token.Message, Token.UpdateModules);
            }
        }
     
        /// <summary>
        /// Gets or sets the show email flag.
        /// </summary>
        public bool ShowEmail
        {
            get
            {
                return this.showEmail;
            }

            set
            {
                this.showEmail = value;
                this.OnPropertyChanged("ShowEmail");
                Messenger.Default.Send(Token.Message, Token.UpdateModules);
            }
        }

        /// <summary>
        /// Gets or sets the module id.
        /// </summary>
        public int NouModuleID { get; set; }

        /// <summary>
        /// Gets or sets the module report id.
        /// </summary>
        public int ReportID { get; set; }

        /// <summary>
        /// Gets or sets the module report contact id.
        /// </summary>
        public int? ContactID { get; set; }

        /// <summary>
        /// Gets or sets the module name.
        /// </summary>
        public string Name  { get; set; }

        /// <summary>
        /// Gets or sets the contact email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the associated modules.
        /// </summary>
        public IList<Module> Modules { get; set; } = new List<Module>();

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event raise.
        /// </summary>
        /// <param name="propertyName">The property whose value will be broadcast.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

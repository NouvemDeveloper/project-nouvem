﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    public class Alert
    {
        /// <summary>
        /// Gets or sets the alert id.
        /// </summary>
        public int AlertID { get; set; }

        /// <summary>
        /// Gets or sets the alert.
        /// </summary>
        public string AlertMessage { get; set; }

        /// <summary>
        /// Gets or sets the alert id.
        /// </summary>
        public int UserToAlertID { get; set; }

        /// <summary>
        /// Gets or sets the alert id.
        /// </summary>
        public int? NouDocStatusID { get; set; }

        /// <summary>
        /// Gets or sets the alert id.
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Gets the user name.
        /// </summary>
        public string Username
        {
            get
            {
                var user = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == this.UserToAlertID);
                if (user != null)
                {
                    return user.UserMaster.FullName;
                }

                return string.Empty;
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Sale.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using System.Reflection;
using Nouvem.BusinessLogic;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.DataLayer;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;

    [Serializable]
    public class Sale : INotifyPropertyChanged
    {
        /// <summary>
        /// The attachments reference.
        /// </summary>
        private IList<Attachment> attachments = new List<Attachment>();

        /// <summary>
        /// The notes.
        /// </summary>
        private string popUpNote;

        private int? referenceId;
        private int? referenceId2;
        private int? bpMasterId;

        /// <summary>
        /// The number.
        /// </summary>
        private int number;

        private bool? excludeFromPurchaseAccounts;

        /// <summary>
        /// The notes.
        /// </summary>
        private string technicalNotes;

        /// <summary>
        /// Batch edit flag.
        /// </summary>
        private bool canEditBatch;

        /// <summary>
        /// Gets or sets the sale id.
        /// </summary>
        public int SaleID { get; set; }

        public int TopDocumentID { get; set; }

        public bool Copy { get; set; }

        /// <summary>
        /// Gets or sets the sale id.
        /// </summary>
        public string SaleIDs { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? APOrderID { get; set; }

        public bool AllowCopy { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? NouOrderMethodID { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int PROrderID { get; set; }

        public int? BaseDocumentReferenceID2 { get; set; }

        public int? BaseDocumentReferenceID3 { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public DateTime? LotAuthorisedDate { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public DateTime? FileTransferedDate { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public bool? IFALevyApplied { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public bool SlaughterOnly { get; set; }

        public bool DispatchAndProcessingOnly { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public bool? Insured { get; set; }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public bool OrderAlreadyCompleted { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public string INMaster { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a batch is to be edited.
        /// </summary>
        public bool EditingBatches { get; set; }

        /// <summary>
        /// Gets or sets the associated contact.
        /// </summary>
        public bool ProductionAttributesSet { get; set; }

        /// <summary>
        /// Gets or sets the sale details.
        /// </summary>
        public IList<Model.DataLayer.Attribute> NonStandardResponses { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public bool? Scrapie { get; set; }

        public DateTime? ExportedDate { get; set; }

        /// <summary>
        /// Gets the batch number.
        /// </summary>
        public int? BatchNumberId
        {
            get
            {
                if (this.SaleDetails != null && this.SaleDetails.Any())
                {
                    return this.SaleDetails.First().BatchNumberId;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? SupplierId { get; set; }

        /// <summary>
        /// Thec reference.
        /// </summary>
        private string reference;

        /// <summary>
        /// Gets or sets the attribute value.
        /// </summary>
        public string Attribute { get; set; }

        /// <summary>
        /// Gets or sets the attribute2 value.
        /// </summary>
        public string Attribute2 { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        public string TimeInTransit { get; set; }

        /// <summary>
        /// Gets or sets an intake haulage charge.
        /// </summary>
        public decimal? HaulageCharge { get; set; }

        /// <summary>
        /// Gets or sets an ap order corresponding ap goods receipt.
        /// </summary>
        public Sale APGoodsReceipt { get; set; }

        /// <summary>
        /// Gets or sets the sale id.
        /// </summary>
        public int? Serial { get; set; }

        /// <summary>
        /// Gets or sets the document numbering id.
        /// </summary>
        public int DocumentNumberingID { get; set; }

        /// <summary>
        /// Gets or sets the document numbering id.
        /// </summary>
        public int? DispatchContainerID { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? TemplateID { get; set; }

        /// <summary>
        /// Gets or sets the herd qas.
        /// </summary>
        public bool? HerdQAS { get; set; }

        /// <summary>
        /// Gets or sets the herd qas.
        /// </summary>
        public bool ExcludeFromAccounts { get; set; }
        
        /// <summary>
        /// Gets or sets the herd qas.
        /// </summary>
        public bool? ExcludeFromPurchaseAccounts
        {
            get { return this.excludeFromPurchaseAccounts; }

            set
            {
                this.excludeFromPurchaseAccounts = value;
                this.ExcludeFromAccounts = value.ToBool();
            }
        }

        /// <summary>
        /// Gets or sets the herd qas.
        /// </summary>
        public string Opened { get; set; }

        /// <summary>
        /// Gets or sets the invoice.
        /// </summary>
        public ARInvoice Invoice { get; set; }

        /// <summary>
        /// Gets or sets the invoice.
        /// </summary>
        public string PrintedInvoiceToYesNo
        {
            get
            {
                if (this.Invoice == null)
                {
                    return Strings.No;
                }

                return this.Invoice.Printed == true ? Strings.Yes : Strings.No;
            }
        }

        /// <summary>
        /// Gets or sets the document numbering id.
        /// </summary>
        public Model.DataLayer.DispatchContainer DispatchContainer { get; set; }

        /// <summary>
        /// Gets or sets the document numbering id.
        /// </summary>
        public bool IsContainerComplete
        {
            get
            {
                if (this.DispatchContainer != null)
                {
                    return this.DispatchContainer.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the price list id.
        /// </summary>
        public int? PriceListID  { get; set; }

        /// <summary>
        /// Gets or sets the price list id.
        /// </summary>
        public int? PriceListIDValue { get; set; }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public decimal? TypicalBatchSize { get; set; }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public decimal? TypicalMixQty { get; set; }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public bool? MultipleReceipts { get; set; }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public string MultipleReceiptsYesNo => this.MultipleReceipts.BoolToYesNo();
        
        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public bool? ReworkStockNonAdjust { get; set; }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public string ReworkStockNonAdjustYesNo => this.ReworkStockNonAdjust.BoolToYesNo();

        /// <summary>
        /// Gets or sets the herd qas.
        /// </summary>
        ///public StockDetail LastBatchAttribute { get; set; }

        /// <summary>
        /// Gets or sets the document numbering id.
        /// </summary>
        public string DispatchContainerName
        {
            get
            {
                if (this.DispatchContainer != null)
                {
                    return this.DispatchContainer.Reference;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the container data.
        /// </summary>
        public string DispatchContainerData
        {
            get
            {
                if (this.DispatchContainer != null)
                {
                    return string.Format(" {0}      {1}      All Printed: {2}                                    Id:{3}", this.DispatchContainer.Reference,
                        this.DispatchContainer.CreationDate.ToShortDateString(), this.AllPrintedToYesNo, this.DispatchContainer.DispatchContainerID);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the sale number.
        /// </summary>
        public DateTime? GradingDate { get; set; }
        
        /// <summary>
        /// Gets or sets the sale number.
        /// </summary>
        public int Number
        {
            get
            {
                return this.number;
            }

            set
            {
                this.number = value;
                this.OnPropertyChanged("Number");
            }
        }

        public string PartnerName { get; set; }
        public string PartnerCode { get; set; }
        public int PalletCount { get; set; }
        public int BoxCount { get; set; }

        /// <summary>
        /// Gets or sets the sale number.
        /// </summary>
        public long? KillNumber { get; set; }

        /// <summary>
        /// Gets or sets the sale number.
        /// </summary>
        public string KillType { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Eartag { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Breed { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Herd { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Grade { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string GroupDescription { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public decimal? ColdWeight { get; set; }

        /// <summary>
        /// Gets or sets the base document number.
        /// </summary>
        public string BaseDocNumber { get; set; }

        /// <summary>
        /// Gets or sets the base document number.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the base document number.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the base document number.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the route id.
        /// </summary>
        public int? RouteID { get; set; }

        /// <summary>
        /// Gets or sets the base document id.
        /// </summary>
        public int? BaseDocumentReferenceID { get; set; }

        /// <summary>
        /// Gets or sets the base document number.
        /// </summary>
        public int BaseDocumentNumber { get; set; }

        /// <summary>
        /// Gets or sets the sale order document number.
        /// </summary>
        public int? QuoteNumber { get; set; }

        /// <summary>
        /// Gets or sets the sale order document number.
        /// </summary>
        public bool IsBatchPayment { get; set; }

        /// <summary>
        /// Gets or sets the sale order document number.
        /// </summary>
        public int? SaleOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the sale order document number.
        /// </summary>
        public int? DispatchNumber { get; set; }

        /// <summary>
        /// Gets or sets the sale order document number.
        /// </summary>
        public int? InvoiceNumber { get; set; }

        /// <summary>
        /// Gets or sets the sale order document number.
        /// </summary>
        public int? IntakeNumber { get; set; }

        /// <summary>
        /// Gets or sets the quote valid date.
        /// </summary>
        public DateTime? QuoteValidDate { get; set; }

        /// <summary>
        /// Gets or sets the document date.
        /// </summary>
        public DateTime? DocumentDate { get; set; }

        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        public DateTime? ScheduledDate { get; set; }

        /// <summary>
        /// Gets or sets the base doc creation date.
        /// </summary>
        public DateTime? BaseDocumentCreationDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document has been printed.
        /// </summary>
        public bool? Printed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document has been printed.
        /// </summary>
        public bool? IsWorkflow { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document has been printed.
        /// </summary>
        public string IsStandard
        {
            get
            {
                if (this.IsWorkflow == false || this.IsWorkflow == null)
                {
                    return Constant.Yes;
                };

                return Constant.No;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the document has been printed.
        /// </summary>
        public int ChequeNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document has been printed.
        /// </summary>
        public bool AllPrinted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document has been printed.
        /// </summary>
        public string PrintedToYesNo
        {
            get
            {
                return this.Printed.ToBool().BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets a workflow status value.
        /// </summary>
        public string WorkflowStatus
        {
            get
            {
                var status = NouvemGlobal.NouDocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.InvoiceNumber);
                if (status != null)
                {
                    return status.Value;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the document has been printed.
        /// </summary>
        public string AllPrintedToYesNo
        {
            get
            {
                return this.AllPrinted.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the route name.
        /// </summary>
        public int? RouteNumber { get; set; }

        /// <summary>
        /// Gets or sets the route.
        /// </summary>
        public RouteMaster RouteMaster { get; set; }

        /// <summary>
        /// Gets or sets the route.
        /// </summary>
        public string RouteName
        {
            get
            {
                return this.RouteMaster != null ? this.RouteMaster.Name : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the sale number.
        /// </summary>
        public int BatchNumberID { get; set; }

        /// <summary>
        /// Gets or sets the sale number.
        /// </summary>
        public string InternalBatchNo { get; set; }

        /// <summary>
        /// Gets or sets the document status id.
        /// </summary>
        public int? NouDocStatusID { get; set; }

        /// <summary>
        /// Gets or sets the document status value.
        /// </summary>
        public string NouDocStatus { get; set; }

        /// <summary>
        /// Gets or sets the kill type value.
        /// </summary>
        public int? NouKillTypeID { get; set; }

        /// <summary>
        /// Gets or sets the bp contact id.
        /// </summary>
        public int? BPContactID { get; set; }

        /// <summary>
        /// Gets or sets the remarks.
        /// </summary>
        public string Remarks { get; set; }
   

        /// <summary>
        /// Gets or sets the dispatch note.
        /// </summary>
        public string DocketNote { get; set; }

        /// <summary>
        /// Gets or sets the pop up note.
        /// </summary>
        public string PopUpNote
        {
            get
            {
                return this.popUpNote;
            }

            set
            {
                this.popUpNote = value;
                this.OnPropertyChanged("PopUpNote");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the dispatch note pops up.
        /// </summary>
        public bool? DispatchNote1PopUp { get; set; }

        /// <summary>
        /// Gets or sets the invoice note.
        /// </summary>
        public string InvoiceNote { get; set; }

        /// <summary>
        /// Gets or sets the vat rate.
        /// </summary>
        public decimal? VAT { get; set; }

        /// <summary>
        /// Gets or sets the document date.
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Gets or sets the total ex vat rate.
        /// </summary>
        public decimal? TotalExVAT { get; set; }

        /// <summary>
        /// Gets or sets the sub total inc vat.
        /// </summary>
        public decimal? SubTotalExVAT { get; set; }

        /// <summary>
        /// Gets or sets the discount inc vat.
        /// </summary>
        public decimal? DiscountIncVAT { get; set; }

        /// <summary>
        /// Gets or sets the grand total inc vat.
        /// </summary>
        public decimal? GrandTotalIncVAT { get; set; }

        /// <summary>
        /// Gets or sets the grand total inc vat.
        /// </summary>
        public decimal? OrderCost { get; set; }

        /// <summary>
        /// Gets or sets the grand total inc vat.
        /// </summary>
        public decimal? OrderMargin { get; set; }

        /// <summary>
        /// Gets or sets the grand total inc vat.
        /// </summary>
        public decimal? DeductionsIncVAT { get; set; }

        /// <summary>
        /// Gets or sets the grand total inc vat.
        /// </summary>
        public decimal? DeductionsVAT { get; set; }

        /// <summary>
        /// Gets or sets the discount percentage.
        /// </summary>
        public decimal? DiscountPercentage { get; set; }

        /// <summary>
        /// Gets or sets the lairage permit no.
        /// </summary>
        public string PermitNo { get; set; }

        /// <summary>
        /// Gets or sets the lairage permit no.
        /// </summary>
        public string MoveToLairageReference { get; set; }

        /// <summary>
        /// Gets or sets a product line.
        /// </summary>
        public APGoodsReceiptDetail ProductLine { get; set; }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public string Reference
        {
            get
            {
                return this.reference;
            }

            set
            {
                this.reference = value;
                this.OnPropertyChanged("Reference");
            }
        }

        /// <summary>
        /// Gets or sets the other reference.
        /// </summary>
        public string OtherReference { get; set; }

        /// <summary>
        /// Gets or sets the sales employee id.
        /// </summary>
        public int? SalesEmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the sales employee id.
        /// </summary>
        public int? UserIDCreation { get; set; }

        /// <summary>
        /// Gets or sets the user creation name.
        /// </summary>
        public string CreatedBy
        {
            get
            {
                var user = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == this.UserIDCreation);
                if (user != null)
                {
                    return user.UserMaster.FullName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the user completion name.
        /// </summary>
        public string CompletedBy
        {
            get
            {
                var user = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == this.UserIDCompletion);
                if (user != null)
                {
                    return user.UserMaster.FullName;
                }

                return string.Empty;
            }
        }      

        /// <summary>
        /// Gets or sets the sales employee id.
        /// </summary>
        public int? UserIDCompletion { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        public string DeliveryTime { get; set; }

        /// <summary>
        /// Gets or sets the shipping date.
        /// </summary>
        public DateTime? ShippingDate { get; set; }

        /// <summary>
        /// Gets or sets the proposed kill date.
        /// </summary>
        public DateTime? ProposedKillDate { get; set; }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the associated device.
        /// </summary>
        public DeviceMaster Device { get; set; }

        /// <summary>
        /// Gets or sets the associated device id.
        /// </summary>
        public int? DeviceId { get; set; }

        /// <summary>
        /// Gets or sets the associated edit date.
        /// </summary>
        public DateTime? EditDate { get; set; }

        /// <summary>
        /// Gets or sets the associated production order.
        /// </summary>
        public ProductionData ProductionOrder { get; set; }

        /// <summary>
        /// Gets or sets po reference number.
        /// </summary>
        public string CustomerPOReference { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the goods in receipt is newly created i.e. has no corresponsing aporder.
        /// </summary>
        public bool IsNewGoodsInReceipt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the sale is on a parent document.
        /// </summary>
        public bool? IsBaseDocument { get; set; }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public ViewBusinessPartner Customer { get; set; }

        /// <summary>
        /// Gets or sets the haulier.
        /// </summary>
        public ViewBusinessPartner Haulier { get; set; }

        public string DeliveryAddressLine1
        {
            get
            {
                if (this.DeliveryAddress != null)
                {
                    if (this.DeliveryAddress.FullAddressLine1.Length > ApplicationSettings.TileDeliveryAddressLength)
                    {
                        return this.DeliveryAddress.FullAddressLine1.Substring(0, ApplicationSettings.TileDeliveryAddressLength);
                    }

                    return this.DeliveryAddress.FullAddressLine1;
                }

                return string.Empty;
            }
        }

        public string InvoiceAddressLine1
        {
            get
            {
                if (this.InvoiceAddress != null)
                {
                    if (this.InvoiceAddress.FullAddressLine1.Length > ApplicationSettings.TileDeliveryAddressLength)
                    {
                        return this.InvoiceAddress.FullAddressLine1.Substring(0, ApplicationSettings.TileDeliveryAddressLength);
                    }

                    return this.InvoiceAddress.FullAddressLine1.Substring(0, 30);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the haulier.
        /// </summary>
        public ViewBusinessPartner Agent { get; set; }

        public int? AgentID { get; set; }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public BPMaster BPCustomer { get; set; }

        public BPCurrency PartnerCurrency { get; set; }

        /// <summary>
        /// Gets or sets the haulier.
        /// </summary>
        public BPMaster BPHaulier { get; set; }

        /// <summary>
        /// Gets or sets the agent.
        /// </summary>
        public BPMaster BPAgent { get; set; }

        /// <summary>
        /// Gets or sets the haulier.
        /// </summary>
        public BPMaster BPSupplier { get; set; }

        /// <summary>
        /// Gets or sets the haulier.
        /// </summary>
        public BPAddress Address { get; set; }

        /// <summary>
        /// Gets or sets the delivery address.
        /// </summary>
        public BusinessPartnerAddress DeliveryAddress { get; set; }

        /// <summary>
        /// Gets or sets the invoice address.
        /// </summary>
        public BusinessPartnerAddress InvoiceAddress { get; set; }

        /// <summary>
        /// Gets or sets the main contact.
        /// </summary>
        public BusinessPartnerContact MainContact { get; set; }

        /// <summary>
        /// Gets or sets the delivery contact.
        /// </summary>
        public BusinessPartnerContact DeliveryContact { get; set; }
       
        /// <summary>
        /// Gets or sets a value indicating whether a purchase order is required.
        /// </summary>
        public bool? PORequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this sale is being copied from an order to goods in or dispath.
        /// </summary>
        public bool CopyingFromOrder { get; set; }

        /// <summary>
        /// Gets or sets the dispatch id's to be marked as invoiced when we are creating a consolidated invoice.
        /// </summary>
        public IList<int> IdsToMarkAsInvoiced { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? ProcessID { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? CustomerID { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public string CustomerName
        {
            get
            {
                var partner =
                    NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == this.CustomerID);
                if (partner != null)
                {
                    return partner.Details.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? BPMasterID
        {
            get
            {
                return this.bpMasterId;
            }

            set
            {
                this.bpMasterId = value;
                this.OnPropertyChanged("BPMasterID");
            }
        }
       
        /// <summary>
        /// Gets or sets the sale attachments.
        /// </summary>
        public IList<Attachment> Attachments
        {
            get
            {
                return this.attachments;
            }

            set
            {
                this.attachments = value;
            }
        }

        /// <summary>
        /// Gets or sets the batch no.
        /// </summary>
        public string BatchNo { get; set; }

        /// <summary>
        /// Gets or sets the sale type.
        /// </summary>
        public ViewType SaleType { get; set; }

        /// <summary>
        /// Gets or sets the sale details.
        /// </summary>
        public IList<SaleDetail> SaleDetails { get; set; }

        /// <summary>
        /// Gets or sets the sale details.
        /// </summary>
        public IList<RecentSaleDetail> RecentSaleDetails { get; set; }

        /// <summary>
        /// Gets or sets the unique node id (used for the document trail treeview).
        /// </summary>
        public int? NodeID { get; set; }

        /// <summary>
        /// Gets or sets the parent id (used for the document trail treeview).
        /// </summary>
        public int? ParentID { get; set; }

        /// <summary>
        /// Gets or sets the sales employee id.
        /// </summary>
        public int? UserIDModified { get; set; }

        /// <summary>
        /// Gets or sets the user completion name.
        /// </summary>
        public string ModifiedBy
        {
            get
            {
                var user = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == this.UserIDModified);
                if (user != null)
                {
                    return user.UserMaster.FullName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the qty.
        /// </summary>
        public int? Quantity { get; set; }

        /// <summary>
        /// Gets or sets the wgt.
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public string ShippingAddress { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public string DelAddress { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? WarehouseID { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? WarehouseIDFrom { get; set; }

        public IList<StockTakeData> StockMoveDetails { get; set; }
        public IList<StockTakeData> StockMoveOrderDetails { get; set; }

        /// <summary>
        /// Gets the warehouse.
        /// </summary>
        public string Warehouse
        {
            get
            {
                var loc = NouvemGlobal.WarehouseLocations.FirstOrDefault(x => x.WarehouseID == this.WarehouseID);
                if (loc != null)
                {
                    return loc.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a batch can be edited.
        /// </summary>
        public bool CanEditBatches
        {
            get
            {
                return this.canEditBatch;
            }

            set
            {
                this.canEditBatch = value;
                this.OnPropertyChanged("CanEditBatch");
            }
        }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? ProductionTypeID { get; set; }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public string ProductionType
        {
            get
            {
                if (this.ProductionTypeID.IsNullOrZeroOrOne())
                {
                    return Strings.Standard;
                }

                return Strings.Receipe;
            }
        }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public int? ReferenceID
        {
            get { return this.referenceId; }
            set
            {
                this.referenceId = value;
                this.OnPropertyChanged("ReferenceID");
            }
        }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public int? ReferenceID2
        {
            get { return this.referenceId2; }
            set
            {
                this.referenceId2 = value;
                this.OnPropertyChanged("ReferenceID2");
            }
        }

        /// <summary>
        /// Gets or sets an ap goods receipt corresponding aporder id.
        /// </summary>
        public int? INMasterID { get; set; }

        /// <summary>
        /// Gets the product.
        /// </summary>
        public string Product
        {
            get
            {
                var loc = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.INMasterID);
                if (loc != null)
                {
                    return loc.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        public string ProductCode
        {
            get
            {
                var loc = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.INMasterID);
                if (loc != null)
                {
                    return loc.Code;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the product name.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the newly created stock transaction id.
        /// </summary>
        public int ReturnedStockTransactionId{ get; set; }

        /// <summary>
        /// Gets the partner invoice note.
        /// </summary>
        public string PartnerInvoiceNote
        {
            get
            {
                if (this.BPCustomer != null)
                {
                    return this.BPCustomer.InvoiceNote;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the partner pop up note.
        /// </summary>
        public string PartnerPopUpNote
        {
            get
            {
                if (this.BPCustomer != null)
                {
                    return this.BPCustomer.PopUpNotes;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the partner pop up.
        /// </summary>
        public string PartnerPopUp { get; set; }

        /// <summary>
        /// Gets or sets the partner invoice.
        /// </summary>
        public string PartnerInvoice { get; set; }

        /// <summary>
        /// Gets the document date with time truncated.
        /// </summary>
        public string DocumentDateTruncateTime
        {
            get
            {
                if (this.DocumentDate == null)
                {
                    return string.Empty;
                }

                return this.DocumentDate.ToDate().ToShortDateString();
            }
        }

        /// <summary>
        /// Gets the delivery date with time truncated.
        /// </summary>
        public string DeliveryDateTruncateTime
        {
            get
            {
                if (this.DeliveryDate == null)
                {
                    return string.Empty;
                }

                return this.DeliveryDate.ToDate().ToShortDateString();
            }
        }

        /// <summary>
        /// Gets the shipping date with time truncated.
        /// </summary>
        public string ShippingDateTruncateTime
        {
            get
            {
                if (this.ShippingDate == null)
                {
                    return string.Empty;
                }

                return this.ShippingDate.ToDate().ToShortDateString();
            }
        }

        private string dynamicDate;
        /// <summary>
        /// Gets the shipping date with time truncated.
        /// </summary>
        public string DynamicDate
        {
            get { return this.dynamicDate; }
            set
            {
                this.dynamicDate = value;
                this.OnPropertyChanged("DynamicDate");
            }
        }

        /// <summary>
        /// Gets the delivery date with time truncated.
        /// </summary>
        public string HaulierName
        {
            get
            {
                if (this.BPHaulier == null)
                {
                    return string.Empty;
                }

                return this.BPHaulier.Name;
            }
        }

        /// <summary>
        /// Gets the document trail display name.
        /// </summary>
        public string DocumentTrailName
        {
            get
            {
                if (!string.IsNullOrEmpty(this.DocumentTrailType))
                {
                    var localName = this.DocumentTrailType;
                    if (this.DocumentTrailType.Equals(Constant.APQuote))
                    {
                        localName = Constant.Quote;
                    }

                    if (this.DocumentTrailType.Equals(Constant.APOrder))
                    {
                        localName = Constant.Order;
                    }

                    return string.Format("{0} - {1}", localName, this.Number);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the sale items count.
        /// </summary>
        public int SaleItemsCount
        {
            get
            {
                return this.SaleDetails != null ? this.SaleDetails.Count : 0;
            }
        }

        /// <summary>
        /// Gets the route.
        /// </summary>
        public string Route
        {
            get
            {
                return this.RouteMaster != null ? this.RouteMaster.Name : string.Empty;
            }
        }

        /// <summary>
        /// Gets the supplier.
        /// </summary>
        public string Supplier
        {
            get
            {
                return this.BPCustomer != null ? this.BPCustomer.Name : string.Empty;
            }
        }

        /// <summary>
        /// Gets the customer.
        /// </summary>
        public string Partner
        {
            get
            {
                return this.BPCustomer != null ? this.BPCustomer.Name : string.Empty;
            }
        }

        /// <summary>
        /// Gets the user name.
        /// </summary>
        public string UserName
        {
            get
            {
                var user = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == this.UserIDCreation);
                if (user != null)
                {
                    return user.UserMaster.FullName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the doc trail type name.
        /// </summary>
        public string DocumentTrailType { get; set; }

        /// <summary>
        /// Gets the order status.
        /// </summary>
        public string OrderStatus
        {
            get
            {
                if (this.NouDocStatusID == null || this.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                {
                    return Properties.OrderStatus.Active;
                }

                if (this.NouDocStatusID == null || this.NouDocStatusID == NouvemGlobal.NouDocStatusActiveEdit.NouDocStatusID)
                {
                    return Properties.OrderStatus.ActiveEdit;
                }

                if (this.NouDocStatusID == NouvemGlobal.NouDocStatusFilled.NouDocStatusID)
                {
                    return Properties.OrderStatus.Filled;
                }

                if (this.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID)
                {
                    return Properties.OrderStatus.InProgress;
                }

                if (this.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                {
                    return Properties.OrderStatus.Cancelled;
                }

                if (this.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    return Properties.OrderStatus.Complete;
                }

                if (this.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID)
                {
                    return Properties.OrderStatus.Exported;
                }

                return Properties.OrderStatus.Complete;
            }
        }

        /// <summary>
        /// Determines if the current dispatch order has been filled.
        /// </summary>
        /// <returns>Flag, as to whether the current order has been filled.</returns>
        public bool IsOrderFilled()
        {
            //if (this.BaseDocumentReferenceID.IsNullOrZero())
            //{
            //    // no base order document, so cant determine if order filled.
            //    return false;
            //}

            return this.SaleDetails != null && this.SaleDetails.Any() && this.SaleDetails.All(x => x.IsFilled);
        }

        /// <summary>
        /// Gets the total qty received on this order.
        /// </summary>
        public double TotalQtyReceived
        {
            get
            {
                if (this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    return 0;
                }

                return this.SaleDetails.Sum(x => x.QuantityReceived.ToDouble());
            }
        }

        /// <summary>
        /// Gets the total wgt received on this order.
        /// </summary>
        public double TotalWgtReceived
        {
            get
            {
                if (this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    return 0;
                }

                return this.SaleDetails.Sum(x => x.WeightReceived.ToDouble());
            }
        }

        /// <summary>
        /// Gets the total qty delivered on this order.
        /// </summary>
        public double TotalQtyDelivered
        {
            get
            {
                if (this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    return 0;
                }

                return this.SaleDetails.Sum(x => x.QuantityDelivered.ToDouble());
            }
        }

        /// <summary>
        /// Gets the total wgt delivered on this order.
        /// </summary>
        public double TotalWgtDelivered
        {
            get
            {
                if (this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    return 0;
                }

                return this.SaleDetails.Sum(x => x.WeightDelivered.ToDouble());
            }
        }

        /// <summary>
        /// Gets the batch product.
        /// </summary>
        public string BatchProduct
        {
            get
            {
                if (this.SaleDetails != null && this.SaleDetails.Any())
                {
                    var productId = this.SaleDetails.First().INMasterID;
                    var product = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == productId);
                    if (product != null)
                    {
                        return product.Name;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or set the prorder batch number.
        /// </summary>
        public BatchNumber ProductionBatchNumber { get; set; }

        /// <summary>
        /// Gets the batch number.
        /// </summary>
        public string BatchNumber
        {
            get
            {
                if (this.SaleDetails != null && this.SaleDetails.Any())
                {
                    return this.SaleDetails.First().BatchNo;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the intake card data to display.
        /// </summary>
        public string DisplayData
        {
            get
            {
                try
                {
                    var data = string.Empty;
                    if (this.SaleType == ViewType.APReceipt)
                    {
                        data = this.SetDisplayCardData(ApplicationSettings.DisplayIntakeData.Split(','));
                    }
                    else if (this.SaleType == ViewType.Workflow)
                    {
                        data = this.SetDisplayCardData(ApplicationSettings.DisplayWorkflowData.Split(','));
                    }
                    else
                    {
                        data = this.SetDisplayCardData(ApplicationSettings.DisplayDispatchData.Split(','));
                    }

                    data = data.Remove(data.LastIndexOf(Environment.NewLine), 1);
                    return data; 
                }
                catch (Exception ex) 
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Set the card display data.
        /// </summary>
        /// <param name="data">The stored property fields to display values for.</param>
        /// <returns>The card display data.</returns>
        public string SetDisplayCardData(string[] data)
        {
            var displayData = string.Empty;
            var typeSource = this.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var type in data)
            {
                foreach (var property in propertyInfo)
                {
                    var fieldName = property.Name;
                    var match = type.Equals(fieldName);

                    if (match)
                    {
                        var value = property.GetValue(this, null);
                        if (value == null)
                        {
                            continue;
                        }
             
                        displayData += string.Format("{0}{1}", value, Environment.NewLine);
                    }
                }
            }

            return displayData;
        }

        /// <summary>
        /// Gets or sets the carcasses killed on an intake.
        /// </summary>
        public int CarcassesOrdered { get; set; }

        /// <summary>
        /// Gets or sets the carcasses killed on an intake.
        /// </summary>
        public int CarcassesSequenced { get; set; }

        /// <summary>
        /// Gets or sets the carcasses killed on an intake.
        /// </summary>
        public int CarcassesKilled { get; set; }

        /// <summary>
        /// Gets or sets the carcasses not yet killed on an intake.
        /// </summary>
        public int CarcassesNotKilled { get; set; }

        /// <summary>
        /// Gets or sets the auto complete recipe production mix flag.
        /// </summary>
        public bool? AutoCompleteMix { get; set; }

        /// <summary>
        /// Gets or sets the carcasses killed on an intake.
        /// </summary>
        public int CarcassesTotal
        {
            get
            {
                return this.CarcassesKilled + this.CarcassesNotKilled;
            }
        }

        /// <summary>
        /// Gets or sets the under 30 animals.
        /// </summary>
        public int UTM { get; set; }

        /// <summary>
        /// Gets the derived otm total.
        /// </summary>
        public int OTM
        {
            get
            {
                return this.CarcassesTotal - this.UTM;
            }
        }

        /// <summary>
        /// Gets or sets the carcasses not yet killed on an intake.
        /// </summary>
        public int QASAnimals { get; set; }

        /// <summary>
        /// Gets the total intake.
        /// </summary>
        public int TotalIntake
        {
            get
            {
                return this.CarcassesKilled + this.CarcassesNotKilled;
            }
        }

        /// <summary>
        /// Gets the total intake.
        /// </summary>
        public string QASSummary
        {
            get
            {
                return string.Format("{0}/{1}", this.QASAnimals, this.TotalIntake);
            }
        }

        /// <summary>
        /// Gets the total intake.
        /// </summary>
        public string KillSummary
        {
            get
            {
                return string.Format("{0}/{1}", this.CarcassesKilled, this.TotalIntake);
            }
        }

        /// <summary>
        /// Gets or sets the payment date.
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a sale is to be peviewed or printed.
        /// </summary>
        public bool Print { get; set; }

        /// <summary>
        /// Gets or sets the stock details.
        /// </summary>
        public IList<StockDetail> StockDetails { get; set; }

        /// <summary>
        /// Gets or sets the deductions.
        /// </summary>
        public IList<PaymentDeductionItem> PaymentDeductionItems { get; set; }

        /// <summary>
        /// The work flow data.
        /// </summary>
        public IList<AttributeAllocationData> WorkflowData { get; set; }

        /// <summary>
        /// The current work flow data.
        /// </summary>
        public AttributeAllocationData CurrentWorkflowData { get; set; }

        /// <summary>
        /// The current work flow data.
        /// </summary>
        public AttributeTemplateData TemplateData { get; set; }

        /// <summary>
        /// The current work flow data.
        /// </summary>
        public AttributeTemplate AttributeTemplate { get; set; }

        /// <summary>
        /// Gets the template name
        /// </summary>
        public string TemplateName
        {
            get
            {
                if (this.AttributeTemplate == null)
                {
                    return string.Empty;
                }

                return this.AttributeTemplate.Name;
            }
        }

        /// <summary>
        /// The work flow notes.
        /// </summary>
        public string TechnicalNotes
        {
            get
            {
                return this.technicalNotes;
            }

            set
            {
                this.technicalNotes = value;
                this.OnPropertyChanged("TechnicalNotes");
            }
        }

        /// <summary>
        /// The work flow notes.
        /// </summary>
        public IList<Note> Notes { get; set; }

        /// <summary>
        /// Gets all the notes.
        /// </summary>
        public string NotesList
        {
            get
            {
                if (this.Notes == null || !this.Notes.Any())
                {
                    return string.Empty;
                }

                if (this.Notes.Count == 1 || !ApplicationSettings.AccumulateNotesAtBatchEdit)
                {
                    return this.Notes.First().Notes;
                }

                return string.Join(",", this.Notes.Select(x => x.Notes));
            }
        }

        /// <summary>
        /// Gets all the notes.
        /// </summary>
        public string EditDateList
        {
            get
            {
                if (this.Notes == null || !this.Notes.Any())
                {
                    return string.Empty;
                }

                if (this.Notes.Count == 1)
                {
                    return this.Notes.First().CreationDate.ToString();
                }

                return string.Join(",", this.Notes.Select(x => x.CreationDate));
            }
        }

        /// <summary>
        /// Gets all the notes.
        /// </summary>
        public string EditedByList
        {
            get
            {
                if (this.Notes == null || !this.Notes.Any())
                {
                    return string.Empty;
                }

                var edits = this.Notes.Select(x => x.UserMasterID);
                var editNames = new List<string>();
                foreach (var edit in edits)
                {
                    var name = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == edit);
                    if (name != null)
                    {
                        editNames.Add(name.UserMaster.FullName);
                    }
                }

                if (editNames.Count == 1)
                {
                    return editNames.First();
                }

                return string.Join(",", editNames.Select(x => x));
            }
        }

        public void SetPopUpNotes()
        {
            this.PopUpNote = this.NotesList;
        }

        public void SetTechnicalNotes()
        {
            this.TechnicalNotes = this.NotesList;
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DateTemplate.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;

    public class DatesTemplateAllocation
    {
        /// <summary>
        /// Gets or sets the date template.
        /// </summary>
        public ViewDateTemplateAllocation TemplateAllocation { get; set; }

        /// <summary>
        /// Gets or sets the date day.
        /// </summary>
        public DateDay DateDay { get; set; }
    }
}

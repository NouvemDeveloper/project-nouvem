﻿// -----------------------------------------------------------------------
// <copyright file="PriceMaster.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;

    /// <summary>
    /// Class which models the pricing list.
    /// </summary>
    public class PriceMaster : INotifyPropertyChanged
    {
        /// <summary>
        /// The currency id.
        /// </summary>
        private int bPCurrencyID;

        /// <summary>
        /// The nou ronding option id.
        /// </summary>
        private int nouRoundingOptionID;

        /// <summary>
        /// The nou ronding rule id.
        /// </summary>
        private int nouRoundingRuleID;

        /// <summary>
        /// The partner id.
        /// </summary>
        private int? bpMasterID;

        /// <summary>
        /// The creation date.
        /// </summary>
        private DateTime? creationDate;

        /// <summary>
        /// The price list name.
        /// </summary>
        private string currentPriceListName;

        /// <summary>
        /// Gets or sets the price list id.
        /// </summary>
        public int PriceListID { get; set; }

        /// <summary>
        /// Gets or sets the price list name.
        /// </summary>
        public string CurrentPriceListName
        {
            get
            {
                return this.currentPriceListName;
            }

            set
            {
                this.currentPriceListName = value;
                this.OnPropertyChanged("CurrentPriceListName");
            }
        }

        /// <summary>
        /// Gets or sets the base price list name.
        /// </summary>
        public string BasePriceListName { get; set; }

        /// <summary>
        /// Gets or sets the base price list id.
        /// </summary>
        public int? BasePriceListID { get; set; }

        /// <summary>
        /// Gets or sets the price list factor.
        /// </summary>
        public double Factor { get; set; }

        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        public DateTime? CreationDate
        {
            get
            {
                return this.creationDate;
            }

            set
            {
                this.creationDate = value;
                this.OnPropertyChanged("CreationDate");
            }
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the base price list.
        /// </summary>
        public PriceList BasePriceList { get; set; }

        /// <summary>
        /// Gets or sets the partner id.
        /// </summary>
        public int? BPMasterID
        {
            get
            {
                return this.bpMasterID;
            }

            set
            {
                this.bpMasterID = value;
                this.OnPropertyChanged("BPMasterID");
            }
        }

        /// <summary>
        /// Gets or sets the price list rounding options id.
        /// </summary>
        public int NouRoundingOptionsID
        {
            get
            {
                return this.nouRoundingOptionID;
            }

            set
            {
                this.nouRoundingOptionID = value;
                this.OnPropertyChanged("NouRoundingOptionID");
            }
        }

        /// <summary>
        /// Gets or sets the price list rounding rules id.
        /// </summary>
        public int NouRoundingRulesID
        {
            get
            {
                return this.nouRoundingRuleID;
            }

            set
            {
                this.nouRoundingRuleID = value;
                this.OnPropertyChanged("NouRoundingRulesID");
            }
        }

        /// <summary>
        /// Gets or sets the price list currency id.
        /// </summary>
        public int BPCurrencyID
        {
            get
            {
                return this.bPCurrencyID;
            }

            set
            {
                this.bPCurrencyID = value;
                this.OnPropertyChanged("BPCurrencyID");
            }
        }

        /// <summary>
        /// Gets or sets the price list details.
        /// </summary>
        public IList<PriceDetail> PriceListDetails { get; set; }

        /// <summary>
        /// Gets or sets the active products count.
        /// </summary>
        public int ActiveProducts { get; set; }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

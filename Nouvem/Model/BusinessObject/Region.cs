﻿// -----------------------------------------------------------------------
// <copyright file="Region.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;

    public class Region
    {
        /// <summary>
        /// Gets or sets the region id.
        /// </summary>
        public int RegionID { get; set; }

        /// <summary>
        /// Gets or sets the region name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the region number.
        /// </summary>
        public int? RegionNumber { get; set; }

        /// <summary>
        /// Gets or sets the remarks.
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime? Deleted { get; set; }
    }
}

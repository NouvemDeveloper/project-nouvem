﻿// -----------------------------------------------------------------------
// <copyright file="AnimalMovement.cs" company="DEM Machines Limited">
// Copyright (c) DEM Machines Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Nouvem.Model.BusinessObject
{
    using System;

    /// <summary>
    /// Class which models the movement of an animal from one location to another, on a specific date.
    /// </summary>
    public class AnimalMovement
    {
        #region Constructor

        /// <summary>
        /// Prevents a default instance of the <see cref="AnimalMovement"/> class from being created.
        /// </summary>
        private AnimalMovement()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the eartag to which the movements relate.
        /// </summary>
        public string Eartag { get; set; }

        /// <summary>
        /// Gets or sets the batch to which the movement relates.
        /// </summary>
        public int BatchId { get; set; }

        /// <summary>
        /// Gets or sets the location (farm, mart) from which the animal moved.
        /// </summary>
        public string FromId { get; set; }

        /// <summary>
        /// Gets or sets the location (farm, mart) to which the animal moved.
        /// </summary>
        public string ToId { get; set; }

        /// <summary>
        /// Gets or sets the date on which the animal movement took place.
        /// </summary>
        public DateTime MovementDate { get; set; }

        /// <summary>
        /// Gets or sets the days allocated for that movement.
        /// </summary>
        public int? DaysResidency { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the herd is approved.
        /// </summary>
        /// <remarks>Mimicking the exiting functionality with this value.</remarks>
        public bool? QualityAssuranceApproved { get; set; }

        /// <summary>
        /// Gets or sets the date on which the quality assurance certificate will expiry.
        /// </summary>
        public DateTime? CertExpiry { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create a new animal movement.
        /// </summary>
        /// <returns>A new instance of an animal movement with default parameters.</returns>
        public static AnimalMovement CreateNewMovement()
        {
            return new AnimalMovement();
        }

        /// <summary>
        /// Create a new animal movement with the parameters passed.
        /// </summary>
        /// <param name="eartag">The eartag of the animal.</param>
        /// <param name="batchId">The batch identifier for the movement.</param>
        /// <param name="fromId">The location (farm, mart) from which the animal moved</param>
        /// <param name="toId">The location (farm, mart) to which the animal moved</param>
        /// <param name="moveDate">The date on which the animal movement took place</param>
        /// <param name="daysResidency">The days allocated for that movement.</param>
        /// <param name="qualityAssuranceApproved">Whether the herd is approved.</param>
        /// <param name="certExpiry">The date on which the quality assurance certificate will expiry.</param>
        /// <returns>A new instance of an animal movement with the parameters passed.</returns>
        public static AnimalMovement CreateMovement(string eartag, int batchId, string fromId, string toId, DateTime moveDate, int? daysResidency, bool? qualityAssuranceApproved, DateTime? certExpiry)
        {
            return new AnimalMovement
            {
                Eartag = eartag,
                BatchId = batchId,
                FromId = fromId,
                ToId = toId,
                MovementDate = moveDate,
                DaysResidency = daysResidency,
                QualityAssuranceApproved = qualityAssuranceApproved,
                CertExpiry = certExpiry
            };
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Route.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;

    public class Route
    {
        /// <summary>
        /// Gets or sets the route id.
        /// </summary>
        public int RouteID { get; set; }

        /// <summary>
        /// Gets or sets the route name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the route number.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the region id.
        /// </summary>
        public int? RegionID { get; set; }

        /// <summary>
        /// Gets or sets the run number.
        /// </summary>
        public int? RunNumber { get; set; }

        /// <summary>
        /// Gets or sets the remarks.
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        public string DisplayName
        {
            get
            {
                return string.Format("{0}|{1}", this.RunNumber, this.Name);
            }
        }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime? Deleted { get; set; }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Inventory.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using System.Reflection;
using Nouvem.Global;
using Nouvem.Shared;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.DataLayer;

    /// <summary>
    /// Class which model the inventory.
    /// </summary>
    public class InventoryItem : INotifyPropertyChanged
    {
        #region field

        /// <summary>
        /// Gets or sets the stock qty.
        /// </summary>
        private decimal stockQty;

        /// <summary>
        /// Gets or sets the stock wgt.
        /// </summary>
        private decimal stockWgt;

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the inventory item master.
        /// </summary>
        public INMaster Master { get; set; }

        /// <summary>
        /// Gets or sets the inventory item parent group.
        /// </summary>
        public INGroup Group { get; set; }

        /// <summary>
        /// Gets or sets the pieces tare container.
        /// </summary>
        public Container PiecesTareContainer { get; set; }

        /// <summary>
        /// Gets or sets the box tare container.
        /// </summary>
        public Container BoxTareContainer { get; set; }

        /// <summary>
        /// Gets or sets the pallet tare container.
        /// </summary>
        public Container PalletTareContainer { get; set; }

        /// <summary>
        /// Gets or sets the linked product tare container.
        /// </summary>
        public Container LinkedProductTareContainer { get; set; }

        /// <summary>
        /// Gets or sets the associated department.
        /// </summary>
        public Department Department { get; set; }

        /// <summary>
        /// Gets or sets the associated date template.
        /// </summary>
        public DateTemplateName DateTemplate { get; set; }

        /// <summary>
        /// Gets or sets the associated traceability template.
        /// </summary>
        public AttributeTemplate TraceabilityTemplate { get; set; }

        /// <summary>
        /// Gets or sets the associated quality template.
        /// </summary>
        public QualityTemplateName QualityTemplate { get; set; }

        /// <summary>
        /// Gets or sets the associated vat code.
        /// </summary>
        public VATCode VatCode { get; set; }

        /// <summary>
        /// Gets or sets the associated stock mode.
        /// </summary>
        public NouStockMode StockMode { get; set; }

        /// <summary>
        /// Gets or sets the associated label field data.
        /// </summary>
        public ProductLabelField LabelField { get; set; }

        /// <summary>
        /// Gets or sets the associated inventory attachments.
        /// </summary>
        public IList<INAttachment> Attachments { get; set; }

        /// <summary>
        /// Gets or sets the associated inventory properties.
        /// </summary>
        public IList<INProperty> Properties { get; set; }

        /// <summary>
        /// Gets or sets the selected pricing data.
        /// </summary>
        public Tuple<PriceList, decimal?> SelectedPriceData { get; set; }

        /// <summary>
        /// Gets or sets the stock qty.
        /// </summary>
        public decimal StockQty
        {
            get { return this.stockQty; }

            set
            {
                this.stockQty = value;
                this.OnPropertyChanged("StockQty");
            }
        }

        /// <summary>
        /// Gets or sets the stock qty.
        /// </summary>
        public decimal StockWgt
        {
            get { return this.stockWgt; }

            set
            {
                this.stockWgt = value;
                this.OnPropertyChanged("StockWgt");
            }
        }

        /// <summary>
        /// Gets or sets the stock qty.
        /// </summary>
        public decimal OutstandingStockQty { get; set; }

        /// <summary>
        /// Gets or sets the stock wgt.
        /// </summary>
        public decimal OutstandingStockWgt { get; set; }

        /// <summary>
        /// Gets or sets the available stock qty.
        /// </summary>
        public decimal AvailableStockQty { get; set; }

        /// <summary>
        /// Gets or sets the available stock qty.
        /// </summary>
        public decimal AvailableStockWgt { get; set; }

        /// <summary>
        /// Flag, as to whether the current product is selected on the ui.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Gets or sets the audit data.
        /// </summary>
        public Audit Audit { get; set; }

        /// <summary>
        /// Gets or sets the shelf life.
        /// </summary>
        public int ShelfLife { get; set; }

        /// <summary>
        /// Gets or sets the shelf life.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public string ExistingProduct
        {
            get
            {
                return (this.Master != null && this.Master.INMasterID > 0).BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public string Code
        {
            get
            {
                return this.Master.Code;
            }
        }

        /// <summary>
        /// Gets the group name.
        /// </summary>
        public string GroupName
        {
            get
            {
                return this.Group?.Name;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public int? SortIndex
        {
            get
            {
                return this.Master?.SortIndex;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.Master.Name;
            }
        }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public string Remarks
        {
            get
            {
                return this.Master.Remarks;
            }
        }

        /// <summary>
        /// Gets the typical pieces.
        /// </summary>
        public int? TypicalPieces
        {
            get
            {
                return this.Master.TypicalPieces;
            }
        }

        /// <summary>
        /// Gets the image.
        /// </summary>
        public byte[] ItemImage
        {
            get
            {
                return this.Master.ItemImage;
            }
        }

        /// <summary>
        /// Gets the code.
        /// </summary>
        public string PurchaseNominalCode
        {
            get
            {
                return this.Master.PurchaseNominalCode;
            }
        }

        /// <summary>
        /// Gets the dept code.
        /// </summary>
        public string PurchaseNominalDeptCode
        {
            get
            {
                return this.Master.PurchaseNominalDeptCode;
            }
        }

        /// <summary>
        /// Gets the sales code.
        /// </summary>
        public string SalesNominalCode
        {
            get
            {
                return this.Master.SalesNominalCode;
            }
        }

        /// <summary>
        /// Gets the sales dept code.
        /// </summary>
        public string SalesNominalDeptCode
        {
            get
            {
                return this.Master.SalesNominalDeptCode;
            }
        }

        /// <summary>
        /// Gets the min weight.
        /// </summary>
        public decimal? MinWeight
        {
            get
            {
                return this.Master.MinWeight;
            }
        }

        /// <summary>
        /// Gets the max weight.
        /// </summary>
        public decimal? MaxWeight
        {
            get
            {
                return this.Master.MaxWeight;
            }
        }

        /// <summary>
        /// Gets the box tare weight.
        /// </summary>
        public double BoxTareWeight
        {
            get
            {
                return this.BoxTareContainer != null ? this.BoxTareContainer.Tare : 0;
            }
        }

        /// <summary>
        /// Gets the box tare weight.
        /// </summary>
        public int? ProductID { get; set; }

        /// <summary>
        /// Gets the box tare weight.
        /// </summary>
        public int? INMasterID
        {
            get
            {
                return this.Master?.INMasterID;
            } 
        }

        /// <summary>
        /// Gets the box tare weight.
        /// </summary>
        public double PalletTareWeight
        {
            get
            {
                return this.PalletTareContainer != null ? this.PalletTareContainer.Tare : 0;
            }
        }

        /// <summary>
        /// Gets the pieces tare weight.
        /// </summary>
        public double PiecesTareWeight
        {
            get
            {
                return this.PiecesTareContainer != null ? this.PiecesTareContainer.Tare : 0;
            }
        }

        private bool swapProduct;
        /// <summary>
        /// Gets or sets a value indicating whether the productl is the currently selected ui product.
        /// </summary>
        public bool SwapProduct
        {
            get
            {
                return this.swapProduct;
            }

            set
            {
                this.swapProduct = value;
                this.OnPropertyChanged("SwapProduct");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the productl is the currently selected ui product.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets the intake card data to display.
        /// </summary>
        public string DisplayData
        {
            get
            {
                try
                {
                    var data = this.SetDisplayCardData(ApplicationSettings.DisplayOutOfProductionData.Split(','));
                    data = data.Remove(data.LastIndexOf(Environment.NewLine), 1);
                    return data;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets a deep copy of the current object.
        /// </summary>
        public InventoryItem Copy
        {
            get
            {
                return new InventoryItem
                {
                    Master = new INMaster
                    {
                        INMasterID = this.Master.INMasterID,
                        Code = this.Master.Code,
                        Name = this.Master.Name,
                        INGroupID = this.Master.INGroupID,
                        TraceabilityTemplateNameID = this.Master.TraceabilityTemplateNameID,
                        DateTemplateNameID = this.Master.DateTemplateNameID,
                        StockItem = this.Master.StockItem,
                        SalesItem = this.Master.SalesItem,
                        PurchaseItem = this.Master.PurchaseItem,
                        FixedAsset = this.Master.FixedAsset,
                        ProductionProduct = this.Master.ProductionProduct,
                        BoxTareContainerID = this.Master.BoxTareContainerID,
                        PiecesTareContainerID = this.Master.PiecesTareContainerID,
                        PalletTareContainerID = this.Master.PalletTareContainerID,
                        MinWeight = this.Master.MinWeight,
                        MaxWeight = this.Master.MaxWeight,
                        NominalWeight = this.Master.NominalWeight,
                        SalesNominalCode = this.Master.SalesNominalCode,
                        SalesNominalDeptCode = this.Master.SalesNominalDeptCode,
                        PurchaseNominalCode = this.Master.PurchaseNominalCode,
                        PurchaseNominalDeptCode = this.Master.PurchaseNominalDeptCode,
                        DepartmentID = this.Master.DepartmentID,
                        TypicalPieces = this.Master.TypicalPieces,
                        VATCodeID = this.Master.VATCodeID,
                        Remarks = this.Master.Remarks,
                        ActiveFrom = this.Master.ActiveFrom,
                        ActiveTo = this.Master.ActiveTo,
                        InActiveFrom = this.Master.InActiveFrom,
                        InActiveTo = this.Master.InActiveTo,
                        ItemImage = this.Master.ItemImage
                    },

                    DateTemplate = new DateTemplateName
                    {
                        DateTemplateNameID = this.DateTemplate != null ? this.DateTemplate.DateTemplateNameID : 0,
                        Name = this.DateTemplate != null ? this.DateTemplate.Name : string.Empty
                    },

                    BoxTareContainer = new Container
                    {
                        ContainerID = this.BoxTareContainer != null ? this.BoxTareContainer.ContainerID : 0,
                        Name = this.BoxTareContainer != null ? this.BoxTareContainer.Name : string.Empty,
                        Tare = this.BoxTareContainer != null ? this.BoxTareContainer.Tare : 0
                    },

                    PiecesTareContainer = new Container
                    {
                        ContainerID = this.PiecesTareContainer != null ? this.PiecesTareContainer.ContainerID : 0,
                        Name = this.PiecesTareContainer != null ? this.PiecesTareContainer.Name : string.Empty,
                        Tare = this.PiecesTareContainer != null ? this.PiecesTareContainer.Tare : 0
                    },

                    PalletTareContainer = new Container
                    {
                        ContainerID = this.PalletTareContainer != null ? this.PalletTareContainer.ContainerID : 0,
                        Name = this.PalletTareContainer != null ? this.PalletTareContainer.Name : string.Empty,
                        Tare = this.PalletTareContainer != null ? this.PalletTareContainer.Tare : 0
                    }
                };
            }
        }

        /// <summary>
        /// Set the card display data.
        /// </summary>
        /// <param name="data">The stored property fields to display values for.</param>
        /// <returns>The card display data.</returns>
        public string SetDisplayCardData(string[] data)
        {
            var displayData = string.Empty;
            var typeSource = this.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var type in data)
            {
                foreach (var property in propertyInfo)
                {
                    var fieldName = property.Name;
                    var match = type.Equals(fieldName);

                    if (match)
                    {
                        var value = property.GetValue(this, null);
                        if (value == null)
                        {
                            continue;
                        }

                        displayData += string.Format("{0}{1}", value, Environment.NewLine);
                    }
                }
            }

            return displayData;
        }


        #endregion

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

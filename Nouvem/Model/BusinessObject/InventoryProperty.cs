﻿// -----------------------------------------------------------------------
// <copyright file="InventoryProperty.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;

    public class InventoryProperty
    {
        /// <summary>
        /// Gets or sets the property details
        /// </summary>
        public INProperty Details { get; set; }

        /// <summary>
        /// Gets or sets a value indicatimng whether the property is selected.
        /// </summary>
        public bool IsSelected { get; set; }
    }
}

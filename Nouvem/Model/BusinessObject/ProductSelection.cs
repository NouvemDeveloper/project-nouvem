﻿// -----------------------------------------------------------------------
// <copyright file="ProductSelection.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.ComponentModel;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class ProductSelection : INotifyPropertyChanged
    {
        /// <summary>
        /// The add to list flag.
        /// </summary>
        private bool addToList;
  
        /// <summary>
        /// Gets or sets the unique node id
        /// </summary>
        public int? NodeID { get; set; }

        /// <summary>
        /// Gets or sets the parent id
        /// </summary>
        public int? ParentID { get; set; }

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the group name.
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the product is a leaf node i.e. it's a product, not a group.
        /// </summary>
        public bool IsLeafNode { get; set; }
       
        /// <summary>
        /// Gets or sets a value indicating whether the product is to be added to the collection.
        /// </summary>
        public bool AddToList
        {
            get
            {
                return this.addToList;
            }

            set
            {
                this.addToList = value;
                this.NotifyPropertyChanged("AddToList");
                Messenger.Default.Send(Tuple.Create(value, this.NodeID), Token.UpdateProductSelections);
            }
        }

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event raise.
        /// </summary>
        /// <param name="propertyName">The property whose value will be broadcast.</param>
        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SqlData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Nouvem.Global;
using Nouvem.Shared;
using Nouvem.ViewModel;

namespace Nouvem.Model.BusinessObject
{
    public class CollectionData : INotifyPropertyChanged
    {
        /// <summary>
        /// Workflow complete flag.
        /// </summary>
        private bool isComplete;

        /// <summary>
        /// Non standard answer given flag.
        /// </summary>
        private bool nonStandard;

        /// <summary>
        /// Gets the intake card data to display.
        /// </summary>
        public void SetDisplayData()
        {
            try
            {
                var properties = ApplicationSettings.DisplayRoutesData.Split(',');
                var localData = this.SetDisplayCardData(properties);
                this.Data = localData.Remove(localData.LastIndexOf(Environment.NewLine), 1);
            }
            catch (Exception ex)
            {
            }
        }

        private string data;

        /// <summary>
        /// Gets or sets the sql data string.
        /// </summary>
        public string Data
        {
            get { return this.data; }
            set
            {
                this.data = value;
                this.OnPropertyChanged("Data");
            }
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int? ParentID { get; set; }

        /// <summary>
        /// Gets or sets the module identifier.
        /// </summary>
        public string Identifier { get; set; }

        public IList<Tuple<string, string, string>> RowData { get; set; } = new List<Tuple<string, string, string>>();

        /// <summary>
        /// Gets or sets the sql data string.
        /// </summary>
        public string Item1 { get; set; }
        public string Item2 { get; set; }
        public string Item3 { get; set; }
        public string Item4 { get; set; }
        public string Item5 { get; set; }
        public string Item6 { get; set; }
        public string Item7 { get; set; }
        public string Item8 { get; set; }
        public string Item9 { get; set; }
        public string Item10 { get; set; }
        public string Item11 { get; set; }
        public string Item12 { get; set; }
        public string Item13 { get; set; }
        public string Item14 { get; set; }
        public string Item15 { get; set; }
        public int Item16 { get; set; }
        public int Item17 { get; set; }
        public int Item18 { get; set; }
        public int Item19 { get; set; }
        public int Item20 { get; set; }
        public decimal Item21 { get; set; }
        public decimal Item22 { get; set; }
        public decimal Item23 { get; set; }
        public decimal Item24 { get; set; }
        public decimal Item25 { get; set; }
        public DateTime Item26 { get; set; }
        public DateTime Item27 { get; set; }
        public DateTime Item28 { get; set; }
        public DateTime Item29 { get; set; }
        public DateTime Item30 { get; set; }

        /// <summary>
        /// Gets or sets the module identifier.
        /// </summary>
        public NouvemViewModelBase DataContext { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the question has been answered or not.
        /// </summary>
        public bool IsComplete
        {
            get
            {
                return this.isComplete;
            }

            set
            {
                this.isComplete = value;
                this.OnPropertyChanged("IsComplete");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a non standard answer has been given.
        /// </summary>
        public bool NonStandard
        {
            get
            {
                return this.nonStandard;
            }

            set
            {
                this.nonStandard = value;
                this.OnPropertyChanged("NonStandard");
            }
        }

        public string GetColumnName(string name)
        {
            foreach (var tuple in this.RowData)
            {
                if (tuple.Item1.CompareIgnoringCase(name))
                {
                    return tuple.Item2;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Set the card display data.
        /// </summary>
        /// <param name="data">The stored property fields to display values for.</param>
        /// <returns>The card display data.</returns>
        public string SetDisplayCardData(string[] data)
        {
            var displayData = string.Empty;
            var typeSource = this.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var type in data)
            {
                foreach (var property in propertyInfo)
                {
                    var fieldName = property.Name;
                    var match = type.Equals(fieldName);

                    if (match)
                    {
                        var value = property.GetValue(this, null);
                        if (value == null)
                        {
                            continue;
                        }

                        displayData += string.Format("{0}{1}", value, Environment.NewLine);
                    }
                }
            }

            return displayData;
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
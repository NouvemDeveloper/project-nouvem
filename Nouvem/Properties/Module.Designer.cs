﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Module {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Module() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Nouvem.Properties.Module", typeof(Module).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dispatch .
        /// </summary>
        internal static string Dispatch {
            get {
                return ResourceManager.GetString("Dispatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Intake.
        /// </summary>
        internal static string Intake {
            get {
                return ResourceManager.GetString("Intake", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invoice.
        /// </summary>
        internal static string Invoice {
            get {
                return ResourceManager.GetString("Invoice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purchase Order.
        /// </summary>
        internal static string PurchaseOrder {
            get {
                return ResourceManager.GetString("PurchaseOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sale Order.
        /// </summary>
        internal static string SaleOrder {
            get {
                return ResourceManager.GetString("SaleOrder", resourceCulture);
            }
        }
    }
}

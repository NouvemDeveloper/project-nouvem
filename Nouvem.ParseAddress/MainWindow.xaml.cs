﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nouvem.ParseAddress
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.ParseAddresses();
        }

        /// <summary>
        /// Method that retrieves the business partners addresses.
        /// </summary>
        /// <returns>A collection of business partner addresses.</returns>
        public IList<BusinessPartnerAddress> GetBusinessPartnerAddresses()
        {
            IList<BusinessPartnerAddress> partnerAddresses;
            try
            {
                using (var entities = new NouvemDunleaveysEntities())
                {
                    partnerAddresses = (from dbAddress in entities.BPAddresses
                                        where !dbAddress.Deleted
                                        select new BusinessPartnerAddress { Details = dbAddress }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return partnerAddresses;
        }

        private void ParseAddresses()
        {
            try
            {
                var addresses = this.GetBusinessPartnerAddresses();
                foreach (var address in addresses)
                {
                    var localAddress = address.Details.AddressLine1;
                    //var splitAddress = localAddress.Split(Environment.NewLine.ToCharArray()).Where(x => !string.IsNullOrEmpty(x)).ToList();
                    var splitAddress = localAddress.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList();
                    if (splitAddress.Count() == 2)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                    }
                    else if (splitAddress.Count == 3)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                        address.Details.AddressLine3 = splitAddress.ElementAt(2);
                    }
                    else if (splitAddress.Count == 4)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                        address.Details.AddressLine3 = splitAddress.ElementAt(2);
                        address.Details.AddressLine4 = splitAddress.ElementAt(3);
                    }
                    else if (splitAddress.Count > 4)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                        address.Details.AddressLine3 = splitAddress.ElementAt(2);

                        var longAddress = string.Empty;
                        for (int i = 0; i < splitAddress.Count; i++)
                        {
                            if (i >= 3)
                            {
                                longAddress += splitAddress.ElementAt(i) + " ";
                            }
                        }

                        if (longAddress.Length > 40)
                        {
                            longAddress = longAddress.Substring(0, 39);
                        }

                        address.Details.AddressLine4 = longAddress;
                    }
                }

                try
                {
                    using (var entities = new NouvemDunleaveysEntities())
                    {
                        foreach (var businessPartnerAddress in addresses)
                        {
                            var addy = entities.BPAddresses.FirstOrDefault(x => x.BPAddressID == businessPartnerAddress.Details.BPAddressID);
                            if (addy != null)
                            {
                                addy.AddressLine1 = businessPartnerAddress.Details.AddressLine1;
                                addy.AddressLine2 = businessPartnerAddress.Details.AddressLine2.Length <= 40 ? businessPartnerAddress.Details.AddressLine2 : businessPartnerAddress.Details.AddressLine2.Substring(0, 39);
                                addy.AddressLine3 = businessPartnerAddress.Details.AddressLine3.Length <= 40 ? businessPartnerAddress.Details.AddressLine3 : businessPartnerAddress.Details.AddressLine3.Substring(0, 39);
                                addy.AddressLine4 = businessPartnerAddress.Details.AddressLine4;
                            }
                        }

                        entities.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.InnerException);
                }

                foreach (var address in addresses)
                {
                    var localAddress = address.Details.AddressLine1;
                    var splitAddress = localAddress.Split(Environment.NewLine.ToCharArray()).Where(x => !string.IsNullOrEmpty(x)).ToList();
                    //var splitAddress = localAddress.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList();
                    if (splitAddress.Count() == 2)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                    }
                    else if (splitAddress.Count == 3)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                        address.Details.AddressLine3 = splitAddress.ElementAt(2);
                    }
                    else if (splitAddress.Count == 4)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                        address.Details.AddressLine3 = splitAddress.ElementAt(2);
                        address.Details.AddressLine4 = splitAddress.ElementAt(3);
                    }
                    else if (splitAddress.Count > 4)
                    {
                        address.Details.AddressLine1 = splitAddress.ElementAt(0);
                        address.Details.AddressLine2 = splitAddress.ElementAt(1);
                        address.Details.AddressLine3 = splitAddress.ElementAt(2);

                        var longAddress = string.Empty;
                        for (int i = 0; i < splitAddress.Count; i++)
                        {
                            if (i >= 3)
                            {
                                longAddress += splitAddress.ElementAt(i) + " ";
                            }
                        }

                        if (longAddress.Length > 40)
                        {
                            longAddress = longAddress.Substring(0, 39);
                        }

                        address.Details.AddressLine4 = longAddress;
                    }
                }

                try
                {
                    using (var entities = new NouvemDunleaveysEntities())
                    {
                        foreach (var businessPartnerAddress in addresses)
                        {
                            var addy = entities.BPAddresses.FirstOrDefault(x => x.BPAddressID == businessPartnerAddress.Details.BPAddressID);
                            if (addy != null)
                            {
                                addy.AddressLine1 = businessPartnerAddress.Details.AddressLine1;
                                addy.AddressLine2 = businessPartnerAddress.Details.AddressLine2.Length <= 40 ? businessPartnerAddress.Details.AddressLine2 : businessPartnerAddress.Details.AddressLine2.Substring(0, 39);
                                addy.AddressLine3 = businessPartnerAddress.Details.AddressLine3.Length <= 40 ? businessPartnerAddress.Details.AddressLine3 : businessPartnerAddress.Details.AddressLine3.Substring(0, 39);
                                addy.AddressLine4 = businessPartnerAddress.Details.AddressLine4;
                            }
                        }

                        entities.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.InnerException);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.InnerException);
            }

            MessageBox.Show("Success");
        }
    }
}

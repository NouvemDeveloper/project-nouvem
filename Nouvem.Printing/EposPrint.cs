﻿// -----------------------------------------------------------------------
// <copyright file="EposPrint.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Printing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing.Printing;
    using Nouvem.Printing.BusinessObject;
    using Nouvem.Printing.Utility;
    
    public class EposPrint
    {
        #region field

        /// <summary>
        /// The singleton reference.
        /// </summary>
        private static EposPrint instance;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EposPrint"/> class.
        /// <param name="addressLine1">The business address line 1.</param>
        /// <param name="addressLine2">The business address line 2.</param>
        /// <param name="addressLine3">The business address line 3.</param>
        /// </summary>
        protected EposPrint(string addressLine1, string addressLine2, string addressLine3)
        {
            this.AddressLine1 = addressLine1;
            this.AddressLine2 = addressLine2;
            this.AddressLine3 = addressLine3;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Returns the epos print singleton.
        /// </summary>
        /// <param name="addressLine1">The business address line 1.</param>
        /// <param name="addressLine2">The business address line 2.</param>
        /// <param name="addressLine3">The business address line 3.</param>
        /// <returns>The epos print singleton</returns>
        public static EposPrint Instance(string addressLine1, string addressLine2, string addressLine3)
        {
            return instance ?? new EposPrint(addressLine1, addressLine2, addressLine3);
        }

        /// <summary>
        /// Gets or sets the address line 1.
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the address line 1.
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the address line 1.
        /// </summary>
        public string AddressLine3 { get; set; }

        #endregion

        /// <summary>
        /// Prints the sale items receipt.
        /// </summary>
        /// <param name="saleItemData">The sale items data (invoice no, description and total).</param>
        /// <param name="invoiceNo">The sub total.</param>
        /// <param name="tax">The vat charged.</param>
        /// <param name="total">The total charge.</param>
        /// <param name="tendered">The amount tendered for payment.</param>
        /// <param name="change">The change given.</param>
        /// <param name="vatReceipt">Flag, as to whether the receipt includes the vat amount.</param>
        public void PrintReceipt(List<SaleItemData> saleItemData, string invoiceNo, string tax, string total, string tendered, string change, bool vatReceipt)
        {
            var buffer = "\x1b\x1d\x61\x1";       
            buffer += string.Format("{0}\n{1}\n{2}\n\n", this.AddressLine1, this.AddressLine2, this.AddressLine3);
            buffer += "\x1b\x1d\x61\x0";            
            buffer += "\x1b\x44\x2\x10\x22\x0";      
            buffer += "Date:" + DateTime.Now.ToString("dd/MM/yyyy") + "\x9" + "                  Time:" + DateTime.Now.ToShortTimeString() + "\n";     
            buffer += "------------------------------------------------ \n";
            buffer += string.Format("                Till Receipt  {0}\n", invoiceNo);
            buffer += "\x1b\x45";                   
            buffer += "SALE\n";
            buffer += "\x1b\x46";    
            
            // add the variable sale items.
            saleItemData.ForEach(item =>
            {
                buffer += string.Format("{0}\x9  {1}\x9\x9         {2}\n", item.Amount, item.Description, item.Total);
            });

            buffer += "\x9                                      --------\n";

            buffer += string.Format("Total \x9\x9        {0}\n", total);
            buffer += "\x9                                      --------\n";
            buffer += string.Format("Tendered \x9\x9        {0}\n", tendered);
            buffer += string.Format("Change \x9\x9        {0}\n", change);
            buffer += "\x9                                      --------\n";

            if (vatReceipt)
            {
                buffer += string.Format("V.A.T. \x9\x9        {0}\n", tax); 
            }
     
            buffer += "------------------------------------------------ \n";
            buffer += string.Format("Total" + "\x6" + "" + "\x9" + "\x1b\x69\x1\x1" + "         €{0}\n", total);   
            buffer += "\x1b\x69\x0\x0";                                                         
            buffer += "------------------------------------------------ \n";
            buffer += "\x1b\x34" + "Refunds and Exchanges" + "\x1b\x35\n";                      
            buffer += "Within " + "\x1b\x2d\x1" + "30 days" + "\x1b\x2d\x0" + " with receipt\n"; 
            buffer += "\x1b\x1d\x61\x1";        
            buffer += "\x1b\x64\x02";                                           
            buffer += "\x7";                                            

            // send data to the printer
            RawPrinterHelper.SendStringToPrinter(new PrinterSettings().PrinterName, buffer);
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="RawPrinterHelper.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Printing.Utility
{
    using System;
    using System.Runtime.InteropServices;

    public class RawPrinterHelper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DOCINFOA"/> class.
        /// </summary>
        /// <remarks>Structure and API declarions:</remarks>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        /// <summary>
        // When the function is given a printer name and an unmanaged array
        // of bytes, the function sends those bytes to the print queue. 
        /// </summary>
        /// <param name="printerName">The printer name.</param>
        /// <param name="bytes">The bytes to send.</param>
        /// <param name="count">The data count.</param>
        /// <returns>A falg, indicating a successful print or not.</returns>
        public static bool SendBytesToPrinter(string printerName, IntPtr bytes, int count)
        {
            var dwError = 0;
            var printer = new IntPtr(0);
            var di = new DOCINFOA();
            var success = false; 

            di.pDocName = "RAW Document";
            di.pDataType = "RAW";

            // Open the printer.
            if (OpenPrinter(printerName.Normalize(), out printer, IntPtr.Zero))
            {
                // Start a document.
                if (StartDocPrinter(printer, 1, di))
                {
                    // Start a page.
                    if (StartPagePrinter(printer))
                    {
                        var written = 0;

                        // Write the bytes.
                        success = WritePrinter(printer, bytes, count, out written);
                        EndPagePrinter(printer);
                    }

                    EndDocPrinter(printer);
                }

                ClosePrinter(printer);
            }

            // If we don't succeed, GetLastError may give more information about why not.
            if (!success)
            {
                throw new Exception(string.Format("Marshal.GetLastWin32Error() Code:{0}", Marshal.GetLastWin32Error()));
            }

            return success;
        }

        /// <summary>
        /// Sends the data string to the printer.
        /// </summary>
        /// <param name="printerName">The printer name.</param>
        /// <param name="dataString">The data string.</param>
        /// <returns>A flag, indicating a successful print or not.</returns>
        public static bool SendStringToPrinter(string printerName, string dataString)
        {
            // Assume that the printer is expecting ANSI text, and then convert the string to ANSI text.
            var pBytes = Marshal.StringToCoTaskMemAnsi(dataString);

            // Send the converted ANSI string to the printer.
            SendBytesToPrinter(printerName, pBytes, dataString.Length);

            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }
    }
}

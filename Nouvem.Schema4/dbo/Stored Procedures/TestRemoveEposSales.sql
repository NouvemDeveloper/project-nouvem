﻿
-- =============================================
-- Author:		brian murray
-- Create date: 12/10/2015
-- Description:	Removes the epos sales data.
-- =============================================
CREATE PROCEDURE [dbo].[TestRemoveEposSales] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   DELETE FROM InvoiceDetail
   DELETE FROM InvoiceHeader
   DELETE FROM StockTransaction
   UPDATE stock set Quantity = 0

END


﻿





-- =============================================
-- Author:		brian murray
-- Create date: 06/07/2016
-- Description:	Gets the dispatch docket transaction details
-- =============================================
CREATE PROCEDURE [dbo].[ReportBatchDetail_Dispatch] 
	-- Add the parameters for the stored procedure here
	@BatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  
	    w.StockLocation,
		w.Name as 'Warehouse',        
		d.Number,	
        b.Name,
		b.Code,		
		s.TransactionWeight,
		s.TransactionQTY,
		dd.QuantityDelivered,
		s.Serial,
		s.Pieces,
		s.TransactionDate,
		i.INMasterID,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		bn.Number AS 'BatchNo',		
		
		  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'SupplierBatch') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'SupplierBatch') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch') 
				      END)) AS 'Supplier Batch'

    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN StockTransaction s
	      ON s.MasterTableID = dd.ARDispatchDetailID
	   INNER JOIN BatchNumber bn
	      ON bn.BatchNumberID = s.BatchNumberID
	   INNER JOIN Warehouse w
	      ON w.WarehouseID = s.WarehouseID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID
       INNER JOIN BPMaster b 
          ON d.BPMasterID_Customer = b.BPMasterID
	   
      
         
     WHERE bn.BatchNumberID = @BatchID AND dd.Deleted IS NULL AND s.Deleted IS NULL and s.isbox is null
     END
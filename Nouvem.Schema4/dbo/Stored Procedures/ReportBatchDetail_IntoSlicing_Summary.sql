﻿









-- =============================================
-- Author:		brian murray
-- Create date: 06/07/2016
-- Description:	Gets the into slicing transaction details
-- =============================================
Create PROCEDURE [dbo].[ReportBatchDetail_IntoSlicing_Summary] 
	-- Add the parameters for the stored procedure here
	@BatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  
	 
		w.Name AS 'Warehouse',  
		SUM(s.TransactionWeight) AS 'Wgt',
		SUM(s.TransactionQTY) AS 'Qty',
		SUM(s.Pieces) AS 'Pieces',	
		i.INMasterID,
		i.Name AS 'ProductName'	
		 

    FROM PROrder p      
	  INNER JOIN StockTransaction s
	     ON s.MasterTableID = p.PROrderID
	  INNER JOIN BatchNumber bn
	     ON bn.BatchNumberID = s.BatchNumberID
	  INNER JOIN Warehouse w
	     ON w.WarehouseID = s.WarehouseID
       INNER JOIN INMaster i
          ON s.INMasterID = i.INMasterID         
      
         
     WHERE bn.BatchNumberID = @BatchID AND p.Deleted IS NULL AND s.Deleted IS NULL
	GROUP BY w.Name, i.INMasterID, i.Name
     END
﻿-- =============================================
-- Author:		brian murray
-- Create date: 12-10-16
-- Description:	Retrieves the deleted label data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportLabelDeletion]
	-- Add the parameters for the stored procedure here
	@StartDate date,
	@EndDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	s.StockTransactionID,
	u.FullName,
	m.Name,
	s.Deleted,
	s.Pieces,
	s.TransactionWeight,
	s.TransactionQTY,
	s.BatchNumberID,
	b.Number
	


	FROM StockTransaction s
	INNER JOIN INMaster m 
	  ON m.INMasterID = s.INMasterID
	LEFT JOIN BatchNumber b
	  ON b.BatchNumberID = s.BatchNumberId
	LEFT JOIN UserMaster u
	  ON u.UserMasterID = s.UserMasterID
	WHERE s.DELETED BETWEEN @StartDate AND @EndDate
END
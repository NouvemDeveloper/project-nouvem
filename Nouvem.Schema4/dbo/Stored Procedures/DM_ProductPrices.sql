﻿




-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the pricing data from the DEMLocal db to the Nouvem inmaster table
-- =============================================
CREATE PROCEDURE [dbo].[DM_ProductPrices]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @ProductCode varchar(30),	
	 @PricePerUnit decimal(18,2),
	 @CostPricePerUnit decimal(18,2),
	 @PriceMethod int,
	 @CostPriceMethod int,
	 @BasePriceListID int,
	 @CostPriceListID int
	

	SET @BasePriceListID = (select PriceListID from pricelist where CurrentPriceListName = 'base price list')
	SET @CostPriceListID = (select PriceListID from pricelist where CurrentPriceListName = 'Cost price list')

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.ProductCode,
			p.PricePerUnit,
			p.CostPriceMethod,
			p.PriceMethod,
			p.CostPriceMethod		
 
     FROM [win-coldstore\sqlexpress].[DEMLocal].[dbo].[Products]  p
	
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @ProductCode,		
		  @PricePerUnit,
		  @CostPricePerUnit,
		  @PriceMethod,
		  @CostPriceMethod

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 	 
	
		DECLARE @INMasterID INT = (SELECT TOP 1 INMasterID FROM INMaster
								  WHERE RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@ProductCode)))
        DECLARE @PriceMethodID INT = (CASE WHEN @PriceMethod = 0 THEN 1 ELSE 3 END)
		DECLARE @CostPriceMethodID INT = (CASE WHEN ISNULL(@CostPriceMethod, 1) = 0 THEN 1 ELSE 3 END)
								  
			IF (@INMasterID > 0)
		    BEGIN
			   
			   INSERT INTO PriceListDetail values(@BasePriceListID, @INMasterID, ISNULL(@PricePerUnit,0), 5,@PriceMethodID,NULL, 0)
			   INSERT INTO PriceListDetail values(@CostPriceListID, @INMasterID, ISNULL(@CostPricePerUnit,0), 5,@CostPriceMethodID,NULL, 0)
			   		
		    END		
	

	 FETCH NEXT FROM PRICE_CURSOR
		  INTO @ProductCode,		
		  @PricePerUnit,
		  @CostPricePerUnit,
		  @PriceMethod,
		  @CostPriceMethod
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END






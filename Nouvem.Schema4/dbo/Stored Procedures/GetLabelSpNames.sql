﻿-- =============================================
-- Author:		brian murray
-- Create date: 28/03/2016
-- Description:	Retrieves the label sp names
-- =============================================
CREATE PROCEDURE GetLabelSpNames 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ROUTINE_NAME AS Name
    FROM nouvem.information_schema.routines 
    WHERE routine_type = 'PROCEDURE' 
    AND ROUTINE_NAME LIKE 'Label%'
    AND LEFT(Routine_Name, 3) NOT IN ('sp_', 'xp_', 'ms_')
END

﻿





-- =============================================
-- Author:		brian murray
-- Create date: 06/09/16
-- Description:	Gets the stock take scanned data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetStockTakeData]
	-- Add the parameters for the stored procedure here
@StockTakeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @BasePriceListId int = (SELECT TOP 1 PriceListID FROM PriceList WHERE CurrentPriceListName = 'Base Price List')
	DECLARE @PriceListId int = (SELECT TOP 1 PriceListID FROM PriceList WHERE CurrentPriceListName = 'Cost Price List')

	SELECT g.INGroupID,
	       g.Name AS 'GroupName',
	       m.Name, 
		   m.Code,
		   stk.[Description],
	       SUM(CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END) AS 'BoxCount', 
		   SUM(st.Wgt) AS 'Weight',
		   SUM(st.Qty) AS 'Pieces',	
		    ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
					  WHERE PriceListID = @BasePriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0) AS 'BasePrice',
		   ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0) AS 'Price',
     CASE WHEN (SELECT Name FROM PriceListDetail 
		              INNER JOIN NouPriceMethod ON PriceListDetail.NouPriceMethodID = NouPriceMethod.NouPriceMethodID
					  WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID)) = 'Qty x Price'
					  THEN 
						 SUM(st.Qty) *
						 ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0)
					  ELSE
						 SUM(st.Wgt) *
						 ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0)
					  END  as 'TotalValue'    	 
	 

	FROM INMaster m INNER JOIN StockTakeDetail st 
	                           ON st.INMasterID = m.INMasterID
					INNER JOIN INGroup g
					           ON m.INGroupID = g.INGroupID		
					INNER JOIN StockTake stk
					           ON stk.StockTakeID = st.StockTakeID
					 LEFT JOIN StockTransaction s
						ON st.StockTransactionID = s.StockTransactionID			
			

	WHERE st.StockTakeID = @StockTakeId    
	
	GROUP BY g.INGroupID, g.Name, m.Name ,m.Code, stk.[Description]	  
	      
END




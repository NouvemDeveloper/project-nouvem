﻿

-- =============================================
-- Author:		brian murray
-- Create date: 25/05/2016
-- Description:	Generates default application settings for input device.
-- =============================================
CREATE PROCEDURE [dbo].[CreateDefaultSettings] 
	-- Add the parameters for the stored procedure here
	@DeviceMasterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update devicesettings set value1 = 'Serial', Value2 = 'COM4,9600,None,One,8', Value3='DEM_GSE355', Value4='C:\Nouvem\Indicator\Alibi', Value5='True', Value6='48', Value7='18', Value8='True' where devicemasterid = @DeviceMasterID and name = 'IndicatorSettings'

update devicesettings set value1 = 'http://win-coldstore:80/reportserver_SQLSERVER2014' where devicemasterid = @DeviceMasterID and name = 'ReportServerPath'
update devicesettings set value1 = 'http://win-coldstore:80/ReportServer_SQLSERVER2014/ReportService2010.asmx' where devicemasterid = @DeviceMasterID and name = 'SSRSWebServiceURL'
update devicesettings set value1 = 'nouvem' where devicemasterid = @DeviceMasterID and name = 'SSRSUserName'
update devicesettings set value1 = 'Nou12345' where devicemasterid = @DeviceMasterID and name = 'SSRSPassword'
update devicesettings set value1 = 'win-coldstore' where devicemasterid = @DeviceMasterID and name = 'SSRSDomain'


update devicesettings set value1 = 'USB', Value2 = 'Printer1', Value3='Portrait' where devicemasterid = @DeviceMasterID and name = 'PrinterSettings'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'AllowAllProductsOutOfProduction'
update devicesettings set Value1 = '3' where DeviceMasterID = @DeviceMasterID and name = 'WaitForScalesToStable'
update devicesettings set Value1 = '2' where DeviceMasterID = @DeviceMasterID and name = 'MessageBoxFlashTime'
update devicesettings set Value1 = '8' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenStaticButtonFontSize'
update devicesettings set Value1 = '150' where DeviceMasterID = @DeviceMasterID and name = 'OutOfProductionProductsHomeMenuWidth'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'AllowDispatchOverAmount'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'AllowDispatchProductNotOnOrder'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'AllowIntakeOverAmount'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'AllowManualBatchCreation'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'AutoCopyToDispatch'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'AutoCopyToGoodsIn'
update devicesettings set Value1 = 'Manual' where DeviceMasterID = @DeviceMasterID and name = 'BatchCreationMode'
update devicesettings set Value1 = '250' where DeviceMasterID = @DeviceMasterID and name = 'CardViewCardWidth'
update devicesettings set Value1 = '100' where DeviceMasterID = @DeviceMasterID and name = 'CardViewNarrowCardWidth'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'ConnectToSSRS'
update devicesettings set Value1 = 'True', Value2='True', Value3='True' where DeviceMasterID = @DeviceMasterID and name = 'DisplayCardHeaderOnly'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'DisplayIntakeOrderItemUpdatedAtAnotherTerminalMsgBox'
update devicesettings set Value1 = '100' where DeviceMasterID = @DeviceMasterID and name = 'LabelHistoryTransactionsNumberToRetrieve'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'ManuallyEnterBatchNumber'
update devicesettings set Value1 = '0.1' where DeviceMasterID = @DeviceMasterID and name = 'MinWeightAllowedIntake'
update devicesettings set Value1 = '0.1' where DeviceMasterID = @DeviceMasterID and name = 'MinWeightAllowedOutOfProduction'
update devicesettings set Value1 = '150' where DeviceMasterID = @DeviceMasterID and name = 'OutOfProductionProductsHomeMenuWidth'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'ResetLabelOnEveryPrint'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'ResetSerialAttributes'
update devicesettings set Value1 = '1' where DeviceMasterID = @DeviceMasterID and name = 'SaleOrderDeliveryDaysForward'
update devicesettings set Value1 = '30' where DeviceMasterID = @DeviceMasterID and name = 'SaleOrderValidUntilDaysForward'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'ScannerMode'
update devicesettings set Value1 = 'COM2,9600,None,One,8', Value2='False' where DeviceMasterID = @DeviceMasterID and name = 'ScannerSettings'
update devicesettings set Value1 = '8' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenCardsFontSize'
update devicesettings set Value1 = '12' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenMenuButtonFontSize'
update devicesettings set Value1 = '70' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenMenuButtonHeight'
update devicesettings set Value1 = '100' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenMenuButtonWidth'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'TouchScreenModeOnly'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'TouchScreenModeOnlyDispatch'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'TouchScreenModeOnlyIntake'
update devicesettings set Value1 = 'False' where DeviceMasterID = @DeviceMasterID and name = 'TouchScreenModeOnlyIntoProduction'
update devicesettings set Value1 = 'True' where DeviceMasterID = @DeviceMasterID and name = 'TouchScreenModeOnlyOutOfProduction'
update devicesettings set Value1 = '26' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenStaticButtonFontSize'
update devicesettings set Value1 = '20' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenTraceabilityControlsFontSize'
update devicesettings set Value1 = '40' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenTraceabilityControlsHeight'
update devicesettings set Value1 = '140' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenTraceabilityControlsLabelWidth'
update devicesettings set Value1 = '2' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenTraceabilityControlsStackedNumber'
update devicesettings set Value1 = '100' where DeviceMasterID = @DeviceMasterID and name = 'TouchscreenTraceabilityControlsWidth'
update devicesettings set Value1 = '1000' where DeviceMasterID = @DeviceMasterID and name = 'TransactionsToTakeForReportSearch'
update devicesettings set Value1 = '2' where DeviceMasterID = @DeviceMasterID and name = 'WeightsPrecision'
 
END



﻿-- =============================================
-- Author:		brian murray
-- Create date: 30/08/16
-- Description:	Updates the header detail totals.
-- =============================================
CREATE PROCEDURE UpdateDispatchDocket
	-- Add the parameters for the stored procedure here
@ARDispatchId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 DECLARE @LinesTotal decimal(18,2) = (select sum(totalexclvat) from ARDispatchDetail
	     where ardispatchid = @ARDispatchId and Deleted is null) 

	 DECLARE @SubTotal decimal(18,2) = (SELECT SubTotalExVat FROM ARDispatch WHERE ARDispatchID = @ARDispatchId)
		
		 if (@LinesTotal != @SubTotal) 
		 BEGIN
	       UPDATE ARDispatch SET SubTotalExVAT = @LinesTotal, GrandTotalIncVAT = @LinesTotal WHERE ARDispatchID = @ARDispatchId
		 END
END

﻿


-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Temporary fix for the non integer product codes not having been applied previously.
-- =============================================
CREATE PROCEDURE [dbo].[DM_Products_Update]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 	
	 @ProductCode varchar(30),
	 @ProductDescription varchar(70)

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.ProductCode,
			p.[Description]		

     FROM  [win-coldstore\sqlexpress].[DEMLocal].[dbo].[Products]  p
	 WHERE ISNUMERIC(p.ProductCode) <> 1 
 
     OPEN PRICE_CURSOR;
  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO 
		  @ProductCode,
		  @ProductDescription
		

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	 
		DECLARE @INMasterID int = (SELECT TOP 1 INMasterID FROM INMaster WHERE
								   RTRIM(LTRIM(Name)) = RTRIM(LTRIM(@ProductDescription)) 
								   AND code = '0' and name <> 'NOT IN USE')
								  
			IF (@INMasterID > 0)
		    BEGIN
			    UPDATE INMaster SET Code = @ProductCode WHERE INMasterID = @INMasterID
		    END		
	

	 FETCH NEXT FROM PRICE_CURSOR
		 INTO  @ProductCode,
		       @ProductDescription
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END




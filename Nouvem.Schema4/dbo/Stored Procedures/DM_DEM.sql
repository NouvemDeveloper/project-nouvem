﻿

-- =============================================
-- Author:		brian murray
-- Create date: 200616
-- Description:	data migration for the dem database.
-- =============================================
CREATE PROCEDURE [dbo].[DM_DEM] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	exec DM_BusinessPartners
	exec DM_Suppliers
	exec DM_BusinessPartnersDelAddresses
	exec DM_ProductGroups
	exec DM_Products
	exec DM_Products_DaysForward
	exec DM_PriceLists
	exec DM_PriceLists_Suppliers
	exec DM_ProductPrices
	exec DM_Products_Tare
END



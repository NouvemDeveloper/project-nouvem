﻿
-- =============================================
-- Author:		brian murray
-- Create date: 14-11-2016
-- Description:	Gets the linked carcass product/wgt.
-- =============================================
CREATE PROCEDURE [dbo].[GetLinkedProduct]
	-- Add the parameters for the stored procedure here
	@Serial nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 p.ProductCode, s.ScaleNettWeight, s.KillDate,s.KillNumber, s.BatchCode FROM [DEMMigrationDB].[dbo].Stock s
	INNER JOIN [DEMMigrationDB].[dbo].Products p
	  ON s.ProductID = p.ID	
	 where s.Barcode = @Serial 
END
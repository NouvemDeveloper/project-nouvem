﻿


-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the pricing data from the DEMLocal db to the Nouvem price lists.
-- =============================================
CREATE PROCEDURE [dbo].[DM_PriceLists_Suppliers]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @BPMasterID int,
	 @Name nvarchar(200)
	

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     select BPMasterID, Name
	 from BPMaster where PriceListID is null and BPGroupID = 1
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @BPMasterID,
	      @Name 

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN  
			INSERT INTO [dbo].[PriceList] VALUES (@Name, NULL, 0, 1, 3, 3, 0,NULL, GETDATE())
			DECLARE @PriceListID int
				    --now we need to get the id of this inserted record
	        	    SET @PriceListID = IDENT_CURRENT('dbo.PriceList')
			       
					  --update the partner with the price list id
					UPDATE BPMaster SET PriceListID = @PriceListID WHERE BPMasterID = @BPMasterID	  
	
		   
	

	 FETCH NEXT FROM PRICE_CURSOR
		 INTO @BPMasterID,
	          @Name 
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END




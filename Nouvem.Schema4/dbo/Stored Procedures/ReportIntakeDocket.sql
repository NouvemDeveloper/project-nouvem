﻿



-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the intake docket details
-- =============================================
CREATE PROCEDURE [dbo].[ReportIntakeDocket]
	-- Add the parameters for the stored procedure here
	@GoodsInID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT gr.Number AS 'Document No', 
	       gr.CreationDate AS 'Intake Date',
		   gr.APGoodsReceiptID,
		   s.TransactionWeight AS 'Weight',
		   s.TransactionQTY AS 'Quantity',
		   b.Name as 'Supplier',
		   b.Code,
		   m.Name AS 'Product',
		   m.INMasterID,
		   s.StockTransactionID,
		   s.Serial AS 'LabelID',
		   bn.Number,

		    (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'Grade') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'Grade') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'Grade')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'Grade') 
				      END)) AS 'Grade',
		   
		   	 (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'KillNumber') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'KillNumber') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber') 
				      END)) AS 'Kill Number',

					 (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'KillDate') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'KillDate') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND DateMaster.DateCode = 'KillDate')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'KillDate') 
				      END)) AS 'Kill Date',

					   (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierBatch') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierBatch') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch') 
				      END)) AS 'Supplier Batch'


		   FROM StockTransaction s      		
		          INNER JOIN APGoodsReceiptDetail grd
	                  ON grd.APGoodsReceiptDetailID = s.MasterTableID
			      INNER JOIN APGoodsReceipt gr
				      ON grd.APGoodsReceiptID = gr.APGoodsReceiptID
				  INNER JOIN INMaster m
				      ON m.INMasterID = s.INMasterID
				  LEFT JOIN BPMaster b
				      ON b.BPMasterID = gr.BPMasterID_Supplier	
				  LEFT JOIN BatchNumber bn
				      ON bn.BatchNumberID = s.BatchNumberID		       
	

	WHERE gr.APGoodsReceiptID = @GoodsInID and s.Deleted is NULL
	         
END





﻿-- =============================================
-- Author:		brian murray
-- Create date: 22-06-16
-- Description:	Updates a device setting
-- =============================================
CREATE PROCEDURE [UpdateDeviceSetting] 
	-- Add the parameters for the stored procedure here
	@DeviceId int,
	@SettingName varchar(100),
	@Value1 varchar(100),
	@Value2 varchar(100),
	@Value3 varchar(100),
	@Value4 varchar(100),
	@Value5 varchar(100),
	@Value6 varchar(100),
	@Value7 varchar(100),
	@Value8 varchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF LEN(@Value1) > 0
	update devicesettings set Value1 = @Value1 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId

	IF LEN(@Value2) > 0
	update devicesettings set Value2 = @Value2 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId

	IF LEN(@Value3) > 0
	update devicesettings set Value3 = @Value3 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId

	IF LEN(@Value4) > 0
	update devicesettings set Value4 = @Value4 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId

	IF LEN(@Value5) > 0
	update devicesettings set Value5 = @Value5 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId

	IF LEN(@Value6) > 0
	update devicesettings set Value6 = @Value6 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId

	IF LEN(@Value7) > 0
	update devicesettings set Value7 = @Value7 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId

	IF LEN(@Value8) > 0
	update devicesettings set Value8 = @Value8 WHERE Name = @SettingName AND DeviceMasterID = @DeviceId


END

﻿

-- =============================================
-- Author:		brian murray
-- Create date: 06/07/2016
-- Description:	Gets the dispatch docket transaction details
-- =============================================
CREATE PROCEDURE [dbo].[ReportBatchDetail_Dispatch_Summary] 
	-- Add the parameters for the stored procedure here
	@BatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  
	 	w.Name as 'Warehouse',      
		SUM(s.TransactionWeight) AS 'Wgt',
		SUM(s.TransactionQTY) AS 'Qty',	
		SUM(s.Pieces) AS 'Pieces',	
		i.INMasterID,
		i.Name AS 'ProductName',
		d.routeid,
		Rm.Name 'RouteName'
		
    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	  INNER JOIN StockTransaction s
	     ON s.MasterTableID = dd.ARDispatchDetailID
	  INNER JOIN BatchNumber bn
	     ON bn.BatchNumberID = s.BatchNumberID
	  INNER JOIN Warehouse w
	     ON w.WarehouseID = s.WarehouseID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID
       INNER JOIN BPMaster b 
          ON d.BPMasterID_Customer = b.BPMasterID
		inner join RouteMaster rm
		on d.RouteID = rm.RouteID
	  
      
         
     WHERE bn.BatchNumberID = @BatchID AND dd.Deleted IS NULL AND s.Deleted IS NULL AND S.ISBOX IS NULL
	 GROUP BY w.Name, i.INMasterID, i.Name, d.RouteID, rm.Name
     END
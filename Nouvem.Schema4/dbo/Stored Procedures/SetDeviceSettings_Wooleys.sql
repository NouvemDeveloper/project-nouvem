﻿





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SetDeviceSettings_Wooleys]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update devicesettings set Value1 = 'Standard' where name = 'IntakeMode'
	update devicesettings set Value1 = 'CarcassSplit' where name = 'IntoProductionMode'
	update devicesettings set Value1 = 'ScanStock' where name = 'DispatchMode'
    update devicesettings set Value1 = 'False' where name = 'CheckWeightBoundaryAtDispatch'
	update devicesettings set Value1 = 'False' where name = 'CheckForPartBoxesAtDispatch'
	update devicesettings set Value1 = 'False' where name = 'AutoCreateInvoiceOnOrderCompletion'
	update devicesettings set Value1 = 'False' where name = 'DispatchOrderNotFilledWarning'
	update devicesettings set Value3 = 'false' where name = 'AutoWeigh'
	update devicesettings set Value1 = 'True' where name = 'AllowDispatchOverAmount'
	Update devicesettings set Value1 = 'True', Value2= '1012', Value4='1008' where name = 'DefaultGroupTemplates'
	update devicesettings set value1 = 'True', Value2='10000000' where name = 'CheckForLinkedDatabaseScannedDispatchSerial'
	update devicesettings set Value1 = 'True' where name = 'ClearPartnersOnEntry'
	update devicesettings set Value1 = 'false' where name = 'ClearProductionOrdersOnEntry'
	update devicesettings set Value1 = 'false' where name = 'CheckWeightBoundaries'
	update devicesettings set Value1 = 'True' where name = 'ScanOrWeighAtDispatch'
	update devicesettings set Value1 = 'True' where name = 'CanSelectCustomersAtOutOfProduction'
	update devicesettings set Value1 = 'Sage50' where name = 'CanSelectCustomersAtOutOfProduction'
	update devicesettings set value7 ='Woolleys' where name = 'AccountsPackage'
END



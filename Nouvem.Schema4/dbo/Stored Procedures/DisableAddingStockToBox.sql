﻿-- =============================================
-- Author:		brian murray
-- Create date: 16-07-16
-- Description:	Used for start of day, to clear any left over unboxed stock.
-- =============================================
CREATE PROCEDURE DisableAddingStockToBox
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE StockTransaction SET CanAddToBox = 0
END
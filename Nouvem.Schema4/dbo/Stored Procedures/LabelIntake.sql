﻿








CREATE PROCEDURE [dbo].[LabelIntake]
  @StockTransactionID INT 

AS
BEGIN    

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT b.Number AS 'Batch/Lot Number',
	       s.Serial AS 'Serial Number',
		   s.Pieces,
		   CONVERT(DECIMAL(18,2), s.TransactionWeight) AS 'Product Net Weight in kg',
		    CONVERT(DECIMAL(18,2), s.Tare) AS 'Tare',		
		   CONVERT(DECIMAL(18,2), s.GrossWeight) AS 'Gross Wgt',
		   CONVERT(DECIMAL(18,0),s.TransactionQTY) as 'TransactionQty',
		   m.Code AS 'Product Code',
		   m.Name as 'Product Name',
		    bpm.Name as 'Partner Name',
		   bpa.AddressLine1 as 'Delivery Address 1',
		   bpa.AddressLine2 as 'Delivery Address 2',
		   bpa.AddressLine3 as 'Delivery Address 3',
		   bpa.AddressLine4 as 'Delivery Address 4',
		   bpaa.AddressLine1 as 'Invoice Address 1',
		   bpaa.AddressLine2 as 'Invoice Address 2',
		   bpaa.AddressLine3 as 'Invoice Address 3',
		   bpaa.AddressLine4 as 'Invoice Address 4',
		   (SELECT bpa.AddressLine1 + ' ' + bpa.AddressLine2 + ' ' + bpa.AddressLine3 + ' ' + bpa.AddressLine4 + ' ' + bpm.Tel) AS 'Delivery Address Full',
		   (SELECT bpaa.AddressLine1 + ' ' + bpaa.AddressLine2 + ' ' + bpaa.AddressLine3 + ' ' + bpaa.AddressLine4 + ' ' + bpm.Tel) AS 'Invoice Address Full',
		    (SELECT bpm.Name + ', ' + bpaa.AddressLine1 + ' ' + bpaa.AddressLine2 + ' ' + bpaa.AddressLine3 + ' ' + bpaa.AddressLine4 + ', ' + bpm.Tel) AS 'Partner Details',
		    p.MultiLineField1 as '#MultiLine Text1',
		   p.MultiLineField2 as '#MultiLine Text2',
		   p.Field1 as '#Text1',
		   p.Field2 as '#Text2',
		   p.Field3 as '#Text3',
		   p.Field4 as '#Text4',
		   p.Field5 as '#Text5',
		   p.Field6 as '#Text6',
		   p.Field7 as '#Text7',
		   p.Field8 as '#Text8',
		   p.Field9 as '#Text9',
		   p.Field10 as '#Text10',
		   p.Field11 as '#Text11',
		   p.Field12 as '#Text12',
		   p.Field13 as '#Text13',
		   p.Field14 as '#Text14',
		   p.Field15 as '#Text15',
		   p.Field16 as '#Text16',
		   p.Field17 as '#Text17',
		   p.Field18 as '#Text18',
		   p.Field19 as '#Text19',
		   p.Field20 as '#Text20',
		   p.Field21 as '#Text21',
		   p.Field22 as '#Text21',
		   p.Field23 as '#Text23',
		   p.Field24 as '#Text24',
		   p.Field25 as '#Text25',
		   p.Field26 as '#Text26',
		   p.Field27 as '#Text27',
		   p.Field28 as '#Text28',
		   p.Field29 as '#Text29',
		   p.Field30 as '#Text30',
	       CONVERT(VARCHAR, s.Serial) AS 'Barcode_Serial',	

		   (SELECT [dbo].[SplitPlant](CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierCutIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierCutIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierCutIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierCutIn') 
				      END)) AS 'Supplier Cut In',

		    (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'Grade') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'Grade') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'Grade')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'Grade') 
				      END)) AS 'Grade',

		   (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'KillNumber') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'KillNumber') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber') 
				      END)) AS 'Kill Number',

					   (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierBatch') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierBatch') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch') 
				      END)) AS 'Supplier Batch',
		 
	         (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'BornIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'BornIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'BornIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'BornIn') 
				      END)) AS 'Born In',

                  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'RearedIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'RearedIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'RearedIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'RearedIn') 
				      END)) AS 'Reared In',

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'FattenedIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'FattenedIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'FattenedIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'FattenedIn') 
				      END)) AS 'Fattened In',

					  (SELECT  [dbo].[SplitPlant](CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SlaughteredIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SlaughteredIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SlaughteredIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SlaughteredIn') 
				      END)) AS 'Slaughtered In',

					  (SELECT [dbo].[SplitPlant](CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CutIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CutIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'CutIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'CutIn') 
				      END)) AS 'Cut In',		
				   
				  	

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'KillDate') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'KillDate') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND DateMaster.DateCode = 'KillDate')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'KillDate') 
				      END)) AS 'Kill Date',

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'PackedOnDate') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'PackedOnDate') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND DateMaster.DateCode = 'PackedOnDate')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'PackedOnDate') 
				      END)) AS 'Packed On Date',

					 (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'UseBy') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'UseBy') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND DateMaster.DateCode = 'UseBy')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'UseBy') 
				      END)) AS 'Use By Date',

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CountryOfOrigin') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CountryOfOrigin') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'CountryOfOrigin')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'CountryOfOrigin') 
				      END)) AS 'Country Of Origin'


	FROM StockTransaction s 
	  INNER join INMaster m on s.INMasterID = m.INMasterID
	  INNER JOIN APGoodsReceiptDetail grd on grd.APGoodsReceiptDetailID = s.MasterTableID
	  INNER JOIN APGoodsReceipt gr ON gr.APGoodsReceiptID = grd.APGoodsReceiptID
	  INNER JOIN BatchNumber b ON s.BatchNumberID = b.BatchNumberID
	  LEFT JOIN ProductLabelField p on m.ProductLabelFieldID = p.ProductLabelFieldID
	  LEFT JOIN BPMaster bpm on bpm.BPMasterID = gr.BPMasterID_Supplier
	  LEFT JOIN BPAddress bpa on bpa.BPAddressID = gr.BPAddressID_Delivery
	  LEFT JOIN BPAddress bpaa on bpaa.BPAddressID = gr.BPAddressID_Invoice

	WHERE s.StockTransactionID = @StockTransactionID
END


﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE TestDispatchSummary
	-- Add the parameters for the stored procedure here
@Dispatchid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select inm.Code, 
	       inm.Name,
		   ard.QuantityDelivered as 'Qty',
		   ard.WeightDelivered as 'Weight',
		   bpm.Name as 'Customer Name',
		   bpa.AddressLine1, BPA.AddressLine2, bpa.AddressLine3, bpa.AddressLine4, bpa.PostCode,
		   ar.Number,
		   ar.DeliveryDate,
		   bpm.Code as 'Customer Code'
	
	 from ARDispatch ar inner join ARDispatchDetail ard on ar.ARDispatchID = ard.ARDispatchID
     inner join INMaster inm on inm.INMasterID = ard.INMasterID
     left join BPMaster bpm on ar.BPMasterID_Customer = bpm.BPMasterID
	 left join BPAddress bpa on bpa.BPAddressID = ar.BPAddressID_Delivery
      where Ar.ARDispatchID = @Dispatchid

END
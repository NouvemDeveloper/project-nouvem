﻿





CREATE PROCEDURE [dbo].[ReportDispatchGetBoxLabels] 
	@BoxLabelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT m.Name, s.Serial, s.TransactionWeight, s.TransactionQTY, 
	CASE WHEN s.Pieces is null or s.Pieces = 0 THEN 1 ELSE s.Pieces END AS Pieces,	
	CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END as 'BoxCount', 
	 s.INMasterID


	FROM StockTransaction s 
	INNER JOIN Warehouse w ON s.WarehouseID = w.WarehouseID
	INNER JOIN INMaster m ON s.INMasterID = m.INMasterID
    WHERE s.StockTransactionID_Container = @BoxLabelID AND s.Deleted IS NULL
END






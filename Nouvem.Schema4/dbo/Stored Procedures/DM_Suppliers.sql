﻿




-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the partner data from the DEMLocal db to the Nouvem partners tables.
-- =============================================
CREATE PROCEDURE [dbo].[DM_Suppliers]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @CustomerName varchar(254),
	 @CustomerCode varchar(50),
	 @ContactName varchar(254),
	 @CustomerNotes varchar(1024),
	 @CustomerFax varchar(254),
	 @CustomerEmail varchar(254),
	 @CustomerPhone varchar(254),
	 @CustomerAddress varchar(254)

	 DECLARE PARTNER_CURSOR CURSOR STATIC FOR
     SELECT s.CompanyName,
	        s.SupplierCode,
			s.ContactName,
			s.Notes,
			s.Fax,
			s.Email,
			s.Phone,
			s.[Address]			
	
 
     FROM [win-coldstore\sqlexpress].[DEMLocal].[dbo].[Suppliers] s
      
 
     OPEN PARTNER_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @CustomerName,
		   @CustomerCode,
	       @ContactName,
	       @CustomerNotes,
	       @CustomerFax,
	       @CustomerEmail,
	       @CustomerPhone,
	       @CustomerAddress
		

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 	 
	 
		--DECLARE @BPMasterID int =  (SELECT TOP 1 BPMasterID FROM BPMaster
		--						   WHERE RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@CustomerCode)) AND RTRIM(LTRIM(Name)) = RTRIM(LTRIM(@CustomerName)))		
								  
			--IF (@BPMasterID > 0)
		 --   BEGIN

			    --UPDATE BPMaster SET Notes = @CustomerNotes, Tel = @CustomerPhone, FAX = @CustomerFax, Email = @CustomerEmail WHERE BPMasterID = @BPMasterID

				INSERT INTO BPMaster (NouBPTypeID, Code, Name,Notes, Tel, Fax, Email) Values(2,@CustomerCode, @CustomerName, @CustomerNotes, @CustomerPhone,@CustomerFax, @CustomerEmail)
				DECLARE @BPMasterID INT = Ident_Current('BPMaster')

				
				IF (@CustomerAddress IS NOT NULL AND @CustomerAddress <> '')
				BEGIN
				    INSERT iNTO BPAddress VALUES (@BPMasterID, @CustomerAddress, '', '', '', '', 1, 1, 0, NULL)
				END				
				
				IF (@ContactName IS NOT NULL AND @ContactName <> '')
				BEGIN
				    INSERT iNTO BPContact VALUES (@BPMasterID, '', @ContactName, NULL,NULL,'','','',1, 0)
				END	

				
		    --END		

	 FETCH NEXT FROM PARTNER_CURSOR
	INTO  @CustomerName,
		   @CustomerCode,
	       @ContactName,
	       @CustomerNotes,
	       @CustomerFax,
	       @CustomerEmail,
	       @CustomerPhone,
	       @CustomerAddress
	 END
	 
	 CLOSE PARTNER_CURSOR;
     DEALLOCATE PARTNER_CURSOR;

	 DECLARE @LocalGroupID INT = (SELECT TOP 1 BPGroupID FROM BPGroup WHERE BPGroupName = 'No Group')
	 IF (@LocalGroupID IS NULL)
	 BEGIN
	   INSERT INTO BPGroup values ('No Group', 0)
	   SET @LocalGroupID = ident_current('BPGroup')
	 END
	 UPDATE BPMaster SET BPGroupID = @LocalGroupID 
END






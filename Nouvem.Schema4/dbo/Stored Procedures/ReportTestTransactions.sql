﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE ReportTestTransactions
	-- Add the parameters for the stored procedure here
	@DispatchID int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select im.INMasterID, im.Code, im.Name, s.Serial, s.TransactionQTY as 'QTY', s.TransactionWeight as 'Weight'

from ARDispatch ard 
inner join ARDispatchDetail ardd on ard.ARDispatchID = ardd.ARDispatchID
inner join StockTransaction s on s.MasterTableID = ardd.ARDispatchDetailID
inner join INMaster im on im.INMasterID = s.INMasterID
where ard.ARDispatchID = @DispatchID
END
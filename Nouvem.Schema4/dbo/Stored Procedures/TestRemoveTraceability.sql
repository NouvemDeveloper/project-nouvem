﻿-- =============================================
-- Author:		brian murray
-- Create date: 19/11/15
-- Description:	Removes the traceability data
-- =============================================
CREATE PROCEDURE TestRemoveTraceability
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from [dbo].[BatchTraceability]
	delete from [dbo].[TransactionTraceability]
	delete from [dbo].[DateTemplateAllocation]
	delete from [dbo].[DateTemplateName]
	delete from [dbo].[DateMaster]
	delete from [dbo].[TraceabilityTemplateAllocation]
	delete from [dbo].[TraceabilityTemplateName]
	delete from [dbo].[TraceabilityTemplateMaster]
	delete from [dbo].[QualityTemplateAllocation]
	delete from [dbo].[QualityTemplateName]
	delete from [dbo].[QualityMaster]
END

﻿CREATE TABLE [dbo].[BPAddressSnapshot] (
    [BPAddressSnapshotID] INT           IDENTITY (1, 1) NOT NULL,
    [AddressLine1]        VARCHAR (254) NOT NULL,
    [AddressLine2]        VARCHAR (40)  NOT NULL,
    [AddressLine3]        VARCHAR (40)  NOT NULL,
    [AddressLine4]        VARCHAR (40)  NULL,
    [PostCode]            VARCHAR (10)  NOT NULL,
    [Billing]             BIT           NULL,
    [Shipping]            BIT           NULL,
    [PlantID]             INT           NULL,
    [Deleted]             BIT           NOT NULL,
    CONSTRAINT [PK_InvoiceHdrBPAddress_Invoice] PRIMARY KEY CLUSTERED ([BPAddressSnapshotID] ASC)
);


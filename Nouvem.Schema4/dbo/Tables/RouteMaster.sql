﻿CREATE TABLE [dbo].[RouteMaster] (
    [RouteID]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50) NOT NULL,
    [Description] NVARCHAR (50) NULL,
    [RegionID]    INT           NULL,
    [RunNumber]   INT           NULL,
    [Remark]      NVARCHAR (50) NULL,
    [Deleted]     DATETIME      NULL,
    CONSTRAINT [PK_RouteMaster] PRIMARY KEY CLUSTERED ([RouteID] ASC),
    CONSTRAINT [FK_RouteMaster_RegionID] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[RegionMaster] ([RegionID]) ON DELETE CASCADE ON UPDATE CASCADE
);


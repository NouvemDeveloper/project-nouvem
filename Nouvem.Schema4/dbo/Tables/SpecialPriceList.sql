﻿CREATE TABLE [dbo].[SpecialPriceList] (
    [SpecialPriceListID] INT             IDENTITY (1, 1) NOT NULL,
    [Price]              DECIMAL (18, 2) NOT NULL,
    [INMasterID]         INT             NOT NULL,
    [BPGroupID]          INT             NULL,
    [BPMasterID]         INT             NULL,
    [StartDate]          DATETIME        NOT NULL,
    [EndDate]            DATETIME        NOT NULL,
    [Deleted]            DATETIME        NULL,
    CONSTRAINT [PK_SpecialPriceList] PRIMARY KEY CLUSTERED ([SpecialPriceListID] ASC),
    CONSTRAINT [FK_SpecialPriceList_BPGroup] FOREIGN KEY ([BPGroupID]) REFERENCES [dbo].[BPGroup] ([BPGroupID]),
    CONSTRAINT [FK_SpecialPriceList_BPMaster] FOREIGN KEY ([BPMasterID]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_SpecialPriceList_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID])
);


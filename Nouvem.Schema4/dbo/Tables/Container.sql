﻿CREATE TABLE [dbo].[Container] (
    [ContainerID]     INT             IDENTITY (1, 1) NOT NULL,
    [ContainerTypeID] INT             NOT NULL,
    [Name]            NVARCHAR (50)   NOT NULL,
    [Tare]            FLOAT (53)      NOT NULL,
    [Photo]           VARBINARY (MAX) NULL,
    [Deleted]         BIT             NOT NULL,
    CONSTRAINT [PK_Container_1] PRIMARY KEY CLUSTERED ([ContainerID] ASC),
    CONSTRAINT [FK_Container_ContainerType] FOREIGN KEY ([ContainerTypeID]) REFERENCES [dbo].[ContainerType] ([ContainerTypeID])
);


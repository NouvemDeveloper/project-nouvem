﻿CREATE TABLE [dbo].[BordBiaRequest] (
    [BordBiaRequestID] INT           IDENTITY (1, 1) NOT NULL,
    [HerdNumber]       NVARCHAR (14) NOT NULL,
    [ServiceLogInID]   INT           NULL,
    [DateStamp]        DATETIME      NULL,
    [BatchID]          INT           NULL,
    [RequestSent]      BIT           NULL,
    [Deleted]          DATETIME      NULL,
    [Exported]         BIT           CONSTRAINT [DF_DEM_BB_REQ_Exported] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DEM_BB_REQ] PRIMARY KEY CLUSTERED ([BordBiaRequestID] ASC)
);


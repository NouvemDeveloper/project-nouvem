﻿CREATE TABLE [dbo].[NouRoundingOptions] (
    [NouRoundingOptionsID] INT            IDENTITY (1, 1) NOT NULL,
    [Description]          NVARCHAR (50)  NOT NULL,
    [Macro]                NVARCHAR (MAX) NOT NULL,
    [Deleted]              BIT            NOT NULL,
    CONSTRAINT [PK_RoundingOptions] PRIMARY KEY CLUSTERED ([NouRoundingOptionsID] ASC)
);


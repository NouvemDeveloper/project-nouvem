﻿CREATE TABLE [dbo].[NouRoundingRules] (
    [NouRoundingRulesID] INT            IDENTITY (1, 1) NOT NULL,
    [Description]        NVARCHAR (50)  NOT NULL,
    [Macro]              NVARCHAR (MAX) NOT NULL,
    [Deleted]            BIT            NOT NULL,
    CONSTRAINT [PK_NouRoundingRules] PRIMARY KEY CLUSTERED ([NouRoundingRulesID] ASC)
);


﻿CREATE TABLE [dbo].[Warehouse] (
    [WarehouseID]      INT           IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (30)  NULL,
    [Deleted]          BIT           NOT NULL,
    [Code]             NVARCHAR (10) NULL,
    [StockLocation]    BIT           DEFAULT ((0)) NOT NULL,
    [AvailableForSale] BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Warehouse] PRIMARY KEY CLUSTERED ([WarehouseID] ASC)
);


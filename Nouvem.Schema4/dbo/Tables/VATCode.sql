﻿CREATE TABLE [dbo].[VATCode] (
    [VATCodeID]   INT             IDENTITY (1, 1) NOT NULL,
    [Code]        NCHAR (20)      NOT NULL,
    [Description] NCHAR (80)      NULL,
    [Percentage]  NUMERIC (18, 2) NULL,
    [Deleted]     BIT             NOT NULL,
    CONSTRAINT [PK_VATCode] PRIMARY KEY CLUSTERED ([VATCodeID] ASC)
);


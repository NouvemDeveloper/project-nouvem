﻿CREATE TABLE [dbo].[CountryCode] (
    [CountryCodeID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]          NVARCHAR (5)  NULL,
    [CountryName]   NVARCHAR (50) NULL,
    [Deleted]       DATETIME      NULL,
    CONSTRAINT [PK_CountryCode] PRIMARY KEY CLUSTERED ([CountryCodeID] ASC)
);


﻿CREATE TABLE [dbo].[ARInvoiceAttachment] (
    [ARInvoiceAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [ARInvoiceID]           INT             NOT NULL,
    [FilePath]              NVARCHAR (50)   NULL,
    [FileName]              NVARCHAR (50)   NOT NULL,
    [AttachmentDate]        DATE            NOT NULL,
    [File]                  VARBINARY (MAX) NOT NULL,
    [Deleted]               DATETIME        NULL,
    CONSTRAINT [PK_ARInvoiceAttachment] PRIMARY KEY CLUSTERED ([ARInvoiceAttachmentID] ASC),
    CONSTRAINT [FK_ARInvoiceAttachment_ARDispatch] FOREIGN KEY ([ARInvoiceID]) REFERENCES [dbo].[ARInvoice] ([ARInvoiceID])
);


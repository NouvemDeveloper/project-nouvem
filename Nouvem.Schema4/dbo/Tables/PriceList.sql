﻿CREATE TABLE [dbo].[PriceList] (
    [PriceListID]          INT           IDENTITY (1, 1) NOT NULL,
    [CurrentPriceListName] NVARCHAR (50) NOT NULL,
    [BasePriceListID]      INT           NULL,
    [Factor]               FLOAT (53)    NOT NULL,
    [NouRoundingOptionsID] INT           NOT NULL,
    [NouRoundingRulesID]   INT           NOT NULL,
    [BPCurrencyID]         INT           NOT NULL,
    [Deleted]              BIT           NOT NULL,
    [UserMasterID]         INT           NULL,
    [CreationDate]         DATETIME      NULL,
    [StartDate]            DATETIME      NULL,
    [EndDate]              DATETIME      NULL,
    [DeviceMasterID]       INT           NULL,
    [EditDate]             DATETIME      NULL,
    [BPMasterID]           INT           NULL,
    CONSTRAINT [PK_PriceList] PRIMARY KEY CLUSTERED ([PriceListID] ASC),
    CONSTRAINT [FK_BPCurrency_PriceList] FOREIGN KEY ([BPCurrencyID]) REFERENCES [dbo].[BPCurrency] ([BPCurrencyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_NouRoundingOptions_PriceList] FOREIGN KEY ([NouRoundingOptionsID]) REFERENCES [dbo].[NouRoundingOptions] ([NouRoundingOptionsID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_NouRoundingRules_PriceList] FOREIGN KEY ([NouRoundingRulesID]) REFERENCES [dbo].[NouRoundingRules] ([NouRoundingRulesID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PriceList_PriceList] FOREIGN KEY ([BasePriceListID]) REFERENCES [dbo].[PriceList] ([PriceListID]),
    CONSTRAINT [FK_PriceList_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);


﻿CREATE TABLE [dbo].[TransactionTraceability] (
    [TransactionTraceabilityID]    INT            IDENTITY (1, 1) NOT NULL,
    [StockTransactionID]           INT            NOT NULL,
    [NouTraceabilityMethodID]      INT            NOT NULL,
    [Value]                        NVARCHAR (100) NOT NULL,
    [Deleted]                      DATETIME       NULL,
    [TraceabilityTemplateMasterID] INT            NULL,
    [DateMasterID]                 INT            NULL,
    [QualityMasterID]              INT            NULL,
    [DynamicName]                  NVARCHAR (20)  NULL,
    CONSTRAINT [PK_TraceabilityValue] PRIMARY KEY CLUSTERED ([TransactionTraceabilityID] ASC),
    CONSTRAINT [FK_TraceabilityValue_NouTraceabilityMethod] FOREIGN KEY ([NouTraceabilityMethodID]) REFERENCES [dbo].[NouTraceabilityMethod] ([NouTraceabilityMethodID]),
    CONSTRAINT [FK_TraceabilityValue_StockTransaction] FOREIGN KEY ([StockTransactionID]) REFERENCES [dbo].[StockTransaction] ([StockTransactionID]),
    CONSTRAINT [FK_TransactionTraceability_DateMaster] FOREIGN KEY ([DateMasterID]) REFERENCES [dbo].[DateMaster] ([DateMasterID]),
    CONSTRAINT [FK_TransactionTraceability_QualityMaster] FOREIGN KEY ([QualityMasterID]) REFERENCES [dbo].[QualityMaster] ([QualityMasterID]),
    CONSTRAINT [FK_TransactionTraceability_TraceabilityTemplateMaster] FOREIGN KEY ([TraceabilityTemplateMasterID]) REFERENCES [dbo].[TraceabilityTemplateMaster] ([TraceabilityTemplatesMasterID])
);


GO
CREATE NONCLUSTERED INDEX [IX_StockTransactionID]
    ON [dbo].[TransactionTraceability]([StockTransactionID] ASC);


GO




-- =============================================
-- Author:		brian murray
-- Create date: 09/09/16
-- Description:	Fail safe, to ensure the line and header totals match,
-- =============================================
CREATE TRIGGER [dbo].[Trigger_TransactionTraceability_ForInsert]
   ON [dbo].[TransactionTraceability]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Value nvarchar(100)
	DECLARE @StocktransactionId int
	DECLARE @DateMasterId int
	DECLARE @TransactionTraceabilityId int
	SELECT  @TransactionTraceabilityId = TransactionTraceabilityId, @DateMasterId = DateMasterId, @Value = Value, @StocktransactionId = StockTransactionID FROM inserted

	IF (@Value = '01/01/0001 00:00:00' and @DateMasterId is not null)
	BEGIN
	  DECLARE @UseById int = (SELECT TOP 1 DateMasterID FROM DateMaster WHERE DateCode = 'UseBy')
	  DECLARE @PackedOnId int = (SELECT TOP 1 DateMasterID FROM DateMaster WHERE DateCode = 'PackedOnDate')
	  DECLARE @KillDateId int = (SELECT TOP 1 DateMasterID FROM DateMaster WHERE DateCode = 'KillDate')

	  if (@DateMasterId = @PackedOnId)
	  BEGIN
	    UPDATE TransactionTraceability SET Value = CONVERT(nvarchar(10), GETDATE(), 103) WHERE TransactionTraceabilityID = @TransactionTraceabilityId
	  END

	   if (@DateMasterId = @KillDateId)
	  BEGIN
	    UPDATE TransactionTraceability SET Value = CONVERT(nvarchar(10), GETDATE(), 103) WHERE TransactionTraceabilityID = @TransactionTraceabilityId
	  END

	  if (@DateMasterId = @UseById)
	  BEGIN
	    DECLARE @Days int =
	    (SELECT TOP 1 [Days] FROM DateDays d
		INNER JOIN INMaster m ON d.INMasterID = m.INMasterID
		INNER JOIN StockTransaction s ON s.INMasterID = m.INMasterID
		WHERE s.StockTransactionID = @StocktransactionId)

		if (@Days is not null)
		BEGIN
		  UPDATE TransactionTraceability SET Value = CONVERT(nvarchar(10), DATEADD(day,@Days,GETDATE()),103) WHERE TransactionTraceabilityID = @TransactionTraceabilityId
		END
		ELSE
		BEGIN
		   UPDATE TransactionTraceability SET Value = CONVERT(nvarchar(10), GETDATE(),103) WHERE TransactionTraceabilityID = @TransactionTraceabilityId
		END	   
	  END


	END

END
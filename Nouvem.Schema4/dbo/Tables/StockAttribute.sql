﻿CREATE TABLE [dbo].[StockAttribute] (
    [StockAttributeID]   INT           IDENTITY (1, 1) NOT NULL,
    [StockTransactionID] INT           NOT NULL,
    [KillDate]           DATETIME      NULL,
    [KillNumber]         INT           NULL,
    [BatchNumber]        NVARCHAR (50) NULL,
    [IsSerial]           BIT           NULL,
    [Deleted]            DATETIME      NULL,
    CONSTRAINT [PK_StockAttribute] PRIMARY KEY CLUSTERED ([StockAttributeID] ASC)
);


﻿CREATE TABLE [dbo].[Category] (
    [CategoryID]  INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (3)  NOT NULL,
    [Description] NVARCHAR (20) NULL,
    [Deleted]     DATETIME      NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([CategoryID] ASC)
);


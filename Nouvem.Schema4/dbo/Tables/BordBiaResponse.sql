﻿CREATE TABLE [dbo].[BordBiaResponse] (
    [BordBiaResponseID] INT            IDENTITY (1, 1) NOT NULL,
    [BordBiaRequestID]  INT            NOT NULL,
    [DateStamp]         DATETIME       NULL,
    [HerdNumber]        NVARCHAR (8)   NULL,
    [HerdOwner]         NVARCHAR (30)  NULL,
    [CertValidDate]     DATE           NULL,
    [FileName]          NVARCHAR (250) NULL,
    [Deleted]           DATETIME       NULL,
    [Exported]          BIT            CONSTRAINT [DF_DEM_BB_RES_Exported] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DEM_BB_RES] PRIMARY KEY CLUSTERED ([BordBiaResponseID] ASC),
    CONSTRAINT [FK_BordBiaResponse_BordBiaRequest] FOREIGN KEY ([BordBiaRequestID]) REFERENCES [dbo].[BordBiaRequest] ([BordBiaRequestID])
);


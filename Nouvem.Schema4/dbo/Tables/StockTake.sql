﻿CREATE TABLE [dbo].[StockTake] (
    [StockTakeID]    INT            IDENTITY (1, 1) NOT NULL,
    [CreationDate]   DATETIME       NOT NULL,
    [Description]    NVARCHAR (100) NULL,
    [WarehouseID]    INT            NULL,
    [Locations]      NVARCHAR (100) NULL,
    [NouDocStatusID] INT            NULL,
    [Deleted]        DATETIME       NULL,
    CONSTRAINT [PK_StockTake] PRIMARY KEY CLUSTERED ([StockTakeID] ASC),
    CONSTRAINT [FK_StockTake_NouDocStatus] FOREIGN KEY ([NouDocStatusID]) REFERENCES [dbo].[NouDocStatus] ([NouDocStatusID]),
    CONSTRAINT [FK_StockTake_StockTake] FOREIGN KEY ([WarehouseID]) REFERENCES [dbo].[Warehouse] ([WarehouseID])
);


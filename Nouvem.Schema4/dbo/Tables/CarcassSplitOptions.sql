﻿CREATE TABLE [dbo].[CarcassSplitOptions] (
    [CarcassSplitOptionsID] INT      IDENTITY (1, 1) NOT NULL,
    [INMasterID]            INT      NOT NULL,
    [DeviceID]              INT      NULL,
    [UserId]                INT      NULL,
    [Deleted]               DATETIME NULL,
    [CreationDate]          DATETIME NULL,
    CONSTRAINT [PK_CarcassSplitOptions] PRIMARY KEY CLUSTERED ([CarcassSplitOptionsID] ASC),
    CONSTRAINT [FK_CarcassSplitOptions_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID])
);


﻿CREATE TABLE [dbo].[StoredLabel] (
    [StoredLabelID] INT             IDENTITY (1, 1) NOT NULL,
    [Label1]        VARBINARY (MAX) NULL,
    [Label2]        VARBINARY (MAX) NULL,
    [Label3]        VARBINARY (MAX) NULL,
    [Label4]        VARBINARY (MAX) NULL,
    [Label5]        VARBINARY (MAX) NULL,
    [Label6]        VARBINARY (MAX) NULL,
    [Label7]        VARBINARY (MAX) NULL,
    [Label8]        VARBINARY (MAX) NULL,
    [Deleted]       DATETIME        NULL,
    CONSTRAINT [PK_StoredLabel] PRIMARY KEY CLUSTERED ([StoredLabelID] ASC)
);


﻿CREATE TABLE [dbo].[BPAddress] (
    [BPAddressID]  INT           IDENTITY (1, 1) NOT NULL,
    [BPMasterID]   INT           NULL,
    [AddressLine1] VARCHAR (254) NOT NULL,
    [AddressLine2] VARCHAR (40)  NULL,
    [AddressLine3] VARCHAR (40)  NULL,
    [AddressLine4] VARCHAR (40)  NULL,
    [PostCode]     VARCHAR (10)  NOT NULL,
    [Billing]      BIT           NULL,
    [Shipping]     BIT           NULL,
    [Deleted]      BIT           NOT NULL,
    [PlantID]      INT           NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([BPAddressID] ASC),
    CONSTRAINT [FK_Address_BusinessPartner] FOREIGN KEY ([BPMasterID]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_Plant_BPAddress] FOREIGN KEY ([PlantID]) REFERENCES [dbo].[Plant] ([PlantID]) ON DELETE CASCADE ON UPDATE CASCADE
);


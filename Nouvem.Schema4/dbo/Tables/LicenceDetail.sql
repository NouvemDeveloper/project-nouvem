﻿CREATE TABLE [dbo].[LicenceDetail] (
    [LicenceDetailID]  INT IDENTITY (1, 1) NOT NULL,
    [LicenceID]        INT NOT NULL,
    [NouLicenceTypeID] INT NOT NULL,
    [LicenceQTY]       INT NOT NULL,
    [Deleted]          BIT NOT NULL,
    CONSTRAINT [PK_LicenceDetail] PRIMARY KEY CLUSTERED ([LicenceDetailID] ASC),
    CONSTRAINT [FK_LicenceDetail_Licence] FOREIGN KEY ([LicenceID]) REFERENCES [dbo].[Licence] ([LicenceID]),
    CONSTRAINT [FK_LicenceDetail_SysLicenceType] FOREIGN KEY ([NouLicenceTypeID]) REFERENCES [dbo].[NouLicenceType] ([NouLicenceTypeID])
);


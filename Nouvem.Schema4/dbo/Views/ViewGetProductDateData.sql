﻿

CREATE VIEW [dbo].[ViewGetProductDateData]
as
select code, m.name,m.INGroupID, g.Name as 'Group', n.DateType,dd.DateDescription, d.Days 
from inmaster m
inner join INGroup g on m.InGroupID = g.ingroupid
inner join datedays d
on d.INMasterID = m.INMasterID
inner join DateMaster dd on dd.Datemasterid = d.Datemasterid
inner join NouDateType n

on n.NouDateTypeID = dd.NouDateTypeID
where m.deleted is null
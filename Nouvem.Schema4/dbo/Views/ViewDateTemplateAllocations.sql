﻿

  CREATE View [dbo].[ViewDateTemplateAllocations]
  AS

  SELECT 
         dtm.Name, 
		 dt.DateTemplateNameID, 
		 dt.DateMasterID, 
		 dt.Batch,
		 dt.[Transaction],
		 dt.[Required],
		 dt.[Reset],
		 dm.DateCode, 
		 dm.DateDescription,
		  n.DateType
  
  FROM DateTemplateAllocation dt 
      inner join DateMaster dm on dt.DateMasterID = dm.DateMasterID
	  inner join DateTemplateName dtm on dt.DateTemplateNameID = dtm.DateTemplateNameID
	  Inner join NouDateType n on dm.NouDateTypeID = n.NouDateTypeID

  WHERE dt.Deleted = 0
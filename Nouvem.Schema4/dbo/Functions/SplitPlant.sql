﻿
CREATE FUNCTION [dbo].[SplitPlant] 
(
	-- Add the parameters for the function here
	@Plant nvarchar(100)
)
RETURNS nvarchar(100)
AS
BEGIN

 DECLARE @Result nvarchar(100) 
 DECLARE @NewLine CHAR(2) = CHAR(13) + CHAR(10)

 if (CHARINDEX(@Result, @NewLine) > 0)
 BEGIN
   Set @Result = SUBSTRING(@Plant,CHARINDEX(@NewLine,@Plant)+2,LEN(@Plant))
 END
 else
 BEGIN
   Set @Result = @Plant
 END

	-- Return the result of the function
	RETURN @Result
END
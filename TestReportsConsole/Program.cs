﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Nouvem.ReportEngine;
using Nouvem.ReportEngine.Factory;
using Nouvem.ReportEngine.Model.Enum;

namespace TestReportsConsole
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            var connectionString =
                "data source=BRIAN-PC;initial catalog=Nouvem;persist security info=True;user id=sa;password=nouvem;";
           // var param1 = new KeyValuePair<string, object>("", "");
            /////var parameters = new List<KeyValuePair<string, object>>();
            //parameters.Add(param1);

            var parameters = new Dictionary<string, object>
            {
                {"StartDate", DateTime.Today.Add(TimeSpan.FromDays(-17))},
                {"EndDate", DateTime.Today}
            };

            var filePath = @"C:\Nouvem\Reports\EposReport.repx";
            if (File.Exists(filePath))
            {
                var report = ReportFactory.Create(connectionString);
                report.CreateReport(filePath, parameters, ReportMode.Preview, 1); 
            }

                       
        }
    }
}
